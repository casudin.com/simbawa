@extends('layouts.master')
@section('title', trans('KRS WAR'))
@section('parentPageTitle', 'KRS')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/custom_dt_customer.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.min.css')}}">
@endsection
@section('content')
<div class="row layout-spacing">
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                                         
            </div>
            <div class="row">
                <div class="col-md-5 col-sm-5 mb-4 mb-sm-0">
                    <h6 class="mt-3 mb-0"> </h6>
                </div>
                <div class="col-md-7 col-sm-7 text-sm-right">
                    <span style="color: red; font-size: 10pt"> Sumber Data <a href="http://api.mic.ac.id"
                            target="_blank">api.mic.ac.id</a></span>
                </div>

            </div>
            <div class="widget-content widget-content-area">
                <div class="col-md-12 col-sm-12 text-sm-right">
                    <a href="{{ route('krs.create') }}" class="btn btn-secondary mb-4 mr-2">Isi KRS <i class="flaticon-plus-2 ml-1"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row layout-spacing">
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                Data KRS Yang Sudah diambil
                <br>
                <div class="row">
                    <div class="col-md-5 col-sm-5 mb-4 mb-sm-0">
                        <h6 class="mt-3 mb-0"> </h6>
                    </div>
                    

                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4 style-1">

                    <table id="userss" class="table style-1  table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> No. </th>
                                <th>{{trans('Mata Kuliah')}}</th>
                                <th>{{trans('SKS')}}</th>
                                <th>Semester</th>
                                <th>Periode Akademik</th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>

@endsection
@section('modal')
<div class="modal fade animated zoomInUp custo-zoomInUp" tabindex="-1" role="dialog"
    aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="myModal">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
    $("#myModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    // $(".select2").select2({
    //     placeholder: 'Cari',
    //     allowClear: true
    // });

var myTable=$('#userss').DataTable({
        processing: true,
        serverSide: true,
        pageLength:10,
       lengthMenu:[[10,15,20,50,100],[10,15,20,50,100]],
       
        searching:false,
        lengthChange:false,
        language: {
                paginate: {
                  previous: "<i class='flaticon-arrow-left-1'></i>",
                  next: "<i class='flaticon-arrow-right'></i>"
                },
                //info: "Showing page _PAGE_ of _PAGES_"
            },
        ajax: {
            url: '{{route("api.krs")}}',
            method: 'POST'
        },
        columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'pelajaran', name: 'pelajaran'},
            {data: 'sks', name: 'sks'},
            {data: 'smt', name: 'smt'},
            {data: 'akademik', name: 'akademik'},

        ],

    });
function hapus(id){

    swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    }).then((result) => {
  if (result.value) {
      $.ajax({
          type: "POST",
          method:"DELETE",
          url: "{{url('admin/user/')}}/"+id,
          data: {id:id},
          dataType: "JSON",
          success: function (response) {
              if (response.status!='error') {
                  swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                ''
                );
                Toast.fire({
                icon: 'success',
                title: 'Your User has been deleted.'
                });
                myTable.ajax.reload();
              } else {

              }
          }
      });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary User is safe :)',
      'error'
    );
  }
})

 }

</script>
@endsection