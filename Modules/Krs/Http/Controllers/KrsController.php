<?php

namespace Modules\Krs\Http\Controllers;

/**
 * A class to get information of KrsController
 * @author Casudin (com.casudin@gmail.com)
 * @version 1.0
 * @since Sunday, August 9th, 2020
 */
 use Illuminate\Http\Request;
 use Illuminate\Http\Response;
 //use Illuminate\Routing\Controller;
 use Nwidart\Modules\Routing\Controller;
 use DataTables;
 use DB;
 use Carbon\Carbon;
 use Auth;
 use App\User;
 use Validator;

 class KrsController extends Controller
 {
     /**
      * Display a listing of the resource.
      * @return Response
      */
     public function index()
     {
         return view('krs::index');
     }
     public function Datalist()
     {
         $data=DB::connection('loker')->table('krs')
        ->join('pelajaran', 'pelajaran.kd_ak', '=', 'krs.kd_pelajaran')
        ->join('periode_akademik', 'periode_akademik.pr_akademik', '=', 'krs.pr_akademik')
        ->select([
            'krs.smt',
            'krs.sks',
            'pelajaran.nama as pelajaran',
            'periode_akademik.nama as akademik'
        ])->orderBy('krs.idx', 'DESC')
        ->where('krs.idp', useridp());
         return Datatables::of($data)
        ->addIndexColumn()
        ->escapeColumns([])

        // ->editColumn('TotalBiaya', function ($data) {
        //     return number_format($data->TotalBiaya);
        // })
        // ->editColumn('potongan', function ($data) {
        //     return number_format($data->potongan);
        // })->editColumn('TotalSetor', function ($data) {
        //     return number_format($data->TotalSetor);
        // })->editColumn('Kekurangan', function ($data) {
        //     return number_format($data->Kekurangan);
        // })
        


        
        ->make(true);
     }

     /**
      * Show the form for creating a new resource.
      * @return Response
      */
     public function create()
     {
         if (!$this->waktu()) {
             return view('krs::krs_tutup');
         } elseif (!$this->izin_krs()) {
             return view('krs::krs_keuangan');
         } elseif (cekStatusMhs()) {
             return abort(419);
         } else {
             return view('krs::create');
         }
     }
     private function izin_krs()
     {
         $aka=DB::connection('loker')->table('periode_akademik')->where('aktif', 1)->first();
         $data=DB::connection('loker')->table('krs_izin')->where('idp', useridp())->where('pr_akademik', $aka->pr_akademik);
         if ($data->count()>0) {
             return true;
         } else {
             return true;
         }
     }
     private function waktu()
     {
         $aka=DB::connection('loker')->table('periode_akademik')->where('aktif', 1)->first();
         $data=DB::connection('loker')->table('krs_variabel')->where('tgl_mulai', '<=', now())
         ->where('tgl_akhir', '>', now())->where('pr_akademik', $aka->pr_akademik);
         if ($data->count()>0) {
             return true;
         } else {
             $x=DB::connection('loker')->table('krs_terlambat')->where('tgl_akhir', '>', now())->where('pr_akademik', $aka->pr_akademik)->where('idp', useridp());
             if ($x->count()>0) {
                 return true;
             } else {
                 return false;
             }
         }
     }

     /**
      * Store a newly created resource in storage.
      * @param Request $req
      * @return Response
      */
     public function store(Request $req)
     {
         DB::beginTransaction();

         try {
             $valid = Validator::make($req->all(), [
                 'name' => 'required',
        ]);
             if ($valid->fails()) {
                 return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
             }
             DB::commit();
             return response()->json(['status'=>'success']);
         } catch (\Exception $e) {
             DB::rollback();
             return response()->json(['status'=>'error','msg'=>$e->getMessage()]);
         }
     }

     /**
      * Show the specified resource.
      * @param int $id
      * @return Response
      */
     public function show($id)
     {
         return view('krs::show');
     }

     /**
      * Show the form for editing the specified resource.
      * @param int $id
      * @return Response
      */
     public function edit($id)
     {
         return view('krs::edit');
     }

     /**
      * Update the specified resource in storage.
      * @param Request $req
      * @param int $id
      * @return Response
      */
     public function update(Request $req, $id)
     {
         DB::beginTransaction();

         try {
             $valid = Validator::make($req->all(), [
                 'name' => 'required',
             ]);
             if ($valid->fails()) {
                 return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
             }
             DB::commit();
             return response()->json(['status'=>'success']);
         } catch (\Exception $e) {
             DB::rollback();
             return response()->json(['status'=>'error','msg'=>$e->getMessage()]);
         }
     }

     /**
      * Remove the specified resource from storage.
      * @param int $id
      * @return Response
      */
     public function destroy($id)
     {
         DB::beginTransaction();

         try {

              // all good
             DB::commit();
             return response()->json(['status'=>'success']);
         } catch (\Exception $e) {
             DB::rollback();
             return response()->json(['status'=>'error','msg'=>$e->getMessage()]);
         }
     }
 }
