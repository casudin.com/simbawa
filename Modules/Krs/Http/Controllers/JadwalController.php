<?php

namespace Modules\Krs\Http\Controllers;

/**
 * A class to get information of JadwalController
 * @author Casudin (com.casudin@gmail.com)
 * @version 1.0
 * @since Sunday, August 9th, 2020
 */
 use Illuminate\Http\Request;
 use Illuminate\Http\Response;
 //use Illuminate\Routing\Controller;
 use Nwidart\Modules\Routing\Controller;
 use DataTables;
 use DB;
 use Carbon\Carbon;
 use Auth;
 use App\User;
 use Validator;

 class JadwalController extends Controller
 {
     /**
      * Display a listing of the resource.
      * @return Response
      */
     public function index()
     {
         return view('krs::jadwal.index');
     }
     public function Datalist()
     {
         $aka=DB::connection('loker')->table('periode_akademik')->where('aktif', 1)->first();
         $data=DB::connection('loker')->table('kelas')
         ->join('dt_hari', 'dt_hari.kd_hari', '=', 'kelas.hari')
         ->join('pengajar', 'pengajar.idp', '=', 'kelas.dosen')
         ->join('pelajaran', 'pelajaran.kd_ak', '=', 'kelas.kd_mk')
         ->join('krs', 'krs.kd_kelas', '=', 'kelas.kd_kelas')
         ->select([
             'pelajaran.nama as pelajaran',
             'dt_hari.hari',
             'pengajar.nidn as dosen',
             'kelas.jam_mulai',
             'kelas.jam_akhir',
             'kelas.kd_ruang',
             'krs.sks'
         ])->where('krs.idp', useridp())
         ->where('kelas.kd_pr_akademik', $aka->pr_akademik);
         return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            // ->editColumn('TotalBiaya', function ($data) {
            //     return number_format($data->TotalBiaya);
            // })
            // ->editColumn('potongan', function ($data) {
            //     return number_format($data->potongan);
            // })->editColumn('TotalSetor', function ($data) {
            //     return number_format($data->TotalSetor);
            // })->editColumn('Kekurangan', function ($data) {
            //     return number_format($data->Kekurangan);
            // })
            


            
            ->make(true);
     }

     /**
      * Show the form for creating a new resource.
      * @return Response
      */
     public function create()
     {
         return view('krs::create');
     }

     /**
      * Store a newly created resource in storage.
      * @param Request $req
      * @return Response
      */
     public function store(Request $req)
     {
         DB::beginTransaction();

         try {
             $valid = Validator::make($req->all(), [
                 'name' => 'required',
        ]);
             if ($valid->fails()) {
                 return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
             }
             DB::commit();
             return response()->json(['status'=>'success']);
         } catch (\Exception $e) {
             DB::rollback();
             return response()->json(['status'=>'error','msg'=>$e->getMessage()]);
         }
     }

     /**
      * Show the specified resource.
      * @param int $id
      * @return Response
      */
     public function show($id)
     {
         return view('krs::show');
     }

     /**
      * Show the form for editing the specified resource.
      * @param int $id
      * @return Response
      */
     public function edit($id)
     {
         return view('krs::edit');
     }

     /**
      * Update the specified resource in storage.
      * @param Request $req
      * @param int $id
      * @return Response
      */
     public function update(Request $req, $id)
     {
         DB::beginTransaction();

         try {
             $valid = Validator::make($req->all(), [
                 'name' => 'required',
             ]);
             if ($valid->fails()) {
                 return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
             }
             DB::commit();
             return response()->json(['status'=>'success']);
         } catch (\Exception $e) {
             DB::rollback();
             return response()->json(['status'=>'error','msg'=>$e->getMessage()]);
         }
     }

     /**
      * Remove the specified resource from storage.
      * @param int $id
      * @return Response
      */
     public function destroy($id)
     {
         DB::beginTransaction();

         try {

              // all good
             DB::commit();
             return response()->json(['status'=>'success']);
         } catch (\Exception $e) {
             DB::rollback();
             return response()->json(['status'=>'error','msg'=>$e->getMessage()]);
         }
     }
 }
