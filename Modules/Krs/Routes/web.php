<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 
    Route::group(['middleware' => ['Login'], 'prefix' => 'krs'], function () {
        Route::get('/index', 'KrsController@index')->name('krs.index');
        Route::get('/index/create', 'KrsController@create')->name('krs.create');
        Route::post('api/index', 'KrsController@Datalist')->name('api.krs');
        Route::resource('jadwal', 'JadwalController');
        Route::post('api/jadwal', 'JadwalController@Datalist')->name('api.jadwal');
    });
