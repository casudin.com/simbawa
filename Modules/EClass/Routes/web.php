<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


    Route::group(['middleware' => ['Login'], 'prefix' => 'e-class'], function () {
        Route::get('/', 'EClassController@index')->name('e-class.dashboard');
    });
