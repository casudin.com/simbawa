<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['Login'], 'prefix' => 'developer'], function () {
    Route::get('/app', 'DeveloperController@index')->name('tentang');
    Route::resource('saran', 'SaranController');
    Route::post('api/saran', 'SaranController@Datalist')->name('api.saran');
    Route::post('delete/saran', 'SaranController@delete')->name('delete.saran');
    Route::post('submitrating','DeveloperController@submitRating')->name('beri.rating');
    Route::get('myrating','DeveloperController@showRating')->name('rating.saya');
});
// Route::group(['middleware' => ['Login'], 'prefix' => 'developer'], function () {
//     Route::resource('developer', 'DeveloperController');
// });