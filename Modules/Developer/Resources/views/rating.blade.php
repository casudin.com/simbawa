<div class="statbox widget box box-shadow">
    <div class="widget-header">                                
        <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                <h4>Beri Penilaian</h4> 
            </div>
        </div>
    </div>
    <div class="widget-content widget-content-area custom-rating">
        <div class="row mb-4">
            
            <div class="col-lg-12 mb-5">
                <h3 class="card-title mr-5">Penilaian Kamu </h3>
                @if (!empty($data->value))
                <div id="ratingankamu">
                @if ($data->value==1)
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill "></i>
                <i class="flaticon-star-fill "></i>
                <i class="flaticon-star-fill "></i>
                <i class="flaticon-star-fill "></i>
                @elseif($data->value==2)
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill "></i>
                <i class="flaticon-star-fill "></i>
                <i class="flaticon-star-fill "></i>
                @elseif($data->value==3) 
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill "></i>
                <i class="flaticon-star-fill "></i>
                @elseif($data->value==4)
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill "></i>
                @elseif($data->value==5)
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill active"></i>
                <i class="flaticon-star-fill active"></i>
                @endif
                <a onclick="edit()" href="javascript:void(0);">Edit</a>
                </div>
               
                @else
                <div class="rating" data-role="rating" data-size="large" data-on-rated="demo_func_onRated"></div>
                @endif
                
                <div class="rating" data-role="rating" data-size="large" data-on-rated="demo_func_onRated" id="ulang"></div>
                @php
                   $xx= DB::table('ratings')
                ->avg('value');
                @endphp
                <p class="card-text">{{ round($xx) }} average based on {{ DB::table('ratings')->count() }} reviews.</p>
            </div>

            <div class="col-lg-12">
                <div class="media">
                    <div class="mr-3">
                        <span class="numeric-value">5</span>
                        <i class="flaticon-star-fill"></i>
                    </div>
                    <div class="media-body">
                        
                        @php
                            $lima=DB::table('ratings')->where('value',5)->count()/$total*100;
                            
                        @endphp
                        <div class="progress progress-md br-30">
                            <div class="progress-bar bg-primary br-30" role="progressbar" style="width: {{ $lima }}%" aria-valuenow="{{ $lima }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="ml-3">
                        <div class="like-count">{{ DB::table('ratings')->where('value',5)->count() }}</div>
                    </div>
                </div>

                <div class="media">
                    <div class="mr-3">
                        <span class="numeric-value">4</span>
                        <i class="flaticon-star-fill"></i>
                    </div>
                    @php
                    $x4=DB::table('ratings')->where('value',4)->count()/$total*100;
                @endphp
                    <div class="media-body">
                        <div class="progress progress-md br-30">
                            <div class="progress-bar bg-success br-30" role="progressbar" style="width: {{ $x4 }}%" aria-valuenow="{{ $x4 }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="ml-3">
                        <div class="like-count">{{ DB::table('ratings')->where('value',4)->count() }}</div>
                    </div>
                </div>

                <div class="media">
                    <div class="mr-3">
                        <span class="numeric-value">3</span>
                        <i class="flaticon-star-fill"></i>
                    </div>
                    @php
                    $x3=DB::table('ratings')->where('value',3)->count()/$total*100;
                @endphp
                    <div class="media-body">
                        <div class="progress progress-md br-30">
                            <div class="progress-bar bg-info br-30" role="progressbar" style="width: {{ $x3 }}%" aria-valuenow="{{ $x3 }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="ml-3">
                        <div class="like-count">{{ DB::table('ratings')->where('value',3)->count() }}</div>
                    </div>
                </div>

                <div class="media">
                    <div class="mr-3">
                        <span class="numeric-value">2</span>
                        <i class="flaticon-star-fill"></i>
                    </div>
                    @php
                    $x2=DB::table('ratings')->where('value',2)->count()/$total*100;
                    @endphp
                    <div class="media-body">
                        <div class="progress progress-md br-30">
                            <div class="progress-bar bg-warning br-30" role="progressbar" style="width: {{ $x2 }}%" aria-valuenow="{{ $x2 }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="ml-3">
                        <div class="like-count">{{ DB::table('ratings')->where('value',2)->count() }}</div>
                    </div>
                </div>

                <div class="media">
                    <div class="mr-3">
                        <span class="numeric-value">1</span>
                        <i class="flaticon-star-fill"></i>
                    </div>
                    @php
                    $x1=DB::table('ratings')->where('value',1)->count()/$total*100;
                    @endphp
                    <div class="media-body">
                        <div class="progress progress-md br-30">
                            <div class="progress-bar bg-danger br-30" role="progressbar" style="width: {{ $x1 }}%" aria-valuenow="{{ $x1 }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="ml-3">
                        <div class="like-count">{{ DB::table('ratings')->where('value',1)->count() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#ulang").hide();
     function edit(){
        $("#ratingankamu").hide();
         
        $("#ulang").show();
    }
</script>