@extends('layouts.master')
@section('title', 'App')
@section('parentPageTitle', 'Developer')
@section('css')
<link href="{{ asset('assets/css/pages/helpdesk.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/design-css/design.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/design-css/design-icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/components/custom-rating.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="helpdesk layout-spacing">

   
    <div class="hd-tab-section">
        <div class="row">
            <div class="col-md-12 mb-5 mt-5">
                <ul class="nav nav-pills mb-5 justify-content-between" id="pills-tab" role="tablist">
                    <li class="col-xl-4 col-lg-4 col-md-6 col-12 nav-item text-center mb-5">
                        <a class="nav-link active" id="pills-statistics-tab" data-toggle="pill" href="#pills-statistics" role="tab" aria-controls="pills-statistics" aria-selected="true">
                            <i class="flaticon-pie-line-chart"></i>
                            <h6 class="mt-3 mb-3">SIMBAWA</h6>
                            <p>Versi 1.0.1</p>
                        </a>
                    </li>
                    
                    <li class="col-xl-4 col-lg-4 col-md-6 col-12 nav-item text-center mb-5">
                        <a class="nav-link" id="pills-performance-tab" data-toggle="pill" href="#pills-performance" role="tab" aria-controls="pills-performance" aria-selected="false">
                            <i class="flaticon-lightning-1"></i>
                            <h6 class="mt-3 mb-3">Laravel</h6>
                            <p>Versi 7.22.x</p>
                        </a>
                    </li>
                    
                    <li class="col-xl-4 col-lg-4 col-md-6 col-12 nav-item text-center mb-5">
                        <a class="nav-link" id="pills-reports-tab" data-toggle="pill" href="#pills-reports" role="tab" aria-controls="pills-reports" aria-selected="false">
                            <i class="flaticon-note-1"></i>
                            <h6 class="mt-3 mb-3">API</h6>
                            <p>Lumen Component (7.x)</p>
                        </a>
                    </li>
 
                </ul>

                <div class="col-xl-12 col-lg-12 layout-spacing" id="myrating">
                   
                </div>

            </div>
        </div>                            
    </div>

    
</div>
@endsection
@section('js')
<script src="{{ asset('assets/js/design-js/design.js') }}"></script>
@endsection
@section('modal')
@endsection
@section('script')
<script>
    load();
    function load(){
        $("#myrating").load("{{ route('rating.saya') }}");
    }
    function demo_func_onRated(value, star, widget){
        $.ajax({
            type: "POST",
            url: "{{ route('beri.rating') }}",
            data: {value:value},
            dataType: "JSON",
            success: function (response) {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title:response.msg,
                    showConfirmButton: false,
                    timer: 3000
                    })
                load();
            }
        });
    
    }
   

</script>
@endsection