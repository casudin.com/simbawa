<?php

namespace Modules\Developer\Http\Controllers;
/**
 * A class to get information of SaranController
 * @author Casudin (com.casudin@gmail.com)
 * @version 1.0
 * @since Saturday, August 8th, 2020
 */
 use Illuminate\Http\Request;
 use Illuminate\Http\Response;
 //use Illuminate\Routing\Controller;
 use Nwidart\Modules\Routing\Controller;
 use DataTables;
 use DB;
 use Carbon\Carbon;
 use Auth;
 use App\User;
use Validator;
use App\Mail\DevKirim;
use App\Mail\DevMasuk;
use Mail;
class SaranController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('developer::saran.index');
    }
    function Datalist()
    {
        $user =useridp();

        if ($user==3806) {
            $data = DB::table('developers');
        } else {
            $data = DB::table('developers')->where('idp', $user);
        }
        return Datatables::of($data)
        ->addIndexColumn()
        ->escapeColumns([])

        ->editColumn('type', function ($data) {
            if ($data->type==1) {
               return 'Request Fitur Baru';
            }elseif($data->type==2){
                return "Umum";
            }elseif($data->type==3){
                return "Bugs";
            }
        })
        // ->editColumn('potongan', function ($data) {
        //     return number_format($data->potongan);
        // })
        
        ->addColumn('action', function ($data) {
            if (useridp()==3806) {
               return '<button type="button" onclick="hapus(' . "'$data->id'" . ')" class="btn btn-danger btn-sm mb-4 mr-2"><i class="flaticon-delete mr-1"></i> Hapus</button> 
               <a href="'.route('saran.edit',[$data->id]).'" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-sm mb-4 mr-2"><i class="flaticon-bell mr-1"></i> Jawab</a> 
                ';
            }else{
                return '<button type="button" onclick="hapus(' . "'$data->id'" . ')" class="btn btn-danger btn-sm mb-4 mr-2"><i class="flaticon-delete mr-1"></i> Hapus</button> 
                ';
            }
        })

 
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('developer::saran.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $req
     * @return Response
     */
    public function store(Request $req)
    {
      DB::beginTransaction();

          try {

              $valid = Validator::make($req->all(), [
                 'type' => 'required',
                 'pesan' => 'required',
        ]);
        if ($valid->fails()) {

            return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
        }
        DB::table('developers')->insert(['idp'=>useridp(),'pesan'=>$req->pesan,'type'=>$req->type,'created_at'=>now()]);
        $data = [
            'name' => 'Casudin Amd. Skom',
            'mhs' => !empty(mhs()) ? mhs() : "-",
            'pesan' => $req->pesan
        ];
        $data1 = [
            'name' => namaMhs(),
            'mhs' => !empty(mhs()) ? mhs() : "-",
            
        ];
        Mail::to('casudin.ahi@gmail.com')             
            ->send(new DevKirim($data));

        Mail::to(emailMhs())
            ->cc('casudin.ahi@gmail.com')
            ->send(new DevMasuk($data1));

         DB::commit();
              return response()->json(['status'=>'success','msg'=>"Berhasil dikirim"]);

          } catch (\Exception $e) {
              DB::rollback();
              return response()->json(['status'=>'error','msg'=>$e->getMessage()]);

          }
    }
    function delete(Request $req){
        DB::table('developers')->where('id',$req->id)->delete();
        return response()->json(['status'=>'success','msg'=>"Berhasil hapus"]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('developer::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if (useridp()==3806) {
            $data=DB::table('developers')->where('id',$id)->first();
        return view('developer::saran.edit',compact('data'));
         }else{
             abort(419);
         }
       
    }

    /**
     * Update the specified resource in storage.
     * @param Request $req
     * @param int $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
      DB::beginTransaction();

          try {

               $valid = Validator::make($req->all(), [
                 'jawaban' => 'required',
             ]);
            if ($valid->fails()) {

                return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
            }
            DB::table('developers')->where('id',$id)->update(['jawaban'=>$req->jawaban,'updated_at'=>now()]);
             DB::commit();
                  return response()->json(['status'=>'success','msg'=>"OK"]);

          } catch (\Exception $e) {
              DB::rollback();
              return response()->json(['status'=>'error','msg'=>$e->getMessage()]);

          }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
      DB::beginTransaction();

          try {

              // all good
            DB::commit();
              return response()->json(['status'=>'success']);

          } catch (\Exception $e) {
              DB::rollback();
              return response()->json(['status'=>'error','msg'=>$e->getMessage()]);

          }
    }
}
