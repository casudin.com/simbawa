<?php

namespace Modules\Developer\Http\Controllers;
/**
 * A class to get information of DeveloperController
 * @author Casudin (com.casudin@gmail.com)
 * @version 1.0
 * @since Saturday, August 8th, 2020
 */
 use Illuminate\Http\Request;
 use Illuminate\Http\Response;
 //use Illuminate\Routing\Controller;
 use Nwidart\Modules\Routing\Controller;
 use DataTables;
 use DB;
 use Carbon\Carbon;
 use Auth;
 use App\User;
use Validator;

class DeveloperController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('developer::index');
    }
    function list()
    {
        $user = User::find(Auth::user()->id);

        if ($user->hasAnyRole(['Owner', 'Super Admin'])) {
            $data = DB::table('');
        } else {
            $data = DB::table('')->where('company_id', cabang());
        }
    }
    function submitRating(){
        DB::table('ratings')->updateOrInsert(['idp'=>useridp()],['value'=>request()->value,'created_at'=>now(),'updated_at'=>now()]);
        return response()->json(['status'=>'success','msg'=>':) Terimakasih']);
    }
    function showRating(){
        $data=DB::table('ratings')->where('idp',useridp())->first();
        
        $total=DB::table('mahasiswa')->where('status','AT')->count();
        return view('developer::rating',compact('data','total'));
    }
    
    

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('developer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $req
     * @return Response
     */
    public function store(Request $req)
    {
      DB::beginTransaction();

          try {

              $valid = Validator::make($req->all(), [
                 'name' => 'required',
        ]);
        if ($valid->fails()) {

            return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
        }
         DB::commit();
              return response()->json(['status'=>'success']);

          } catch (\Exception $e) {
              DB::rollback();
              return response()->json(['status'=>'error','msg'=>$e->getMessage()]);

          }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('developer::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('developer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $req
     * @param int $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
      DB::beginTransaction();

          try {

               $valid = Validator::make($req->all(), [
                 'name' => 'required',
             ]);
            if ($valid->fails()) {

                return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
            }
             DB::commit();
                  return response()->json(['status'=>'success']);

          } catch (\Exception $e) {
              DB::rollback();
              return response()->json(['status'=>'error','msg'=>$e->getMessage()]);

          }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
      DB::beginTransaction();

          try {

              // all good
            DB::commit();
              return response()->json(['status'=>'success']);

          } catch (\Exception $e) {
              DB::rollback();
              return response()->json(['status'=>'error','msg'=>$e->getMessage()]);

          }
    }
}
