const mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css')
mix.styles([
    'public/bootstrap/css/bootstrap.min.css',
    'public/assets/css/plugins.css',
    'public/plugins/animate/animate.css',
    'public/assets/css/modals/component.css',
    'public/assets/css/ui-kit/custom-modal.css',
    'public/alert/dist/sweetalert2.min.css'
], 'public/css/all.css').version()
mix.js([
    'public/assets/js/libs/jquery-3.1.1.min.js',
    'public/bootstrap/js/popper.min.js',
    'public/bootstrap/js/bootstrap.min.js',

    'public/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js',
    'public/assets/js/app.js', 'public/alert/dist/sweetalert2.all.min.js'
], 'public/js/all.js').version()