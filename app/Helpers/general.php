<?php

use Ramsey\Uuid\Uuid;

if (!function_exists('DummyFunction')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function DummyFunction()
    {
    }
}
function bahasa()
{
    return request()->session()->get('locale');
}
function unik()
{
    return Uuid::uuid4()->toString();
}
function userRole($id)
{
    $user = App\User::find($id);
    return    $user->roles->pluck('name');
}
function romawi($num)
{
    $n = intval($num);
    $res = '';

    $roman_numerals = array(
        'M'  => 1000,
        'CM' => 900,
        'D'  => 500,
        'CD' => 400,
        'C'  => 100,
        'XC' => 90,
        'L'  => 50,
        'XL' => 40,
        'X'  => 10,
        'IX' => 9,
        'V'  => 5,
        'IV' => 4,
        'I'  => 1
    );

    foreach ($roman_numerals as $roman => $number) {
        $matches = intval($n / $number);

        $res .= str_repeat($roman, $matches);

        $n = $n % $number;
    }

    return $res;
}
function convert_ke_bulan($idbulan, $full = false)
{
    $shortName = array(
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Ags',
        'Sep',
        'Okt',
        'Nov',
        'Des',
    );

    if ($full) {
        $fullName = array(
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember',
        );

        return $fullName[$idbulan - 1];
    }

    return $shortName[$idbulan - 1];
}
function jam($date)
{
    return  ' <i class="flaticon-time"></i> ' . date('H:i', strtotime($date));
}
function jam2($date)
{
    return   date('H:i', strtotime($date));
}
function tgl($date)
{
    return   date('Y-m-d', strtotime($date));
}
function tgl_indo($tgldb, $separator_asal = '-', $separator_tujuan = ' ', $full = false)
{
    if (empty($tgldb) && (strlen($tgldb) < 4)) {
        return null;
    }
    /* $tgldb formatnya 2015-05-29 , rubah menjadi 29-Mei-2015 */
    /* cek apakah mengandung jam atau detik, panjang max = 10 karakter */
    $tgldb = substr($tgldb, 0, 10);

    $tgl = explode($separator_asal, $tgldb);
    if (!(count($tgl) > 1)) {
        return $tgldb;
    }
    $newTgl = array(
        $tgl[2],
        convert_ke_bulan($tgl[1], $full),
        $tgl[0],
    );

    return implode($separator_tujuan, $newTgl);
}
function role()
{
    $data = session()->get('user')->data->role;
    $role = array();
    foreach ($data as $key) {
        $role[] = $key->id_group;
    }
    return $role;
}
function useridp()
{
    return Session::get('user')->data->idp;
}
function mhs()
{
    $r = DB::table('_mahasiswa')->where('idp', useridp())->first();
    return $r->nama;
}
function nimMhs()
{
    $r = DB::table('view_totalbayar')->where('idp', useridp())->first();
    return $r->nim;
}
function periodeakademik()
{
    $data = DB::table('periode_akademik')->where('aktif', 1)->first();
    return $data->pr_akademik;
}
function cekEmail()
{
    $data = DB::table('data_pribadi')->where('idp', useridp())->first();
    if (empty($data->email1)) {
        return response()->json(['status' => 'empty']);
    } else {
        return response()->json(['status' => 'ok']);
    }
}
function emailMhs()
{
    $data = DB::table('_mahasiswa')->where('idp', useridp())->first();
    return $data->email1;
}
function namaMhs()
{
    $data = DB::table('_mahasiswa')->where('idp', useridp())->first();
    return $data->nama;
}
function get_user($idp)
{
    $data = DB::table('_mahasiswa')->where('idp', $idp)->first();
    return $data;
}
function namaUser()
{
    return session()->get('user')->data->nama;
}
function dosen()
{
    if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role())) {
        return true;
    } else {
        return false;
    }
}
function slug($data)
{
    return Str::slug($data, '-');
}
function cekStatusMhs()
{
    $data = DB::connection('loker')->table('mahasiswa')->where('idp', useridp())->where('status', 'AT');
    if ($data->count() > 0) {
        return true;
    } else {
        return false;
    }
}
