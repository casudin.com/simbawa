<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class PesertaExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;
    public function view(): View
    {
        $data = DB::table('tb_peserta')->get();
        return view('ujian.peserta.export', [
            'data' => $data
        ]);
    }
    public function collection()
    {
    }
}
