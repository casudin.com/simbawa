<?php

namespace App\Http\Controllers;

use App\Mail\Pembayaran;
use App\Mail\StatusPembayaran;
use App\Mail\Terimakasih;
use Illuminate\Http\Request;
use Validator;
use DataTables;
use DB;
use Mail;
use Session;

class PembayaranController extends Controller
{
    public function index()
    {
        return view('pembayaran.index');
    }
    public function listData(Request $req)
    {
        if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role())) {
            $data = DB::table('tb_buktitrf')
                ->select([
                    'tb_pengguna.nama',
                    'tb_buktitrf.foto',
                    'tb_buktitrf.tgl',
                    'tb_buktitrf.ket',
                    'tb_buktitrf.status',
                    'tb_buktitrf.id'
                ])
                ->join('tb_pengguna', 'tb_pengguna.idp', '=', 'tb_buktitrf.idp')->orderByDesc('tb_buktitrf.id');
        } else {
            $data = DB::table('tb_buktitrf')
                ->select([
                    'tb_pengguna.nama',
                    'tb_buktitrf.foto',
                    'tb_buktitrf.tgl',
                    'tb_buktitrf.ket',
                    'tb_buktitrf.status',
                    'tb_buktitrf.id'
                ])
                ->join('tb_pengguna', 'tb_pengguna.idp', '=', 'tb_buktitrf.idp')->orderByDesc('tb_buktitrf.id')
                ->where('tb_buktitrf.idp', useridp())->orderByDesc('tb_buktitrf.id');
        }
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])



            ->editColumn('tgl', function ($data) {
                return tgl_indo($data->tgl);
            })
            ->editColumn('status', function ($data) {
                return $data->status;
            })->editColumn('foto', function ($data) {
                return '<a href="#" onclick="unduh(' . "'$data->id'" . ')" style="color:blue">
                        Unduh 
                    </a>';
            })


            ->addColumn('action', function ($data) {
                if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role())) {
                    if ($data->status == 'Selesai' || $data->status == 'Dikembalikan') {
                        return '';
                    } else {
                        return '<div class="dropdown custom-dropdown">
                                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            <i class="flaticon-dot-three"></i>
                                                        </a>

                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink1">
                                                           <a class="dropdown-item" href="' . route('pembayaran.edit', $data->id) . '" data-toggle="modal" data-target="#myModal">Update</a>
                                                           <a class="dropdown-item" href="' . route('proses.bayar', $data->id) . '" data-toggle="modal" data-target="#myModal">Input Pembayaran</a>
                                                            <a class="dropdown-item" onclick="hapus(' . "'$data->id'" . ')">Delete</a>

                                                        </div>
                                                    </div>';
                    }
                } else {
                    if ($data->status == 'Selesai' ||  $data->status == 'Dikembalikan') {
                        return '';
                    } else {
                        return '<div class="dropdown custom-dropdown">
                                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            <i class="flaticon-dot-three"></i>
                                                        </a>

                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink1">

                                                           
                                                            <a class="dropdown-item" onclick="hapus(' . "'$data->id'" . ')">Delete</a>

                                                        </div>
                                                    </div>';
                    }
                }
            })

            ->make(true);
    }
    public function create()
    {
        return view('pembayaran.create');
    }
    public function edit($id)
    {
        if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role())) {
            $data = DB::table('tb_buktitrf')->where('id', $id)->first();
            return view('pembayaran.edit', compact('data'));
        } else {
            return abort(401);
        }
    }
    public function proses(Request $req, $id)
    {
        if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role())) {
            if ($req->isMethod('post')) {
                DB::beginTransaction();
                try {
                    $valid = \Validator::make($req->all(), [
                        'jumlah' => 'required|numeric',
                        'bank' => 'required',
                        'terbilang' => 'required',
                        'rekening' => 'required',
                        'tgl_trasfer' => 'required:date',
                    ]);
                    if ($valid->fails()) {
                        return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
                    }
                    $akademik = DB::table('periode_akademik')->where('aktif', 1)->first();
                    $xx = DB::table('transaksi')->insert([
                        'no_kwitansi' => $req->kwitansi,
                        'tgl_transaksi' => now(),
                        'idp' => $req->idp,
                        'nama_bank' => $req->bank,
                        'tgl_trans_bank' => $req->tgl_trasfer,
                        'setor' => $req->jumlah,
                        'wajib' => 1,
                        'penerima' => useridp(),
                        'keterangan' => '',
                        'terbilang' => $req->terbilang,
                        'posting' => 0,
                    ]);
                    $tr = DB::table('transaksi_detail')->insert([
                        'idx' => '',
                        'no_kwitansi' => $req->kwitansi,
                        'kd_pr_akademik' => $akademik->pr_akademik,
                        'rekening' => $req->rekening,
                        'jumlah' => $req->jumlah,
                        'angsuran' => 0,
                        'no_biaya' => 0
                    ]);
                    DB::table('tb_buktitrf')->where('id', $id)->update(['status' => 'Selesai']);
                    //  $r = DB::table('tb_buktitrf')->where('id', $id)->first();
                    // if (file_exists($file = public_path('media/' . $r->foto))) {
                    //     unlink($file);
                    // }
                    DB::commit();

                    return response()->json(['status' => 'success', 'msg' => 'Data Berhasil Di Perbarui']);
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
                }
            }
            $data = DB::table('tb_buktitrf')->where('id', $id)->first();
            $mhs = DB::table('_mahasiswa')->where('idp', $data->idp)->first();
            $no = $this->kwitansi();
            $bank = DB::table('dt_bank')->get();
            $rekening = DB::table('dt_rekening')->get();
            return view('pembayaran.input', compact('data', 'no', 'mhs', 'bank', 'rekening'));
        } else {
            return abort(401);
        }
    }
    public function kwitansi()
    {
        $today = date("Ym");
        $today2 = date("Y-m-d");
        $query = DB::table('_transaksi')->select(DB::raw('max(no_kwitansi) as kode'))->first();
        $data = $query;
        $lastNoTransaksi = $data->kode;
        $lastNoUrut = substr($lastNoTransaksi, 6, 5);
        $nextNoUrut = $lastNoUrut + 1;
        $thn = substr($today, 2);
        $t = "T";
        $n = "-";
        $gabung = $t . $thn . $n;
        return  $nextNoTransaksi = $gabung . sprintf('%04s', $nextNoUrut);
    }
    public function update(Request $req, $id)
    {
        if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role())) {
            DB::table('tb_buktitrf')->where('id', $id)->update([
                'ket' => $req->ket,
                'status' => $req->status
            ]);
            $dd = DB::table('tb_buktitrf')->where('id', $id)->first();
            $data = [
                'name' => get_user($dd->idp)->nama,
                'ket' => $req->ket,
                'status' => $req->status
            ];
            if ($dd->status == "Dikembalikan") {
                if (\Storage::disk('ftp')->exists('bukti/' . $dd->foto)) {
                    \Storage::disk('ftp')->delete('bukti/' . $dd->foto);
                }
            }
            Mail::to(get_user($dd->idp)->email1)
                ->cc('casudin@programmer.net')
                ->send(new StatusPembayaran($data));

            return response()->json(['status' => 'success', 'msg' => 'Data Berhasil diperbarui']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Acces Denied!!'], 401);
        }
    }
    public function filter(Request $req)
    {
        return view('pembayaran.filter');
    }
    public function store(Request $req)
    {
        DB::beginTransaction();
        try {
            $valid = \Validator::make($req->all(), [
                'foto' => 'required|mimes:jpeg,png,jpg,gif,svg|max:600',
                'keterangan' => 'required',
                'jumlah' => 'required'
            ]);
            if ($valid->fails()) {
                return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
            }
            if ($req->has('foto')) {
                $file = $req->file('foto');
                $destinationPath = public_path('media');
                $extension = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                // Valid extensions
                $validextensions = array("jpeg", "jpg", "png", 'JPG', 'JPEG');
                if (in_array(strtolower($extension), $validextensions)) {
                    $fileName =  rand(11111, 99999) . '-' . slug(namaUser()) . '.' . $extension;
                    $xx = \File::get($file);
                    \Storage::disk('ftp')->put('bukti/' . $fileName, $xx);
                    DB::table('tb_buktitrf')->insert([
                        'idp' => useridp(),
                        'tgl' => now(),
                        'foto' => $fileName,
                        'ket' => $req->keterangan . ' sebesar ' . number_format($req->jumlah),
                        'status' => 'Proses',

                    ]);
                }
            }
            $data = [
                'name' => 'Ilman Kadori Skom. Mkom',
                'mhs' => !empty(mhs()) ? mhs() : "-",
                'jumlah' => $req->keterangan . ' ' . number_format($req->jumlah),
            ];
            $data1 = [
                'name' => namaMhs(),
                'mhs' => !empty(mhs()) ? mhs() : "-",
                'jumlah' => $req->keterangan . ' ' . number_format($req->jumlah),
            ];
            Mail::to('stmikmic2@gmail.com')
                ->cc('casudin@programmer.net')
                ->send(new Pembayaran($data));

            Mail::to(emailMhs())
                ->cc('casudin@programmer.net')
                ->send(new Terimakasih($data1));


            DB::commit(); // all good
            return response()->json(['status' => 'success', 'msg' => 'Data Berhasil Disimpan']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }
    public function hapus(Request $req)
    {
        $x = DB::table('tb_buktitrf')->where('id', $req->id)->first();
        if (\Storage::disk('ftp')->exists('bukti/' . $x->foto)) {
            \Storage::disk('ftp')->delete('bukti/' . $x->foto);
        }
        DB::table('tb_buktitrf')->where('id', $req->id)->delete();
        return response()->json(['status' => 'success deleted', 'msg' => 'Data Berhasil Di Hapus']);
    }
    public function tagihanKu()
    {
        return view('pembayaran.tagihan');
    }
    public function tagihan()
    {
        $data = DB::table('view_kekurangan')->where('nim', nimMhs());
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            ->editColumn('TotalBiaya', function ($data) {
                return number_format($data->TotalBiaya);
            })
            ->editColumn('potongan', function ($data) {
                return number_format($data->potongan);
            })->editColumn('TotalSetor', function ($data) {
                return number_format($data->TotalSetor);
            })->editColumn('Kekurangan', function ($data) {
                return number_format($data->Kekurangan);
            })

            ->addColumn('action', function ($data) {
                return '<a href="' . route('detil.tagihanKu') . '"  data-toggle="modal" data-target="#myModal"  class="btn  bs-tooltip btn-dark mb-4 mr-2 rounded btn-sm">
                <i class="flaticon-money"></i>Detil</a>';
            })


            ->make(true);
    }
    public function detilBiaya()
    {
        $idp = useridp();
        $akademik = periodeakademik();
        $data = DB::table('_jumlah_biaya')->where('idp', useridp())->orderBy('prioritas')->get();

        $histori = DB::select("SELECT transaksi_detail.no_kwitansi,_petugas_fo.nama,biaya.kd_pr_akademik,dt_rekening.rekening,(biaya.harga*biaya.qty) as harga,biaya.pot_persen,biaya.pot_harga as Pot_Harga,
                                    ((biaya.harga*biaya.qty) * (biaya.pot_persen/100)) as potongan,
                                    ((biaya.harga*biaya.qty)-((biaya.harga*biaya.qty) * (biaya.pot_persen/100))-biaya.pot_harga) as Biaya,tgl_transaksi,tgl_trans_bank,bank,transaksi_detail.jumlah 
                                    FROM biaya
                                    left join transaksi_detail on biaya.no_biaya=transaksi_detail.no_biaya
                                    left join transaksi on transaksi.no_kwitansi=transaksi_detail.no_kwitansi
                                    left join _petugas_fo on _petugas_fo.idp=transaksi.penerima
                                    left join dt_bank on dt_bank.kd_bank=transaksi.nama_bank
                                    left join dt_rekening on dt_rekening.kd_rekening=biaya.rekening  
                                    where biaya.idp='$idp'  order by biaya.kd_pr_akademik desc, dt_rekening.rekening desc");
        $periode = DB::table('periode_akademik')->where('aktif', 1)->first();
        return view('pembayaran.detilBiaya', compact('data', 'histori', 'periode'));
    }
    public function full()
    {
        return view('pembayaran.full');
    }
    public function donlot(Request $req)
    {
        $db = DB::table('tb_buktitrf')->where('id', base64_decode(request()->token))->first();
        if ($req->ajax()) {
            if (\Storage::disk('ftp')->exists('bukti/' . $db->foto)) {
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'error', 'msg' => "file yang diminta tidak tersedia"]);
            }
        }
        return  \Storage::disk('ftp')->download('bukti/' . $db->foto);
    }
}
