<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
use Excel;

class PembayaranMhsController extends Controller
{
    public function index()
    {
        return view('rekap.pembayaran');
    }
    public function dataList()
    {
        $data = DB::table('view_kekurangan');
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            ->editColumn('TotalBiaya', function ($data) {
                return number_format($data->TotalBiaya);
            })
            ->editColumn('potongan', function ($data) {
                return number_format($data->potongan);
            })->editColumn('TotalSetor', function ($data) {
                return number_format($data->TotalSetor);
            })->editColumn('Kekurangan', function ($data) {
                return number_format($data->Kekurangan);
            })
            ->addColumn('action', function ($data) {
                return '<a href="' . route('detil.pembayaran', [$data->idp]) . '" data-toggle="modal" data-target="#myModal"><i class="flaticon-fill-area icon"></i>Detil</a>';
            })


            ->setTotalRecords(100)
            ->make(true);
    }
    public function detail($idp)
    {
        $data = DB::select("SELECT transaksi_detail.no_kwitansi,_petugas_fo.nama,biaya.kd_pr_akademik,dt_rekening.rekening,(biaya.harga*biaya.qty) as harga,biaya.pot_persen,biaya.pot_harga as Pot_Harga,
                                    ((biaya.harga*biaya.qty) * (biaya.pot_persen/100)) as potongan,
                                    ((biaya.harga*biaya.qty)-((biaya.harga*biaya.qty) * (biaya.pot_persen/100))-biaya.pot_harga) as Biaya,tgl_transaksi,tgl_trans_bank,bank,transaksi_detail.jumlah 
                                    FROM biaya
                                    left join transaksi_detail on biaya.no_biaya=transaksi_detail.no_biaya
                                    left join transaksi on transaksi.no_kwitansi=transaksi_detail.no_kwitansi
                                    left join _petugas_fo on _petugas_fo.idp=transaksi.penerima
                                    left join dt_bank on dt_bank.kd_bank=transaksi.nama_bank
                                    left join dt_rekening on dt_rekening.kd_rekening=biaya.rekening  
                                    where biaya.idp='$idp'  order by biaya.kd_pr_akademik desc, dt_rekening.rekening desc");
        $biaya = DB::table('_jumlah_biaya')->where('idp', $idp)->orderBy('prioritas')->get();
        return view('rekap.detil', compact('data', 'biaya'));
    }
}
