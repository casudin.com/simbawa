<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Curl\Curl;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['Login']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role())) {
            $mhs = DB::table("_mahasiswa")->where('sekarang', 1)->where('idp', useridp())->first();
            $biaya = DB::table('_jumlah_biaya')->select(DB::raw('sum(jumlah_biaya) as biaya'))->where('idp', useridp())->first();
            $sisa = DB::table('transaksi')->select(DB::raw('sum(setor) as jumlah'))->where('idp', useridp())->first();
            $bayar = DB::table('transaksi')->join('transaksi_detail', 'transaksi_detail.no_kwitansi', '=', 'transaksi.no_kwitansi')
                ->select(DB::raw('ifnull(sum(transaksi_detail.jumlah),0) as pembayaran'))
                ->where('transaksi.idp', useridp())->where('transaksi.wajib', 1)->first();
            $khs=array();
            $nilai=array();
        } else {
            $mhs = DB::table("_mahasiswa")->where('sekarang', 1)->where('idp', useridp())->first();
            $biaya = DB::table('_jumlah_biaya')->select(DB::raw('sum(jumlah_biaya) as biaya'))->where('idp', useridp())->first();
            $sisa = DB::table('transaksi')->select(DB::raw('sum(setor) as jumlah'))->where('idp', useridp())->first();
            $bayar = DB::table('transaksi')->join('transaksi_detail', 'transaksi_detail.no_kwitansi', '=', 'transaksi.no_kwitansi')
                ->select(DB::raw('ifnull(sum(transaksi_detail.jumlah),0) as pembayaran'))
                ->where('transaksi.idp', useridp())->where('transaksi.wajib', 1)->first();
            $khs=$this->khs(useridp());
            $nilai=$this->nilai(useridp());
        }
       
        return view('home', compact('mhs', 'biaya', 'sisa', 'bayar', 'khs', 'nilai'));
    }
    private function khs($idp)
    {
        $kurikulum=DB::connection('loker')->table('mahasiswa')->where('idp', $idp)->where('sekarang', 1)->first();
        $data=DB::connection('loker')->select("SELECT
                    khs.idp,
                    p.kd_ak as kd_pelajaran,
                    p.nama,
                    ps.smt,
                    ps.sks,
                    khs.grade,
                    khs.bobot
                FROM
                    pelajaran_sks as ps
                    JOIN pelajaran as p ON (p.kd_ak = ps.kd_pelajaran)
                    left join khs_detail as khs on(khs.kd_mk=p.kd_ak and khs.idp='$idp')
                where kd_kurikulum = '$kurikulum->kd_kurikulum'  
                order by ps.smt,p.nama");
        
        
       
        $mapel=DB::connection('loker')->select("SELECT
        p.kd_ak AS kd_pelajaran,
        p.nama,
        khs.sks,
        khs.grade,
        khs.bobot,
        periode_akademik.nama AS akademik,
        periode_akademik.pr_akademik as kode
    FROM
        pelajaran AS p
    JOIN nilai AS khs
    ON
        khs.kd_ak = p.kd_ak
    JOIN periode_akademik ON periode_akademik.pr_akademik = khs.pr_akademik
    WHERE
        khs.idp = '$idp'
    ORDER BY
        p.nama");
        $sks=0;
        $sks_harus =0;
        $mutu_harus=0;
        $sks_sudah =0;
        $mutu=0;
        $mutu_sudah=0;
        $ipk=0;
        $pr_akademik=array();
        foreach ($mapel as $key) {
            if ($key->bobot!="") {
                $mutu = $key->sks*$key->bobot;
                $sks_sudah = $sks_sudah+$key->sks;
                $mutu_sudah=$mutu_sudah+$mutu;
            } else {
                $mutu=0;
            }
            $sks_harus = $sks_harus+$key->sks;
            $mutu_harus = $mutu_harus+$mutu;
            if ($mutu_sudah!=0) {
                $ipk= number_format($mutu_sudah/$sks_sudah, 2);
            } else {
                $ipk= number_format(0, 2);
            }
            $pr_akademik[]=
            [
                'periode'=>$key->kode.'-'.$key->akademik,
                'ipk'=>$ipk,
            ];
        }
        

         
                
        return $pr_akademik; 
    }
    private function nilai($idp)
    {
        $totalpoin=array();
        $absen=0;
        $tugas=0;
        $uts=0;
        $uas=0;
        $pelajaran=DB::connection('loker')->table('pelajaran')
        ->join('krs', 'krs.kd_pelajaran', '=', 'pelajaran.kd_ak')
        ->join('nilai', 'nilai.kd_ak', '=', 'pelajaran.kd_ak')
        ->select([
            'pelajaran.nama as pelajaran',
            'nilai.no_nilai',
            'nilai.bobot',
            
        ])->where('nilai.idp', $idp)->where('krs.idp', $idp)->orderBy('pelajaran.nama', 'asc')
        ->get();

        foreach ($pelajaran as $key) {
            $nilai=DB::connection('loker')->table('nilai_detail')->where('no_nilai', $key->no_nilai)
            ->select([
                'kd_jenis_nilai as jenis','nilai','persentase'
            ])->get();
            $totalpoin[]=[
                'pelajaran'=>$key->pelajaran,
                'poin'=>$nilai,
            ];
        }
       
      
         
        return $totalpoin;
    }
}
