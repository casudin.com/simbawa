<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;

class SoalUjianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ujian.soal.index');
    }
    public function dataList()
    {
        $data = DB::table('tb_jadwal_ujian')
            ->select([
                'tb_jadwal_ujian.id',
                'tb_dt_ujian.name',
                'tb_soal_ujian.id as soal_id',
                'tb_soal_ujian.file',
                'tb_soal_ujian.links',
                'tb_soal_ujian.user',
                'tb_jadwal_ujian.start',
                'tb_jadwal_ujian.keterangan',
                'tb_jadwal_ujian.mkKode',
                'tb_jadwal_ujian.finish',
                'tb_jadwal_ujian.kuota',
                'pelajaran.nama'
            ])
            ->leftjoin('tb_soal_ujian', 'tb_jadwal_ujian.id', '=', 'tb_soal_ujian.jadwal_id')
            ->join('tb_dt_ujian', 'tb_dt_ujian.id', '=', 'tb_jadwal_ujian.ujianId')
            ->join('pelajaran', 'pelajaran.kd_ak', '=', 'tb_jadwal_ujian.mkKode')
            ->orderByDesc('tb_jadwal_ujian.id');
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            ->editColumn('start', function ($data) {
                return tgl_indo($data->start) . jam($data->start);
            })->editColumn('finish', function ($data) {
                return tgl_indo($data->finish) . jam($data->finish);
            })
            // ->editColumn('status', function ($data) {
            //     if ($data->status == 1) {
            //         return 'Aktif';
            //     } else {
            //         return 'Suspend';
            //     }
            // })
            ->addColumn('action', function ($data) {
                if (!empty($data->soal_id)) {
                    return
                        '<div class="btn-group  mb-4 mr-2">
                                            <button type="button" class="btn btn-primary br-left-30">Options</button>
                                            <button type="button" class="btn btn-primary br-right-30 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu">
                                               <a href="' . route('soal.edit', [$data->soal_id]) . '" data-toggle="modal" data-target="#myModal" class="dropdown-item"><i class="flaticon-upload-file mr-1"></i>Ganti Soal</a>     
                                             <a href="' . asset('media/' . $data->file) . '" data-fancybox data-caption="File" class="dropdown-item"><i class="flaticon-search-1 mr-1"></i>Lihat</a>
                                        
                                            </div>
                                        </div>';
                } else {
                    return '<div class="btn-group  mb-4 mr-2">
                                            <button type="button" class="btn btn-primary br-left-30">Options</button>
                                            <button type="button" class="btn btn-primary br-right-30 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu">
                                               <a href="' . route('soal.create', ["jadwal_id" => $data->id, 'mk' => $data->mkKode]) . '" data-toggle="modal" data-target="#myModal" class="dropdown-item"><i class="flaticon-upload-file mr-1"></i>Upload Soal</a>
                                                
                                            </div>
                                        </div>';
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pelajaran = DB::table('pelajaran')->where('kd_ak', request()->mk)->first();
        $jadwal = DB::table('tb_jadwal_ujian')->where('id', request()->jadwal_id)->first();
        $ujian = DB::table('tb_dt_ujian')->where('id', $jadwal->ujianId)->first();
        return view('ujian.soal.create', compact('pelajaran', 'ujian', 'jadwal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        try {
            $valid = \Validator::make($req->all(), [

                'file' => 'required|mimes:pdf|max:5120',

            ]);
            if ($valid->fails()) {
                return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
            }
            if ($req->has('file')) {
                $file = $req->file('file');
                $destinationPath = public_path('media');
                $extension = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                // Valid extensions
                $validextensions = array("pdf");
                if (in_array(strtolower($extension), $validextensions)) {
                    // Rename file
                    $fileName =  rand(11111, 99999) . '.' . $extension;
                    // Uploading file to given path
                    $file->move($destinationPath, $fileName);
                    DB::table('tb_soal_ujian')->insert([
                        'jadwal_id' => $req->jadwal,
                        'file' => $fileName,
                        'links' => $req->link,
                        'user' => namaUser(),
                        'created_at' => now(),
                        'updated_at' => now()

                    ]);
                }
                return response()->json(['status' => 'success', 'msg' => 'Data Berhasil Disimpan']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('tb_soal_ujian')->where('id', $id)->first();
        return view('ujian.soal.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        try {
            $soal = DB::table('tb_soal_ujian')->where('id', $id)->first();

            $valid = \Validator::make($req->all(), [
                'file' => 'required|mimes:pdf|max:5120',
            ]);
            if ($valid->fails()) {
                return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
            }
            if ($req->has('file')) {
                $file = $req->file('file');
                $destinationPath = public_path('media');
                $extension = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                // Valid extensions
                $validextensions = array("pdf");
                if (in_array(strtolower($extension), $validextensions)) {
                    // Rename file
                    $fileName =  rand(11111, 99999) . '.' . $extension;
                    // Uploading file to given path
                    $file->move($destinationPath, $fileName);
                    if (file_exists($file = public_path('media/' . $soal->file))) {
                        unlink($file);
                    }
                    DB::table('tb_soal_ujian')->where('id', $id)->update([
                        'file' => $fileName,
                        'links' => $req->link,
                        'user' => namaUser(),
                        'created_at' => now(),
                        'updated_at' => now()

                    ]);
                }
            }
            return response()->json(['status' => 'success', 'msg' => 'Data Berhasil Disimpan']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
