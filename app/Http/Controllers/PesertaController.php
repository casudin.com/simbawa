<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl\Curl;

use Validator;
use DataTables;
use App\Imports\PesrtaImport;
use DB;
use Excel;
use App\Exports\PesertaExport;

class PesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ujian.peserta.index');
    }
    public function dataList()
    {
        $data = DB::table('tb_peserta')->orderByDesc('id');
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            ->editColumn('expired', function ($data) {
                return tgl_indo($data->expired);
            })->editColumn('created_at', function ($data) {
                return tgl_indo($data->created_at);
            })
            // ->editColumn('status', function ($data) {
            //     if ($data->status == 1) {
            //         return 'Aktif';
            //     } else {
            //         return 'Suspend';
            //     }
            // })
            ->addColumn('action', function ($data) {
                return '<i class="flaticon-delete-fill icon" onclick="hapus(' . "'$data->id'" . ')"></i>';
            })
            ->make(true);
    }
    public function template()
    {
        $file = public_path('upload-peserta.xlsx');
        return response()->download($file);
    }
    public function upload(Request $req)
    {
        if ($req->isMethod('post')) {
            try {
                $validator = Validator::make($req->all(), [
                    'file' => 'required|mimes:xls,xlsx,csv,ods'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status' => 'error',  'msg' => $validator->messages()->first()]);
                }
                if ($req->hasFile('file')) {
                    $file = $req->file('file'); //GET FILE
                    $import = new PesrtaImport;
                    Excel::import($import, $file);
                    $errors = $import->getError();
                    if (count($errors) > 0) {
                        DB::rollback();
                        return response()->json(['status' => 'error', 'msg' => json_encode($errors) .  ' :( ']);
                    }
                    return response()->json(['status' => 'success', 'msg' => "Data Berhasil Di Import"]);
                }
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
            }
        }
        return view('ujian.peserta.upload');
    }
    public function pesertaDelete()
    {
        DB::table('tb_peserta')->delete();
        return response()->json(['status' => 'success', 'msg' => "Data Berhasil Di Kosongkan"]);
    }
    public function export()
    {
        return Excel::download(new PesertaExport, date('Y') . 'data-peserta.xlsx');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ujian.peserta.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        DB::beginTransaction();
        try {
            $valid = \Validator::make($req->all(), [

                'expired' => 'required',
    
            ]);
            if ($valid->fails()) {
                return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
            }
            DB::table('tb_peserta')->updateOrInsert(['nim' => $req->mhs], [
                'nama' => $req->nama,
                'idp' => $req->idp,
                'expired' => $req->expired,
                'token' => rand(11111, 99999),
                'created_at' => now(),
                'updated_at' => now()
            ]);
            DB::commit();
            // all good
            return response()->json(['status' => 'success', 'msg' => 'berhasil']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tb_peserta')->where('id', $id)->delete();
        return response()->json(['status' => 'success', 'msg' => "Berhasil"]);
    }
}
