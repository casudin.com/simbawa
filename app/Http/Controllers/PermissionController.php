<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use DataTables;

class PermissionController extends Controller
{
    public function permissionList()
    {
        $data = Permission::all();
        return  DataTables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])



            ->editColumn('created_at', function ($data) {
                return tgl_indo($data->created_at);
            })
            ->editColumn('updated_at', function ($data) {
                return tgl_indo($data->updated_at);
            })



            ->addColumn('action', function ($data) {
                return '<i class="flaticon-delete-fill icon" onclick="hapusPermission(' . "'$data->id'" . ')"></i>';
            })
            ->setTotalRecords(100)
            ->make(true);
    }
    public function create()
    {
        return view('admin.acces.permission_create');
    }
    public function store(Request $req)
    {
        $valid = \Validator::make($req->all(), [
            'name' => 'required|string|max:50'
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
        }
        $role = Permission::firstOrCreate($req->all());
        return response()->json(['status' => 'success', 'msg' => "Berhasil"]);
    }
    public function edit($id)
    {
    }
    public function update(Request $req, $id)
    {
    }
    public function destroy($id)
    {
        Permission::destroy($id);
        return response()->json(['status' => 'success', 'msg' => "Berhasil"]);
    }
}
