<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;

class SoalMhsController extends Controller
{
    public function index()
    {
        $data = DB::table('tb_jadwal_ujian')
            ->select([
                'tb_jadwal_ujian.id',
                'tb_dt_ujian.name',
                'tb_soal_ujian.id as soal_id',
                'tb_soal_ujian.file',
                'tb_soal_ujian.links',
                'tb_soal_ujian.user',
                'tb_jadwal_ujian.start',
                'tb_jadwal_ujian.keterangan',
                'tb_jadwal_ujian.mkKode',
                'tb_jadwal_ujian.finish',
                'tb_jadwal_ujian.kuota',
                'pelajaran.nama'
            ])
            ->leftjoin('tb_soal_ujian', 'tb_jadwal_ujian.id', '=', 'tb_soal_ujian.jadwal_id')
            ->join('tb_dt_ujian', 'tb_dt_ujian.id', '=', 'tb_jadwal_ujian.ujianId')
            ->join('pelajaran', 'pelajaran.kd_ak', '=', 'tb_jadwal_ujian.mkKode')
            ->orderByDesc('tb_jadwal_ujian.id')
            ->paginate(8);

        return view('dashboard.index', compact('data'));
    }
    public function cekToken(Request $req)
    {
        try {
            $data = DB::table('tb_peserta')->where('idp', useridp())->where('token', $req->token);
            if ($data->count() == 0) {
                return response()->json(['status' => 'error', 'msg' => 'Token Tidak Berlaku / sudah Expired']);
            } elseif ($data->first()->expired < now()) {
                return response()->json(['status' => 'error', 'msg' => 'Token Tidak Berlaku / Sudah Expired']);
            } else {
                DB::table('tb_enroll')->updateOrInsert(['idp' => useridp(), 'jadwal_id' => $req->id], [
                    'token' => md5(uniqid(rand(), true)),
                    'created_at' => now(),
                ]);
                $xx = DB::table('tb_enroll')->where('idp', useridp())->where('jadwal_id', $req->id)
                    ->first();
                return response()->json(['status' => 'success', 'msg' => 'Success', 'token' => $xx->token, 'data' => $data->count(), 'jadwal' => $xx->jadwal_id]);
            }


            // return response()->json(['status'=>'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }
    public function soal($data, $jadwal)
    {
        DB::beginTransaction();
        try {
            $xx = DB::table('tb_enroll')->where('token', $data)->where('idp', useridp())
                ->where('jadwal_id', $jadwal)->first();
            $soal = DB::table('tb_soal_ujian')->where('jadwal_id', $jadwal)->first();
            $file = public_path('media/' . $soal->file);
            return response()->download($file);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function uploadSoal(Request $req)
    {
        return view('dashboard.upload');
    }
    public function uplod(Request $req)
    {
        try {
            $valid = \Validator::make($req->all(), [

                'file' => 'required|mimes:pdf|max:1024',

            ]);
            if ($valid->fails()) {
                return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
            }


            $xx = DB::table('tb_enroll')->where('id', $req->id)->where('token', $req->token)
                ->where('idp', useridp())->first();


            // if (file_exists($file = public_path('media/jawaban/' . $xx->file))) {
            //     unlink($file);
            // }
            if (!empty($xx->file)) {
                unlink(public_path('media/jawaban/' . $xx->file));
            }

            if ($req->has('file')) {
                $file = $req->file('file');
                $destinationPath = public_path('media/jawaban/');
                $extension = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                // Valid extensions
                $validextensions = array("pdf");
                if (in_array(strtolower($extension), $validextensions)) {
                    // Rename file
                    $fileName =  rand(11111, 99999) . '.' . $extension;

                    // $xx=\File::get($file);
                    // Storage::disk('ftp')->put($fileName, $xx);
                    $file->move($destinationPath, $fileName);
                    DB::table('tb_enroll')
                        ->where('id', $req->id)
                        ->where('token', $req->token)
                        ->where('idp', useridp())
                        ->update([
                            'file' => $fileName,
                            'updated_at' => now()
                        ]);

                    return response()->json(['status' => 'success', 'msg' => 'Data Berhasil Disimpan']);
                }
            }

            // all good
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }
}
