<?php

namespace App\Http\Controllers;

use App\Mail\BuktiBayar;
use Illuminate\Http\Request;
use DataTables;
use DB;
use Mail;
use Session;

class HistoryController extends Controller
{
    public function index()
    {
        return view('pembayaran.histori.index');
    }
    public function dataList()
    {
        $idp = useridp();
        $data = DB::select("SELECT transaksi_detail.no_kwitansi,_petugas_fo.nama,biaya.kd_pr_akademik,dt_rekening.rekening,(biaya.harga*biaya.qty) as harga,biaya.pot_persen,biaya.pot_harga as Pot_Harga,
                                    ((biaya.harga*biaya.qty) * (biaya.pot_persen/100)) as potongan,
                                    ((biaya.harga*biaya.qty)-((biaya.harga*biaya.qty) * (biaya.pot_persen/100))-biaya.pot_harga) as Biaya,tgl_transaksi,tgl_trans_bank,bank,transaksi_detail.jumlah 
                                    FROM biaya
                                    left join transaksi_detail on biaya.no_biaya=transaksi_detail.no_biaya
                                    left join transaksi on transaksi.no_kwitansi=transaksi_detail.no_kwitansi
                                    left join _petugas_fo on _petugas_fo.idp=transaksi.penerima
                                    left join dt_bank on dt_bank.kd_bank=transaksi.nama_bank
                                    left join dt_rekening on dt_rekening.kd_rekening=biaya.rekening  
                                    where biaya.idp='$idp' order by biaya.kd_pr_akademik desc, dt_rekening.rekening desc");
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])



            ->editColumn('tgl_transaksi', function ($data) {
                return tgl_indo($data->tgl_transaksi);
            })
            ->editColumn('harga', function ($data) {
                return '' . number_format($data->harga);
            })->editColumn('potongan', function ($data) {
                return '' . number_format($data->potongan);
            })->editColumn('jumlah', function ($data) {
                return '' . number_format($data->jumlah);
            })


            ->addColumn('action', function ($data) {
                if (empty($data->no_kwitansi)) {
                    return '';
                } else {
                    return '<a href="' . route('kirim.bukti', ['no_kwitansi' => $data->no_kwitansi]) . '"  data-toggle="modal" data-target="#myModal"  class="mt-1 btn btn-button-3 bs-tooltip btn-dark mb-4 mr-2 rounded-circle" title="kirim Bukti Pembayaran Via Email">
                <i class="flaticon-mailbox"></i></a>';
                }
            })
            ->setTotalRecords(100)
            ->make(true);
    }
    public function rincian()
    {
        $idp = useridp();
        $data = DB::select("SELECT sum(biaya.harga*biaya.qty) as totbiy,
                                        (sum((biaya.harga*biaya.qty) * (biaya.pot_persen/100))+sum(biaya.pot_harga)) as potongan,
                                        (sum((biaya.harga*biaya.qty)-(biaya.harga*biaya.qty) * (biaya.pot_persen/100))-sum(biaya.pot_harga)) as totalbiaya 
                                        FROM biaya where idp='$idp'");
        $deposit = DB::select("SELECT sum(deposit) as deposit 
                                            from deposit where idp='$idp'");
        $semua = DB::select("SELECT transaksi_detail.no_kwitansi,_petugas_fo.nama,biaya.kd_pr_akademik,dt_rekening.rekening,(biaya.harga*biaya.qty) as harga,biaya.pot_persen,biaya.pot_harga as Pot_Harga,
                                    ((biaya.harga*biaya.qty) * (biaya.pot_persen/100)) as potongan,
                                    ((biaya.harga*biaya.qty)-((biaya.harga*biaya.qty) * (biaya.pot_persen/100))-biaya.pot_harga) as Biaya,tgl_transaksi,tgl_trans_bank,bank,transaksi_detail.jumlah 
                                    FROM biaya
                                    left join transaksi_detail on biaya.no_biaya=transaksi_detail.no_biaya
                                    left join transaksi on transaksi.no_kwitansi=transaksi_detail.no_kwitansi
                                    left join _petugas_fo on _petugas_fo.idp=transaksi.penerima
                                    left join dt_bank on dt_bank.kd_bank=transaksi.nama_bank
                                    left join dt_rekening on dt_rekening.kd_rekening=biaya.rekening  
                                    where biaya.idp='$idp' order by biaya.kd_pr_akademik desc, dt_rekening.rekening desc");
        $totalbayar = 0;
        foreach ($semua as $key) {
            $totalbayar += $key->jumlah;
        }
        return view('pembayaran.histori.rincian', compact('data', 'deposit', 'totalbayar'));
    }
    public function sendEmail($data)
    {
        Mail::send('dashboard::mailbox.mail', $data, function ($message) {
            $message->to(request()->penerima)
                ->subject(request()->subjek);
        });
    }

    public function kirim(Request $req)
    {
        $id = request()->no_kwitansi;
        if ($req->isMethod('post')) {
            DB::beginTransaction();
            try {
                $valid = \Validator::make($req->all(), [
                    'email' => 'required',
                ]);
                if ($valid->fails()) {
                    return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
                }
                $d = DB::table('transaksi')->join('_mahasiswa', '_mahasiswa.idp', '=', 'transaksi.idp')
                    ->select([
                        'transaksi.no_kwitansi',
                        '_mahasiswa.nim',
                        '_mahasiswa.nama',
                        '_mahasiswa.jurusan_alias',
                        '_mahasiswa.jurusan',
                        'transaksi.setor',
                        'transaksi.terbilang',

                    ])
                    ->where('transaksi.no_kwitansi', $id)->first();
                $data = [
                    'name' => $d->nama,
                    'nim' => $d->nim,
                    'kwitansi' => $d->no_kwitansi,
                    'jurusan' => $d->jurusan_alias . ' ' . $d->jurusan,
                    'total' => $d->setor,

                ];
                Mail::to(request()->email)
                    ->cc('casudin.ahi@gmail.com')
                    ->send(new BuktiBayar($data));
                DB::commit();

                return response()->json(['status' => 'success', 'msg' => 'Bukti Pembayaran Berhasil Dikirim']);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
            }
        }
        if (empty(request()->no_kwitansi)) {
            return response()->json(['status' => 'error', 'msg' => "Pastikan Yang Anda Pilih terdapat Nomor Kwitansi"]);
        }
        return view('pembayaran.histori.kirim', compact('id'));
    }
}
