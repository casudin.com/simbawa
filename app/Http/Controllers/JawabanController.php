<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;

class JawabanController extends Controller
{
    public function index()
    {
        $ujian=DB::table('tb_dt_ujian')->where('finish', '>', now())->get();
        $pelajaran=DB::table('pelajaran')->get();
        return view('ujian.jawaban.index', compact('ujian', 'pelajaran'));
    }
    public function dataList(Request $req)
    {
        $data=DB::table('tb_jadwal_ujian')
        ->join('tb_dt_ujian', 'tb_dt_ujian.id', '=', 'tb_jadwal_ujian.ujianId')
        ->join('pelajaran', 'pelajaran.kd_ak', '=', 'tb_jadwal_ujian.mkKode')
        ->leftjoin('tb_soal_ujian', 'tb_soal_ujian.jadwal_id', '=', 'tb_jadwal_ujian.id')
        ->leftJoin('tb_enroll', 'tb_enroll.jadwal_id', '=', 'tb_jadwal_ujian.id')
        ->leftjoin('tb_peserta', 'tb_peserta.idp', '=', 'tb_enroll.idp')
        ->select([
            'tb_dt_ujian.id as ujian',
            'tb_dt_ujian.name as nama_ujian',
            'tb_enroll.id',
            'tb_enroll.file as jawaban',
            'tb_enroll.nilai',
            'pelajaran.nama as pelajaran',
            'tb_jadwal_ujian.keterangan',
            'tb_soal_ujian.file as soal',
            'tb_peserta.nama as peserta'
        ]) ;
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])
             ->editColumn('soal', function ($data) {
                 if (empty($data->soal)) {
                     return '';
                 } else {
                     return '<a href="'.route('und.soal', ['dokumen'=>$data->soal]).'">Unduh</a>';
                 }
             })->editColumn('jawaban', function ($data) {
                 if (empty($data->jawaban)) {
                     return '';
                 } else {
                     return '<a href="'.route('und.jawaban', ['dokumen'=>$data->jawaban]).'">Unduh</a>';
                 }
             })

            ->filter(function ($query) use ($req) {
                if ($req->mhs) {
                    $query->where('tb_peserta.nama', 'like', "%{$req->get('mhs')}%");
                }
                if ($req->mk) {
                    $query->where('tb_jadwal_ujian.mkKode', 'like', "%{$req->get('mk')}%");
                }
                if ($req->ujian) {
                    $query->where('tb_dt_ujian.id', 'like', "%{$req->get('ujian')}%");
                }
            })
            
            ->addColumn('action', function ($data) {
                return '<i class="flaticon-delete-fill icon" onclick="hapus(' . "'$data->id'" . ')"></i>';
            })
            ->make(true);
    }
    public function soal()
    {
        $file = public_path('media/'.request()->dokumen);
        return response()->download($file);
    }
    public function jawaban()
    {
        $file = public_path('media/jawaban/'.request()->dokumen);
        return response()->download($file);
    }
}
