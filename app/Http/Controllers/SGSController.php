<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
use Session;

class SGSController extends Controller
{
    public function index()
    {
        return view('sgs.index');
    }
    public function listData()
    {
        $idp = useridp();
        if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role())) {
            $data = DB::select("SELECT id,idpembawa,idygdibawa,nim,nama,jurusan,keterangan from tb_sgs,_mahasiswa where  tb_sgs.idygdibawa=_mahasiswa.idp");
        } else {
            $data = DB::select("SELECT idpembawa,idygdibawa,nim,nama,jurusan,keterangan from tb_sgs,_mahasiswa where  tb_sgs.idygdibawa=_mahasiswa.idp and idpembawa='$idp'");
        }

        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            // ->editColumn('TotalBiaya', function ($data) {
            //     return number_format($data->TotalBiaya);
            // })
            // ->editColumn('potongan', function ($data) {
            //     return number_format($data->potongan);
            // })->editColumn('TotalSetor', function ($data) {
            //     return number_format($data->TotalSetor);
            // })->editColumn('Kekurangan', function ($data) {
            //     return number_format($data->Kekurangan);
            // })



            ->setTotalRecords(100)
            ->make(true);
    }
}
