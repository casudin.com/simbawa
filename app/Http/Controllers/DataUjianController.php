<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;

class DataUjianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ujian.data-ujian.index');
    }
    public function dataList()
    {
        $data = DB::table('tb_dt_ujian');
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            ->editColumn('start', function ($data) {
                return tgl_indo($data->start);
            })->editColumn('finish', function ($data) {
                return tgl_indo($data->finish);
            })
            // ->editColumn('status', function ($data) {
            //     if ($data->status == 1) {
            //         return 'Aktif';
            //     } else {
            //         return 'Suspend';
            //     }
            // })
            ->addColumn('action', function ($data) {
                return '<i class="flaticon-delete-fill icon" onclick="hapus(' . "'$data->id'" . ')"></i>';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ujian.data-ujian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        try {
            $valid = \Validator::make($req->all(), [
                'name' => 'required',
                'start' => 'required',
                'finish' => 'required',

            ]);
            if ($valid->fails()) {
                return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
            }
            DB::table('tb_dt_ujian')->insert([
                'name' => $req->name,
                'start' => $req->start,
                'finish' => $req->finish,
                'created_at' => now(),
                'updated_at' => now()
            ]);
            return response()->json(['status' => 'success', 'msg' => 'Data Berhasil Di Simpan']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tb_dt_ujian')->where('id', $id)->delete();
        return response()->json(['status' => 'success', 'msg' => 'Data Berhasil Di Hapus']);
    }
}
