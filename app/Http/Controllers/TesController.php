<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\User;
use Session;
use App\Mail\BuktiBayar;
use App\Mail\Pembayaran;
use Mail;
use Storage;
class TesController extends Controller
{
    public function index(Request $req)
    {
      //  return  Storage::disk('ftp')->download('2_5f299bac97ef5.png');
        if($req->method()=="POST")
        {
            if($req->hasFile('profile_image')) {
         
                //get filename with extension
                $filenamewithextension = $req->file('profile_image')->getClientOriginalName();
         
                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
         
                //get file extension
                $extension = $req->file('profile_image')->getClientOriginalExtension();
         
                //filename to store
                $filenametostore = $filename.'_'.uniqid().'.'.$extension;
         
                //Upload File to external server
                Storage::disk('ftp')->put($filenametostore, fopen($req->file('profile_image'), 'r+'));
         
                //Store $filenametostore in the database
            }
         
            return redirect('tes')->with('status', "Image uploaded successfully.");
        }else{
            return view('tes');
        }
    }
}
