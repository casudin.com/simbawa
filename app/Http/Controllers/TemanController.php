<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;

class TemanController extends Controller
{
    public function index()
    {
        return view('teman');
    }
    public function sekampung()
    {
        $user = DB::table('_mahasiswa')->where('idp', useridp())->first();
        $data = DB::table('_mahasiswa')->where('tmp_lahir', 'LIKE', '%' . $user->tmp_lahir . '%')->orderBy('nama', 'ASC');
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            ->editColumn('program_alias', function ($data) {
                return $data->program_alias . '-' . $data->jurusan;
            })->editColumn('tmp_lahir', function ($data) {
                return $data->tmp_lahir . ' - ' . $data->alamat;
            })
            // ->editColumn('status', function ($data) {
            //     if ($data->status == 1) {
            //         return 'Aktif';
            //     } else {
            //         return 'Suspend';
            //     }
            // })




            // ->setTotalRecords(100)
            ->make(true);
    }
    public function seangkatan()
    {
        $user = DB::table('_mahasiswa')->where('idp', useridp())->first();
        $data = DB::table('_mahasiswa')->where('angkatan', 'LIKE', '%' . $user->angkatan . '%')->orderBy('nama', 'ASC');
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            ->editColumn('program_alias', function ($data) {
                return $data->program_alias . '-' . $data->jurusan;
            })->editColumn('tmp_lahir', function ($data) {
                return $data->tmp_lahir . ' - ' . $data->alamat;
            })
            // ->editColumn('status', function ($data) {
            //     if ($data->status == 1) {
            //         return 'Aktif';
            //     } else {
            //         return 'Suspend';
            //     }
            // })




            // ->setTotalRecords(100)
            ->make(true);
    }
    public function sejurusan()
    {
        $user = DB::table('_mahasiswa')->where('idp', useridp())->first();
        $data = DB::table('_mahasiswa')->where('jurusan_alias', 'LIKE', '%' . $user->jurusan_alias . '%')->orderBy('nama', 'ASC');
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            ->editColumn('program_alias', function ($data) {
                return $data->program_alias . '-' . $data->jurusan;
            })->editColumn('tmp_lahir', function ($data) {
                return $data->tmp_lahir . ' - ' . $data->alamat;
            })
            // ->editColumn('status', function ($data) {
            //     if ($data->status == 1) {
            //         return 'Aktif';
            //     } else {
            //         return 'Suspend';
            //     }
            // })




            // ->setTotalRecords(100)
            ->make(true);
    }
}
