<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl\Curl;
use App\Providers\RouteServiceProvider;

class ApiController extends Controller
{
    protected $redirectTo = RouteServiceProvider::HOME;
    public function login(Request $req)
    {
        $valid = \Validator::make($req->all(), [
            'password' => 'required',
            'username' => 'required',
            'captcha' => 'required|captcha'
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
        }
        $data = ['username' => request()->username, 'password' => request()->password];
        $user = $this->get_login($data);

        if ($user->status != 'success') {
            return response()->json(['status' => 'error', 'msg' => $user->msg, 'server asnswer' => $user]);
        } else {
            \Session::put('login', true);
            \Session::put('user', $user);

            return response()->json(['status' => 'success']);
        }
    }
    public function get_login($a)
    {
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->post(config('app.api_login'), $a);
        if ($curl->error) {
            return 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
        } else {
            return $curl->response;
        }
    }
}
