<?php

namespace App\Http\Controllers;

use App\Imports\JadwalImport;
use DB;
use DataTables;
use Illuminate\Http\Request;
use Excel;

class JadwalUjianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ujian.jadwal.index');
    }
    public function dataList()
    {
        $data = DB::table('tb_jadwal_ujian')
            ->select([
                'tb_jadwal_ujian.id', 'tb_jadwal_ujian.kuota', 'tb_jadwal_ujian.start', 'tb_jadwal_ujian.finish', 'tb_jadwal_ujian.keterangan',
                'pelajaran.nama', 'pelajaran.kd_ak',
                'tb_dt_ujian.name', 'tb_dt_ujian.start as dt_ujian_start', 'tb_dt_ujian.finish as dt_ujian_finish'
            ])
            ->join('tb_dt_ujian', 'tb_dt_ujian.id', '=', 'tb_jadwal_ujian.ujianId')
            ->join('pelajaran', 'pelajaran.kd_ak', '=', 'tb_jadwal_ujian.mkKode')
            ->orderByDesc('tb_jadwal_ujian.id');
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            ->editColumn('start', function ($data) {
                return tgl_indo($data->start) . jam($data->start);
            })->editColumn('finish', function ($data) {
                return tgl_indo($data->finish) . jam($data->finish);
            })
            // ->editColumn('status', function ($data) {
            //     if ($data->status == 1) {
            //         return 'Aktif';
            //     } else {
            //         return 'Suspend';
            //     }
            // })
            ->addColumn('action', function ($data) {
                return '<button type="button"  onclick="hapus(' . "'$data->id'" . ')" class="btn btn-danger rounded-circle mb-4 mr-2 btn-sm"><i class="flaticon-delete-fill flaticon-circle-p icon"></i></button><a href="' . route('jadwal.edit', [$data->id]) . '" data-toggle="modal" data-target="#myModal"  class="btn btn-warning mb-4 mr-2 rounded-circle btn-sm"><i class="flaticon-edit-3 flaticon-circle-p icon"></i></a>';
                // return '<i class="flaticon-delete-fill icon" onclick="hapus(' . "'$data->id'" . ')"></i>';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pelajaran = DB::table('pelajaran')->get();
        $ujian = DB::table('tb_dt_ujian')->get();
        //->whereDate('start', '<=', now())
        //->whereDate('finish', '>', now())->get();
        return view('ujian.jadwal.create', compact('pelajaran', 'ujian'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        try {
            $valid = \Validator::make($req->all(), [
                'keterangan' => 'required',
                'start' => 'required',
                'finish' => 'required'
            ]);
            if ($valid->fails()) {
                return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
            }
            DB::table('tb_jadwal_ujian')->insert([
                'mkKode' => $req->mk,
                'ujianId' => $req->ujian,
                'kuota' => $req->kuota,
                'start' => $req->start . ' ' . $req->time_start,
                'finish' => $req->finish . ' ' . $req->time_finish,
                'keterangan' => $req->keterangan,
                'created_at' => now(),
                'updated_at' => now()
            ]);
            return response()->json(['status' => 'success', 'msg' => 'Data Berhasil Disimpan']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('tb_jadwal_ujian')->where('id', $id)->first();
        $pelajaran = DB::table('pelajaran')->get();
        $ujian = DB::table('tb_dt_ujian')->get();
        //->whereDate('start', '<=', now())
        //->whereDate('finish', '>', now())->get();
        return view('ujian.jadwal.edit', compact('pelajaran', 'ujian', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        try {
            $valid = \Validator::make($req->all(), [
                'keterangan' => 'required',
                'start' => 'required',
                'finish' => 'required'
            ]);
            if ($valid->fails()) {
                return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
            }
            DB::table('tb_jadwal_ujian')->where('id', $id)->update([
                'mkKode' => $req->mk,
                'ujianId' => $req->ujian,
                'kuota' => $req->kuota,
                'start' => $req->start . ' ' . $req->time_start,
                'finish' => $req->finish . ' ' . $req->time_finish,
                'keterangan' => $req->keterangan,
                'created_at' => now(),
                'updated_at' => now()
            ]);
            return response()->json(['status' => 'success', 'msg' => 'Data Berhasil Disimpan']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus()
    {
        $data = DB::table('tb_jadwal_ujian')->get();
        foreach ($data as $key) {
            $soal = DB::table('tb_soal_ujian')->whereIn('jadwal_id', [$key->id])->get();
            foreach ($soal as $val) {
                if (file_exists($file = public_path('media/' . $val->file))) {
                    unlink($file);
                    DB::table('tb_soal_ujian')->whereIn('jadwal_id', [$key->id])->delete();
                }
            }
        }
        DB::table('tb_jadwal_ujian')->delete();
        return response()->json(['status' => 'success', 'msg' => "Data Berhasil Di Kosongkan"]);
    }
    public function destroy($id)
    {
        $x = DB::table('tb_jadwal_ujian')->where('id', $id)->first();
        $soal = DB::table('tb_soal_ujian')->where('jadwal_id', $x->id)->first();
        if (file_exists($file = public_path('media/' . $soal->file))) {
            unlink($file);
            DB::table('tb_soal_ujian')->where('jadwal_id', $x->id)->delete();
        }

        DB::table('tb_jadwal_ujian')->where('id', $id)->delete();
        return response()->json(['status' => 'success', 'msg' => 'Data Berhasil Disimpan']);
    }
    public function template()
    {
        $file = public_path('upload_jadwal.xlsx');
        return response()->download($file);
    }
    public function upload(Request $req)
    {
        if ($req->isMethod('post')) {
            try {
                $validator = \Validator::make($req->all(), [
                    'file' => 'required|mimes:xls,xlsx,csv,ods',
                    'ujian' => 'required'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status' => 'error',  'msg' => $validator->messages()->first()]);
                }
                if ($req->hasFile('file')) {
                    $file = $req->file('file'); //GET FILE
                    $import = new JadwalImport($req->ujian);
                    Excel::import($import, $file);
                    $errors = $import->getError();
                    if (count($errors) > 0) {
                        DB::rollback();
                        return response()->json(['status' => 'error', 'msg' => json_encode($errors) .  ' :( ']);
                    }
                    return response()->json(['status' => 'success', 'msg' => "Data Berhasil Di Import"]);
                }
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
            }
        }
        $ujian = DB::table('tb_dt_ujian')->get();
        return view('ujian.jadwal.upload', compact('ujian'));
    }
}
