<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl\Curl;

use Validator;
use DataTables;

class PenggunaController extends Controller
{
    function index()
    {

        return view('pengguna.index');
    }
    function listData()
    {
        $data = $this->mahasiswa();
        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])

            ->editColumn('program_alias', function ($data) {
                return $data->program_alias . '-' . $data->jurusan;
            })
            // ->editColumn('status', function ($data) {
            //     if ($data->status == 1) {
            //         return 'Aktif';
            //     } else {
            //         return 'Suspend';
            //     }
            // })

            // ->editColumn('name', function ($data) {
            //     return $data->title . " " . $data->name . " (" . umur($data->ttl) . "th)";
            // })
            // ->editColumn('ttl', function ($data) {
            //     if (!empty($data->ttl)) {
            //         return tgl($data->ttl);
            //     } else {
            //         return "-";
            //     }
            // })
            // ->editColumn('created_at', function ($data) {
            //     if (!empty($data->created_at)) {
            //         return tgl($data->created_at);
            //     } else {
            //         return "-";
            //     }
            // })
            // ->filter(function ($query) use ($req) {
            //     if ($req->has('nama')) {
            //         $query->where('pasien.name', 'like', "%{$req->get('nama')}%");
            //     }

            //     if ($req->has('rm')) {
            //         $query->where('pasien.rm', 'like', "%" .  $req->get('rm') . "%");
            //     }
            // })


            ->setTotalRecords(100)
            ->make(true);
    }
    function mahasiswa()
    {
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->post(config('app.api_mhs'), ['token' => '123']);
        if ($curl->error) {
            return 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
        } else {
            return $curl->response->result;
        }
    }
}
