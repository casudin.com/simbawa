<?php

namespace App\Http\Middleware;

use Closure;

class Petugas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role())) {
            return $next($request);
        } else {
            return redirect()->guest('/login');
            // abort(401);
        }
    }
}
