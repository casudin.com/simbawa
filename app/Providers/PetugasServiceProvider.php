<?php

namespace App\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;

class PetugasServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('petugas', function ($expression) {
            if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role())) {
                return $expression;
            } else {
                abort(401);
            }
        });
    }
}
