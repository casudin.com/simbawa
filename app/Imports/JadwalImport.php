<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithChunkReading; //IMPORT CHUNK READING
use Illuminate\Contracts\Queue\ShouldQueue; //IMPORT SHOUDLQUEUE

class JadwalImport implements WithHeadingRow, ToCollection, WithChunkReading
{
    /**
     * @param Collection $collection
     */
    private $error = [];
    public function __construct($ujian)
    {
        $this->ujian = $ujian;
    }
    public function collection(Collection $rows)
    {
        $i = 1;
        foreach ($rows as $key) {
            $mk = DB::table('pelajaran')->where('nama', $key['matakuliah']);
            if ($mk->count() == 0) {
                $text = 'Baris ke ' .  $i . ' dengan Matakuliah :  ' . $key['matakuliah'] . " : Tidak Tersedia di Tabel Pelajaran";
                array_push($this->error, $text);
            } else {
                DB::table('tb_jadwal_ujian')->insert([
                    'mkKode' => $mk->first()->kd_ak,
                    'ujianId' => $this->ujian,
                    'kuota' => $key['kuota'],
                    'start' => $key['tanggal_mulai'] . ' ' . $key['jam_mulai'],
                    'finish' => $key['tanggal_akhir'] . ' ' . $key['jam_akhir'],
                    'keterangan' => $key['keterangan'],
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }

            $i++;
        }
    }
    public function chunkSize(): int
    {
        return 3000; //ANGKA TERSEBUT PERTANDA JUMLAH BARIS YANG AKAN DIEKSEKUSI
    }
    public function getError(): array
    {
        return $this->error;
    }
}
