<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithChunkReading; //IMPORT CHUNK READING
use Illuminate\Contracts\Queue\ShouldQueue; //IMPORT SHOUDLQUEUE

class PesrtaImport implements WithHeadingRow, ToCollection, WithChunkReading
{
    /**
     * @param Collection $collection
     */
    private $error = [];
    public function collection(Collection $rows)
    {
        $i = 1;
        foreach ($rows as $key) {
            $mhs = DB::table('_mahasiswa')->where('nim', $key['nim']);
            if ($mhs->count() == 0) {
                $text = 'Baris ke ' .  $i . ' dengan NIM :  ' . $key['nim'] . " : not found";
                array_push($this->error, $text);
            } else {
                DB::table('tb_peserta')->updateOrInsert(['nim' => $key['nim']], [
                    'nama' => $key['nama'],
                    'idp' => $mhs->first()->idp,
                    'expired' => $key['token_expired'],
                    'token' => rand(11111, 99999),
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }

            $i++;
        }
    }
    public function chunkSize(): int
    {
        return 3000; //ANGKA TERSEBUT PERTANDA JUMLAH BARIS YANG AKAN DIEKSEKUSI
    }
    public function getError(): array
    {
        return $this->error;
    }
}
