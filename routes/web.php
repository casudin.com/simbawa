<?php

//use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('cekLogin', 'Auth\LoginController@cek')->name('cek.login');
Route::get('empty/email', function () {
    return cekEmail();
})->name('cekId');
Route::post('api/users/update/email', function () {
    DB::table('data_pribadi')->where('idp', useridp())->update(['email1' => request()->email]);
    DB::connection('loker')->table('data_pribadi')->where('idp', useridp())->update(['email1' => request()->email]);
    return response()->json(['status' => "success", 'data' => request()->email], 200);
})->name('api.update.email');
Route::post('api/login', 'ApiController@login')->name('sso.login');
Route::get('aja/logout', function () {
    session()->forget(['login', 'user']);
    return redirect()->route('login');
})->name('logout.aja');
Route::get('/tes', 'TesController@index');
Route::post('/tes', 'TesController@index');
Route::livewire('/users', 'user.index')->name('users.index');
Auth::routes(['register' => false]);
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function () {
    Route::resource('user', 'UsersController');
    Route::post('user/api', 'UsersController@list')->name('user.api');
    Route::group(['prefix' => 'acl'], function () {
        Route::get('/', 'RoleController@index')
            ->name('access');
        Route::resource('role', 'RoleController');
        Route::get('show/permission/role', 'RoleController@rolePermission')->name('role.permisiion');
        Route::post('show/permission/role/set', 'RoleController@setRolePermission')->name('role.permisiion.set');
        Route::post('api/role', 'RoleController@roleList')->name('api.role');
        Route::resource('permission', 'PermissionController');
        Route::post('api/permission', 'PermissionController@permissionList')->name('api.permission');
    });
});
Route::get('lang/{language}', 'LocalizationController@switch')->name('localization.switch');
// Route::group(['middleware' => ['Login'], 'prefix' => 'admin'], function () {
// });
Route::group(['middleware' => ['Login'], 'prefix' => 'layanan'], function () {
    Route::resource('pembayaran', 'PembayaranController');
    Route::get('full/pembayaran', 'PembayaranController@full')->name('full');
    Route::get('donlot/bukti/', 'PembayaranController@donlot')->name('donlot.bukti');
    Route::get('proses/bayar/{id}', 'PembayaranController@proses')->name('proses.bayar');
    Route::post('proses/bayar/{id}', 'PembayaranController@proses')->name('proses.bayar');
    Route::post('api/pembayaran', 'PembayaranController@listData')->name('api.pembayaran');
    Route::post('api/tagihan/pembayaran', 'PembayaranController@tagihan')->name('api.tagihan.pembayaran');
    Route::get('tagihan/pembayaran', 'PembayaranController@tagihanKu')->name('tagihan.tagihanKu');
    Route::get('detil/biaya', 'PembayaranController@detilBiaya')->name('detil.tagihanKu');
    Route::post('hapus/pembayaran', 'PembayaranController@hapus')->name('hapus.pembayaran');
    Route::post('api/histori', 'HistoryController@dataList')->name('api.histori');
    Route::get('histori', 'HistoryController@index')->name('histori.pembayaran');
    Route::get('kirim/bukti', 'HistoryController@kirim')->name('kirim.bukti');
    Route::post('kirim/bukti', 'HistoryController@kirim')->name('kirim.bukti');
    Route::get('rincian/biaya', 'HistoryController@rincian')->name('rincian.user');
    Route::get('sgs', 'SGSController@index')->name('sgs');
    Route::post('sgs', 'SGSController@listData')->name('sgs.api');
    Route::get('teman', 'TemanController@index')->name('teman');
    Route::post('api/teman/sekampung', 'TemanController@sekampung')->name('teman.sekampung');
    Route::post('api/teman/seangkatan', 'TemanController@seangkatan')->name('teman.seangkatan');
    Route::post('api/teman/sejurusan', 'TemanController@sejurusan')->name('teman.sejurusan');
});
Route::group(['middleware' => ['Petugas'], 'prefix' => 'sistem'], function () {
    Route::resource('pengguna', 'PenggunaController');
    Route::post('api/pengguna', 'PenggunaController@listData')->name('api.pengguna');
    Route::get('rekap-pembayaran', 'PembayaranMhsController@index')->name('rekap.pembayaran');
    Route::get('/rekap/detil/{nim}', 'PembayaranMhsController@detail')->name('detil.pembayaran');
    Route::post('api/rekap-pembayaran', 'PembayaranMhsController@dataList')->name('api.rekap.pembayaran');
});
Route::get('reload/capta', function () {
    return response()->json(['captcha' => captcha_img()]);
})->name('reload.ca');
Route::group(['middleware' => ['Petugas'], 'prefix' => 'ujian'], function () {
    Route::resource('peserta', 'PesertaController');
    Route::get('export/peserta', 'PesertaController@export')->name('export.peserta');
    Route::post('api/peserta', 'PesertaController@dataList')->name('api.peserta');
    Route::get('template/peserta', 'PesertaController@template')->name('template');
    Route::get('template/upload', 'PesertaController@upload')->name('template.upload');
    Route::post('template/upload', 'PesertaController@upload')->name('template.upload');
    Route::post('kosongkan/peserta', 'PesertaController@pesertaDelete')->name('destoyed.peserta');
    Route::get('mahasiswa', function (Request $req) {
        if (!empty($req->has('q'))) {
            $cari = $req->q;
            $data = DB::table('_mahasiswa')->where('nama', 'LIKE', '%' . $cari . '%')->limit(100)
                ->orderBy('nama', 'ASC')->get();
            return response()->json($data);
        } else {
            $data = DB::table('_mahasiswa')->orderBy('nama', 'ASC')->limit(100)->get();
            return response()->json($data);
        }
    })->name('all.mhs');
    Route::get('mhs/info', function (Request $req) {
        $data = DB::table('_mahasiswa')->where('nim', request()->nim)->first();
        return response()->json(['nim' => $data->nim, 'idp' => $data->idp, 'nama' => $data->nama]);
    })->name('mhs.info');
    Route::resource('data-ujian', 'DataUjianController');
    Route::post('api/list/ujian', 'DataUjianController@dataList')->name('list.ujian');
    Route::resource('jadwal', 'JadwalUjianController');
    Route::post('api/list/jadwal-ujian', 'JadwalUjianController@dataList')->name('jadwal.list.ujian');
    Route::get('template/jadwal', 'JadwalUjianController@template')->name('template.jadwal');
    Route::post('template/jadwal/upload', 'JadwalUjianController@upload')->name('template.ujian.upload');
    Route::get('template/jadwal/upload', 'JadwalUjianController@upload')->name('template.ujian.upload');
    Route::post('kosongkan/jadwal/ujian', 'JadwalUjianController@hapus')->name('destoyed.jadwal');
    Route::resource('soal', 'SoalUjianController');
    Route::post('api/soal', 'SoalUjianController@dataList')->name('list.soal');
    Route::get('jawaban-mhs', 'JawabanController@index')->name('jawaban.mhs');
    Route::post('api/jawaban-mhs', 'JawabanController@dataList')->name('list.jawaban.mhs');
    Route::get('data/soal', 'JawabanController@soal')->name('und.soal');
    Route::get('data/jwb', 'JawabanController@jawaban')->name('und.jawaban');
});
// Route::group(['middleware' => ['Login'], 'prefix' => 'dashboard'], function () {
//     Route::get('ujian', 'SoalMhsController@index')->name('soal.mhs');
//     Route::post('token/ujian', 'SoalMhsController@cekToken')->name('cekToken.soal.mhs');
//     Route::get('soal/ujian/{token}/{jadwal}', 'SoalMhsController@soal')->name('soal.untuk.siswa');
//     Route::post('upload/file/ujian/', 'SoalMhsController@uplod')->name('soal.file.siswa');
//     Route::get('upload/file/ujian', 'SoalMhsController@uploadSoal')->name('soal.xx.siswa');
// });
