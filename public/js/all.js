/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/jquery/dist/jquery.js":
/*!********************************************!*\
  !*** ./node_modules/jquery/dist/jquery.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * jQuery JavaScript Library v3.5.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2020-05-04T22:49Z
 */
( function( global, factory ) {

	"use strict";

	if (  true && typeof module.exports === "object" ) {

		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
// throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
// arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
// enough that all such attempts are guarded in a try block.
"use strict";

var arr = [];

var getProto = Object.getPrototypeOf;

var slice = arr.slice;

var flat = arr.flat ? function( array ) {
	return arr.flat.call( array );
} : function( array ) {
	return arr.concat.apply( [], array );
};


var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var fnToString = hasOwn.toString;

var ObjectFunctionString = fnToString.call( Object );

var support = {};

var isFunction = function isFunction( obj ) {

      // Support: Chrome <=57, Firefox <=52
      // In some browsers, typeof returns "function" for HTML <object> elements
      // (i.e., `typeof document.createElement( "object" ) === "function"`).
      // We don't want to classify *any* DOM node as a function.
      return typeof obj === "function" && typeof obj.nodeType !== "number";
  };


var isWindow = function isWindow( obj ) {
		return obj != null && obj === obj.window;
	};


var document = window.document;



	var preservedScriptAttributes = {
		type: true,
		src: true,
		nonce: true,
		noModule: true
	};

	function DOMEval( code, node, doc ) {
		doc = doc || document;

		var i, val,
			script = doc.createElement( "script" );

		script.text = code;
		if ( node ) {
			for ( i in preservedScriptAttributes ) {

				// Support: Firefox 64+, Edge 18+
				// Some browsers don't support the "nonce" property on scripts.
				// On the other hand, just using `getAttribute` is not enough as
				// the `nonce` attribute is reset to an empty string whenever it
				// becomes browsing-context connected.
				// See https://github.com/whatwg/html/issues/2369
				// See https://html.spec.whatwg.org/#nonce-attributes
				// The `node.getAttribute` check was added for the sake of
				// `jQuery.globalEval` so that it can fake a nonce-containing node
				// via an object.
				val = node[ i ] || node.getAttribute && node.getAttribute( i );
				if ( val ) {
					script.setAttribute( i, val );
				}
			}
		}
		doc.head.appendChild( script ).parentNode.removeChild( script );
	}


function toType( obj ) {
	if ( obj == null ) {
		return obj + "";
	}

	// Support: Android <=2.3 only (functionish RegExp)
	return typeof obj === "object" || typeof obj === "function" ?
		class2type[ toString.call( obj ) ] || "object" :
		typeof obj;
}
/* global Symbol */
// Defining this global in .eslintrc.json would create a danger of using the global
// unguarded in another place, it seems safer to define global only for this module



var
	version = "3.5.1",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {

		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	};

jQuery.fn = jQuery.prototype = {

	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {

		// Return all the elements in a clean array
		if ( num == null ) {
			return slice.call( this );
		}

		// Return just the one element from the set
		return num < 0 ? this[ num + this.length ] : this[ num ];
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	each: function( callback ) {
		return jQuery.each( this, callback );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map( this, function( elem, i ) {
			return callback.call( elem, i, elem );
		} ) );
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	even: function() {
		return this.pushStack( jQuery.grep( this, function( _elem, i ) {
			return ( i + 1 ) % 2;
		} ) );
	},

	odd: function() {
		return this.pushStack( jQuery.grep( this, function( _elem, i ) {
			return i % 2;
		} ) );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor();
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[ 0 ] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !isFunction( target ) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {

		// Only deal with non-null/undefined values
		if ( ( options = arguments[ i ] ) != null ) {

			// Extend the base object
			for ( name in options ) {
				copy = options[ name ];

				// Prevent Object.prototype pollution
				// Prevent never-ending loop
				if ( name === "__proto__" || target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
					( copyIsArray = Array.isArray( copy ) ) ) ) {
					src = target[ name ];

					// Ensure proper type for the source value
					if ( copyIsArray && !Array.isArray( src ) ) {
						clone = [];
					} else if ( !copyIsArray && !jQuery.isPlainObject( src ) ) {
						clone = {};
					} else {
						clone = src;
					}
					copyIsArray = false;

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend( {

	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isPlainObject: function( obj ) {
		var proto, Ctor;

		// Detect obvious negatives
		// Use toString instead of jQuery.type to catch host objects
		if ( !obj || toString.call( obj ) !== "[object Object]" ) {
			return false;
		}

		proto = getProto( obj );

		// Objects with no prototype (e.g., `Object.create( null )`) are plain
		if ( !proto ) {
			return true;
		}

		// Objects with prototype are plain iff they were constructed by a global Object function
		Ctor = hasOwn.call( proto, "constructor" ) && proto.constructor;
		return typeof Ctor === "function" && fnToString.call( Ctor ) === ObjectFunctionString;
	},

	isEmptyObject: function( obj ) {
		var name;

		for ( name in obj ) {
			return false;
		}
		return true;
	},

	// Evaluates a script in a provided context; falls back to the global one
	// if not specified.
	globalEval: function( code, options, doc ) {
		DOMEval( code, { nonce: options && options.nonce }, doc );
	},

	each: function( obj, callback ) {
		var length, i = 0;

		if ( isArrayLike( obj ) ) {
			length = obj.length;
			for ( ; i < length; i++ ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else {
			for ( i in obj ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		}

		return obj;
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArrayLike( Object( arr ) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	// Support: Android <=4.0 only, PhantomJS 1 only
	// push.apply(_, arraylike) throws on ancient WebKit
	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var length, value,
			i = 0,
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArrayLike( elems ) ) {
			length = elems.length;
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return flat( ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
} );

if ( typeof Symbol === "function" ) {
	jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( _i, name ) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

	// Support: real iOS 8.2 only (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = !!obj && "length" in obj && obj.length,
		type = toType( obj );

	if ( isFunction( obj ) || isWindow( obj ) ) {
		return false;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.3.5
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://js.foundation/
 *
 * Date: 2020-03-14
 */
( function( window ) {
var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	nonnativeSelectorCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// Instance methods
	hasOwn = ( {} ).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	pushNative = arr.push,
	push = arr.push,
	slice = arr.slice,

	// Use a stripped-down indexOf as it's faster than native
	// https://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[ i ] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|" +
		"ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",

	// https://www.w3.org/TR/css-syntax-3/#ident-token-diagram
	identifier = "(?:\\\\[\\da-fA-F]{1,6}" + whitespace +
		"?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +

		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +

		// "Attribute values must be CSS identifiers [capture 5]
		// or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" +
		whitespace + "*\\]",

	pseudos = ":(" + identifier + ")(?:\\((" +

		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +

		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +

		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" +
		whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace +
		"*" ),
	rdescend = new RegExp( whitespace + "|>" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + identifier + ")" ),
		"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
		"TAG": new RegExp( "^(" + identifier + "|[*])" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" +
			whitespace + "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" +
			whitespace + "*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),

		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace +
			"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + whitespace +
			"*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rhtml = /HTML$/i,
	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,

	// CSS escapes
	// http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\[\\da-fA-F]{1,6}" + whitespace + "?|\\\\([^\\r\\n\\f])", "g" ),
	funescape = function( escape, nonHex ) {
		var high = "0x" + escape.slice( 1 ) - 0x10000;

		return nonHex ?

			// Strip the backslash prefix from a non-hex escape sequence
			nonHex :

			// Replace a hexadecimal escape sequence with the encoded Unicode code point
			// Support: IE <=11+
			// For values outside the Basic Multilingual Plane (BMP), manually construct a
			// surrogate pair
			high < 0 ?
				String.fromCharCode( high + 0x10000 ) :
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// CSS string/identifier serialization
	// https://drafts.csswg.org/cssom/#common-serializing-idioms
	rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
	fcssescape = function( ch, asCodePoint ) {
		if ( asCodePoint ) {

			// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
			if ( ch === "\0" ) {
				return "\uFFFD";
			}

			// Control characters and (dependent upon position) numbers get escaped as code points
			return ch.slice( 0, -1 ) + "\\" +
				ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
		}

		// Other potentially-special ASCII characters get backslash-escaped
		return "\\" + ch;
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	},

	inDisabledFieldset = addCombinator(
		function( elem ) {
			return elem.disabled === true && elem.nodeName.toLowerCase() === "fieldset";
		},
		{ dir: "parentNode", next: "legend" }
	);

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		( arr = slice.call( preferredDoc.childNodes ) ),
		preferredDoc.childNodes
	);

	// Support: Android<4.0
	// Detect silently failing push.apply
	// eslint-disable-next-line no-unused-expressions
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			pushNative.apply( target, slice.call( els ) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;

			// Can't trust NodeList.length
			while ( ( target[ j++ ] = els[ i++ ] ) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var m, i, elem, nid, match, groups, newSelector,
		newContext = context && context.ownerDocument,

		// nodeType defaults to 9, since context defaults to document
		nodeType = context ? context.nodeType : 9;

	results = results || [];

	// Return early from calls with invalid selector or context
	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	// Try to shortcut find operations (as opposed to filters) in HTML documents
	if ( !seed ) {
		setDocument( context );
		context = context || document;

		if ( documentIsHTML ) {

			// If the selector is sufficiently simple, try using a "get*By*" DOM method
			// (excepting DocumentFragment context, where the methods don't exist)
			if ( nodeType !== 11 && ( match = rquickExpr.exec( selector ) ) ) {

				// ID selector
				if ( ( m = match[ 1 ] ) ) {

					// Document context
					if ( nodeType === 9 ) {
						if ( ( elem = context.getElementById( m ) ) ) {

							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}

					// Element context
					} else {

						// Support: IE, Opera, Webkit
						// TODO: identify versions
						// getElementById can match elements by name instead of ID
						if ( newContext && ( elem = newContext.getElementById( m ) ) &&
							contains( context, elem ) &&
							elem.id === m ) {

							results.push( elem );
							return results;
						}
					}

				// Type selector
				} else if ( match[ 2 ] ) {
					push.apply( results, context.getElementsByTagName( selector ) );
					return results;

				// Class selector
				} else if ( ( m = match[ 3 ] ) && support.getElementsByClassName &&
					context.getElementsByClassName ) {

					push.apply( results, context.getElementsByClassName( m ) );
					return results;
				}
			}

			// Take advantage of querySelectorAll
			if ( support.qsa &&
				!nonnativeSelectorCache[ selector + " " ] &&
				( !rbuggyQSA || !rbuggyQSA.test( selector ) ) &&

				// Support: IE 8 only
				// Exclude object elements
				( nodeType !== 1 || context.nodeName.toLowerCase() !== "object" ) ) {

				newSelector = selector;
				newContext = context;

				// qSA considers elements outside a scoping root when evaluating child or
				// descendant combinators, which is not what we want.
				// In such cases, we work around the behavior by prefixing every selector in the
				// list with an ID selector referencing the scope context.
				// The technique has to be used as well when a leading combinator is used
				// as such selectors are not recognized by querySelectorAll.
				// Thanks to Andrew Dupont for this technique.
				if ( nodeType === 1 &&
					( rdescend.test( selector ) || rcombinators.test( selector ) ) ) {

					// Expand context for sibling selectors
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
						context;

					// We can use :scope instead of the ID hack if the browser
					// supports it & if we're not changing the context.
					if ( newContext !== context || !support.scope ) {

						// Capture the context ID, setting it first if necessary
						if ( ( nid = context.getAttribute( "id" ) ) ) {
							nid = nid.replace( rcssescape, fcssescape );
						} else {
							context.setAttribute( "id", ( nid = expando ) );
						}
					}

					// Prefix every selector in the list
					groups = tokenize( selector );
					i = groups.length;
					while ( i-- ) {
						groups[ i ] = ( nid ? "#" + nid : ":scope" ) + " " +
							toSelector( groups[ i ] );
					}
					newSelector = groups.join( "," );
				}

				try {
					push.apply( results,
						newContext.querySelectorAll( newSelector )
					);
					return results;
				} catch ( qsaError ) {
					nonnativeSelectorCache( selector, true );
				} finally {
					if ( nid === expando ) {
						context.removeAttribute( "id" );
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {

		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {

			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return ( cache[ key + " " ] = value );
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created element and returns a boolean result
 */
function assert( fn ) {
	var el = document.createElement( "fieldset" );

	try {
		return !!fn( el );
	} catch ( e ) {
		return false;
	} finally {

		// Remove from its parent by default
		if ( el.parentNode ) {
			el.parentNode.removeChild( el );
		}

		// release memory in IE
		el = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split( "|" ),
		i = arr.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[ i ] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			a.sourceIndex - b.sourceIndex;

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( ( cur = cur.nextSibling ) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return ( name === "input" || name === "button" ) && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for :enabled/:disabled
 * @param {Boolean} disabled true for :disabled; false for :enabled
 */
function createDisabledPseudo( disabled ) {

	// Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
	return function( elem ) {

		// Only certain elements can match :enabled or :disabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
		if ( "form" in elem ) {

			// Check for inherited disabledness on relevant non-disabled elements:
			// * listed form-associated elements in a disabled fieldset
			//   https://html.spec.whatwg.org/multipage/forms.html#category-listed
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
			// * option elements in a disabled optgroup
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
			// All such elements have a "form" property.
			if ( elem.parentNode && elem.disabled === false ) {

				// Option elements defer to a parent optgroup if present
				if ( "label" in elem ) {
					if ( "label" in elem.parentNode ) {
						return elem.parentNode.disabled === disabled;
					} else {
						return elem.disabled === disabled;
					}
				}

				// Support: IE 6 - 11
				// Use the isDisabled shortcut property to check for disabled fieldset ancestors
				return elem.isDisabled === disabled ||

					// Where there is no isDisabled, check manually
					/* jshint -W018 */
					elem.isDisabled !== !disabled &&
					inDisabledFieldset( elem ) === disabled;
			}

			return elem.disabled === disabled;

		// Try to winnow out elements that can't be disabled before trusting the disabled property.
		// Some victims get caught in our net (label, legend, menu, track), but it shouldn't
		// even exist on them, let alone have a boolean value.
		} else if ( "label" in elem ) {
			return elem.disabled === disabled;
		}

		// Remaining elements are neither :enabled nor :disabled
		return false;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction( function( argument ) {
		argument = +argument;
		return markFunction( function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ ( j = matchIndexes[ i ] ) ] ) {
					seed[ j ] = !( matches[ j ] = seed[ j ] );
				}
			}
		} );
	} );
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	var namespace = elem.namespaceURI,
		docElem = ( elem.ownerDocument || elem ).documentElement;

	// Support: IE <=8
	// Assume HTML when documentElement doesn't yet exist, such as inside loading iframes
	// https://bugs.jquery.com/ticket/4833
	return !rhtml.test( namespace || docElem && docElem.nodeName || "HTML" );
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, subWindow,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// Return early if doc is invalid or already selected
	// Support: IE 11+, Edge 17 - 18+
	// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
	// two documents; shallow comparisons work.
	// eslint-disable-next-line eqeqeq
	if ( doc == document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Update global variables
	document = doc;
	docElem = document.documentElement;
	documentIsHTML = !isXML( document );

	// Support: IE 9 - 11+, Edge 12 - 18+
	// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
	// Support: IE 11+, Edge 17 - 18+
	// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
	// two documents; shallow comparisons work.
	// eslint-disable-next-line eqeqeq
	if ( preferredDoc != document &&
		( subWindow = document.defaultView ) && subWindow.top !== subWindow ) {

		// Support: IE 11, Edge
		if ( subWindow.addEventListener ) {
			subWindow.addEventListener( "unload", unloadHandler, false );

		// Support: IE 9 - 10 only
		} else if ( subWindow.attachEvent ) {
			subWindow.attachEvent( "onunload", unloadHandler );
		}
	}

	// Support: IE 8 - 11+, Edge 12 - 18+, Chrome <=16 - 25 only, Firefox <=3.6 - 31 only,
	// Safari 4 - 5 only, Opera <=11.6 - 12.x only
	// IE/Edge & older browsers don't support the :scope pseudo-class.
	// Support: Safari 6.0 only
	// Safari 6.0 supports :scope but it's an alias of :root there.
	support.scope = assert( function( el ) {
		docElem.appendChild( el ).appendChild( document.createElement( "div" ) );
		return typeof el.querySelectorAll !== "undefined" &&
			!el.querySelectorAll( ":scope fieldset div" ).length;
	} );

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert( function( el ) {
		el.className = "i";
		return !el.getAttribute( "className" );
	} );

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert( function( el ) {
		el.appendChild( document.createComment( "" ) );
		return !el.getElementsByTagName( "*" ).length;
	} );

	// Support: IE<9
	support.getElementsByClassName = rnative.test( document.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programmatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert( function( el ) {
		docElem.appendChild( el ).id = expando;
		return !document.getElementsByName || !document.getElementsByName( expando ).length;
	} );

	// ID filter and find
	if ( support.getById ) {
		Expr.filter[ "ID" ] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute( "id" ) === attrId;
			};
		};
		Expr.find[ "ID" ] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var elem = context.getElementById( id );
				return elem ? [ elem ] : [];
			}
		};
	} else {
		Expr.filter[ "ID" ] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" &&
					elem.getAttributeNode( "id" );
				return node && node.value === attrId;
			};
		};

		// Support: IE 6 - 7 only
		// getElementById is not reliable as a find shortcut
		Expr.find[ "ID" ] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var node, i, elems,
					elem = context.getElementById( id );

				if ( elem ) {

					// Verify the id attribute
					node = elem.getAttributeNode( "id" );
					if ( node && node.value === id ) {
						return [ elem ];
					}

					// Fall back on getElementsByName
					elems = context.getElementsByName( id );
					i = 0;
					while ( ( elem = elems[ i++ ] ) ) {
						node = elem.getAttributeNode( "id" );
						if ( node && node.value === id ) {
							return [ elem ];
						}
					}
				}

				return [];
			}
		};
	}

	// Tag
	Expr.find[ "TAG" ] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,

				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( ( elem = results[ i++ ] ) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find[ "CLASS" ] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See https://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( ( support.qsa = rnative.test( document.querySelectorAll ) ) ) {

		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert( function( el ) {

			var input;

			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// https://bugs.jquery.com/ticket/12359
			docElem.appendChild( el ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\r\\' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( el.querySelectorAll( "[msallowcapture^='']" ).length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !el.querySelectorAll( "[selected]" ).length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
			if ( !el.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push( "~=" );
			}

			// Support: IE 11+, Edge 15 - 18+
			// IE 11/Edge don't find elements on a `[name='']` query in some cases.
			// Adding a temporary attribute to the document before the selection works
			// around the issue.
			// Interestingly, IE 10 & older don't seem to have the issue.
			input = document.createElement( "input" );
			input.setAttribute( "name", "" );
			el.appendChild( input );
			if ( !el.querySelectorAll( "[name='']" ).length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*name" + whitespace + "*=" +
					whitespace + "*(?:''|\"\")" );
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !el.querySelectorAll( ":checked" ).length ) {
				rbuggyQSA.push( ":checked" );
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibling-combinator selector` fails
			if ( !el.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push( ".#.+[+~]" );
			}

			// Support: Firefox <=3.6 - 5 only
			// Old Firefox doesn't throw on a badly-escaped identifier.
			el.querySelectorAll( "\\\f" );
			rbuggyQSA.push( "[\\r\\n\\f]" );
		} );

		assert( function( el ) {
			el.innerHTML = "<a href='' disabled='disabled'></a>" +
				"<select disabled='disabled'><option/></select>";

			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = document.createElement( "input" );
			input.setAttribute( "type", "hidden" );
			el.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( el.querySelectorAll( "[name=d]" ).length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( el.querySelectorAll( ":enabled" ).length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Support: IE9-11+
			// IE's :disabled selector does not pick up the children of disabled fieldsets
			docElem.appendChild( el ).disabled = true;
			if ( el.querySelectorAll( ":disabled" ).length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Support: Opera 10 - 11 only
			// Opera 10-11 does not throw on post-comma invalid pseudos
			el.querySelectorAll( "*,:x" );
			rbuggyQSA.push( ",.*:" );
		} );
	}

	if ( ( support.matchesSelector = rnative.test( ( matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector ) ) ) ) {

		assert( function( el ) {

			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( el, "*" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( el, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		} );
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join( "|" ) );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join( "|" ) );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully self-exclusive
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			) );
		} :
		function( a, b ) {
			if ( b ) {
				while ( ( b = b.parentNode ) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		// Support: IE 11+, Edge 17 - 18+
		// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
		// two documents; shallow comparisons work.
		// eslint-disable-next-line eqeqeq
		compare = ( a.ownerDocument || a ) == ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			( !support.sortDetached && b.compareDocumentPosition( a ) === compare ) ) {

			// Choose the first element that is related to our preferred document
			// Support: IE 11+, Edge 17 - 18+
			// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
			// two documents; shallow comparisons work.
			// eslint-disable-next-line eqeqeq
			if ( a == document || a.ownerDocument == preferredDoc &&
				contains( preferredDoc, a ) ) {
				return -1;
			}

			// Support: IE 11+, Edge 17 - 18+
			// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
			// two documents; shallow comparisons work.
			// eslint-disable-next-line eqeqeq
			if ( b == document || b.ownerDocument == preferredDoc &&
				contains( preferredDoc, b ) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {

		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {

			// Support: IE 11+, Edge 17 - 18+
			// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
			// two documents; shallow comparisons work.
			/* eslint-disable eqeqeq */
			return a == document ? -1 :
				b == document ? 1 :
				/* eslint-enable eqeqeq */
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( ( cur = cur.parentNode ) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( ( cur = cur.parentNode ) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[ i ] === bp[ i ] ) {
			i++;
		}

		return i ?

			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[ i ], bp[ i ] ) :

			// Otherwise nodes in our document sort first
			// Support: IE 11+, Edge 17 - 18+
			// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
			// two documents; shallow comparisons work.
			/* eslint-disable eqeqeq */
			ap[ i ] == preferredDoc ? -1 :
			bp[ i ] == preferredDoc ? 1 :
			/* eslint-enable eqeqeq */
			0;
	};

	return document;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	setDocument( elem );

	if ( support.matchesSelector && documentIsHTML &&
		!nonnativeSelectorCache[ expr + " " ] &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||

				// As well, disconnected nodes are said to be in a document
				// fragment in IE 9
				elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch ( e ) {
			nonnativeSelectorCache( expr, true );
		}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {

	// Set document vars if needed
	// Support: IE 11+, Edge 17 - 18+
	// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
	// two documents; shallow comparisons work.
	// eslint-disable-next-line eqeqeq
	if ( ( context.ownerDocument || context ) != document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {

	// Set document vars if needed
	// Support: IE 11+, Edge 17 - 18+
	// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
	// two documents; shallow comparisons work.
	// eslint-disable-next-line eqeqeq
	if ( ( elem.ownerDocument || elem ) != document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],

		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			( val = elem.getAttributeNode( name ) ) && val.specified ?
				val.value :
				null;
};

Sizzle.escape = function( sel ) {
	return ( sel + "" ).replace( rcssescape, fcssescape );
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( ( elem = results[ i++ ] ) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {

		// If no nodeType, this is expected to be an array
		while ( ( node = elem[ i++ ] ) ) {

			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {

		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {

			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}

	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[ 1 ] = match[ 1 ].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[ 3 ] = ( match[ 3 ] || match[ 4 ] ||
				match[ 5 ] || "" ).replace( runescape, funescape );

			if ( match[ 2 ] === "~=" ) {
				match[ 3 ] = " " + match[ 3 ] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {

			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[ 1 ] = match[ 1 ].toLowerCase();

			if ( match[ 1 ].slice( 0, 3 ) === "nth" ) {

				// nth-* requires argument
				if ( !match[ 3 ] ) {
					Sizzle.error( match[ 0 ] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[ 4 ] = +( match[ 4 ] ?
					match[ 5 ] + ( match[ 6 ] || 1 ) :
					2 * ( match[ 3 ] === "even" || match[ 3 ] === "odd" ) );
				match[ 5 ] = +( ( match[ 7 ] + match[ 8 ] ) || match[ 3 ] === "odd" );

				// other types prohibit arguments
			} else if ( match[ 3 ] ) {
				Sizzle.error( match[ 0 ] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[ 6 ] && match[ 2 ];

			if ( matchExpr[ "CHILD" ].test( match[ 0 ] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[ 3 ] ) {
				match[ 2 ] = match[ 4 ] || match[ 5 ] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&

				// Get excess from tokenize (recursively)
				( excess = tokenize( unquoted, true ) ) &&

				// advance to the next closing parenthesis
				( excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length ) ) {

				// excess is a negative index
				match[ 0 ] = match[ 0 ].slice( 0, excess );
				match[ 2 ] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() {
					return true;
				} :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				( pattern = new RegExp( "(^|" + whitespace +
					")" + className + "(" + whitespace + "|$)" ) ) && classCache(
						className, function( elem ) {
							return pattern.test(
								typeof elem.className === "string" && elem.className ||
								typeof elem.getAttribute !== "undefined" &&
									elem.getAttribute( "class" ) ||
								""
							);
				} );
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				/* eslint-disable max-len */

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
				/* eslint-enable max-len */

			};
		},

		"CHILD": function( type, what, _argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, _context, xml ) {
					var cache, uniqueCache, outerCache, node, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType,
						diff = false;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( ( node = node[ dir ] ) ) {
									if ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) {

										return false;
									}
								}

								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {

							// Seek `elem` from a previously-cached index

							// ...in a gzip-friendly way
							node = parent;
							outerCache = node[ expando ] || ( node[ expando ] = {} );

							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ node.uniqueID ] ||
								( outerCache[ node.uniqueID ] = {} );

							cache = uniqueCache[ type ] || [];
							nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
							diff = nodeIndex && cache[ 2 ];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( ( node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								( diff = nodeIndex = 0 ) || start.pop() ) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						} else {

							// Use previously-cached element index if available
							if ( useCache ) {

								// ...in a gzip-friendly way
								node = elem;
								outerCache = node[ expando ] || ( node[ expando ] = {} );

								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									( outerCache[ node.uniqueID ] = {} );

								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex;
							}

							// xml :nth-child(...)
							// or :nth-last-child(...) or :nth(-last)?-of-type(...)
							if ( diff === false ) {

								// Use the same loop as above to seek `elem` from the start
								while ( ( node = ++nodeIndex && node && node[ dir ] ||
									( diff = nodeIndex = 0 ) || start.pop() ) ) {

									if ( ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) &&
										++diff ) {

										// Cache the index of each encountered element
										if ( useCache ) {
											outerCache = node[ expando ] ||
												( node[ expando ] = {} );

											// Support: IE <9 only
											// Defend against cloned attroperties (jQuery gh-1709)
											uniqueCache = outerCache[ node.uniqueID ] ||
												( outerCache[ node.uniqueID ] = {} );

											uniqueCache[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {

			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction( function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[ i ] );
							seed[ idx ] = !( matches[ idx ] = matched[ i ] );
						}
					} ) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {

		// Potentially complex pseudos
		"not": markFunction( function( selector ) {

			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction( function( seed, matches, _context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( ( elem = unmatched[ i ] ) ) {
							seed[ i ] = !( matches[ i ] = elem );
						}
					}
				} ) :
				function( elem, _context, xml ) {
					input[ 0 ] = elem;
					matcher( input, null, xml, results );

					// Don't keep the element (issue #299)
					input[ 0 ] = null;
					return !results.pop();
				};
		} ),

		"has": markFunction( function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		} ),

		"contains": markFunction( function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || getText( elem ) ).indexOf( text ) > -1;
			};
		} ),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {

			// lang value must be a valid identifier
			if ( !ridentifier.test( lang || "" ) ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( ( elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute( "xml:lang" ) || elem.getAttribute( "lang" ) ) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( ( elem = elem.parentNode ) && elem.nodeType === 1 );
				return false;
			};
		} ),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement &&
				( !document.hasFocus || document.hasFocus() ) &&
				!!( elem.type || elem.href || ~elem.tabIndex );
		},

		// Boolean properties
		"enabled": createDisabledPseudo( false ),
		"disabled": createDisabledPseudo( true ),

		"checked": function( elem ) {

			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return ( nodeName === "input" && !!elem.checked ) ||
				( nodeName === "option" && !!elem.selected );
		},

		"selected": function( elem ) {

			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				// eslint-disable-next-line no-unused-expressions
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {

			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos[ "empty" ]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( ( attr = elem.getAttribute( "type" ) ) == null ||
					attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo( function() {
			return [ 0 ];
		} ),

		"last": createPositionalPseudo( function( _matchIndexes, length ) {
			return [ length - 1 ];
		} ),

		"eq": createPositionalPseudo( function( _matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		} ),

		"even": createPositionalPseudo( function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		} ),

		"odd": createPositionalPseudo( function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		} ),

		"lt": createPositionalPseudo( function( matchIndexes, length, argument ) {
			var i = argument < 0 ?
				argument + length :
				argument > length ?
					length :
					argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		} ),

		"gt": createPositionalPseudo( function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		} )
	}
};

Expr.pseudos[ "nth" ] = Expr.pseudos[ "eq" ];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || ( match = rcomma.exec( soFar ) ) ) {
			if ( match ) {

				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[ 0 ].length ) || soFar;
			}
			groups.push( ( tokens = [] ) );
		}

		matched = false;

		// Combinators
		if ( ( match = rcombinators.exec( soFar ) ) ) {
			matched = match.shift();
			tokens.push( {
				value: matched,

				// Cast descendant combinators to space
				type: match[ 0 ].replace( rtrim, " " )
			} );
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( ( match = matchExpr[ type ].exec( soFar ) ) && ( !preFilters[ type ] ||
				( match = preFilters[ type ]( match ) ) ) ) {
				matched = match.shift();
				tokens.push( {
					value: matched,
					type: type,
					matches: match
				} );
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :

			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[ i ].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		skip = combinator.next,
		key = skip || dir,
		checkNonElements = base && key === "parentNode",
		doneName = done++;

	return combinator.first ?

		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( ( elem = elem[ dir ] ) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
			return false;
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, uniqueCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
			if ( xml ) {
				while ( ( elem = elem[ dir ] ) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( ( elem = elem[ dir ] ) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || ( elem[ expando ] = {} );

						// Support: IE <9 only
						// Defend against cloned attroperties (jQuery gh-1709)
						uniqueCache = outerCache[ elem.uniqueID ] ||
							( outerCache[ elem.uniqueID ] = {} );

						if ( skip && skip === elem.nodeName.toLowerCase() ) {
							elem = elem[ dir ] || elem;
						} else if ( ( oldCache = uniqueCache[ key ] ) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return ( newCache[ 2 ] = oldCache[ 2 ] );
						} else {

							// Reuse newcache so results back-propagate to previous elements
							uniqueCache[ key ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( ( newCache[ 2 ] = matcher( elem, context, xml ) ) ) {
								return true;
							}
						}
					}
				}
			}
			return false;
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[ i ]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[ 0 ];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[ i ], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( ( elem = unmatched[ i ] ) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction( function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts(
				selector || "*",
				context.nodeType ? [ context ] : context,
				[]
			),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?

				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( ( elem = temp[ i ] ) ) {
					matcherOut[ postMap[ i ] ] = !( matcherIn[ postMap[ i ] ] = elem );
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {

					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( ( elem = matcherOut[ i ] ) ) {

							// Restore matcherIn since elem is not yet a final match
							temp.push( ( matcherIn[ i ] = elem ) );
						}
					}
					postFinder( null, ( matcherOut = [] ), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( ( elem = matcherOut[ i ] ) &&
						( temp = postFinder ? indexOf( seed, elem ) : preMap[ i ] ) > -1 ) {

						seed[ temp ] = !( results[ temp ] = elem );
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	} );
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[ 0 ].type ],
		implicitRelative = leadingRelative || Expr.relative[ " " ],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				( checkContext = context ).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );

			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( ( matcher = Expr.relative[ tokens[ i ].type ] ) ) {
			matchers = [ addCombinator( elementMatcher( matchers ), matcher ) ];
		} else {
			matcher = Expr.filter[ tokens[ i ].type ].apply( null, tokens[ i ].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {

				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[ j ].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(

					// If the preceding token was a descendant combinator, insert an implicit any-element `*`
					tokens
						.slice( 0, i - 1 )
						.concat( { value: tokens[ i - 2 ].type === " " ? "*" : "" } )
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( ( tokens = tokens.slice( j ) ) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,

				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find[ "TAG" ]( "*", outermost ),

				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = ( dirruns += contextBackup == null ? 1 : Math.random() || 0.1 ),
				len = elems.length;

			if ( outermost ) {

				// Support: IE 11+, Edge 17 - 18+
				// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
				// two documents; shallow comparisons work.
				// eslint-disable-next-line eqeqeq
				outermostContext = context == document || context || outermost;
			}

			// Add elements passing elementMatchers directly to results
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && ( elem = elems[ i ] ) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;

					// Support: IE 11+, Edge 17 - 18+
					// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
					// two documents; shallow comparisons work.
					// eslint-disable-next-line eqeqeq
					if ( !context && elem.ownerDocument != document ) {
						setDocument( elem );
						xml = !documentIsHTML;
					}
					while ( ( matcher = elementMatchers[ j++ ] ) ) {
						if ( matcher( elem, context || document, xml ) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {

					// They will have gone through all possible matchers
					if ( ( elem = !matcher && elem ) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// `i` is now the count of elements visited above, and adding it to `matchedCount`
			// makes the latter nonnegative.
			matchedCount += i;

			// Apply set filters to unmatched elements
			// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
			// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
			// no element matchers and no seed.
			// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
			// case, which will result in a "00" `matchedCount` that differs from `i` but is also
			// numerically zero.
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( ( matcher = setMatchers[ j++ ] ) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {

					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !( unmatched[ i ] || setMatched[ i ] ) ) {
								setMatched[ i ] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {

		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[ i ] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache(
			selector,
			matcherFromGroupMatchers( elementMatchers, setMatchers )
		);

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( ( selector = compiled.selector || selector ) );

	results = results || [];

	// Try to minimize operations if there is only one selector in the list and no seed
	// (the latter of which guarantees us context)
	if ( match.length === 1 ) {

		// Reduce context if the leading compound selector is an ID
		tokens = match[ 0 ] = match[ 0 ].slice( 0 );
		if ( tokens.length > 2 && ( token = tokens[ 0 ] ).type === "ID" &&
			context.nodeType === 9 && documentIsHTML && Expr.relative[ tokens[ 1 ].type ] ) {

			context = ( Expr.find[ "ID" ]( token.matches[ 0 ]
				.replace( runescape, funescape ), context ) || [] )[ 0 ];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr[ "needsContext" ].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[ i ];

			// Abort if we hit a combinator
			if ( Expr.relative[ ( type = token.type ) ] ) {
				break;
			}
			if ( ( find = Expr.find[ type ] ) ) {

				// Search, expanding context for leading sibling combinators
				if ( ( seed = find(
					token.matches[ 0 ].replace( runescape, funescape ),
					rsibling.test( tokens[ 0 ].type ) && testContext( context.parentNode ) ||
						context
				) ) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split( "" ).sort( sortOrder ).join( "" ) === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert( function( el ) {

	// Should return 1, but returns 4 (following)
	return el.compareDocumentPosition( document.createElement( "fieldset" ) ) & 1;
} );

// Support: IE<8
// Prevent attribute/property "interpolation"
// https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert( function( el ) {
	el.innerHTML = "<a href='#'></a>";
	return el.firstChild.getAttribute( "href" ) === "#";
} ) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	} );
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert( function( el ) {
	el.innerHTML = "<input/>";
	el.firstChild.setAttribute( "value", "" );
	return el.firstChild.getAttribute( "value" ) === "";
} ) ) {
	addHandle( "value", function( elem, _name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	} );
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert( function( el ) {
	return el.getAttribute( "disabled" ) == null;
} ) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
				( val = elem.getAttributeNode( name ) ) && val.specified ?
					val.value :
					null;
		}
	} );
}

return Sizzle;

} )( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;

// Deprecated
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;
jQuery.escapeSelector = Sizzle.escape;




var dir = function( elem, dir, until ) {
	var matched = [],
		truncate = until !== undefined;

	while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
		if ( elem.nodeType === 1 ) {
			if ( truncate && jQuery( elem ).is( until ) ) {
				break;
			}
			matched.push( elem );
		}
	}
	return matched;
};


var siblings = function( n, elem ) {
	var matched = [];

	for ( ; n; n = n.nextSibling ) {
		if ( n.nodeType === 1 && n !== elem ) {
			matched.push( n );
		}
	}

	return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;



function nodeName( elem, name ) {

  return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();

};
var rsingleTag = ( /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i );



// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			return !!qualifier.call( elem, i, elem ) !== not;
		} );
	}

	// Single element
	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		} );
	}

	// Arraylike of elements (jQuery, arguments, Array)
	if ( typeof qualifier !== "string" ) {
		return jQuery.grep( elements, function( elem ) {
			return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
		} );
	}

	// Filtered directly for both simple and complex selectors
	return jQuery.filter( qualifier, elements, not );
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	if ( elems.length === 1 && elem.nodeType === 1 ) {
		return jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [];
	}

	return jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
		return elem.nodeType === 1;
	} ) );
};

jQuery.fn.extend( {
	find: function( selector ) {
		var i, ret,
			len = this.length,
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter( function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			} ) );
		}

		ret = this.pushStack( [] );

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		return len > 1 ? jQuery.uniqueSort( ret ) : ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow( this, selector || [], false ) );
	},
	not: function( selector ) {
		return this.pushStack( winnow( this, selector || [], true ) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	// Shortcut simple #id case for speed
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,

	init = jQuery.fn.init = function( selector, context, root ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Method init() accepts an alternate rootjQuery
		// so migrate can support jQuery.sub (gh-2101)
		root = root || rootjQuery;

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[ 0 ] === "<" &&
				selector[ selector.length - 1 ] === ">" &&
				selector.length >= 3 ) {

				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && ( match[ 1 ] || !context ) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[ 1 ] ) {
					context = context instanceof jQuery ? context[ 0 ] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[ 1 ],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {

							// Properties of context are called as methods if possible
							if ( isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[ 2 ] );

					if ( elem ) {

						// Inject the element directly into the jQuery object
						this[ 0 ] = elem;
						this.length = 1;
					}
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || root ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this[ 0 ] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( isFunction( selector ) ) {
			return root.ready !== undefined ?
				root.ready( selector ) :

				// Execute immediately if ready is not present
				selector( jQuery );
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.fn.extend( {
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter( function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[ i ] ) ) {
					return true;
				}
			}
		} );
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			targets = typeof selectors !== "string" && jQuery( selectors );

		// Positional selectors never match, since there's no _selection_ context
		if ( !rneedsContext.test( selectors ) ) {
			for ( ; i < l; i++ ) {
				for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

					// Always skip document fragments
					if ( cur.nodeType < 11 && ( targets ?
						targets.index( cur ) > -1 :

						// Don't pass non-elements to Sizzle
						cur.nodeType === 1 &&
							jQuery.find.matchesSelector( cur, selectors ) ) ) {

						matched.push( cur );
						break;
					}
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.uniqueSort(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter( selector )
		);
	}
} );

function sibling( cur, dir ) {
	while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each( {
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, _i, until ) {
		return dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, _i, until ) {
		return dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, _i, until ) {
		return dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return siblings( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return siblings( elem.firstChild );
	},
	contents: function( elem ) {
		if ( elem.contentDocument != null &&

			// Support: IE 11+
			// <object> elements with no `data` attribute has an object
			// `contentDocument` with a `null` prototype.
			getProto( elem.contentDocument ) ) {

			return elem.contentDocument;
		}

		// Support: IE 9 - 11 only, iOS 7 only, Android Browser <=4.3 only
		// Treat the template element as a regular one in browsers that
		// don't support it.
		if ( nodeName( elem, "template" ) ) {
			elem = elem.content || elem;
		}

		return jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {

			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.uniqueSort( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
} );
var rnothtmlwhite = ( /[^\x20\t\r\n\f]+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
	var object = {};
	jQuery.each( options.match( rnothtmlwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	} );
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		createOptions( options ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,

		// Last fire value for non-forgettable lists
		memory,

		// Flag to know if list was already fired
		fired,

		// Flag to prevent firing
		locked,

		// Actual callback list
		list = [],

		// Queue of execution data for repeatable lists
		queue = [],

		// Index of currently firing callback (modified by add/remove as needed)
		firingIndex = -1,

		// Fire callbacks
		fire = function() {

			// Enforce single-firing
			locked = locked || options.once;

			// Execute callbacks for all pending executions,
			// respecting firingIndex overrides and runtime changes
			fired = firing = true;
			for ( ; queue.length; firingIndex = -1 ) {
				memory = queue.shift();
				while ( ++firingIndex < list.length ) {

					// Run callback and check for early termination
					if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
						options.stopOnFalse ) {

						// Jump to end and forget the data so .add doesn't re-fire
						firingIndex = list.length;
						memory = false;
					}
				}
			}

			// Forget the data if we're done with it
			if ( !options.memory ) {
				memory = false;
			}

			firing = false;

			// Clean up if we're done firing for good
			if ( locked ) {

				// Keep an empty list if we have data for future add calls
				if ( memory ) {
					list = [];

				// Otherwise, this object is spent
				} else {
					list = "";
				}
			}
		},

		// Actual Callbacks object
		self = {

			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {

					// If we have memory from a past run, we should fire after adding
					if ( memory && !firing ) {
						firingIndex = list.length - 1;
						queue.push( memory );
					}

					( function add( args ) {
						jQuery.each( args, function( _, arg ) {
							if ( isFunction( arg ) ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && toType( arg ) !== "string" ) {

								// Inspect recursively
								add( arg );
							}
						} );
					} )( arguments );

					if ( memory && !firing ) {
						fire();
					}
				}
				return this;
			},

			// Remove a callback from the list
			remove: function() {
				jQuery.each( arguments, function( _, arg ) {
					var index;
					while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
						list.splice( index, 1 );

						// Handle firing indexes
						if ( index <= firingIndex ) {
							firingIndex--;
						}
					}
				} );
				return this;
			},

			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ?
					jQuery.inArray( fn, list ) > -1 :
					list.length > 0;
			},

			// Remove all callbacks from the list
			empty: function() {
				if ( list ) {
					list = [];
				}
				return this;
			},

			// Disable .fire and .add
			// Abort any current/pending executions
			// Clear all callbacks and values
			disable: function() {
				locked = queue = [];
				list = memory = "";
				return this;
			},
			disabled: function() {
				return !list;
			},

			// Disable .fire
			// Also disable .add unless we have memory (since it would have no effect)
			// Abort any pending executions
			lock: function() {
				locked = queue = [];
				if ( !memory && !firing ) {
					list = memory = "";
				}
				return this;
			},
			locked: function() {
				return !!locked;
			},

			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( !locked ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					queue.push( args );
					if ( !firing ) {
						fire();
					}
				}
				return this;
			},

			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},

			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


function Identity( v ) {
	return v;
}
function Thrower( ex ) {
	throw ex;
}

function adoptValue( value, resolve, reject, noValue ) {
	var method;

	try {

		// Check for promise aspect first to privilege synchronous behavior
		if ( value && isFunction( ( method = value.promise ) ) ) {
			method.call( value ).done( resolve ).fail( reject );

		// Other thenables
		} else if ( value && isFunction( ( method = value.then ) ) ) {
			method.call( value, resolve, reject );

		// Other non-thenables
		} else {

			// Control `resolve` arguments by letting Array#slice cast boolean `noValue` to integer:
			// * false: [ value ].slice( 0 ) => resolve( value )
			// * true: [ value ].slice( 1 ) => resolve()
			resolve.apply( undefined, [ value ].slice( noValue ) );
		}

	// For Promises/A+, convert exceptions into rejections
	// Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
	// Deferred#then to conditionally suppress rejection.
	} catch ( value ) {

		// Support: Android 4.0 only
		// Strict mode functions invoked without .call/.apply get global-object context
		reject.apply( undefined, [ value ] );
	}
}

jQuery.extend( {

	Deferred: function( func ) {
		var tuples = [

				// action, add listener, callbacks,
				// ... .then handlers, argument index, [final state]
				[ "notify", "progress", jQuery.Callbacks( "memory" ),
					jQuery.Callbacks( "memory" ), 2 ],
				[ "resolve", "done", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 0, "resolved" ],
				[ "reject", "fail", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 1, "rejected" ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				"catch": function( fn ) {
					return promise.then( null, fn );
				},

				// Keep pipe for back-compat
				pipe: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;

					return jQuery.Deferred( function( newDefer ) {
						jQuery.each( tuples, function( _i, tuple ) {

							// Map tuples (progress, done, fail) to arguments (done, fail, progress)
							var fn = isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];

							// deferred.progress(function() { bind to newDefer or newDefer.notify })
							// deferred.done(function() { bind to newDefer or newDefer.resolve })
							// deferred.fail(function() { bind to newDefer or newDefer.reject })
							deferred[ tuple[ 1 ] ]( function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && isFunction( returned.promise ) ) {
									returned.promise()
										.progress( newDefer.notify )
										.done( newDefer.resolve )
										.fail( newDefer.reject );
								} else {
									newDefer[ tuple[ 0 ] + "With" ](
										this,
										fn ? [ returned ] : arguments
									);
								}
							} );
						} );
						fns = null;
					} ).promise();
				},
				then: function( onFulfilled, onRejected, onProgress ) {
					var maxDepth = 0;
					function resolve( depth, deferred, handler, special ) {
						return function() {
							var that = this,
								args = arguments,
								mightThrow = function() {
									var returned, then;

									// Support: Promises/A+ section 2.3.3.3.3
									// https://promisesaplus.com/#point-59
									// Ignore double-resolution attempts
									if ( depth < maxDepth ) {
										return;
									}

									returned = handler.apply( that, args );

									// Support: Promises/A+ section 2.3.1
									// https://promisesaplus.com/#point-48
									if ( returned === deferred.promise() ) {
										throw new TypeError( "Thenable self-resolution" );
									}

									// Support: Promises/A+ sections 2.3.3.1, 3.5
									// https://promisesaplus.com/#point-54
									// https://promisesaplus.com/#point-75
									// Retrieve `then` only once
									then = returned &&

										// Support: Promises/A+ section 2.3.4
										// https://promisesaplus.com/#point-64
										// Only check objects and functions for thenability
										( typeof returned === "object" ||
											typeof returned === "function" ) &&
										returned.then;

									// Handle a returned thenable
									if ( isFunction( then ) ) {

										// Special processors (notify) just wait for resolution
										if ( special ) {
											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special )
											);

										// Normal processors (resolve) also hook into progress
										} else {

											// ...and disregard older resolution values
											maxDepth++;

											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special ),
												resolve( maxDepth, deferred, Identity,
													deferred.notifyWith )
											);
										}

									// Handle all other returned values
									} else {

										// Only substitute handlers pass on context
										// and multiple values (non-spec behavior)
										if ( handler !== Identity ) {
											that = undefined;
											args = [ returned ];
										}

										// Process the value(s)
										// Default process is resolve
										( special || deferred.resolveWith )( that, args );
									}
								},

								// Only normal processors (resolve) catch and reject exceptions
								process = special ?
									mightThrow :
									function() {
										try {
											mightThrow();
										} catch ( e ) {

											if ( jQuery.Deferred.exceptionHook ) {
												jQuery.Deferred.exceptionHook( e,
													process.stackTrace );
											}

											// Support: Promises/A+ section 2.3.3.3.4.1
											// https://promisesaplus.com/#point-61
											// Ignore post-resolution exceptions
											if ( depth + 1 >= maxDepth ) {

												// Only substitute handlers pass on context
												// and multiple values (non-spec behavior)
												if ( handler !== Thrower ) {
													that = undefined;
													args = [ e ];
												}

												deferred.rejectWith( that, args );
											}
										}
									};

							// Support: Promises/A+ section 2.3.3.3.1
							// https://promisesaplus.com/#point-57
							// Re-resolve promises immediately to dodge false rejection from
							// subsequent errors
							if ( depth ) {
								process();
							} else {

								// Call an optional hook to record the stack, in case of exception
								// since it's otherwise lost when execution goes async
								if ( jQuery.Deferred.getStackHook ) {
									process.stackTrace = jQuery.Deferred.getStackHook();
								}
								window.setTimeout( process );
							}
						};
					}

					return jQuery.Deferred( function( newDefer ) {

						// progress_handlers.add( ... )
						tuples[ 0 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onProgress ) ?
									onProgress :
									Identity,
								newDefer.notifyWith
							)
						);

						// fulfilled_handlers.add( ... )
						tuples[ 1 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onFulfilled ) ?
									onFulfilled :
									Identity
							)
						);

						// rejected_handlers.add( ... )
						tuples[ 2 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onRejected ) ?
									onRejected :
									Thrower
							)
						);
					} ).promise();
				},

				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 5 ];

			// promise.progress = list.add
			// promise.done = list.add
			// promise.fail = list.add
			promise[ tuple[ 1 ] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(
					function() {

						// state = "resolved" (i.e., fulfilled)
						// state = "rejected"
						state = stateString;
					},

					// rejected_callbacks.disable
					// fulfilled_callbacks.disable
					tuples[ 3 - i ][ 2 ].disable,

					// rejected_handlers.disable
					// fulfilled_handlers.disable
					tuples[ 3 - i ][ 3 ].disable,

					// progress_callbacks.lock
					tuples[ 0 ][ 2 ].lock,

					// progress_handlers.lock
					tuples[ 0 ][ 3 ].lock
				);
			}

			// progress_handlers.fire
			// fulfilled_handlers.fire
			// rejected_handlers.fire
			list.add( tuple[ 3 ].fire );

			// deferred.notify = function() { deferred.notifyWith(...) }
			// deferred.resolve = function() { deferred.resolveWith(...) }
			// deferred.reject = function() { deferred.rejectWith(...) }
			deferred[ tuple[ 0 ] ] = function() {
				deferred[ tuple[ 0 ] + "With" ]( this === deferred ? undefined : this, arguments );
				return this;
			};

			// deferred.notifyWith = list.fireWith
			// deferred.resolveWith = list.fireWith
			// deferred.rejectWith = list.fireWith
			deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
		} );

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( singleValue ) {
		var

			// count of uncompleted subordinates
			remaining = arguments.length,

			// count of unprocessed arguments
			i = remaining,

			// subordinate fulfillment data
			resolveContexts = Array( i ),
			resolveValues = slice.call( arguments ),

			// the master Deferred
			master = jQuery.Deferred(),

			// subordinate callback factory
			updateFunc = function( i ) {
				return function( value ) {
					resolveContexts[ i ] = this;
					resolveValues[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( !( --remaining ) ) {
						master.resolveWith( resolveContexts, resolveValues );
					}
				};
			};

		// Single- and empty arguments are adopted like Promise.resolve
		if ( remaining <= 1 ) {
			adoptValue( singleValue, master.done( updateFunc( i ) ).resolve, master.reject,
				!remaining );

			// Use .then() to unwrap secondary thenables (cf. gh-3000)
			if ( master.state() === "pending" ||
				isFunction( resolveValues[ i ] && resolveValues[ i ].then ) ) {

				return master.then();
			}
		}

		// Multiple arguments are aggregated like Promise.all array elements
		while ( i-- ) {
			adoptValue( resolveValues[ i ], updateFunc( i ), master.reject );
		}

		return master.promise();
	}
} );


// These usually indicate a programmer mistake during development,
// warn about them ASAP rather than swallowing them by default.
var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;

jQuery.Deferred.exceptionHook = function( error, stack ) {

	// Support: IE 8 - 9 only
	// Console exists when dev tools are open, which can happen at any time
	if ( window.console && window.console.warn && error && rerrorNames.test( error.name ) ) {
		window.console.warn( "jQuery.Deferred exception: " + error.message, error.stack, stack );
	}
};




jQuery.readyException = function( error ) {
	window.setTimeout( function() {
		throw error;
	} );
};




// The deferred used on DOM ready
var readyList = jQuery.Deferred();

jQuery.fn.ready = function( fn ) {

	readyList
		.then( fn )

		// Wrap jQuery.readyException in a function so that the lookup
		// happens at the time of error handling instead of callback
		// registration.
		.catch( function( error ) {
			jQuery.readyException( error );
		} );

	return this;
};

jQuery.extend( {

	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );
	}
} );

jQuery.ready.then = readyList.then;

// The ready event handler and self cleanup method
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed );
	window.removeEventListener( "load", completed );
	jQuery.ready();
}

// Catch cases where $(document).ready() is called
// after the browser event has already occurred.
// Support: IE <=9 - 10 only
// Older IE sometimes signals "interactive" too soon
if ( document.readyState === "complete" ||
	( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

	// Handle it asynchronously to allow scripts the opportunity to delay ready
	window.setTimeout( jQuery.ready );

} else {

	// Use the handy event callback
	document.addEventListener( "DOMContentLoaded", completed );

	// A fallback to window.onload, that will always work
	window.addEventListener( "load", completed );
}




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( toType( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			access( elems, fn, i, key[ i ], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {

			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, _key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn(
					elems[ i ], key, raw ?
					value :
					value.call( elems[ i ], i, fn( elems[ i ], key ) )
				);
			}
		}
	}

	if ( chainable ) {
		return elems;
	}

	// Gets
	if ( bulk ) {
		return fn.call( elems );
	}

	return len ? fn( elems[ 0 ], key ) : emptyGet;
};


// Matches dashed string for camelizing
var rmsPrefix = /^-ms-/,
	rdashAlpha = /-([a-z])/g;

// Used by camelCase as callback to replace()
function fcamelCase( _all, letter ) {
	return letter.toUpperCase();
}

// Convert dashed to camelCase; used by the css and data modules
// Support: IE <=9 - 11, Edge 12 - 15
// Microsoft forgot to hump their vendor prefix (#9572)
function camelCase( string ) {
	return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
}
var acceptData = function( owner ) {

	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

	cache: function( owner ) {

		// Check if the owner object already has a cache
		var value = owner[ this.expando ];

		// If not, create one
		if ( !value ) {
			value = {};

			// We can accept data for non-element nodes in modern browsers,
			// but we should not, see #8335.
			// Always return an empty object.
			if ( acceptData( owner ) ) {

				// If it is a node unlikely to be stringify-ed or looped over
				// use plain assignment
				if ( owner.nodeType ) {
					owner[ this.expando ] = value;

				// Otherwise secure it in a non-enumerable property
				// configurable must be true to allow the property to be
				// deleted when data is removed
				} else {
					Object.defineProperty( owner, this.expando, {
						value: value,
						configurable: true
					} );
				}
			}
		}

		return value;
	},
	set: function( owner, data, value ) {
		var prop,
			cache = this.cache( owner );

		// Handle: [ owner, key, value ] args
		// Always use camelCase key (gh-2257)
		if ( typeof data === "string" ) {
			cache[ camelCase( data ) ] = value;

		// Handle: [ owner, { properties } ] args
		} else {

			// Copy the properties one-by-one to the cache object
			for ( prop in data ) {
				cache[ camelCase( prop ) ] = data[ prop ];
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		return key === undefined ?
			this.cache( owner ) :

			// Always use camelCase key (gh-2257)
			owner[ this.expando ] && owner[ this.expando ][ camelCase( key ) ];
	},
	access: function( owner, key, value ) {

		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				( ( key && typeof key === "string" ) && value === undefined ) ) {

			return this.get( owner, key );
		}

		// When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i,
			cache = owner[ this.expando ];

		if ( cache === undefined ) {
			return;
		}

		if ( key !== undefined ) {

			// Support array or space separated string of keys
			if ( Array.isArray( key ) ) {

				// If key is an array of keys...
				// We always set camelCase keys, so remove that.
				key = key.map( camelCase );
			} else {
				key = camelCase( key );

				// If a key with the spaces exists, use it.
				// Otherwise, create an array by matching non-whitespace
				key = key in cache ?
					[ key ] :
					( key.match( rnothtmlwhite ) || [] );
			}

			i = key.length;

			while ( i-- ) {
				delete cache[ key[ i ] ];
			}
		}

		// Remove the expando if there's no more data
		if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

			// Support: Chrome <=35 - 45
			// Webkit & Blink performance suffers when deleting properties
			// from DOM nodes, so set to undefined instead
			// https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
			if ( owner.nodeType ) {
				owner[ this.expando ] = undefined;
			} else {
				delete owner[ this.expando ];
			}
		}
	},
	hasData: function( owner ) {
		var cache = owner[ this.expando ];
		return cache !== undefined && !jQuery.isEmptyObject( cache );
	}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /[A-Z]/g;

function getData( data ) {
	if ( data === "true" ) {
		return true;
	}

	if ( data === "false" ) {
		return false;
	}

	if ( data === "null" ) {
		return null;
	}

	// Only convert to a number if it doesn't change the string
	if ( data === +data + "" ) {
		return +data;
	}

	if ( rbrace.test( data ) ) {
		return JSON.parse( data );
	}

	return data;
}

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = getData( data );
			} catch ( e ) {}

			// Make sure we set the data so it isn't changed later
			dataUser.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend( {
	hasData: function( elem ) {
		return dataUser.hasData( elem ) || dataPriv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return dataUser.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		dataUser.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to dataPriv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return dataPriv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		dataPriv.remove( elem, name );
	}
} );

jQuery.fn.extend( {
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = dataUser.get( elem );

				if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE 11 only
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = camelCase( name.slice( 5 ) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					dataPriv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each( function() {
				dataUser.set( this, key );
			} );
		}

		return access( this, function( value ) {
			var data;

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {

				// Attempt to get data from the cache
				// The key will always be camelCased in Data
				data = dataUser.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each( function() {

				// We always store the camelCased key
				dataUser.set( this, key, value );
			} );
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each( function() {
			dataUser.remove( this, key );
		} );
	}
} );


jQuery.extend( {
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = dataPriv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || Array.isArray( data ) ) {
					queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
			empty: jQuery.Callbacks( "once memory" ).add( function() {
				dataPriv.remove( elem, [ type + "queue", key ] );
			} )
		} );
	}
} );

jQuery.fn.extend( {
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[ 0 ], type );
		}

		return data === undefined ?
			this :
			this.each( function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			} );
	},
	dequeue: function( type ) {
		return this.each( function() {
			jQuery.dequeue( this, type );
		} );
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},

	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var documentElement = document.documentElement;



	var isAttached = function( elem ) {
			return jQuery.contains( elem.ownerDocument, elem );
		},
		composed = { composed: true };

	// Support: IE 9 - 11+, Edge 12 - 18+, iOS 10.0 - 10.2 only
	// Check attachment across shadow DOM boundaries when possible (gh-3504)
	// Support: iOS 10.0-10.2 only
	// Early iOS 10 versions support `attachShadow` but not `getRootNode`,
	// leading to errors. We need to check for `getRootNode`.
	if ( documentElement.getRootNode ) {
		isAttached = function( elem ) {
			return jQuery.contains( elem.ownerDocument, elem ) ||
				elem.getRootNode( composed ) === elem.ownerDocument;
		};
	}
var isHiddenWithinTree = function( elem, el ) {

		// isHiddenWithinTree might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;

		// Inline style trumps all
		return elem.style.display === "none" ||
			elem.style.display === "" &&

			// Otherwise, check computed style
			// Support: Firefox <=43 - 45
			// Disconnected elements can have computed display: none, so first confirm that elem is
			// in the document.
			isAttached( elem ) &&

			jQuery.css( elem, "display" ) === "none";
	};



function adjustCSS( elem, prop, valueParts, tween ) {
	var adjusted, scale,
		maxIterations = 20,
		currentValue = tween ?
			function() {
				return tween.cur();
			} :
			function() {
				return jQuery.css( elem, prop, "" );
			},
		initial = currentValue(),
		unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

		// Starting value computation is required for potential unit mismatches
		initialInUnit = elem.nodeType &&
			( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
			rcssNum.exec( jQuery.css( elem, prop ) );

	if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

		// Support: Firefox <=54
		// Halve the iteration target value to prevent interference from CSS upper bounds (gh-2144)
		initial = initial / 2;

		// Trust units reported by jQuery.css
		unit = unit || initialInUnit[ 3 ];

		// Iteratively approximate from a nonzero starting point
		initialInUnit = +initial || 1;

		while ( maxIterations-- ) {

			// Evaluate and update our best guess (doubling guesses that zero out).
			// Finish if the scale equals or crosses 1 (making the old*new product non-positive).
			jQuery.style( elem, prop, initialInUnit + unit );
			if ( ( 1 - scale ) * ( 1 - ( scale = currentValue() / initial || 0.5 ) ) <= 0 ) {
				maxIterations = 0;
			}
			initialInUnit = initialInUnit / scale;

		}

		initialInUnit = initialInUnit * 2;
		jQuery.style( elem, prop, initialInUnit + unit );

		// Make sure we update the tween properties later on
		valueParts = valueParts || [];
	}

	if ( valueParts ) {
		initialInUnit = +initialInUnit || +initial || 0;

		// Apply relative offset (+=/-=) if specified
		adjusted = valueParts[ 1 ] ?
			initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
			+valueParts[ 2 ];
		if ( tween ) {
			tween.unit = unit;
			tween.start = initialInUnit;
			tween.end = adjusted;
		}
	}
	return adjusted;
}


var defaultDisplayMap = {};

function getDefaultDisplay( elem ) {
	var temp,
		doc = elem.ownerDocument,
		nodeName = elem.nodeName,
		display = defaultDisplayMap[ nodeName ];

	if ( display ) {
		return display;
	}

	temp = doc.body.appendChild( doc.createElement( nodeName ) );
	display = jQuery.css( temp, "display" );

	temp.parentNode.removeChild( temp );

	if ( display === "none" ) {
		display = "block";
	}
	defaultDisplayMap[ nodeName ] = display;

	return display;
}

function showHide( elements, show ) {
	var display, elem,
		values = [],
		index = 0,
		length = elements.length;

	// Determine new display value for elements that need to change
	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		display = elem.style.display;
		if ( show ) {

			// Since we force visibility upon cascade-hidden elements, an immediate (and slow)
			// check is required in this first loop unless we have a nonempty display value (either
			// inline or about-to-be-restored)
			if ( display === "none" ) {
				values[ index ] = dataPriv.get( elem, "display" ) || null;
				if ( !values[ index ] ) {
					elem.style.display = "";
				}
			}
			if ( elem.style.display === "" && isHiddenWithinTree( elem ) ) {
				values[ index ] = getDefaultDisplay( elem );
			}
		} else {
			if ( display !== "none" ) {
				values[ index ] = "none";

				// Remember what we're overwriting
				dataPriv.set( elem, "display", display );
			}
		}
	}

	// Set the display of the elements in a second loop to avoid constant reflow
	for ( index = 0; index < length; index++ ) {
		if ( values[ index ] != null ) {
			elements[ index ].style.display = values[ index ];
		}
	}

	return elements;
}

jQuery.fn.extend( {
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each( function() {
			if ( isHiddenWithinTree( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		} );
	}
} );
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([a-z][^\/\0>\x20\t\r\n\f]*)/i );

var rscriptType = ( /^$|^module$|\/(?:java|ecma)script/i );



( function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Android 4.0 - 4.3 only
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Android <=4.1 only
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE <=11 only
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;

	// Support: IE <=9 only
	// IE <=9 replaces <option> tags with their contents when inserted outside of
	// the select element.
	div.innerHTML = "<option></option>";
	support.option = !!div.lastChild;
} )();


// We have to close these tags to support XHTML (#13200)
var wrapMap = {

	// XHTML parsers do not magically insert elements in the
	// same way that tag soup parsers do. So we cannot shorten
	// this by omitting <tbody> or other required elements.
	thead: [ 1, "<table>", "</table>" ],
	col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
	tr: [ 2, "<table><tbody>", "</tbody></table>" ],
	td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

	_default: [ 0, "", "" ]
};

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

// Support: IE <=9 only
if ( !support.option ) {
	wrapMap.optgroup = wrapMap.option = [ 1, "<select multiple='multiple'>", "</select>" ];
}


function getAll( context, tag ) {

	// Support: IE <=9 - 11 only
	// Use typeof to avoid zero-argument method invocation on host objects (#15151)
	var ret;

	if ( typeof context.getElementsByTagName !== "undefined" ) {
		ret = context.getElementsByTagName( tag || "*" );

	} else if ( typeof context.querySelectorAll !== "undefined" ) {
		ret = context.querySelectorAll( tag || "*" );

	} else {
		ret = [];
	}

	if ( tag === undefined || tag && nodeName( context, tag ) ) {
		return jQuery.merge( [ context ], ret );
	}

	return ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		dataPriv.set(
			elems[ i ],
			"globalEval",
			!refElements || dataPriv.get( refElements[ i ], "globalEval" )
		);
	}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
	var elem, tmp, tag, wrap, attached, j,
		fragment = context.createDocumentFragment(),
		nodes = [],
		i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		elem = elems[ i ];

		if ( elem || elem === 0 ) {

			// Add nodes directly
			if ( toType( elem ) === "object" ) {

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

			// Convert non-html into a text node
			} else if ( !rhtml.test( elem ) ) {
				nodes.push( context.createTextNode( elem ) );

			// Convert html into DOM nodes
			} else {
				tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

				// Deserialize a standard representation
				tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
				wrap = wrapMap[ tag ] || wrapMap._default;
				tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

				// Descend through wrappers to the right content
				j = wrap[ 0 ];
				while ( j-- ) {
					tmp = tmp.lastChild;
				}

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, tmp.childNodes );

				// Remember the top-level container
				tmp = fragment.firstChild;

				// Ensure the created nodes are orphaned (#12392)
				tmp.textContent = "";
			}
		}
	}

	// Remove wrapper from fragment
	fragment.textContent = "";

	i = 0;
	while ( ( elem = nodes[ i++ ] ) ) {

		// Skip elements already in the context collection (trac-4087)
		if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
			if ( ignored ) {
				ignored.push( elem );
			}
			continue;
		}

		attached = isAttached( elem );

		// Append to fragment
		tmp = getAll( fragment.appendChild( elem ), "script" );

		// Preserve script evaluation history
		if ( attached ) {
			setGlobalEval( tmp );
		}

		// Capture executables
		if ( scripts ) {
			j = 0;
			while ( ( elem = tmp[ j++ ] ) ) {
				if ( rscriptType.test( elem.type || "" ) ) {
					scripts.push( elem );
				}
			}
		}
	}

	return fragment;
}


var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

// Support: IE <=9 - 11+
// focus() and blur() are asynchronous, except when they are no-op.
// So expect focus to be synchronous when the element is already active,
// and blur to be synchronous when the element is not already active.
// (focus and blur are always synchronous in other supported browsers,
// this just defines when we can count on it).
function expectSync( elem, type ) {
	return ( elem === safeActiveElement() ) === ( type === "focus" );
}

// Support: IE <=9 only
// Accessing document.activeElement can throw unexpectedly
// https://bugs.jquery.com/ticket/13393
function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
	var origFn, type;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {

		// ( types-Object, selector, data )
		if ( typeof selector !== "string" ) {

			// ( types-Object, data )
			data = data || selector;
			selector = undefined;
		}
		for ( type in types ) {
			on( elem, type, selector, data, types[ type ], one );
		}
		return elem;
	}

	if ( data == null && fn == null ) {

		// ( types, fn )
		fn = selector;
		data = selector = undefined;
	} else if ( fn == null ) {
		if ( typeof selector === "string" ) {

			// ( types, selector, fn )
			fn = data;
			data = undefined;
		} else {

			// ( types, data, fn )
			fn = data;
			data = selector;
			selector = undefined;
		}
	}
	if ( fn === false ) {
		fn = returnFalse;
	} else if ( !fn ) {
		return elem;
	}

	if ( one === 1 ) {
		origFn = fn;
		fn = function( event ) {

			// Can use an empty set, since event contains the info
			jQuery().off( event );
			return origFn.apply( this, arguments );
		};

		// Use same guid so caller can remove using origFn
		fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return elem.each( function() {
		jQuery.event.add( this, types, fn, data, selector );
	} );
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.get( elem );

		// Only attach events to objects that accept data
		if ( !acceptData( elem ) ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Ensure that invalid selectors throw exceptions at attach time
		// Evaluate against documentElement in case elem is a non-element node (e.g., document)
		if ( selector ) {
			jQuery.find.matchesSelector( documentElement, selector );
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !( events = elemData.events ) ) {
			events = elemData.events = Object.create( null );
		}
		if ( !( eventHandle = elemData.handle ) ) {
			eventHandle = elemData.handle = function( e ) {

				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend( {
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join( "." )
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !( handlers = events[ type ] ) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup ||
					special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

		if ( !elemData || !( events = elemData.events ) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[ 2 ] &&
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector ||
						selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown ||
					special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove data and the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			dataPriv.remove( elem, "handle events" );
		}
	},

	dispatch: function( nativeEvent ) {

		var i, j, ret, matched, handleObj, handlerQueue,
			args = new Array( arguments.length ),

			// Make a writable jQuery.Event from the native event object
			event = jQuery.event.fix( nativeEvent ),

			handlers = (
					dataPriv.get( this, "events" ) || Object.create( null )
				)[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[ 0 ] = event;

		for ( i = 1; i < arguments.length; i++ ) {
			args[ i ] = arguments[ i ];
		}

		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( ( handleObj = matched.handlers[ j++ ] ) &&
				!event.isImmediatePropagationStopped() ) {

				// If the event is namespaced, then each handler is only invoked if it is
				// specially universal or its namespaces are a superset of the event's.
				if ( !event.rnamespace || handleObj.namespace === false ||
					event.rnamespace.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
						handleObj.handler ).apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( ( event.result = ret ) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, handleObj, sel, matchedHandlers, matchedSelectors,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		if ( delegateCount &&

			// Support: IE <=9
			// Black-hole SVG <use> instance trees (trac-13180)
			cur.nodeType &&

			// Support: Firefox <=42
			// Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
			// https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
			// Support: IE 11 only
			// ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
			!( event.type === "click" && event.button >= 1 ) ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && !( event.type === "click" && cur.disabled === true ) ) {
					matchedHandlers = [];
					matchedSelectors = {};
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matchedSelectors[ sel ] === undefined ) {
							matchedSelectors[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) > -1 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matchedSelectors[ sel ] ) {
							matchedHandlers.push( handleObj );
						}
					}
					if ( matchedHandlers.length ) {
						handlerQueue.push( { elem: cur, handlers: matchedHandlers } );
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		cur = this;
		if ( delegateCount < handlers.length ) {
			handlerQueue.push( { elem: cur, handlers: handlers.slice( delegateCount ) } );
		}

		return handlerQueue;
	},

	addProp: function( name, hook ) {
		Object.defineProperty( jQuery.Event.prototype, name, {
			enumerable: true,
			configurable: true,

			get: isFunction( hook ) ?
				function() {
					if ( this.originalEvent ) {
							return hook( this.originalEvent );
					}
				} :
				function() {
					if ( this.originalEvent ) {
							return this.originalEvent[ name ];
					}
				},

			set: function( value ) {
				Object.defineProperty( this, name, {
					enumerable: true,
					configurable: true,
					writable: true,
					value: value
				} );
			}
		} );
	},

	fix: function( originalEvent ) {
		return originalEvent[ jQuery.expando ] ?
			originalEvent :
			new jQuery.Event( originalEvent );
	},

	special: {
		load: {

			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		click: {

			// Utilize native event to ensure correct state for checkable inputs
			setup: function( data ) {

				// For mutual compressibility with _default, replace `this` access with a local var.
				// `|| data` is dead code meant only to preserve the variable through minification.
				var el = this || data;

				// Claim the first handler
				if ( rcheckableType.test( el.type ) &&
					el.click && nodeName( el, "input" ) ) {

					// dataPriv.set( el, "click", ... )
					leverageNative( el, "click", returnTrue );
				}

				// Return false to allow normal processing in the caller
				return false;
			},
			trigger: function( data ) {

				// For mutual compressibility with _default, replace `this` access with a local var.
				// `|| data` is dead code meant only to preserve the variable through minification.
				var el = this || data;

				// Force setup before triggering a click
				if ( rcheckableType.test( el.type ) &&
					el.click && nodeName( el, "input" ) ) {

					leverageNative( el, "click" );
				}

				// Return non-false to allow normal event-path propagation
				return true;
			},

			// For cross-browser consistency, suppress native .click() on links
			// Also prevent it if we're currently inside a leveraged native-event stack
			_default: function( event ) {
				var target = event.target;
				return rcheckableType.test( target.type ) &&
					target.click && nodeName( target, "input" ) &&
					dataPriv.get( target, "click" ) ||
					nodeName( target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	}
};

// Ensure the presence of an event listener that handles manually-triggered
// synthetic events by interrupting progress until reinvoked in response to
// *native* events that it fires directly, ensuring that state changes have
// already occurred before other listeners are invoked.
function leverageNative( el, type, expectSync ) {

	// Missing expectSync indicates a trigger call, which must force setup through jQuery.event.add
	if ( !expectSync ) {
		if ( dataPriv.get( el, type ) === undefined ) {
			jQuery.event.add( el, type, returnTrue );
		}
		return;
	}

	// Register the controller as a special universal handler for all event namespaces
	dataPriv.set( el, type, false );
	jQuery.event.add( el, type, {
		namespace: false,
		handler: function( event ) {
			var notAsync, result,
				saved = dataPriv.get( this, type );

			if ( ( event.isTrigger & 1 ) && this[ type ] ) {

				// Interrupt processing of the outer synthetic .trigger()ed event
				// Saved data should be false in such cases, but might be a leftover capture object
				// from an async native handler (gh-4350)
				if ( !saved.length ) {

					// Store arguments for use when handling the inner native event
					// There will always be at least one argument (an event object), so this array
					// will not be confused with a leftover capture object.
					saved = slice.call( arguments );
					dataPriv.set( this, type, saved );

					// Trigger the native event and capture its result
					// Support: IE <=9 - 11+
					// focus() and blur() are asynchronous
					notAsync = expectSync( this, type );
					this[ type ]();
					result = dataPriv.get( this, type );
					if ( saved !== result || notAsync ) {
						dataPriv.set( this, type, false );
					} else {
						result = {};
					}
					if ( saved !== result ) {

						// Cancel the outer synthetic event
						event.stopImmediatePropagation();
						event.preventDefault();
						return result.value;
					}

				// If this is an inner synthetic event for an event with a bubbling surrogate
				// (focus or blur), assume that the surrogate already propagated from triggering the
				// native event and prevent that from happening again here.
				// This technically gets the ordering wrong w.r.t. to `.trigger()` (in which the
				// bubbling surrogate propagates *after* the non-bubbling base), but that seems
				// less bad than duplication.
				} else if ( ( jQuery.event.special[ type ] || {} ).delegateType ) {
					event.stopPropagation();
				}

			// If this is a native event triggered above, everything is now in order
			// Fire an inner synthetic event with the original arguments
			} else if ( saved.length ) {

				// ...and capture the result
				dataPriv.set( this, type, {
					value: jQuery.event.trigger(

						// Support: IE <=9 - 11+
						// Extend with the prototype to reset the above stopImmediatePropagation()
						jQuery.extend( saved[ 0 ], jQuery.Event.prototype ),
						saved.slice( 1 ),
						this
					)
				} );

				// Abort handling of the native event
				event.stopImmediatePropagation();
			}
		}
	} );
}

jQuery.removeEvent = function( elem, type, handle ) {

	// This "if" is needed for plain objects
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle );
	}
};

jQuery.Event = function( src, props ) {

	// Allow instantiation without the 'new' keyword
	if ( !( this instanceof jQuery.Event ) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&

				// Support: Android <=2.3 only
				src.returnValue === false ?
			returnTrue :
			returnFalse;

		// Create target properties
		// Support: Safari <=6 - 7 only
		// Target should not be a text node (#504, #13143)
		this.target = ( src.target && src.target.nodeType === 3 ) ?
			src.target.parentNode :
			src.target;

		this.currentTarget = src.currentTarget;
		this.relatedTarget = src.relatedTarget;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || Date.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	constructor: jQuery.Event,
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,
	isSimulated: false,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && !this.isSimulated ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Includes all common event props including KeyEvent and MouseEvent specific props
jQuery.each( {
	altKey: true,
	bubbles: true,
	cancelable: true,
	changedTouches: true,
	ctrlKey: true,
	detail: true,
	eventPhase: true,
	metaKey: true,
	pageX: true,
	pageY: true,
	shiftKey: true,
	view: true,
	"char": true,
	code: true,
	charCode: true,
	key: true,
	keyCode: true,
	button: true,
	buttons: true,
	clientX: true,
	clientY: true,
	offsetX: true,
	offsetY: true,
	pointerId: true,
	pointerType: true,
	screenX: true,
	screenY: true,
	targetTouches: true,
	toElement: true,
	touches: true,

	which: function( event ) {
		var button = event.button;

		// Add which for key events
		if ( event.which == null && rkeyEvent.test( event.type ) ) {
			return event.charCode != null ? event.charCode : event.keyCode;
		}

		// Add which for click: 1 === left; 2 === middle; 3 === right
		if ( !event.which && button !== undefined && rmouseEvent.test( event.type ) ) {
			if ( button & 1 ) {
				return 1;
			}

			if ( button & 2 ) {
				return 3;
			}

			if ( button & 4 ) {
				return 2;
			}

			return 0;
		}

		return event.which;
	}
}, jQuery.event.addProp );

jQuery.each( { focus: "focusin", blur: "focusout" }, function( type, delegateType ) {
	jQuery.event.special[ type ] = {

		// Utilize native event if possible so blur/focus sequence is correct
		setup: function() {

			// Claim the first handler
			// dataPriv.set( this, "focus", ... )
			// dataPriv.set( this, "blur", ... )
			leverageNative( this, type, expectSync );

			// Return false to allow normal processing in the caller
			return false;
		},
		trigger: function() {

			// Force setup before trigger
			leverageNative( this, type );

			// Return non-false to allow normal event-path propagation
			return true;
		},

		delegateType: delegateType
	};
} );

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://bugs.chromium.org/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mouseenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
} );

jQuery.fn.extend( {

	on: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn );
	},
	one: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {

			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ?
					handleObj.origType + "." + handleObj.namespace :
					handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {

			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {

			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each( function() {
			jQuery.event.remove( this, types, fn, selector );
		} );
	}
} );


var

	// Support: IE <=10 - 11, Edge 12 - 13 only
	// In IE/Edge using regex groups here causes severe slowdowns.
	// See https://connect.microsoft.com/IE/feedback/details/1736512/
	rnoInnerhtml = /<script|<style|<link/i,

	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// Prefer a tbody over its parent table for containing new rows
function manipulationTarget( elem, content ) {
	if ( nodeName( elem, "table" ) &&
		nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {

		return jQuery( elem ).children( "tbody" )[ 0 ] || elem;
	}

	return elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	if ( ( elem.type || "" ).slice( 0, 5 ) === "true/" ) {
		elem.type = elem.type.slice( 5 );
	} else {
		elem.removeAttribute( "type" );
	}

	return elem;
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( dataPriv.hasData( src ) ) {
		pdataOld = dataPriv.get( src );
		events = pdataOld.events;

		if ( events ) {
			dataPriv.remove( dest, "handle events" );

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( dataUser.hasData( src ) ) {
		udataOld = dataUser.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		dataUser.set( dest, udataCur );
	}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

function domManip( collection, args, callback, ignored ) {

	// Flatten any nested arrays
	args = flat( args );

	var fragment, first, scripts, hasScripts, node, doc,
		i = 0,
		l = collection.length,
		iNoClone = l - 1,
		value = args[ 0 ],
		valueIsFunction = isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( valueIsFunction ||
			( l > 1 && typeof value === "string" &&
				!support.checkClone && rchecked.test( value ) ) ) {
		return collection.each( function( index ) {
			var self = collection.eq( index );
			if ( valueIsFunction ) {
				args[ 0 ] = value.call( this, index, self.html() );
			}
			domManip( self, args, callback, ignored );
		} );
	}

	if ( l ) {
		fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
		first = fragment.firstChild;

		if ( fragment.childNodes.length === 1 ) {
			fragment = first;
		}

		// Require either new content or an interest in ignored elements to invoke the callback
		if ( first || ignored ) {
			scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
			hasScripts = scripts.length;

			// Use the original fragment for the last item
			// instead of the first because it can end up
			// being emptied incorrectly in certain situations (#8070).
			for ( ; i < l; i++ ) {
				node = fragment;

				if ( i !== iNoClone ) {
					node = jQuery.clone( node, true, true );

					// Keep references to cloned scripts for later restoration
					if ( hasScripts ) {

						// Support: Android <=4.0 only, PhantomJS 1 only
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( scripts, getAll( node, "script" ) );
					}
				}

				callback.call( collection[ i ], node, i );
			}

			if ( hasScripts ) {
				doc = scripts[ scripts.length - 1 ].ownerDocument;

				// Reenable scripts
				jQuery.map( scripts, restoreScript );

				// Evaluate executable scripts on first document insertion
				for ( i = 0; i < hasScripts; i++ ) {
					node = scripts[ i ];
					if ( rscriptType.test( node.type || "" ) &&
						!dataPriv.access( node, "globalEval" ) &&
						jQuery.contains( doc, node ) ) {

						if ( node.src && ( node.type || "" ).toLowerCase()  !== "module" ) {

							// Optional AJAX dependency, but won't run scripts if not present
							if ( jQuery._evalUrl && !node.noModule ) {
								jQuery._evalUrl( node.src, {
									nonce: node.nonce || node.getAttribute( "nonce" )
								}, doc );
							}
						} else {
							DOMEval( node.textContent.replace( rcleanScript, "" ), node, doc );
						}
					}
				}
			}
		}
	}

	return collection;
}

function remove( elem, selector, keepData ) {
	var node,
		nodes = selector ? jQuery.filter( selector, elem ) : elem,
		i = 0;

	for ( ; ( node = nodes[ i ] ) != null; i++ ) {
		if ( !keepData && node.nodeType === 1 ) {
			jQuery.cleanData( getAll( node ) );
		}

		if ( node.parentNode ) {
			if ( keepData && isAttached( node ) ) {
				setGlobalEval( getAll( node, "script" ) );
			}
			node.parentNode.removeChild( node );
		}
	}

	return elem;
}

jQuery.extend( {
	htmlPrefilter: function( html ) {
		return html;
	},

	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = isAttached( elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	cleanData: function( elems ) {
		var data, elem, type,
			special = jQuery.event.special,
			i = 0;

		for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
			if ( acceptData( elem ) ) {
				if ( ( data = elem[ dataPriv.expando ] ) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataPriv.expando ] = undefined;
				}
				if ( elem[ dataUser.expando ] ) {

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataUser.expando ] = undefined;
				}
			}
		}
	}
} );

jQuery.fn.extend( {
	detach: function( selector ) {
		return remove( this, selector, true );
	},

	remove: function( selector ) {
		return remove( this, selector );
	},

	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each( function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				} );
		}, null, value, arguments.length );
	},

	append: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		} );
	},

	prepend: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		} );
	},

	before: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		} );
	},

	after: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		} );
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; ( elem = this[ i ] ) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map( function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		} );
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = jQuery.htmlPrefilter( value );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch ( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var ignored = [];

		// Make the changes, replacing each non-ignored context element with the new content
		return domManip( this, arguments, function( elem ) {
			var parent = this.parentNode;

			if ( jQuery.inArray( this, ignored ) < 0 ) {
				jQuery.cleanData( getAll( this ) );
				if ( parent ) {
					parent.replaceChild( elem, this );
				}
			}

		// Force callback invocation
		}, ignored );
	}
} );

jQuery.each( {
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: Android <=4.0 only, PhantomJS 1 only
			// .get() because push.apply(_, arraylike) throws on ancient WebKit
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
} );
var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

		// Support: IE <=11 only, Firefox <=30 (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		var view = elem.ownerDocument.defaultView;

		if ( !view || !view.opener ) {
			view = window;
		}

		return view.getComputedStyle( elem );
	};

var swap = function( elem, options, callback ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.call( elem );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};


var rboxStyle = new RegExp( cssExpand.join( "|" ), "i" );



( function() {

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computeStyleTests() {

		// This is a singleton, we need to execute it only once
		if ( !div ) {
			return;
		}

		container.style.cssText = "position:absolute;left:-11111px;width:60px;" +
			"margin-top:1px;padding:0;border:0";
		div.style.cssText =
			"position:relative;display:block;box-sizing:border-box;overflow:scroll;" +
			"margin:auto;border:1px;padding:1px;" +
			"width:60%;top:1%";
		documentElement.appendChild( container ).appendChild( div );

		var divStyle = window.getComputedStyle( div );
		pixelPositionVal = divStyle.top !== "1%";

		// Support: Android 4.0 - 4.3 only, Firefox <=3 - 44
		reliableMarginLeftVal = roundPixelMeasures( divStyle.marginLeft ) === 12;

		// Support: Android 4.0 - 4.3 only, Safari <=9.1 - 10.1, iOS <=7.0 - 9.3
		// Some styles come back with percentage values, even though they shouldn't
		div.style.right = "60%";
		pixelBoxStylesVal = roundPixelMeasures( divStyle.right ) === 36;

		// Support: IE 9 - 11 only
		// Detect misreporting of content dimensions for box-sizing:border-box elements
		boxSizingReliableVal = roundPixelMeasures( divStyle.width ) === 36;

		// Support: IE 9 only
		// Detect overflow:scroll screwiness (gh-3699)
		// Support: Chrome <=64
		// Don't get tricked when zoom affects offsetWidth (gh-4029)
		div.style.position = "absolute";
		scrollboxSizeVal = roundPixelMeasures( div.offsetWidth / 3 ) === 12;

		documentElement.removeChild( container );

		// Nullify the div so it wouldn't be stored in the memory and
		// it will also be a sign that checks already performed
		div = null;
	}

	function roundPixelMeasures( measure ) {
		return Math.round( parseFloat( measure ) );
	}

	var pixelPositionVal, boxSizingReliableVal, scrollboxSizeVal, pixelBoxStylesVal,
		reliableTrDimensionsVal, reliableMarginLeftVal,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	// Finish early in limited (non-browser) environments
	if ( !div.style ) {
		return;
	}

	// Support: IE <=9 - 11 only
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	jQuery.extend( support, {
		boxSizingReliable: function() {
			computeStyleTests();
			return boxSizingReliableVal;
		},
		pixelBoxStyles: function() {
			computeStyleTests();
			return pixelBoxStylesVal;
		},
		pixelPosition: function() {
			computeStyleTests();
			return pixelPositionVal;
		},
		reliableMarginLeft: function() {
			computeStyleTests();
			return reliableMarginLeftVal;
		},
		scrollboxSize: function() {
			computeStyleTests();
			return scrollboxSizeVal;
		},

		// Support: IE 9 - 11+, Edge 15 - 18+
		// IE/Edge misreport `getComputedStyle` of table rows with width/height
		// set in CSS while `offset*` properties report correct values.
		// Behavior in IE 9 is more subtle than in newer versions & it passes
		// some versions of this test; make sure not to make it pass there!
		reliableTrDimensions: function() {
			var table, tr, trChild, trStyle;
			if ( reliableTrDimensionsVal == null ) {
				table = document.createElement( "table" );
				tr = document.createElement( "tr" );
				trChild = document.createElement( "div" );

				table.style.cssText = "position:absolute;left:-11111px";
				tr.style.height = "1px";
				trChild.style.height = "9px";

				documentElement
					.appendChild( table )
					.appendChild( tr )
					.appendChild( trChild );

				trStyle = window.getComputedStyle( tr );
				reliableTrDimensionsVal = parseInt( trStyle.height ) > 3;

				documentElement.removeChild( table );
			}
			return reliableTrDimensionsVal;
		}
	} );
} )();


function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,

		// Support: Firefox 51+
		// Retrieving style before computed somehow
		// fixes an issue with getting wrong values
		// on detached elements
		style = elem.style;

	computed = computed || getStyles( elem );

	// getPropertyValue is needed for:
	//   .css('filter') (IE 9 only, #12537)
	//   .css('--customProperty) (#3144)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];

		if ( ret === "" && !isAttached( elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// A tribute to the "awesome hack by Dean Edwards"
		// Android Browser returns percentage for some values,
		// but width seems to be reliably pixels.
		// This is against the CSSOM draft spec:
		// https://drafts.csswg.org/cssom/#resolved-values
		if ( !support.pixelBoxStyles() && rnumnonpx.test( ret ) && rboxStyle.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?

		// Support: IE <=9 - 11 only
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {

	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {

				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return ( this.get = hookFn ).apply( this, arguments );
		}
	};
}


var cssPrefixes = [ "Webkit", "Moz", "ms" ],
	emptyStyle = document.createElement( "div" ).style,
	vendorProps = {};

// Return a vendor-prefixed property or undefined
function vendorPropName( name ) {

	// Check for vendor prefixed names
	var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in emptyStyle ) {
			return name;
		}
	}
}

// Return a potentially-mapped jQuery.cssProps or vendor prefixed property
function finalPropName( name ) {
	var final = jQuery.cssProps[ name ] || vendorProps[ name ];

	if ( final ) {
		return final;
	}
	if ( name in emptyStyle ) {
		return name;
	}
	return vendorProps[ name ] = vendorPropName( name ) || name;
}


var

	// Swappable if display is none or starts with table
	// except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rcustomProp = /^--/,
	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	};

function setPositiveNumber( _elem, value, subtract ) {

	// Any relative (+/-) values have already been
	// normalized at this point
	var matches = rcssNum.exec( value );
	return matches ?

		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
		value;
}

function boxModelAdjustment( elem, dimension, box, isBorderBox, styles, computedVal ) {
	var i = dimension === "width" ? 1 : 0,
		extra = 0,
		delta = 0;

	// Adjustment may not be necessary
	if ( box === ( isBorderBox ? "border" : "content" ) ) {
		return 0;
	}

	for ( ; i < 4; i += 2 ) {

		// Both box models exclude margin
		if ( box === "margin" ) {
			delta += jQuery.css( elem, box + cssExpand[ i ], true, styles );
		}

		// If we get here with a content-box, we're seeking "padding" or "border" or "margin"
		if ( !isBorderBox ) {

			// Add padding
			delta += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// For "border" or "margin", add border
			if ( box !== "padding" ) {
				delta += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );

			// But still keep track of it otherwise
			} else {
				extra += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}

		// If we get here with a border-box (content + padding + border), we're seeking "content" or
		// "padding" or "margin"
		} else {

			// For "content", subtract padding
			if ( box === "content" ) {
				delta -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// For "content" or "padding", subtract border
			if ( box !== "margin" ) {
				delta -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	// Account for positive content-box scroll gutter when requested by providing computedVal
	if ( !isBorderBox && computedVal >= 0 ) {

		// offsetWidth/offsetHeight is a rounded sum of content, padding, scroll gutter, and border
		// Assuming integer scroll gutter, subtract the rest and round down
		delta += Math.max( 0, Math.ceil(
			elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
			computedVal -
			delta -
			extra -
			0.5

		// If offsetWidth/offsetHeight is unknown, then we can't determine content-box scroll gutter
		// Use an explicit zero to avoid NaN (gh-3964)
		) ) || 0;
	}

	return delta;
}

function getWidthOrHeight( elem, dimension, extra ) {

	// Start with computed style
	var styles = getStyles( elem ),

		// To avoid forcing a reflow, only fetch boxSizing if we need it (gh-4322).
		// Fake content-box until we know it's needed to know the true value.
		boxSizingNeeded = !support.boxSizingReliable() || extra,
		isBorderBox = boxSizingNeeded &&
			jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
		valueIsBorderBox = isBorderBox,

		val = curCSS( elem, dimension, styles ),
		offsetProp = "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 );

	// Support: Firefox <=54
	// Return a confounding non-pixel value or feign ignorance, as appropriate.
	if ( rnumnonpx.test( val ) ) {
		if ( !extra ) {
			return val;
		}
		val = "auto";
	}


	// Support: IE 9 - 11 only
	// Use offsetWidth/offsetHeight for when box sizing is unreliable.
	// In those cases, the computed value can be trusted to be border-box.
	if ( ( !support.boxSizingReliable() && isBorderBox ||

		// Support: IE 10 - 11+, Edge 15 - 18+
		// IE/Edge misreport `getComputedStyle` of table rows with width/height
		// set in CSS while `offset*` properties report correct values.
		// Interestingly, in some cases IE 9 doesn't suffer from this issue.
		!support.reliableTrDimensions() && nodeName( elem, "tr" ) ||

		// Fall back to offsetWidth/offsetHeight when value is "auto"
		// This happens for inline elements with no explicit setting (gh-3571)
		val === "auto" ||

		// Support: Android <=4.1 - 4.3 only
		// Also use offsetWidth/offsetHeight for misreported inline dimensions (gh-3602)
		!parseFloat( val ) && jQuery.css( elem, "display", false, styles ) === "inline" ) &&

		// Make sure the element is visible & connected
		elem.getClientRects().length ) {

		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

		// Where available, offsetWidth/offsetHeight approximate border box dimensions.
		// Where not available (e.g., SVG), assume unreliable box-sizing and interpret the
		// retrieved value as a content box dimension.
		valueIsBorderBox = offsetProp in elem;
		if ( valueIsBorderBox ) {
			val = elem[ offsetProp ];
		}
	}

	// Normalize "" and auto
	val = parseFloat( val ) || 0;

	// Adjust for the element's box model
	return ( val +
		boxModelAdjustment(
			elem,
			dimension,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles,

			// Provide the current computed size to request scroll gutter calculation (gh-3589)
			val
		)
	) + "px";
}

jQuery.extend( {

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"animationIterationCount": true,
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"gridArea": true,
		"gridColumn": true,
		"gridColumnEnd": true,
		"gridColumnStart": true,
		"gridRow": true,
		"gridRowEnd": true,
		"gridRowStart": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = camelCase( name ),
			isCustomProp = rcustomProp.test( name ),
			style = elem.style;

		// Make sure that we're working with the right name. We don't
		// want to query the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
				value = adjustCSS( elem, name, ret );

				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add the unit (except for certain CSS properties)
			// The isCustomProp check can be removed in jQuery 4.0 when we only auto-append
			// "px" to a few hardcoded values.
			if ( type === "number" && !isCustomProp ) {
				value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
			}

			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !( "set" in hooks ) ||
				( value = hooks.set( elem, value, extra ) ) !== undefined ) {

				if ( isCustomProp ) {
					style.setProperty( name, value );
				} else {
					style[ name ] = value;
				}
			}

		} else {

			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks &&
				( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = camelCase( name ),
			isCustomProp = rcustomProp.test( name );

		// Make sure that we're working with the right name. We don't
		// want to modify the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || isFinite( num ) ? num || 0 : val;
		}

		return val;
	}
} );

jQuery.each( [ "height", "width" ], function( _i, dimension ) {
	jQuery.cssHooks[ dimension ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&

					// Support: Safari 8+
					// Table columns in Safari have non-zero offsetWidth & zero
					// getBoundingClientRect().width unless display is changed.
					// Support: IE <=11 only
					// Running getBoundingClientRect on a disconnected node
					// in IE throws an error.
					( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
						swap( elem, cssShow, function() {
							return getWidthOrHeight( elem, dimension, extra );
						} ) :
						getWidthOrHeight( elem, dimension, extra );
			}
		},

		set: function( elem, value, extra ) {
			var matches,
				styles = getStyles( elem ),

				// Only read styles.position if the test has a chance to fail
				// to avoid forcing a reflow.
				scrollboxSizeBuggy = !support.scrollboxSize() &&
					styles.position === "absolute",

				// To avoid forcing a reflow, only fetch boxSizing if we need it (gh-3991)
				boxSizingNeeded = scrollboxSizeBuggy || extra,
				isBorderBox = boxSizingNeeded &&
					jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
				subtract = extra ?
					boxModelAdjustment(
						elem,
						dimension,
						extra,
						isBorderBox,
						styles
					) :
					0;

			// Account for unreliable border-box dimensions by comparing offset* to computed and
			// faking a content-box to get border and padding (gh-3699)
			if ( isBorderBox && scrollboxSizeBuggy ) {
				subtract -= Math.ceil(
					elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
					parseFloat( styles[ dimension ] ) -
					boxModelAdjustment( elem, dimension, "border", false, styles ) -
					0.5
				);
			}

			// Convert to pixels if value adjustment is needed
			if ( subtract && ( matches = rcssNum.exec( value ) ) &&
				( matches[ 3 ] || "px" ) !== "px" ) {

				elem.style[ dimension ] = value;
				value = jQuery.css( elem, dimension );
			}

			return setPositiveNumber( elem, value, subtract );
		}
	};
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
	function( elem, computed ) {
		if ( computed ) {
			return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
				elem.getBoundingClientRect().left -
					swap( elem, { marginLeft: 0 }, function() {
						return elem.getBoundingClientRect().left;
					} )
				) + "px";
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each( {
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split( " " ) : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( prefix !== "margin" ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
} );

jQuery.fn.extend( {
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( Array.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	}
} );


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || jQuery.easing._default;
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			// Use a property on the element directly when it is not a DOM element,
			// or when there is no matching style property that exists.
			if ( tween.elem.nodeType !== 1 ||
				tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );

			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {

			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.nodeType === 1 && (
					jQuery.cssHooks[ tween.prop ] ||
					tween.elem.style[ finalPropName( tween.prop ) ] != null ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE <=9 only
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	},
	_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, inProgress,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rrun = /queueHooks$/;

function schedule() {
	if ( inProgress ) {
		if ( document.hidden === false && window.requestAnimationFrame ) {
			window.requestAnimationFrame( schedule );
		} else {
			window.setTimeout( schedule, jQuery.fx.interval );
		}

		jQuery.fx.tick();
	}
}

// Animations created synchronously will run synchronously
function createFxNow() {
	window.setTimeout( function() {
		fxNow = undefined;
	} );
	return ( fxNow = Date.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
		isBox = "width" in props || "height" in props,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHiddenWithinTree( elem ),
		dataShow = dataPriv.get( elem, "fxshow" );

	// Queue-skipping animations hijack the fx hooks
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always( function() {

			// Ensure the complete handler is called before this completes
			anim.always( function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			} );
		} );
	}

	// Detect show/hide animations
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.test( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// Pretend to be hidden if this is a "show" and
				// there is still data from a stopped show/hide
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;

				// Ignore all other no-op show/hide data
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
		}
	}

	// Bail out if this is a no-op like .hide().hide()
	propTween = !jQuery.isEmptyObject( props );
	if ( !propTween && jQuery.isEmptyObject( orig ) ) {
		return;
	}

	// Restrict "overflow" and "display" styles during box animations
	if ( isBox && elem.nodeType === 1 ) {

		// Support: IE <=9 - 11, Edge 12 - 15
		// Record all 3 overflow attributes because IE does not infer the shorthand
		// from identically-valued overflowX and overflowY and Edge just mirrors
		// the overflowX value there.
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Identify a display type, preferring old show/hide data over the CSS cascade
		restoreDisplay = dataShow && dataShow.display;
		if ( restoreDisplay == null ) {
			restoreDisplay = dataPriv.get( elem, "display" );
		}
		display = jQuery.css( elem, "display" );
		if ( display === "none" ) {
			if ( restoreDisplay ) {
				display = restoreDisplay;
			} else {

				// Get nonempty value(s) by temporarily forcing visibility
				showHide( [ elem ], true );
				restoreDisplay = elem.style.display || restoreDisplay;
				display = jQuery.css( elem, "display" );
				showHide( [ elem ] );
			}
		}

		// Animate inline elements as inline-block
		if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
			if ( jQuery.css( elem, "float" ) === "none" ) {

				// Restore the original display value at the end of pure show/hide animations
				if ( !propTween ) {
					anim.done( function() {
						style.display = restoreDisplay;
					} );
					if ( restoreDisplay == null ) {
						display = style.display;
						restoreDisplay = display === "none" ? "" : display;
					}
				}
				style.display = "inline-block";
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always( function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		} );
	}

	// Implement show/hide animations
	propTween = false;
	for ( prop in orig ) {

		// General show/hide setup for this element animation
		if ( !propTween ) {
			if ( dataShow ) {
				if ( "hidden" in dataShow ) {
					hidden = dataShow.hidden;
				}
			} else {
				dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
			}

			// Store hidden/visible for toggle so `.stop().toggle()` "reverses"
			if ( toggle ) {
				dataShow.hidden = !hidden;
			}

			// Show elements before animating them
			if ( hidden ) {
				showHide( [ elem ], true );
			}

			/* eslint-disable no-loop-func */

			anim.done( function() {

			/* eslint-enable no-loop-func */

				// The final step of a "hide" animation is actually hiding the element
				if ( !hidden ) {
					showHide( [ elem ] );
				}
				dataPriv.remove( elem, "fxshow" );
				for ( prop in orig ) {
					jQuery.style( elem, prop, orig[ prop ] );
				}
			} );
		}

		// Per-property setup
		propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
		if ( !( prop in dataShow ) ) {
			dataShow[ prop ] = propTween.start;
			if ( hidden ) {
				propTween.end = propTween.start;
				propTween.start = 0;
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( Array.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = Animation.prefilters.length,
		deferred = jQuery.Deferred().always( function() {

			// Don't match elem in the :animated selector
			delete tick.elem;
		} ),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

				// Support: Android 2.3 only
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ] );

			// If there's more to do, yield
			if ( percent < 1 && length ) {
				return remaining;
			}

			// If this was an empty animation, synthesize a final progress notification
			if ( !length ) {
				deferred.notifyWith( elem, [ animation, 1, 0 ] );
			}

			// Resolve the animation and report its conclusion
			deferred.resolveWith( elem, [ animation ] );
			return false;
		},
		animation = deferred.promise( {
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, {
				specialEasing: {},
				easing: jQuery.easing._default
			}, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,

					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.notifyWith( elem, [ animation, 1, 0 ] );
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		} ),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length; index++ ) {
		result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			if ( isFunction( result.stop ) ) {
				jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
					result.stop.bind( result );
			}
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	// Attach callbacks from options
	animation
		.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		} )
	);

	return animation;
}

jQuery.Animation = jQuery.extend( Animation, {

	tweeners: {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value );
			adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
			return tween;
		} ]
	},

	tweener: function( props, callback ) {
		if ( isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.match( rnothtmlwhite );
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length; index++ ) {
			prop = props[ index ];
			Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
			Animation.tweeners[ prop ].unshift( callback );
		}
	},

	prefilters: [ defaultPrefilter ],

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			Animation.prefilters.unshift( callback );
		} else {
			Animation.prefilters.push( callback );
		}
	}
} );

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !isFunction( easing ) && easing
	};

	// Go to the end state if fx are off
	if ( jQuery.fx.off ) {
		opt.duration = 0;

	} else {
		if ( typeof opt.duration !== "number" ) {
			if ( opt.duration in jQuery.fx.speeds ) {
				opt.duration = jQuery.fx.speeds[ opt.duration ];

			} else {
				opt.duration = jQuery.fx.speeds._default;
			}
		}
	}

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend( {
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHiddenWithinTree ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate( { opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {

				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || dataPriv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue ) {
			this.queue( type || "fx", [] );
		}

		return this.each( function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = dataPriv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this &&
					( type == null || timers[ index ].queue === type ) ) {

					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		} );
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each( function() {
			var index,
				data = dataPriv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		} );
	}
} );

jQuery.each( [ "toggle", "show", "hide" ], function( _i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
} );

// Generate shortcuts for custom animations
jQuery.each( {
	slideDown: genFx( "show" ),
	slideUp: genFx( "hide" ),
	slideToggle: genFx( "toggle" ),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = Date.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];

		// Run the timer and safely remove it when done (allowing for external removal)
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	jQuery.fx.start();
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
	if ( inProgress ) {
		return;
	}

	inProgress = true;
	schedule();
};

jQuery.fx.stop = function() {
	inProgress = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,

	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// https://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = window.setTimeout( next, time );
		hooks.stop = function() {
			window.clearTimeout( timeout );
		};
	} );
};


( function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: Android <=4.3 only
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE <=11 only
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: IE <=11 only
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
} )();


var boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each( function() {
			jQuery.removeAttr( this, name );
		} );
	}
} );

jQuery.extend( {
	attr: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set attributes on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		// Attribute hooks are determined by the lowercase version
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			hooks = jQuery.attrHooks[ name.toLowerCase() ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
		}

		if ( value !== undefined ) {
			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;
			}

			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			elem.setAttribute( name, value + "" );
			return value;
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		ret = jQuery.find.attr( elem, name );

		// Non-existent attributes return null, we normalize to undefined
		return ret == null ? undefined : ret;
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	},

	removeAttr: function( elem, value ) {
		var name,
			i = 0,

			// Attribute names can contain non-HTML whitespace characters
			// https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
			attrNames = value && value.match( rnothtmlwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( ( name = attrNames[ i++ ] ) ) {
				elem.removeAttribute( name );
			}
		}
	}
} );

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {

			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};

jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( _i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle,
			lowercaseName = name.toLowerCase();

		if ( !isXML ) {

			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ lowercaseName ];
			attrHandle[ lowercaseName ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				lowercaseName :
				null;
			attrHandle[ lowercaseName ] = handle;
		}
		return ret;
	};
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each( function() {
			delete this[ jQuery.propFix[ name ] || name ];
		} );
	}
} );

jQuery.extend( {
	prop: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			return ( elem[ name ] = value );
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		return elem[ name ];
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {

				// Support: IE <=9 - 11 only
				// elem.tabIndex doesn't always return the
				// correct value when it hasn't been explicitly set
				// https://web.archive.org/web/20141116233347/http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				if ( tabindex ) {
					return parseInt( tabindex, 10 );
				}

				if (
					rfocusable.test( elem.nodeName ) ||
					rclickable.test( elem.nodeName ) &&
					elem.href
				) {
					return 0;
				}

				return -1;
			}
		}
	},

	propFix: {
		"for": "htmlFor",
		"class": "className"
	}
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
// eslint rule "no-unused-expressions" is disabled for this code
// since it considers such accessions noop
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		},
		set: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent ) {
				parent.selectedIndex;

				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
		}
	};
}

jQuery.each( [
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
} );




	// Strip and collapse whitespace according to HTML spec
	// https://infra.spec.whatwg.org/#strip-and-collapse-ascii-whitespace
	function stripAndCollapse( value ) {
		var tokens = value.match( rnothtmlwhite ) || [];
		return tokens.join( " " );
	}


function getClass( elem ) {
	return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

function classesToArray( value ) {
	if ( Array.isArray( value ) ) {
		return value;
	}
	if ( typeof value === "string" ) {
		return value.match( rnothtmlwhite ) || [];
	}
	return [];
}

jQuery.fn.extend( {
	addClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		classes = classesToArray( value );

		if ( classes.length ) {
			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( !arguments.length ) {
			return this.attr( "class", "" );
		}

		classes = classesToArray( value );

		if ( classes.length ) {
			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );

				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {

						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value,
			isValidValue = type === "string" || Array.isArray( value );

		if ( typeof stateVal === "boolean" && isValidValue ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( isFunction( value ) ) {
			return this.each( function( i ) {
				jQuery( this ).toggleClass(
					value.call( this, i, getClass( this ), stateVal ),
					stateVal
				);
			} );
		}

		return this.each( function() {
			var className, i, self, classNames;

			if ( isValidValue ) {

				// Toggle individual class names
				i = 0;
				self = jQuery( this );
				classNames = classesToArray( value );

				while ( ( className = classNames[ i++ ] ) ) {

					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( value === undefined || type === "boolean" ) {
				className = getClass( this );
				if ( className ) {

					// Store className if set
					dataPriv.set( this, "__className__", className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				if ( this.setAttribute ) {
					this.setAttribute( "class",
						className || value === false ?
						"" :
						dataPriv.get( this, "__className__" ) || ""
					);
				}
			}
		} );
	},

	hasClass: function( selector ) {
		var className, elem,
			i = 0;

		className = " " + selector + " ";
		while ( ( elem = this[ i++ ] ) ) {
			if ( elem.nodeType === 1 &&
				( " " + stripAndCollapse( getClass( elem ) ) + " " ).indexOf( className ) > -1 ) {
					return true;
			}
		}

		return false;
	}
} );




var rreturn = /\r/g;

jQuery.fn.extend( {
	val: function( value ) {
		var hooks, ret, valueIsFunction,
			elem = this[ 0 ];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] ||
					jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks &&
					"get" in hooks &&
					( ret = hooks.get( elem, "value" ) ) !== undefined
				) {
					return ret;
				}

				ret = elem.value;

				// Handle most common string cases
				if ( typeof ret === "string" ) {
					return ret.replace( rreturn, "" );
				}

				// Handle cases where value is null/undef or number
				return ret == null ? "" : ret;
			}

			return;
		}

		valueIsFunction = isFunction( value );

		return this.each( function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( valueIsFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( Array.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				} );
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		} );
	}
} );

jQuery.extend( {
	valHooks: {
		option: {
			get: function( elem ) {

				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :

					// Support: IE <=10 - 11 only
					// option.text throws exceptions (#14686, #14858)
					// Strip and collapse whitespace
					// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
					stripAndCollapse( jQuery.text( elem ) );
			}
		},
		select: {
			get: function( elem ) {
				var value, option, i,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one",
					values = one ? null : [],
					max = one ? index + 1 : options.length;

				if ( index < 0 ) {
					i = max;

				} else {
					i = one ? index : 0;
				}

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// Support: IE <=9 only
					// IE8-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&

							// Don't return options that are disabled or in a disabled optgroup
							!option.disabled &&
							( !option.parentNode.disabled ||
								!nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];

					/* eslint-disable no-cond-assign */

					if ( option.selected =
						jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
					) {
						optionSet = true;
					}

					/* eslint-enable no-cond-assign */
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( Array.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute( "value" ) === null ? "on" : elem.value;
		};
	}
} );




// Return jQuery for attributes-only inclusion


support.focusin = "onfocusin" in window;


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	stopPropagationCallback = function( e ) {
		e.stopPropagation();
	};

jQuery.extend( jQuery.event, {

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special, lastElement,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

		cur = lastElement = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf( "." ) > -1 ) {

			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split( "." );
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf( ":" ) < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join( "." );
		event.rnamespace = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === ( elem.ownerDocument || document ) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {
			lastElement = cur;
			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = (
					dataPriv.get( cur, "events" ) || Object.create( null )
				)[ event.type ] &&
				dataPriv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( ( !special._default ||
				special._default.apply( eventPath.pop(), data ) === false ) &&
				acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && isFunction( elem[ type ] ) && !isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;

					if ( event.isPropagationStopped() ) {
						lastElement.addEventListener( type, stopPropagationCallback );
					}

					elem[ type ]();

					if ( event.isPropagationStopped() ) {
						lastElement.removeEventListener( type, stopPropagationCallback );
					}

					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	// Piggyback on a donor event to simulate a different one
	// Used only for `focus(in | out)` events
	simulate: function( type, elem, event ) {
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true
			}
		);

		jQuery.event.trigger( e, null, elem );
	}

} );

jQuery.fn.extend( {

	trigger: function( type, data ) {
		return this.each( function() {
			jQuery.event.trigger( type, data, this );
		} );
	},
	triggerHandler: function( type, data ) {
		var elem = this[ 0 ];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
} );


// Support: Firefox <=44
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
	jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
			jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
		};

		jQuery.event.special[ fix ] = {
			setup: function() {

				// Handle: regular nodes (via `this.ownerDocument`), window
				// (via `this.document`) & document (via `this`).
				var doc = this.ownerDocument || this.document || this,
					attaches = dataPriv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this.document || this,
					attaches = dataPriv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					dataPriv.remove( doc, fix );

				} else {
					dataPriv.access( doc, fix, attaches );
				}
			}
		};
	} );
}
var location = window.location;

var nonce = { guid: Date.now() };

var rquery = ( /\?/ );



// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE 9 - 11 only
	// IE throws on parseFromString with invalid input.
	try {
		xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( Array.isArray( obj ) ) {

		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {

				// Treat each array item as a scalar.
				add( prefix, v );

			} else {

				// Item is non-scalar (array or object), encode its numeric index.
				buildParams(
					prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
					v,
					traditional,
					add
				);
			}
		} );

	} else if ( !traditional && toType( obj ) === "object" ) {

		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {

		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, valueOrFunction ) {

			// If value is a function, invoke it and use its return value
			var value = isFunction( valueOrFunction ) ?
				valueOrFunction() :
				valueOrFunction;

			s[ s.length ] = encodeURIComponent( key ) + "=" +
				encodeURIComponent( value == null ? "" : value );
		};

	if ( a == null ) {
		return "";
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( Array.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		} );

	} else {

		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" );
};

jQuery.fn.extend( {
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map( function() {

			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		} )
		.filter( function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		} )
		.map( function( _i, elem ) {
			var val = jQuery( this ).val();

			if ( val == null ) {
				return null;
			}

			if ( Array.isArray( val ) ) {
				return jQuery.map( val, function( val ) {
					return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
				} );
			}

			return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		} ).get();
	}
} );


var
	r20 = /%20/g,
	rhash = /#.*$/,
	rantiCache = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Anchor tag for parsing the document origin
	originAnchor = document.createElement( "a" );
	originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnothtmlwhite ) || [];

		if ( isFunction( func ) ) {

			// For each dataType in the dataTypeExpression
			while ( ( dataType = dataTypes[ i++ ] ) ) {

				// Prepend if requested
				if ( dataType[ 0 ] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

				// Otherwise append
				} else {
					( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" &&
				!seekingTransport && !inspected[ dataTypeOrTransport ] ) {

				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		} );
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {

		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}

		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},

		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

			// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {

								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s.throws ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return {
								state: "parsererror",
								error: conv ? e : "No conversion from " + prev + " to " + current
							};
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend( {

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: location.href,
		type: "GET",
		isLocal: rlocalProtocol.test( location.protocol ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",

		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /\bxml\b/,
			html: /\bhtml/,
			json: /\bjson\b/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": JSON.parse,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,

			// URL without anti-cache param
			cacheURL,

			// Response headers
			responseHeadersString,
			responseHeaders,

			// timeout handle
			timeoutTimer,

			// Url cleanup var
			urlAnchor,

			// Request state (becomes false upon send and true upon completion)
			completed,

			// To know if global events are to be dispatched
			fireGlobals,

			// Loop variable
			i,

			// uncached part of the url
			uncached,

			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),

			// Callbacks context
			callbackContext = s.context || s,

			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context &&
				( callbackContext.nodeType || callbackContext.jquery ) ?
					jQuery( callbackContext ) :
					jQuery.event,

			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks( "once memory" ),

			// Status-dependent callbacks
			statusCode = s.statusCode || {},

			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},

			// Default abort message
			strAbort = "canceled",

			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( completed ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
								responseHeaders[ match[ 1 ].toLowerCase() + " " ] =
									( responseHeaders[ match[ 1 ].toLowerCase() + " " ] || [] )
										.concat( match[ 2 ] );
							}
						}
						match = responseHeaders[ key.toLowerCase() + " " ];
					}
					return match == null ? null : match.join( ", " );
				},

				// Raw string
				getAllResponseHeaders: function() {
					return completed ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					if ( completed == null ) {
						name = requestHeadersNames[ name.toLowerCase() ] =
							requestHeadersNames[ name.toLowerCase() ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( completed == null ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( completed ) {

							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						} else {

							// Lazy-add the new callbacks in a way that preserves old ones
							for ( code in map ) {
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR );

		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || location.href ) + "" )
			.replace( rprotocol, location.protocol + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = ( s.dataType || "*" ).toLowerCase().match( rnothtmlwhite ) || [ "" ];

		// A cross-domain request is in order when the origin doesn't match the current origin.
		if ( s.crossDomain == null ) {
			urlAnchor = document.createElement( "a" );

			// Support: IE <=8 - 11, Edge 12 - 15
			// IE throws exception on accessing the href property if url is malformed,
			// e.g. http://example.com:80x/
			try {
				urlAnchor.href = s.url;

				// Support: IE <=8 - 11 only
				// Anchor's host property isn't correctly set when s.url is relative
				urlAnchor.href = urlAnchor.href;
				s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
					urlAnchor.protocol + "//" + urlAnchor.host;
			} catch ( e ) {

				// If there is an error parsing the URL, assume it is crossDomain,
				// it can be rejected by the transport if it is invalid
				s.crossDomain = true;
			}
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( completed ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger( "ajaxStart" );
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		// Remove hash to simplify url manipulation
		cacheURL = s.url.replace( rhash, "" );

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// Remember the hash so we can put it back
			uncached = s.url.slice( cacheURL.length );

			// If data is available and should be processed, append data to url
			if ( s.data && ( s.processData || typeof s.data === "string" ) ) {
				cacheURL += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data;

				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add or update anti-cache param if needed
			if ( s.cache === false ) {
				cacheURL = cacheURL.replace( rantiCache, "$1" );
				uncached = ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ( nonce.guid++ ) +
					uncached;
			}

			// Put hash and anti-cache on the URL that will be requested (gh-1732)
			s.url = cacheURL + uncached;

		// Change '%20' to '+' if this is encoded form body content (gh-2658)
		} else if ( s.data && s.processData &&
			( s.contentType || "" ).indexOf( "application/x-www-form-urlencoded" ) === 0 ) {
			s.data = s.data.replace( r20, "+" );
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
				s.accepts[ s.dataTypes[ 0 ] ] +
					( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend &&
			( s.beforeSend.call( callbackContext, jqXHR, s ) === false || completed ) ) {

			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		completeDeferred.add( s.complete );
		jqXHR.done( s.success );
		jqXHR.fail( s.error );

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}

			// If request was aborted inside ajaxSend, stop there
			if ( completed ) {
				return jqXHR;
			}

			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = window.setTimeout( function() {
					jqXHR.abort( "timeout" );
				}, s.timeout );
			}

			try {
				completed = false;
				transport.send( requestHeaders, done );
			} catch ( e ) {

				// Rethrow post-completion exceptions
				if ( completed ) {
					throw e;
				}

				// Propagate others as results
				done( -1, e );
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Ignore repeat invocations
			if ( completed ) {
				return;
			}

			completed = true;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				window.clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Use a noop converter for missing script
			if ( !isSuccess && jQuery.inArray( "script", s.dataTypes ) > -1 ) {
				s.converters[ "text script" ] = function() {};
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader( "Last-Modified" );
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader( "etag" );
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {

				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger( "ajaxStop" );
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
} );

jQuery.each( [ "get", "post" ], function( _i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {

		// Shift arguments if data argument was omitted
		if ( isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		// The url can be an options object (which then must have .url)
		return jQuery.ajax( jQuery.extend( {
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		}, jQuery.isPlainObject( url ) && url ) );
	};
} );

jQuery.ajaxPrefilter( function( s ) {
	var i;
	for ( i in s.headers ) {
		if ( i.toLowerCase() === "content-type" ) {
			s.contentType = s.headers[ i ] || "";
		}
	}
} );


jQuery._evalUrl = function( url, options, doc ) {
	return jQuery.ajax( {
		url: url,

		// Make this explicit, since user can override this through ajaxSetup (#11264)
		type: "GET",
		dataType: "script",
		cache: true,
		async: false,
		global: false,

		// Only evaluate the response if it is successful (gh-4126)
		// dataFilter is not invoked for failure responses, so using it instead
		// of the default converter is kludgy but it works.
		converters: {
			"text script": function() {}
		},
		dataFilter: function( response ) {
			jQuery.globalEval( response, options, doc );
		}
	} );
};


jQuery.fn.extend( {
	wrapAll: function( html ) {
		var wrap;

		if ( this[ 0 ] ) {
			if ( isFunction( html ) ) {
				html = html.call( this[ 0 ] );
			}

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map( function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			} ).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapInner( html.call( this, i ) );
			} );
		}

		return this.each( function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		} );
	},

	wrap: function( html ) {
		var htmlIsFunction = isFunction( html );

		return this.each( function( i ) {
			jQuery( this ).wrapAll( htmlIsFunction ? html.call( this, i ) : html );
		} );
	},

	unwrap: function( selector ) {
		this.parent( selector ).not( "body" ).each( function() {
			jQuery( this ).replaceWith( this.childNodes );
		} );
		return this;
	}
} );


jQuery.expr.pseudos.hidden = function( elem ) {
	return !jQuery.expr.pseudos.visible( elem );
};
jQuery.expr.pseudos.visible = function( elem ) {
	return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
};




jQuery.ajaxSettings.xhr = function() {
	try {
		return new window.XMLHttpRequest();
	} catch ( e ) {}
};

var xhrSuccessStatus = {

		// File protocol always yields status code 0, assume 200
		0: 200,

		// Support: IE <=9 only
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
	var callback, errorCallback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr();

				xhr.open(
					options.type,
					options.url,
					options.async,
					options.username,
					options.password
				);

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
					headers[ "X-Requested-With" ] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							callback = errorCallback = xhr.onload =
								xhr.onerror = xhr.onabort = xhr.ontimeout =
									xhr.onreadystatechange = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {

								// Support: IE <=9 only
								// On a manual native abort, IE9 throws
								// errors on any property access that is not readyState
								if ( typeof xhr.status !== "number" ) {
									complete( 0, "error" );
								} else {
									complete(

										// File: protocol always yields status 0; see #8605, #14207
										xhr.status,
										xhr.statusText
									);
								}
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,

									// Support: IE <=9 only
									// IE9 has no XHR2 but throws on binary (trac-11426)
									// For XHR2 non-text, let the caller handle it (gh-2498)
									( xhr.responseType || "text" ) !== "text"  ||
									typeof xhr.responseText !== "string" ?
										{ binary: xhr.response } :
										{ text: xhr.responseText },
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				errorCallback = xhr.onerror = xhr.ontimeout = callback( "error" );

				// Support: IE 9 only
				// Use onreadystatechange to replace onabort
				// to handle uncaught aborts
				if ( xhr.onabort !== undefined ) {
					xhr.onabort = errorCallback;
				} else {
					xhr.onreadystatechange = function() {

						// Check readyState before timeout as it changes
						if ( xhr.readyState === 4 ) {

							// Allow onerror to be called first,
							// but that will not handle a native abort
							// Also, save errorCallback to a variable
							// as xhr.onerror cannot be accessed
							window.setTimeout( function() {
								if ( callback ) {
									errorCallback();
								}
							} );
						}
					};
				}

				// Create the abort callback
				callback = callback( "abort" );

				try {

					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {

					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




// Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)
jQuery.ajaxPrefilter( function( s ) {
	if ( s.crossDomain ) {
		s.contents.script = false;
	}
} );

// Install script dataType
jQuery.ajaxSetup( {
	accepts: {
		script: "text/javascript, application/javascript, " +
			"application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /\b(?:java|ecma)script\b/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

	// This transport only deals with cross domain or forced-by-attrs requests
	if ( s.crossDomain || s.scriptAttrs ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery( "<script>" )
					.attr( s.scriptAttrs || {} )
					.prop( { charset: s.scriptCharset, src: s.url } )
					.on( "load error", callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					} );

				// Use native DOM manipulation to avoid our domManip AJAX trickery
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce.guid++ ) );
		this[ callback ] = true;
		return callback;
	}
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" &&
				( s.contentType || "" )
					.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
				rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters[ "script json" ] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// Force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always( function() {

			// If previous value didn't exist - remove it
			if ( overwritten === undefined ) {
				jQuery( window ).removeProp( callbackName );

			// Otherwise restore preexisting value
			} else {
				window[ callbackName ] = overwritten;
			}

			// Save back as free
			if ( s[ callbackName ] ) {

				// Make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// Save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		} );

		// Delegate to script
		return "script";
	}
} );




// Support: Safari 8 only
// In Safari 8 documents created via document.implementation.createHTMLDocument
// collapse sibling forms: the second one becomes a child of the first one.
// Because of that, this security measure has to be disabled in Safari 8.
// https://bugs.webkit.org/show_bug.cgi?id=137337
support.createHTMLDocument = ( function() {
	var body = document.implementation.createHTMLDocument( "" ).body;
	body.innerHTML = "<form></form><form></form>";
	return body.childNodes.length === 2;
} )();


// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( typeof data !== "string" ) {
		return [];
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}

	var base, parsed, scripts;

	if ( !context ) {

		// Stop scripts or inline event handlers from being executed immediately
		// by using document.implementation
		if ( support.createHTMLDocument ) {
			context = document.implementation.createHTMLDocument( "" );

			// Set the base href for the created document
			// so any parsed elements with URLs
			// are based on the document's URL (gh-2965)
			base = context.createElement( "base" );
			base.href = document.location.href;
			context.head.appendChild( base );
		} else {
			context = document;
		}
	}

	parsed = rsingleTag.exec( data );
	scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[ 1 ] ) ];
	}

	parsed = buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	var selector, type, response,
		self = this,
		off = url.indexOf( " " );

	if ( off > -1 ) {
		selector = stripAndCollapse( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax( {
			url: url,

			// If "type" variable is undefined, then "GET" method will be used.
			// Make value of this field explicit since
			// user can override it through ajaxSetup method
			type: type || "GET",
			dataType: "html",
			data: params
		} ).done( function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		// If the request succeeds, this function gets "data", "status", "jqXHR"
		// but they are ignored because response was set above.
		// If it fails, this function gets "jqXHR", "status", "error"
		} ).always( callback && function( jqXHR, status ) {
			self.each( function() {
				callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
			} );
		} );
	}

	return this;
};




jQuery.expr.pseudos.animated = function( elem ) {
	return jQuery.grep( jQuery.timers, function( fn ) {
		return elem === fn.elem;
	} ).length;
};




jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( isFunction( options ) ) {

			// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
			options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			if ( typeof props.top === "number" ) {
				props.top += "px";
			}
			if ( typeof props.left === "number" ) {
				props.left += "px";
			}
			curElem.css( props );
		}
	}
};

jQuery.fn.extend( {

	// offset() relates an element's border box to the document origin
	offset: function( options ) {

		// Preserve chaining for setter
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each( function( i ) {
					jQuery.offset.setOffset( this, options, i );
				} );
		}

		var rect, win,
			elem = this[ 0 ];

		if ( !elem ) {
			return;
		}

		// Return zeros for disconnected and hidden (display: none) elements (gh-2310)
		// Support: IE <=11 only
		// Running getBoundingClientRect on a
		// disconnected node in IE throws an error
		if ( !elem.getClientRects().length ) {
			return { top: 0, left: 0 };
		}

		// Get document-relative position by adding viewport scroll to viewport-relative gBCR
		rect = elem.getBoundingClientRect();
		win = elem.ownerDocument.defaultView;
		return {
			top: rect.top + win.pageYOffset,
			left: rect.left + win.pageXOffset
		};
	},

	// position() relates an element's margin box to its offset parent's padding box
	// This corresponds to the behavior of CSS absolute positioning
	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset, doc,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// position:fixed elements are offset from the viewport, which itself always has zero offset
		if ( jQuery.css( elem, "position" ) === "fixed" ) {

			// Assume position:fixed implies availability of getBoundingClientRect
			offset = elem.getBoundingClientRect();

		} else {
			offset = this.offset();

			// Account for the *real* offset parent, which can be the document or its root element
			// when a statically positioned element is identified
			doc = elem.ownerDocument;
			offsetParent = elem.offsetParent || doc.documentElement;
			while ( offsetParent &&
				( offsetParent === doc.body || offsetParent === doc.documentElement ) &&
				jQuery.css( offsetParent, "position" ) === "static" ) {

				offsetParent = offsetParent.parentNode;
			}
			if ( offsetParent && offsetParent !== elem && offsetParent.nodeType === 1 ) {

				// Incorporate borders into its offset, since they are outside its content origin
				parentOffset = jQuery( offsetParent ).offset();
				parentOffset.top += jQuery.css( offsetParent, "borderTopWidth", true );
				parentOffset.left += jQuery.css( offsetParent, "borderLeftWidth", true );
			}
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	// This method will return documentElement in the following cases:
	// 1) For the element inside the iframe without offsetParent, this method will return
	//    documentElement of the parent window
	// 2) For the hidden or detached element
	// 3) For body or html element, i.e. in case of the html node - it will return itself
	//
	// but those exceptions were never presented as a real life use-cases
	// and might be considered as more preferable results.
	//
	// This logic, however, is not guaranteed and can change at any point in the future
	offsetParent: function() {
		return this.map( function() {
			var offsetParent = this.offsetParent;

			while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || documentElement;
		} );
	}
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {

			// Coalesce documents and windows
			var win;
			if ( isWindow( elem ) ) {
				win = elem;
			} else if ( elem.nodeType === 9 ) {
				win = elem.defaultView;
			}

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : win.pageXOffset,
					top ? val : win.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length );
	};
} );

// Support: Safari <=7 - 9.1, Chrome <=37 - 49
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( _i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );

				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
		function( defaultExtra, funcName ) {

		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( isWindow( elem ) ) {

					// $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
					return funcName.indexOf( "outer" ) === 0 ?
						elem[ "inner" + name ] :
						elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?

					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable );
		};
	} );
} );


jQuery.each( [
	"ajaxStart",
	"ajaxStop",
	"ajaxComplete",
	"ajaxError",
	"ajaxSuccess",
	"ajaxSend"
], function( _i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
} );




jQuery.fn.extend( {

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {

		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ?
			this.off( selector, "**" ) :
			this.off( types, selector || "**", fn );
	},

	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	}
} );

jQuery.each( ( "blur focus focusin focusout resize scroll click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup contextmenu" ).split( " " ),
	function( _i, name ) {

		// Handle event binding
		jQuery.fn[ name ] = function( data, fn ) {
			return arguments.length > 0 ?
				this.on( name, null, data, fn ) :
				this.trigger( name );
		};
	} );




// Support: Android <=4.0 only
// Make sure we trim BOM and NBSP
var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

// Bind a function to a context, optionally partially applying any
// arguments.
// jQuery.proxy is deprecated to promote standards (specifically Function#bind)
// However, it is not slated for removal any time soon
jQuery.proxy = function( fn, context ) {
	var tmp, args, proxy;

	if ( typeof context === "string" ) {
		tmp = fn[ context ];
		context = fn;
		fn = tmp;
	}

	// Quick check to determine if target is callable, in the spec
	// this throws a TypeError, but we will just return undefined.
	if ( !isFunction( fn ) ) {
		return undefined;
	}

	// Simulated bind
	args = slice.call( arguments, 2 );
	proxy = function() {
		return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
	};

	// Set the guid of unique handler to the same of original handler, so it can be removed
	proxy.guid = fn.guid = fn.guid || jQuery.guid++;

	return proxy;
};

jQuery.holdReady = function( hold ) {
	if ( hold ) {
		jQuery.readyWait++;
	} else {
		jQuery.ready( true );
	}
};
jQuery.isArray = Array.isArray;
jQuery.parseJSON = JSON.parse;
jQuery.nodeName = nodeName;
jQuery.isFunction = isFunction;
jQuery.isWindow = isWindow;
jQuery.camelCase = camelCase;
jQuery.type = toType;

jQuery.now = Date.now;

jQuery.isNumeric = function( obj ) {

	// As of jQuery 3.0, isNumeric is limited to
	// strings and numbers (primitives or objects)
	// that can be coerced to finite numbers (gh-2662)
	var type = jQuery.type( obj );
	return ( type === "number" || type === "string" ) &&

		// parseFloat NaNs numeric-cast false positives ("")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		!isNaN( obj - parseFloat( obj ) );
};

jQuery.trim = function( text ) {
	return text == null ?
		"" :
		( text + "" ).replace( rtrim, "" );
};



// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( true ) {
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function() {
		return jQuery;
	}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
}




var

	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( typeof noGlobal === "undefined" ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;
} );


/***/ }),

/***/ "./node_modules/popper.js/dist/esm/popper.js":
/*!***************************************************!*\
  !*** ./node_modules/popper.js/dist/esm/popper.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/**!
 * @fileOverview Kickass library to create and place poppers near their reference elements.
 * @version 1.16.1
 * @license
 * Copyright (c) 2016 Federico Zivolo and contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
var isBrowser = typeof window !== 'undefined' && typeof document !== 'undefined' && typeof navigator !== 'undefined';

var timeoutDuration = function () {
  var longerTimeoutBrowsers = ['Edge', 'Trident', 'Firefox'];
  for (var i = 0; i < longerTimeoutBrowsers.length; i += 1) {
    if (isBrowser && navigator.userAgent.indexOf(longerTimeoutBrowsers[i]) >= 0) {
      return 1;
    }
  }
  return 0;
}();

function microtaskDebounce(fn) {
  var called = false;
  return function () {
    if (called) {
      return;
    }
    called = true;
    window.Promise.resolve().then(function () {
      called = false;
      fn();
    });
  };
}

function taskDebounce(fn) {
  var scheduled = false;
  return function () {
    if (!scheduled) {
      scheduled = true;
      setTimeout(function () {
        scheduled = false;
        fn();
      }, timeoutDuration);
    }
  };
}

var supportsMicroTasks = isBrowser && window.Promise;

/**
* Create a debounced version of a method, that's asynchronously deferred
* but called in the minimum time possible.
*
* @method
* @memberof Popper.Utils
* @argument {Function} fn
* @returns {Function}
*/
var debounce = supportsMicroTasks ? microtaskDebounce : taskDebounce;

/**
 * Check if the given variable is a function
 * @method
 * @memberof Popper.Utils
 * @argument {Any} functionToCheck - variable to check
 * @returns {Boolean} answer to: is a function?
 */
function isFunction(functionToCheck) {
  var getType = {};
  return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

/**
 * Get CSS computed property of the given element
 * @method
 * @memberof Popper.Utils
 * @argument {Eement} element
 * @argument {String} property
 */
function getStyleComputedProperty(element, property) {
  if (element.nodeType !== 1) {
    return [];
  }
  // NOTE: 1 DOM access here
  var window = element.ownerDocument.defaultView;
  var css = window.getComputedStyle(element, null);
  return property ? css[property] : css;
}

/**
 * Returns the parentNode or the host of the element
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @returns {Element} parent
 */
function getParentNode(element) {
  if (element.nodeName === 'HTML') {
    return element;
  }
  return element.parentNode || element.host;
}

/**
 * Returns the scrolling parent of the given element
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @returns {Element} scroll parent
 */
function getScrollParent(element) {
  // Return body, `getScroll` will take care to get the correct `scrollTop` from it
  if (!element) {
    return document.body;
  }

  switch (element.nodeName) {
    case 'HTML':
    case 'BODY':
      return element.ownerDocument.body;
    case '#document':
      return element.body;
  }

  // Firefox want us to check `-x` and `-y` variations as well

  var _getStyleComputedProp = getStyleComputedProperty(element),
      overflow = _getStyleComputedProp.overflow,
      overflowX = _getStyleComputedProp.overflowX,
      overflowY = _getStyleComputedProp.overflowY;

  if (/(auto|scroll|overlay)/.test(overflow + overflowY + overflowX)) {
    return element;
  }

  return getScrollParent(getParentNode(element));
}

/**
 * Returns the reference node of the reference object, or the reference object itself.
 * @method
 * @memberof Popper.Utils
 * @param {Element|Object} reference - the reference element (the popper will be relative to this)
 * @returns {Element} parent
 */
function getReferenceNode(reference) {
  return reference && reference.referenceNode ? reference.referenceNode : reference;
}

var isIE11 = isBrowser && !!(window.MSInputMethodContext && document.documentMode);
var isIE10 = isBrowser && /MSIE 10/.test(navigator.userAgent);

/**
 * Determines if the browser is Internet Explorer
 * @method
 * @memberof Popper.Utils
 * @param {Number} version to check
 * @returns {Boolean} isIE
 */
function isIE(version) {
  if (version === 11) {
    return isIE11;
  }
  if (version === 10) {
    return isIE10;
  }
  return isIE11 || isIE10;
}

/**
 * Returns the offset parent of the given element
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @returns {Element} offset parent
 */
function getOffsetParent(element) {
  if (!element) {
    return document.documentElement;
  }

  var noOffsetParent = isIE(10) ? document.body : null;

  // NOTE: 1 DOM access here
  var offsetParent = element.offsetParent || null;
  // Skip hidden elements which don't have an offsetParent
  while (offsetParent === noOffsetParent && element.nextElementSibling) {
    offsetParent = (element = element.nextElementSibling).offsetParent;
  }

  var nodeName = offsetParent && offsetParent.nodeName;

  if (!nodeName || nodeName === 'BODY' || nodeName === 'HTML') {
    return element ? element.ownerDocument.documentElement : document.documentElement;
  }

  // .offsetParent will return the closest TH, TD or TABLE in case
  // no offsetParent is present, I hate this job...
  if (['TH', 'TD', 'TABLE'].indexOf(offsetParent.nodeName) !== -1 && getStyleComputedProperty(offsetParent, 'position') === 'static') {
    return getOffsetParent(offsetParent);
  }

  return offsetParent;
}

function isOffsetContainer(element) {
  var nodeName = element.nodeName;

  if (nodeName === 'BODY') {
    return false;
  }
  return nodeName === 'HTML' || getOffsetParent(element.firstElementChild) === element;
}

/**
 * Finds the root node (document, shadowDOM root) of the given element
 * @method
 * @memberof Popper.Utils
 * @argument {Element} node
 * @returns {Element} root node
 */
function getRoot(node) {
  if (node.parentNode !== null) {
    return getRoot(node.parentNode);
  }

  return node;
}

/**
 * Finds the offset parent common to the two provided nodes
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element1
 * @argument {Element} element2
 * @returns {Element} common offset parent
 */
function findCommonOffsetParent(element1, element2) {
  // This check is needed to avoid errors in case one of the elements isn't defined for any reason
  if (!element1 || !element1.nodeType || !element2 || !element2.nodeType) {
    return document.documentElement;
  }

  // Here we make sure to give as "start" the element that comes first in the DOM
  var order = element1.compareDocumentPosition(element2) & Node.DOCUMENT_POSITION_FOLLOWING;
  var start = order ? element1 : element2;
  var end = order ? element2 : element1;

  // Get common ancestor container
  var range = document.createRange();
  range.setStart(start, 0);
  range.setEnd(end, 0);
  var commonAncestorContainer = range.commonAncestorContainer;

  // Both nodes are inside #document

  if (element1 !== commonAncestorContainer && element2 !== commonAncestorContainer || start.contains(end)) {
    if (isOffsetContainer(commonAncestorContainer)) {
      return commonAncestorContainer;
    }

    return getOffsetParent(commonAncestorContainer);
  }

  // one of the nodes is inside shadowDOM, find which one
  var element1root = getRoot(element1);
  if (element1root.host) {
    return findCommonOffsetParent(element1root.host, element2);
  } else {
    return findCommonOffsetParent(element1, getRoot(element2).host);
  }
}

/**
 * Gets the scroll value of the given element in the given side (top and left)
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @argument {String} side `top` or `left`
 * @returns {number} amount of scrolled pixels
 */
function getScroll(element) {
  var side = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'top';

  var upperSide = side === 'top' ? 'scrollTop' : 'scrollLeft';
  var nodeName = element.nodeName;

  if (nodeName === 'BODY' || nodeName === 'HTML') {
    var html = element.ownerDocument.documentElement;
    var scrollingElement = element.ownerDocument.scrollingElement || html;
    return scrollingElement[upperSide];
  }

  return element[upperSide];
}

/*
 * Sum or subtract the element scroll values (left and top) from a given rect object
 * @method
 * @memberof Popper.Utils
 * @param {Object} rect - Rect object you want to change
 * @param {HTMLElement} element - The element from the function reads the scroll values
 * @param {Boolean} subtract - set to true if you want to subtract the scroll values
 * @return {Object} rect - The modifier rect object
 */
function includeScroll(rect, element) {
  var subtract = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  var scrollTop = getScroll(element, 'top');
  var scrollLeft = getScroll(element, 'left');
  var modifier = subtract ? -1 : 1;
  rect.top += scrollTop * modifier;
  rect.bottom += scrollTop * modifier;
  rect.left += scrollLeft * modifier;
  rect.right += scrollLeft * modifier;
  return rect;
}

/*
 * Helper to detect borders of a given element
 * @method
 * @memberof Popper.Utils
 * @param {CSSStyleDeclaration} styles
 * Result of `getStyleComputedProperty` on the given element
 * @param {String} axis - `x` or `y`
 * @return {number} borders - The borders size of the given axis
 */

function getBordersSize(styles, axis) {
  var sideA = axis === 'x' ? 'Left' : 'Top';
  var sideB = sideA === 'Left' ? 'Right' : 'Bottom';

  return parseFloat(styles['border' + sideA + 'Width']) + parseFloat(styles['border' + sideB + 'Width']);
}

function getSize(axis, body, html, computedStyle) {
  return Math.max(body['offset' + axis], body['scroll' + axis], html['client' + axis], html['offset' + axis], html['scroll' + axis], isIE(10) ? parseInt(html['offset' + axis]) + parseInt(computedStyle['margin' + (axis === 'Height' ? 'Top' : 'Left')]) + parseInt(computedStyle['margin' + (axis === 'Height' ? 'Bottom' : 'Right')]) : 0);
}

function getWindowSizes(document) {
  var body = document.body;
  var html = document.documentElement;
  var computedStyle = isIE(10) && getComputedStyle(html);

  return {
    height: getSize('Height', body, html, computedStyle),
    width: getSize('Width', body, html, computedStyle)
  };
}

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();





var defineProperty = function (obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

/**
 * Given element offsets, generate an output similar to getBoundingClientRect
 * @method
 * @memberof Popper.Utils
 * @argument {Object} offsets
 * @returns {Object} ClientRect like output
 */
function getClientRect(offsets) {
  return _extends({}, offsets, {
    right: offsets.left + offsets.width,
    bottom: offsets.top + offsets.height
  });
}

/**
 * Get bounding client rect of given element
 * @method
 * @memberof Popper.Utils
 * @param {HTMLElement} element
 * @return {Object} client rect
 */
function getBoundingClientRect(element) {
  var rect = {};

  // IE10 10 FIX: Please, don't ask, the element isn't
  // considered in DOM in some circumstances...
  // This isn't reproducible in IE10 compatibility mode of IE11
  try {
    if (isIE(10)) {
      rect = element.getBoundingClientRect();
      var scrollTop = getScroll(element, 'top');
      var scrollLeft = getScroll(element, 'left');
      rect.top += scrollTop;
      rect.left += scrollLeft;
      rect.bottom += scrollTop;
      rect.right += scrollLeft;
    } else {
      rect = element.getBoundingClientRect();
    }
  } catch (e) {}

  var result = {
    left: rect.left,
    top: rect.top,
    width: rect.right - rect.left,
    height: rect.bottom - rect.top
  };

  // subtract scrollbar size from sizes
  var sizes = element.nodeName === 'HTML' ? getWindowSizes(element.ownerDocument) : {};
  var width = sizes.width || element.clientWidth || result.width;
  var height = sizes.height || element.clientHeight || result.height;

  var horizScrollbar = element.offsetWidth - width;
  var vertScrollbar = element.offsetHeight - height;

  // if an hypothetical scrollbar is detected, we must be sure it's not a `border`
  // we make this check conditional for performance reasons
  if (horizScrollbar || vertScrollbar) {
    var styles = getStyleComputedProperty(element);
    horizScrollbar -= getBordersSize(styles, 'x');
    vertScrollbar -= getBordersSize(styles, 'y');

    result.width -= horizScrollbar;
    result.height -= vertScrollbar;
  }

  return getClientRect(result);
}

function getOffsetRectRelativeToArbitraryNode(children, parent) {
  var fixedPosition = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  var isIE10 = isIE(10);
  var isHTML = parent.nodeName === 'HTML';
  var childrenRect = getBoundingClientRect(children);
  var parentRect = getBoundingClientRect(parent);
  var scrollParent = getScrollParent(children);

  var styles = getStyleComputedProperty(parent);
  var borderTopWidth = parseFloat(styles.borderTopWidth);
  var borderLeftWidth = parseFloat(styles.borderLeftWidth);

  // In cases where the parent is fixed, we must ignore negative scroll in offset calc
  if (fixedPosition && isHTML) {
    parentRect.top = Math.max(parentRect.top, 0);
    parentRect.left = Math.max(parentRect.left, 0);
  }
  var offsets = getClientRect({
    top: childrenRect.top - parentRect.top - borderTopWidth,
    left: childrenRect.left - parentRect.left - borderLeftWidth,
    width: childrenRect.width,
    height: childrenRect.height
  });
  offsets.marginTop = 0;
  offsets.marginLeft = 0;

  // Subtract margins of documentElement in case it's being used as parent
  // we do this only on HTML because it's the only element that behaves
  // differently when margins are applied to it. The margins are included in
  // the box of the documentElement, in the other cases not.
  if (!isIE10 && isHTML) {
    var marginTop = parseFloat(styles.marginTop);
    var marginLeft = parseFloat(styles.marginLeft);

    offsets.top -= borderTopWidth - marginTop;
    offsets.bottom -= borderTopWidth - marginTop;
    offsets.left -= borderLeftWidth - marginLeft;
    offsets.right -= borderLeftWidth - marginLeft;

    // Attach marginTop and marginLeft because in some circumstances we may need them
    offsets.marginTop = marginTop;
    offsets.marginLeft = marginLeft;
  }

  if (isIE10 && !fixedPosition ? parent.contains(scrollParent) : parent === scrollParent && scrollParent.nodeName !== 'BODY') {
    offsets = includeScroll(offsets, parent);
  }

  return offsets;
}

function getViewportOffsetRectRelativeToArtbitraryNode(element) {
  var excludeScroll = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  var html = element.ownerDocument.documentElement;
  var relativeOffset = getOffsetRectRelativeToArbitraryNode(element, html);
  var width = Math.max(html.clientWidth, window.innerWidth || 0);
  var height = Math.max(html.clientHeight, window.innerHeight || 0);

  var scrollTop = !excludeScroll ? getScroll(html) : 0;
  var scrollLeft = !excludeScroll ? getScroll(html, 'left') : 0;

  var offset = {
    top: scrollTop - relativeOffset.top + relativeOffset.marginTop,
    left: scrollLeft - relativeOffset.left + relativeOffset.marginLeft,
    width: width,
    height: height
  };

  return getClientRect(offset);
}

/**
 * Check if the given element is fixed or is inside a fixed parent
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @argument {Element} customContainer
 * @returns {Boolean} answer to "isFixed?"
 */
function isFixed(element) {
  var nodeName = element.nodeName;
  if (nodeName === 'BODY' || nodeName === 'HTML') {
    return false;
  }
  if (getStyleComputedProperty(element, 'position') === 'fixed') {
    return true;
  }
  var parentNode = getParentNode(element);
  if (!parentNode) {
    return false;
  }
  return isFixed(parentNode);
}

/**
 * Finds the first parent of an element that has a transformed property defined
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @returns {Element} first transformed parent or documentElement
 */

function getFixedPositionOffsetParent(element) {
  // This check is needed to avoid errors in case one of the elements isn't defined for any reason
  if (!element || !element.parentElement || isIE()) {
    return document.documentElement;
  }
  var el = element.parentElement;
  while (el && getStyleComputedProperty(el, 'transform') === 'none') {
    el = el.parentElement;
  }
  return el || document.documentElement;
}

/**
 * Computed the boundaries limits and return them
 * @method
 * @memberof Popper.Utils
 * @param {HTMLElement} popper
 * @param {HTMLElement} reference
 * @param {number} padding
 * @param {HTMLElement} boundariesElement - Element used to define the boundaries
 * @param {Boolean} fixedPosition - Is in fixed position mode
 * @returns {Object} Coordinates of the boundaries
 */
function getBoundaries(popper, reference, padding, boundariesElement) {
  var fixedPosition = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

  // NOTE: 1 DOM access here

  var boundaries = { top: 0, left: 0 };
  var offsetParent = fixedPosition ? getFixedPositionOffsetParent(popper) : findCommonOffsetParent(popper, getReferenceNode(reference));

  // Handle viewport case
  if (boundariesElement === 'viewport') {
    boundaries = getViewportOffsetRectRelativeToArtbitraryNode(offsetParent, fixedPosition);
  } else {
    // Handle other cases based on DOM element used as boundaries
    var boundariesNode = void 0;
    if (boundariesElement === 'scrollParent') {
      boundariesNode = getScrollParent(getParentNode(reference));
      if (boundariesNode.nodeName === 'BODY') {
        boundariesNode = popper.ownerDocument.documentElement;
      }
    } else if (boundariesElement === 'window') {
      boundariesNode = popper.ownerDocument.documentElement;
    } else {
      boundariesNode = boundariesElement;
    }

    var offsets = getOffsetRectRelativeToArbitraryNode(boundariesNode, offsetParent, fixedPosition);

    // In case of HTML, we need a different computation
    if (boundariesNode.nodeName === 'HTML' && !isFixed(offsetParent)) {
      var _getWindowSizes = getWindowSizes(popper.ownerDocument),
          height = _getWindowSizes.height,
          width = _getWindowSizes.width;

      boundaries.top += offsets.top - offsets.marginTop;
      boundaries.bottom = height + offsets.top;
      boundaries.left += offsets.left - offsets.marginLeft;
      boundaries.right = width + offsets.left;
    } else {
      // for all the other DOM elements, this one is good
      boundaries = offsets;
    }
  }

  // Add paddings
  padding = padding || 0;
  var isPaddingNumber = typeof padding === 'number';
  boundaries.left += isPaddingNumber ? padding : padding.left || 0;
  boundaries.top += isPaddingNumber ? padding : padding.top || 0;
  boundaries.right -= isPaddingNumber ? padding : padding.right || 0;
  boundaries.bottom -= isPaddingNumber ? padding : padding.bottom || 0;

  return boundaries;
}

function getArea(_ref) {
  var width = _ref.width,
      height = _ref.height;

  return width * height;
}

/**
 * Utility used to transform the `auto` placement to the placement with more
 * available space.
 * @method
 * @memberof Popper.Utils
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function computeAutoPlacement(placement, refRect, popper, reference, boundariesElement) {
  var padding = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;

  if (placement.indexOf('auto') === -1) {
    return placement;
  }

  var boundaries = getBoundaries(popper, reference, padding, boundariesElement);

  var rects = {
    top: {
      width: boundaries.width,
      height: refRect.top - boundaries.top
    },
    right: {
      width: boundaries.right - refRect.right,
      height: boundaries.height
    },
    bottom: {
      width: boundaries.width,
      height: boundaries.bottom - refRect.bottom
    },
    left: {
      width: refRect.left - boundaries.left,
      height: boundaries.height
    }
  };

  var sortedAreas = Object.keys(rects).map(function (key) {
    return _extends({
      key: key
    }, rects[key], {
      area: getArea(rects[key])
    });
  }).sort(function (a, b) {
    return b.area - a.area;
  });

  var filteredAreas = sortedAreas.filter(function (_ref2) {
    var width = _ref2.width,
        height = _ref2.height;
    return width >= popper.clientWidth && height >= popper.clientHeight;
  });

  var computedPlacement = filteredAreas.length > 0 ? filteredAreas[0].key : sortedAreas[0].key;

  var variation = placement.split('-')[1];

  return computedPlacement + (variation ? '-' + variation : '');
}

/**
 * Get offsets to the reference element
 * @method
 * @memberof Popper.Utils
 * @param {Object} state
 * @param {Element} popper - the popper element
 * @param {Element} reference - the reference element (the popper will be relative to this)
 * @param {Element} fixedPosition - is in fixed position mode
 * @returns {Object} An object containing the offsets which will be applied to the popper
 */
function getReferenceOffsets(state, popper, reference) {
  var fixedPosition = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

  var commonOffsetParent = fixedPosition ? getFixedPositionOffsetParent(popper) : findCommonOffsetParent(popper, getReferenceNode(reference));
  return getOffsetRectRelativeToArbitraryNode(reference, commonOffsetParent, fixedPosition);
}

/**
 * Get the outer sizes of the given element (offset size + margins)
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @returns {Object} object containing width and height properties
 */
function getOuterSizes(element) {
  var window = element.ownerDocument.defaultView;
  var styles = window.getComputedStyle(element);
  var x = parseFloat(styles.marginTop || 0) + parseFloat(styles.marginBottom || 0);
  var y = parseFloat(styles.marginLeft || 0) + parseFloat(styles.marginRight || 0);
  var result = {
    width: element.offsetWidth + y,
    height: element.offsetHeight + x
  };
  return result;
}

/**
 * Get the opposite placement of the given one
 * @method
 * @memberof Popper.Utils
 * @argument {String} placement
 * @returns {String} flipped placement
 */
function getOppositePlacement(placement) {
  var hash = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' };
  return placement.replace(/left|right|bottom|top/g, function (matched) {
    return hash[matched];
  });
}

/**
 * Get offsets to the popper
 * @method
 * @memberof Popper.Utils
 * @param {Object} position - CSS position the Popper will get applied
 * @param {HTMLElement} popper - the popper element
 * @param {Object} referenceOffsets - the reference offsets (the popper will be relative to this)
 * @param {String} placement - one of the valid placement options
 * @returns {Object} popperOffsets - An object containing the offsets which will be applied to the popper
 */
function getPopperOffsets(popper, referenceOffsets, placement) {
  placement = placement.split('-')[0];

  // Get popper node sizes
  var popperRect = getOuterSizes(popper);

  // Add position, width and height to our offsets object
  var popperOffsets = {
    width: popperRect.width,
    height: popperRect.height
  };

  // depending by the popper placement we have to compute its offsets slightly differently
  var isHoriz = ['right', 'left'].indexOf(placement) !== -1;
  var mainSide = isHoriz ? 'top' : 'left';
  var secondarySide = isHoriz ? 'left' : 'top';
  var measurement = isHoriz ? 'height' : 'width';
  var secondaryMeasurement = !isHoriz ? 'height' : 'width';

  popperOffsets[mainSide] = referenceOffsets[mainSide] + referenceOffsets[measurement] / 2 - popperRect[measurement] / 2;
  if (placement === secondarySide) {
    popperOffsets[secondarySide] = referenceOffsets[secondarySide] - popperRect[secondaryMeasurement];
  } else {
    popperOffsets[secondarySide] = referenceOffsets[getOppositePlacement(secondarySide)];
  }

  return popperOffsets;
}

/**
 * Mimics the `find` method of Array
 * @method
 * @memberof Popper.Utils
 * @argument {Array} arr
 * @argument prop
 * @argument value
 * @returns index or -1
 */
function find(arr, check) {
  // use native find if supported
  if (Array.prototype.find) {
    return arr.find(check);
  }

  // use `filter` to obtain the same behavior of `find`
  return arr.filter(check)[0];
}

/**
 * Return the index of the matching object
 * @method
 * @memberof Popper.Utils
 * @argument {Array} arr
 * @argument prop
 * @argument value
 * @returns index or -1
 */
function findIndex(arr, prop, value) {
  // use native findIndex if supported
  if (Array.prototype.findIndex) {
    return arr.findIndex(function (cur) {
      return cur[prop] === value;
    });
  }

  // use `find` + `indexOf` if `findIndex` isn't supported
  var match = find(arr, function (obj) {
    return obj[prop] === value;
  });
  return arr.indexOf(match);
}

/**
 * Loop trough the list of modifiers and run them in order,
 * each of them will then edit the data object.
 * @method
 * @memberof Popper.Utils
 * @param {dataObject} data
 * @param {Array} modifiers
 * @param {String} ends - Optional modifier name used as stopper
 * @returns {dataObject}
 */
function runModifiers(modifiers, data, ends) {
  var modifiersToRun = ends === undefined ? modifiers : modifiers.slice(0, findIndex(modifiers, 'name', ends));

  modifiersToRun.forEach(function (modifier) {
    if (modifier['function']) {
      // eslint-disable-line dot-notation
      console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
    }
    var fn = modifier['function'] || modifier.fn; // eslint-disable-line dot-notation
    if (modifier.enabled && isFunction(fn)) {
      // Add properties to offsets to make them a complete clientRect object
      // we do this before each modifier to make sure the previous one doesn't
      // mess with these values
      data.offsets.popper = getClientRect(data.offsets.popper);
      data.offsets.reference = getClientRect(data.offsets.reference);

      data = fn(data, modifier);
    }
  });

  return data;
}

/**
 * Updates the position of the popper, computing the new offsets and applying
 * the new style.<br />
 * Prefer `scheduleUpdate` over `update` because of performance reasons.
 * @method
 * @memberof Popper
 */
function update() {
  // if popper is destroyed, don't perform any further update
  if (this.state.isDestroyed) {
    return;
  }

  var data = {
    instance: this,
    styles: {},
    arrowStyles: {},
    attributes: {},
    flipped: false,
    offsets: {}
  };

  // compute reference element offsets
  data.offsets.reference = getReferenceOffsets(this.state, this.popper, this.reference, this.options.positionFixed);

  // compute auto placement, store placement inside the data object,
  // modifiers will be able to edit `placement` if needed
  // and refer to originalPlacement to know the original value
  data.placement = computeAutoPlacement(this.options.placement, data.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding);

  // store the computed placement inside `originalPlacement`
  data.originalPlacement = data.placement;

  data.positionFixed = this.options.positionFixed;

  // compute the popper offsets
  data.offsets.popper = getPopperOffsets(this.popper, data.offsets.reference, data.placement);

  data.offsets.popper.position = this.options.positionFixed ? 'fixed' : 'absolute';

  // run the modifiers
  data = runModifiers(this.modifiers, data);

  // the first `update` will call `onCreate` callback
  // the other ones will call `onUpdate` callback
  if (!this.state.isCreated) {
    this.state.isCreated = true;
    this.options.onCreate(data);
  } else {
    this.options.onUpdate(data);
  }
}

/**
 * Helper used to know if the given modifier is enabled.
 * @method
 * @memberof Popper.Utils
 * @returns {Boolean}
 */
function isModifierEnabled(modifiers, modifierName) {
  return modifiers.some(function (_ref) {
    var name = _ref.name,
        enabled = _ref.enabled;
    return enabled && name === modifierName;
  });
}

/**
 * Get the prefixed supported property name
 * @method
 * @memberof Popper.Utils
 * @argument {String} property (camelCase)
 * @returns {String} prefixed property (camelCase or PascalCase, depending on the vendor prefix)
 */
function getSupportedPropertyName(property) {
  var prefixes = [false, 'ms', 'Webkit', 'Moz', 'O'];
  var upperProp = property.charAt(0).toUpperCase() + property.slice(1);

  for (var i = 0; i < prefixes.length; i++) {
    var prefix = prefixes[i];
    var toCheck = prefix ? '' + prefix + upperProp : property;
    if (typeof document.body.style[toCheck] !== 'undefined') {
      return toCheck;
    }
  }
  return null;
}

/**
 * Destroys the popper.
 * @method
 * @memberof Popper
 */
function destroy() {
  this.state.isDestroyed = true;

  // touch DOM only if `applyStyle` modifier is enabled
  if (isModifierEnabled(this.modifiers, 'applyStyle')) {
    this.popper.removeAttribute('x-placement');
    this.popper.style.position = '';
    this.popper.style.top = '';
    this.popper.style.left = '';
    this.popper.style.right = '';
    this.popper.style.bottom = '';
    this.popper.style.willChange = '';
    this.popper.style[getSupportedPropertyName('transform')] = '';
  }

  this.disableEventListeners();

  // remove the popper if user explicitly asked for the deletion on destroy
  // do not use `remove` because IE11 doesn't support it
  if (this.options.removeOnDestroy) {
    this.popper.parentNode.removeChild(this.popper);
  }
  return this;
}

/**
 * Get the window associated with the element
 * @argument {Element} element
 * @returns {Window}
 */
function getWindow(element) {
  var ownerDocument = element.ownerDocument;
  return ownerDocument ? ownerDocument.defaultView : window;
}

function attachToScrollParents(scrollParent, event, callback, scrollParents) {
  var isBody = scrollParent.nodeName === 'BODY';
  var target = isBody ? scrollParent.ownerDocument.defaultView : scrollParent;
  target.addEventListener(event, callback, { passive: true });

  if (!isBody) {
    attachToScrollParents(getScrollParent(target.parentNode), event, callback, scrollParents);
  }
  scrollParents.push(target);
}

/**
 * Setup needed event listeners used to update the popper position
 * @method
 * @memberof Popper.Utils
 * @private
 */
function setupEventListeners(reference, options, state, updateBound) {
  // Resize event listener on window
  state.updateBound = updateBound;
  getWindow(reference).addEventListener('resize', state.updateBound, { passive: true });

  // Scroll event listener on scroll parents
  var scrollElement = getScrollParent(reference);
  attachToScrollParents(scrollElement, 'scroll', state.updateBound, state.scrollParents);
  state.scrollElement = scrollElement;
  state.eventsEnabled = true;

  return state;
}

/**
 * It will add resize/scroll events and start recalculating
 * position of the popper element when they are triggered.
 * @method
 * @memberof Popper
 */
function enableEventListeners() {
  if (!this.state.eventsEnabled) {
    this.state = setupEventListeners(this.reference, this.options, this.state, this.scheduleUpdate);
  }
}

/**
 * Remove event listeners used to update the popper position
 * @method
 * @memberof Popper.Utils
 * @private
 */
function removeEventListeners(reference, state) {
  // Remove resize event listener on window
  getWindow(reference).removeEventListener('resize', state.updateBound);

  // Remove scroll event listener on scroll parents
  state.scrollParents.forEach(function (target) {
    target.removeEventListener('scroll', state.updateBound);
  });

  // Reset state
  state.updateBound = null;
  state.scrollParents = [];
  state.scrollElement = null;
  state.eventsEnabled = false;
  return state;
}

/**
 * It will remove resize/scroll events and won't recalculate popper position
 * when they are triggered. It also won't trigger `onUpdate` callback anymore,
 * unless you call `update` method manually.
 * @method
 * @memberof Popper
 */
function disableEventListeners() {
  if (this.state.eventsEnabled) {
    cancelAnimationFrame(this.scheduleUpdate);
    this.state = removeEventListeners(this.reference, this.state);
  }
}

/**
 * Tells if a given input is a number
 * @method
 * @memberof Popper.Utils
 * @param {*} input to check
 * @return {Boolean}
 */
function isNumeric(n) {
  return n !== '' && !isNaN(parseFloat(n)) && isFinite(n);
}

/**
 * Set the style to the given popper
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element - Element to apply the style to
 * @argument {Object} styles
 * Object with a list of properties and values which will be applied to the element
 */
function setStyles(element, styles) {
  Object.keys(styles).forEach(function (prop) {
    var unit = '';
    // add unit if the value is numeric and is one of the following
    if (['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(prop) !== -1 && isNumeric(styles[prop])) {
      unit = 'px';
    }
    element.style[prop] = styles[prop] + unit;
  });
}

/**
 * Set the attributes to the given popper
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element - Element to apply the attributes to
 * @argument {Object} styles
 * Object with a list of properties and values which will be applied to the element
 */
function setAttributes(element, attributes) {
  Object.keys(attributes).forEach(function (prop) {
    var value = attributes[prop];
    if (value !== false) {
      element.setAttribute(prop, attributes[prop]);
    } else {
      element.removeAttribute(prop);
    }
  });
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Object} data.styles - List of style properties - values to apply to popper element
 * @argument {Object} data.attributes - List of attribute properties - values to apply to popper element
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The same data object
 */
function applyStyle(data) {
  // any property present in `data.styles` will be applied to the popper,
  // in this way we can make the 3rd party modifiers add custom styles to it
  // Be aware, modifiers could override the properties defined in the previous
  // lines of this modifier!
  setStyles(data.instance.popper, data.styles);

  // any property present in `data.attributes` will be applied to the popper,
  // they will be set as HTML attributes of the element
  setAttributes(data.instance.popper, data.attributes);

  // if arrowElement is defined and arrowStyles has some properties
  if (data.arrowElement && Object.keys(data.arrowStyles).length) {
    setStyles(data.arrowElement, data.arrowStyles);
  }

  return data;
}

/**
 * Set the x-placement attribute before everything else because it could be used
 * to add margins to the popper margins needs to be calculated to get the
 * correct popper offsets.
 * @method
 * @memberof Popper.modifiers
 * @param {HTMLElement} reference - The reference element used to position the popper
 * @param {HTMLElement} popper - The HTML element used as popper
 * @param {Object} options - Popper.js options
 */
function applyStyleOnLoad(reference, popper, options, modifierOptions, state) {
  // compute reference element offsets
  var referenceOffsets = getReferenceOffsets(state, popper, reference, options.positionFixed);

  // compute auto placement, store placement inside the data object,
  // modifiers will be able to edit `placement` if needed
  // and refer to originalPlacement to know the original value
  var placement = computeAutoPlacement(options.placement, referenceOffsets, popper, reference, options.modifiers.flip.boundariesElement, options.modifiers.flip.padding);

  popper.setAttribute('x-placement', placement);

  // Apply `position` to popper before anything else because
  // without the position applied we can't guarantee correct computations
  setStyles(popper, { position: options.positionFixed ? 'fixed' : 'absolute' });

  return options;
}

/**
 * @function
 * @memberof Popper.Utils
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Boolean} shouldRound - If the offsets should be rounded at all
 * @returns {Object} The popper's position offsets rounded
 *
 * The tale of pixel-perfect positioning. It's still not 100% perfect, but as
 * good as it can be within reason.
 * Discussion here: https://github.com/FezVrasta/popper.js/pull/715
 *
 * Low DPI screens cause a popper to be blurry if not using full pixels (Safari
 * as well on High DPI screens).
 *
 * Firefox prefers no rounding for positioning and does not have blurriness on
 * high DPI screens.
 *
 * Only horizontal placement and left/right values need to be considered.
 */
function getRoundedOffsets(data, shouldRound) {
  var _data$offsets = data.offsets,
      popper = _data$offsets.popper,
      reference = _data$offsets.reference;
  var round = Math.round,
      floor = Math.floor;

  var noRound = function noRound(v) {
    return v;
  };

  var referenceWidth = round(reference.width);
  var popperWidth = round(popper.width);

  var isVertical = ['left', 'right'].indexOf(data.placement) !== -1;
  var isVariation = data.placement.indexOf('-') !== -1;
  var sameWidthParity = referenceWidth % 2 === popperWidth % 2;
  var bothOddWidth = referenceWidth % 2 === 1 && popperWidth % 2 === 1;

  var horizontalToInteger = !shouldRound ? noRound : isVertical || isVariation || sameWidthParity ? round : floor;
  var verticalToInteger = !shouldRound ? noRound : round;

  return {
    left: horizontalToInteger(bothOddWidth && !isVariation && shouldRound ? popper.left - 1 : popper.left),
    top: verticalToInteger(popper.top),
    bottom: verticalToInteger(popper.bottom),
    right: horizontalToInteger(popper.right)
  };
}

var isFirefox = isBrowser && /Firefox/i.test(navigator.userAgent);

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function computeStyle(data, options) {
  var x = options.x,
      y = options.y;
  var popper = data.offsets.popper;

  // Remove this legacy support in Popper.js v2

  var legacyGpuAccelerationOption = find(data.instance.modifiers, function (modifier) {
    return modifier.name === 'applyStyle';
  }).gpuAcceleration;
  if (legacyGpuAccelerationOption !== undefined) {
    console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');
  }
  var gpuAcceleration = legacyGpuAccelerationOption !== undefined ? legacyGpuAccelerationOption : options.gpuAcceleration;

  var offsetParent = getOffsetParent(data.instance.popper);
  var offsetParentRect = getBoundingClientRect(offsetParent);

  // Styles
  var styles = {
    position: popper.position
  };

  var offsets = getRoundedOffsets(data, window.devicePixelRatio < 2 || !isFirefox);

  var sideA = x === 'bottom' ? 'top' : 'bottom';
  var sideB = y === 'right' ? 'left' : 'right';

  // if gpuAcceleration is set to `true` and transform is supported,
  //  we use `translate3d` to apply the position to the popper we
  // automatically use the supported prefixed version if needed
  var prefixedProperty = getSupportedPropertyName('transform');

  // now, let's make a step back and look at this code closely (wtf?)
  // If the content of the popper grows once it's been positioned, it
  // may happen that the popper gets misplaced because of the new content
  // overflowing its reference element
  // To avoid this problem, we provide two options (x and y), which allow
  // the consumer to define the offset origin.
  // If we position a popper on top of a reference element, we can set
  // `x` to `top` to make the popper grow towards its top instead of
  // its bottom.
  var left = void 0,
      top = void 0;
  if (sideA === 'bottom') {
    // when offsetParent is <html> the positioning is relative to the bottom of the screen (excluding the scrollbar)
    // and not the bottom of the html element
    if (offsetParent.nodeName === 'HTML') {
      top = -offsetParent.clientHeight + offsets.bottom;
    } else {
      top = -offsetParentRect.height + offsets.bottom;
    }
  } else {
    top = offsets.top;
  }
  if (sideB === 'right') {
    if (offsetParent.nodeName === 'HTML') {
      left = -offsetParent.clientWidth + offsets.right;
    } else {
      left = -offsetParentRect.width + offsets.right;
    }
  } else {
    left = offsets.left;
  }
  if (gpuAcceleration && prefixedProperty) {
    styles[prefixedProperty] = 'translate3d(' + left + 'px, ' + top + 'px, 0)';
    styles[sideA] = 0;
    styles[sideB] = 0;
    styles.willChange = 'transform';
  } else {
    // othwerise, we use the standard `top`, `left`, `bottom` and `right` properties
    var invertTop = sideA === 'bottom' ? -1 : 1;
    var invertLeft = sideB === 'right' ? -1 : 1;
    styles[sideA] = top * invertTop;
    styles[sideB] = left * invertLeft;
    styles.willChange = sideA + ', ' + sideB;
  }

  // Attributes
  var attributes = {
    'x-placement': data.placement
  };

  // Update `data` attributes, styles and arrowStyles
  data.attributes = _extends({}, attributes, data.attributes);
  data.styles = _extends({}, styles, data.styles);
  data.arrowStyles = _extends({}, data.offsets.arrow, data.arrowStyles);

  return data;
}

/**
 * Helper used to know if the given modifier depends from another one.<br />
 * It checks if the needed modifier is listed and enabled.
 * @method
 * @memberof Popper.Utils
 * @param {Array} modifiers - list of modifiers
 * @param {String} requestingName - name of requesting modifier
 * @param {String} requestedName - name of requested modifier
 * @returns {Boolean}
 */
function isModifierRequired(modifiers, requestingName, requestedName) {
  var requesting = find(modifiers, function (_ref) {
    var name = _ref.name;
    return name === requestingName;
  });

  var isRequired = !!requesting && modifiers.some(function (modifier) {
    return modifier.name === requestedName && modifier.enabled && modifier.order < requesting.order;
  });

  if (!isRequired) {
    var _requesting = '`' + requestingName + '`';
    var requested = '`' + requestedName + '`';
    console.warn(requested + ' modifier is required by ' + _requesting + ' modifier in order to work, be sure to include it before ' + _requesting + '!');
  }
  return isRequired;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function arrow(data, options) {
  var _data$offsets$arrow;

  // arrow depends on keepTogether in order to work
  if (!isModifierRequired(data.instance.modifiers, 'arrow', 'keepTogether')) {
    return data;
  }

  var arrowElement = options.element;

  // if arrowElement is a string, suppose it's a CSS selector
  if (typeof arrowElement === 'string') {
    arrowElement = data.instance.popper.querySelector(arrowElement);

    // if arrowElement is not found, don't run the modifier
    if (!arrowElement) {
      return data;
    }
  } else {
    // if the arrowElement isn't a query selector we must check that the
    // provided DOM node is child of its popper node
    if (!data.instance.popper.contains(arrowElement)) {
      console.warn('WARNING: `arrow.element` must be child of its popper element!');
      return data;
    }
  }

  var placement = data.placement.split('-')[0];
  var _data$offsets = data.offsets,
      popper = _data$offsets.popper,
      reference = _data$offsets.reference;

  var isVertical = ['left', 'right'].indexOf(placement) !== -1;

  var len = isVertical ? 'height' : 'width';
  var sideCapitalized = isVertical ? 'Top' : 'Left';
  var side = sideCapitalized.toLowerCase();
  var altSide = isVertical ? 'left' : 'top';
  var opSide = isVertical ? 'bottom' : 'right';
  var arrowElementSize = getOuterSizes(arrowElement)[len];

  //
  // extends keepTogether behavior making sure the popper and its
  // reference have enough pixels in conjunction
  //

  // top/left side
  if (reference[opSide] - arrowElementSize < popper[side]) {
    data.offsets.popper[side] -= popper[side] - (reference[opSide] - arrowElementSize);
  }
  // bottom/right side
  if (reference[side] + arrowElementSize > popper[opSide]) {
    data.offsets.popper[side] += reference[side] + arrowElementSize - popper[opSide];
  }
  data.offsets.popper = getClientRect(data.offsets.popper);

  // compute center of the popper
  var center = reference[side] + reference[len] / 2 - arrowElementSize / 2;

  // Compute the sideValue using the updated popper offsets
  // take popper margin in account because we don't have this info available
  var css = getStyleComputedProperty(data.instance.popper);
  var popperMarginSide = parseFloat(css['margin' + sideCapitalized]);
  var popperBorderSide = parseFloat(css['border' + sideCapitalized + 'Width']);
  var sideValue = center - data.offsets.popper[side] - popperMarginSide - popperBorderSide;

  // prevent arrowElement from being placed not contiguously to its popper
  sideValue = Math.max(Math.min(popper[len] - arrowElementSize, sideValue), 0);

  data.arrowElement = arrowElement;
  data.offsets.arrow = (_data$offsets$arrow = {}, defineProperty(_data$offsets$arrow, side, Math.round(sideValue)), defineProperty(_data$offsets$arrow, altSide, ''), _data$offsets$arrow);

  return data;
}

/**
 * Get the opposite placement variation of the given one
 * @method
 * @memberof Popper.Utils
 * @argument {String} placement variation
 * @returns {String} flipped placement variation
 */
function getOppositeVariation(variation) {
  if (variation === 'end') {
    return 'start';
  } else if (variation === 'start') {
    return 'end';
  }
  return variation;
}

/**
 * List of accepted placements to use as values of the `placement` option.<br />
 * Valid placements are:
 * - `auto`
 * - `top`
 * - `right`
 * - `bottom`
 * - `left`
 *
 * Each placement can have a variation from this list:
 * - `-start`
 * - `-end`
 *
 * Variations are interpreted easily if you think of them as the left to right
 * written languages. Horizontally (`top` and `bottom`), `start` is left and `end`
 * is right.<br />
 * Vertically (`left` and `right`), `start` is top and `end` is bottom.
 *
 * Some valid examples are:
 * - `top-end` (on top of reference, right aligned)
 * - `right-start` (on right of reference, top aligned)
 * - `bottom` (on bottom, centered)
 * - `auto-end` (on the side with more space available, alignment depends by placement)
 *
 * @static
 * @type {Array}
 * @enum {String}
 * @readonly
 * @method placements
 * @memberof Popper
 */
var placements = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'];

// Get rid of `auto` `auto-start` and `auto-end`
var validPlacements = placements.slice(3);

/**
 * Given an initial placement, returns all the subsequent placements
 * clockwise (or counter-clockwise).
 *
 * @method
 * @memberof Popper.Utils
 * @argument {String} placement - A valid placement (it accepts variations)
 * @argument {Boolean} counter - Set to true to walk the placements counterclockwise
 * @returns {Array} placements including their variations
 */
function clockwise(placement) {
  var counter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  var index = validPlacements.indexOf(placement);
  var arr = validPlacements.slice(index + 1).concat(validPlacements.slice(0, index));
  return counter ? arr.reverse() : arr;
}

var BEHAVIORS = {
  FLIP: 'flip',
  CLOCKWISE: 'clockwise',
  COUNTERCLOCKWISE: 'counterclockwise'
};

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function flip(data, options) {
  // if `inner` modifier is enabled, we can't use the `flip` modifier
  if (isModifierEnabled(data.instance.modifiers, 'inner')) {
    return data;
  }

  if (data.flipped && data.placement === data.originalPlacement) {
    // seems like flip is trying to loop, probably there's not enough space on any of the flippable sides
    return data;
  }

  var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, options.boundariesElement, data.positionFixed);

  var placement = data.placement.split('-')[0];
  var placementOpposite = getOppositePlacement(placement);
  var variation = data.placement.split('-')[1] || '';

  var flipOrder = [];

  switch (options.behavior) {
    case BEHAVIORS.FLIP:
      flipOrder = [placement, placementOpposite];
      break;
    case BEHAVIORS.CLOCKWISE:
      flipOrder = clockwise(placement);
      break;
    case BEHAVIORS.COUNTERCLOCKWISE:
      flipOrder = clockwise(placement, true);
      break;
    default:
      flipOrder = options.behavior;
  }

  flipOrder.forEach(function (step, index) {
    if (placement !== step || flipOrder.length === index + 1) {
      return data;
    }

    placement = data.placement.split('-')[0];
    placementOpposite = getOppositePlacement(placement);

    var popperOffsets = data.offsets.popper;
    var refOffsets = data.offsets.reference;

    // using floor because the reference offsets may contain decimals we are not going to consider here
    var floor = Math.floor;
    var overlapsRef = placement === 'left' && floor(popperOffsets.right) > floor(refOffsets.left) || placement === 'right' && floor(popperOffsets.left) < floor(refOffsets.right) || placement === 'top' && floor(popperOffsets.bottom) > floor(refOffsets.top) || placement === 'bottom' && floor(popperOffsets.top) < floor(refOffsets.bottom);

    var overflowsLeft = floor(popperOffsets.left) < floor(boundaries.left);
    var overflowsRight = floor(popperOffsets.right) > floor(boundaries.right);
    var overflowsTop = floor(popperOffsets.top) < floor(boundaries.top);
    var overflowsBottom = floor(popperOffsets.bottom) > floor(boundaries.bottom);

    var overflowsBoundaries = placement === 'left' && overflowsLeft || placement === 'right' && overflowsRight || placement === 'top' && overflowsTop || placement === 'bottom' && overflowsBottom;

    // flip the variation if required
    var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;

    // flips variation if reference element overflows boundaries
    var flippedVariationByRef = !!options.flipVariations && (isVertical && variation === 'start' && overflowsLeft || isVertical && variation === 'end' && overflowsRight || !isVertical && variation === 'start' && overflowsTop || !isVertical && variation === 'end' && overflowsBottom);

    // flips variation if popper content overflows boundaries
    var flippedVariationByContent = !!options.flipVariationsByContent && (isVertical && variation === 'start' && overflowsRight || isVertical && variation === 'end' && overflowsLeft || !isVertical && variation === 'start' && overflowsBottom || !isVertical && variation === 'end' && overflowsTop);

    var flippedVariation = flippedVariationByRef || flippedVariationByContent;

    if (overlapsRef || overflowsBoundaries || flippedVariation) {
      // this boolean to detect any flip loop
      data.flipped = true;

      if (overlapsRef || overflowsBoundaries) {
        placement = flipOrder[index + 1];
      }

      if (flippedVariation) {
        variation = getOppositeVariation(variation);
      }

      data.placement = placement + (variation ? '-' + variation : '');

      // this object contains `position`, we want to preserve it along with
      // any additional property we may add in the future
      data.offsets.popper = _extends({}, data.offsets.popper, getPopperOffsets(data.instance.popper, data.offsets.reference, data.placement));

      data = runModifiers(data.instance.modifiers, data, 'flip');
    }
  });
  return data;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function keepTogether(data) {
  var _data$offsets = data.offsets,
      popper = _data$offsets.popper,
      reference = _data$offsets.reference;

  var placement = data.placement.split('-')[0];
  var floor = Math.floor;
  var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;
  var side = isVertical ? 'right' : 'bottom';
  var opSide = isVertical ? 'left' : 'top';
  var measurement = isVertical ? 'width' : 'height';

  if (popper[side] < floor(reference[opSide])) {
    data.offsets.popper[opSide] = floor(reference[opSide]) - popper[measurement];
  }
  if (popper[opSide] > floor(reference[side])) {
    data.offsets.popper[opSide] = floor(reference[side]);
  }

  return data;
}

/**
 * Converts a string containing value + unit into a px value number
 * @function
 * @memberof {modifiers~offset}
 * @private
 * @argument {String} str - Value + unit string
 * @argument {String} measurement - `height` or `width`
 * @argument {Object} popperOffsets
 * @argument {Object} referenceOffsets
 * @returns {Number|String}
 * Value in pixels, or original string if no values were extracted
 */
function toValue(str, measurement, popperOffsets, referenceOffsets) {
  // separate value from unit
  var split = str.match(/((?:\-|\+)?\d*\.?\d*)(.*)/);
  var value = +split[1];
  var unit = split[2];

  // If it's not a number it's an operator, I guess
  if (!value) {
    return str;
  }

  if (unit.indexOf('%') === 0) {
    var element = void 0;
    switch (unit) {
      case '%p':
        element = popperOffsets;
        break;
      case '%':
      case '%r':
      default:
        element = referenceOffsets;
    }

    var rect = getClientRect(element);
    return rect[measurement] / 100 * value;
  } else if (unit === 'vh' || unit === 'vw') {
    // if is a vh or vw, we calculate the size based on the viewport
    var size = void 0;
    if (unit === 'vh') {
      size = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    } else {
      size = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    }
    return size / 100 * value;
  } else {
    // if is an explicit pixel unit, we get rid of the unit and keep the value
    // if is an implicit unit, it's px, and we return just the value
    return value;
  }
}

/**
 * Parse an `offset` string to extrapolate `x` and `y` numeric offsets.
 * @function
 * @memberof {modifiers~offset}
 * @private
 * @argument {String} offset
 * @argument {Object} popperOffsets
 * @argument {Object} referenceOffsets
 * @argument {String} basePlacement
 * @returns {Array} a two cells array with x and y offsets in numbers
 */
function parseOffset(offset, popperOffsets, referenceOffsets, basePlacement) {
  var offsets = [0, 0];

  // Use height if placement is left or right and index is 0 otherwise use width
  // in this way the first offset will use an axis and the second one
  // will use the other one
  var useHeight = ['right', 'left'].indexOf(basePlacement) !== -1;

  // Split the offset string to obtain a list of values and operands
  // The regex addresses values with the plus or minus sign in front (+10, -20, etc)
  var fragments = offset.split(/(\+|\-)/).map(function (frag) {
    return frag.trim();
  });

  // Detect if the offset string contains a pair of values or a single one
  // they could be separated by comma or space
  var divider = fragments.indexOf(find(fragments, function (frag) {
    return frag.search(/,|\s/) !== -1;
  }));

  if (fragments[divider] && fragments[divider].indexOf(',') === -1) {
    console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
  }

  // If divider is found, we divide the list of values and operands to divide
  // them by ofset X and Y.
  var splitRegex = /\s*,\s*|\s+/;
  var ops = divider !== -1 ? [fragments.slice(0, divider).concat([fragments[divider].split(splitRegex)[0]]), [fragments[divider].split(splitRegex)[1]].concat(fragments.slice(divider + 1))] : [fragments];

  // Convert the values with units to absolute pixels to allow our computations
  ops = ops.map(function (op, index) {
    // Most of the units rely on the orientation of the popper
    var measurement = (index === 1 ? !useHeight : useHeight) ? 'height' : 'width';
    var mergeWithPrevious = false;
    return op
    // This aggregates any `+` or `-` sign that aren't considered operators
    // e.g.: 10 + +5 => [10, +, +5]
    .reduce(function (a, b) {
      if (a[a.length - 1] === '' && ['+', '-'].indexOf(b) !== -1) {
        a[a.length - 1] = b;
        mergeWithPrevious = true;
        return a;
      } else if (mergeWithPrevious) {
        a[a.length - 1] += b;
        mergeWithPrevious = false;
        return a;
      } else {
        return a.concat(b);
      }
    }, [])
    // Here we convert the string values into number values (in px)
    .map(function (str) {
      return toValue(str, measurement, popperOffsets, referenceOffsets);
    });
  });

  // Loop trough the offsets arrays and execute the operations
  ops.forEach(function (op, index) {
    op.forEach(function (frag, index2) {
      if (isNumeric(frag)) {
        offsets[index] += frag * (op[index2 - 1] === '-' ? -1 : 1);
      }
    });
  });
  return offsets;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @argument {Number|String} options.offset=0
 * The offset value as described in the modifier description
 * @returns {Object} The data object, properly modified
 */
function offset(data, _ref) {
  var offset = _ref.offset;
  var placement = data.placement,
      _data$offsets = data.offsets,
      popper = _data$offsets.popper,
      reference = _data$offsets.reference;

  var basePlacement = placement.split('-')[0];

  var offsets = void 0;
  if (isNumeric(+offset)) {
    offsets = [+offset, 0];
  } else {
    offsets = parseOffset(offset, popper, reference, basePlacement);
  }

  if (basePlacement === 'left') {
    popper.top += offsets[0];
    popper.left -= offsets[1];
  } else if (basePlacement === 'right') {
    popper.top += offsets[0];
    popper.left += offsets[1];
  } else if (basePlacement === 'top') {
    popper.left += offsets[0];
    popper.top -= offsets[1];
  } else if (basePlacement === 'bottom') {
    popper.left += offsets[0];
    popper.top += offsets[1];
  }

  data.popper = popper;
  return data;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function preventOverflow(data, options) {
  var boundariesElement = options.boundariesElement || getOffsetParent(data.instance.popper);

  // If offsetParent is the reference element, we really want to
  // go one step up and use the next offsetParent as reference to
  // avoid to make this modifier completely useless and look like broken
  if (data.instance.reference === boundariesElement) {
    boundariesElement = getOffsetParent(boundariesElement);
  }

  // NOTE: DOM access here
  // resets the popper's position so that the document size can be calculated excluding
  // the size of the popper element itself
  var transformProp = getSupportedPropertyName('transform');
  var popperStyles = data.instance.popper.style; // assignment to help minification
  var top = popperStyles.top,
      left = popperStyles.left,
      transform = popperStyles[transformProp];

  popperStyles.top = '';
  popperStyles.left = '';
  popperStyles[transformProp] = '';

  var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, boundariesElement, data.positionFixed);

  // NOTE: DOM access here
  // restores the original style properties after the offsets have been computed
  popperStyles.top = top;
  popperStyles.left = left;
  popperStyles[transformProp] = transform;

  options.boundaries = boundaries;

  var order = options.priority;
  var popper = data.offsets.popper;

  var check = {
    primary: function primary(placement) {
      var value = popper[placement];
      if (popper[placement] < boundaries[placement] && !options.escapeWithReference) {
        value = Math.max(popper[placement], boundaries[placement]);
      }
      return defineProperty({}, placement, value);
    },
    secondary: function secondary(placement) {
      var mainSide = placement === 'right' ? 'left' : 'top';
      var value = popper[mainSide];
      if (popper[placement] > boundaries[placement] && !options.escapeWithReference) {
        value = Math.min(popper[mainSide], boundaries[placement] - (placement === 'right' ? popper.width : popper.height));
      }
      return defineProperty({}, mainSide, value);
    }
  };

  order.forEach(function (placement) {
    var side = ['left', 'top'].indexOf(placement) !== -1 ? 'primary' : 'secondary';
    popper = _extends({}, popper, check[side](placement));
  });

  data.offsets.popper = popper;

  return data;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function shift(data) {
  var placement = data.placement;
  var basePlacement = placement.split('-')[0];
  var shiftvariation = placement.split('-')[1];

  // if shift shiftvariation is specified, run the modifier
  if (shiftvariation) {
    var _data$offsets = data.offsets,
        reference = _data$offsets.reference,
        popper = _data$offsets.popper;

    var isVertical = ['bottom', 'top'].indexOf(basePlacement) !== -1;
    var side = isVertical ? 'left' : 'top';
    var measurement = isVertical ? 'width' : 'height';

    var shiftOffsets = {
      start: defineProperty({}, side, reference[side]),
      end: defineProperty({}, side, reference[side] + reference[measurement] - popper[measurement])
    };

    data.offsets.popper = _extends({}, popper, shiftOffsets[shiftvariation]);
  }

  return data;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function hide(data) {
  if (!isModifierRequired(data.instance.modifiers, 'hide', 'preventOverflow')) {
    return data;
  }

  var refRect = data.offsets.reference;
  var bound = find(data.instance.modifiers, function (modifier) {
    return modifier.name === 'preventOverflow';
  }).boundaries;

  if (refRect.bottom < bound.top || refRect.left > bound.right || refRect.top > bound.bottom || refRect.right < bound.left) {
    // Avoid unnecessary DOM access if visibility hasn't changed
    if (data.hide === true) {
      return data;
    }

    data.hide = true;
    data.attributes['x-out-of-boundaries'] = '';
  } else {
    // Avoid unnecessary DOM access if visibility hasn't changed
    if (data.hide === false) {
      return data;
    }

    data.hide = false;
    data.attributes['x-out-of-boundaries'] = false;
  }

  return data;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function inner(data) {
  var placement = data.placement;
  var basePlacement = placement.split('-')[0];
  var _data$offsets = data.offsets,
      popper = _data$offsets.popper,
      reference = _data$offsets.reference;

  var isHoriz = ['left', 'right'].indexOf(basePlacement) !== -1;

  var subtractLength = ['top', 'left'].indexOf(basePlacement) === -1;

  popper[isHoriz ? 'left' : 'top'] = reference[basePlacement] - (subtractLength ? popper[isHoriz ? 'width' : 'height'] : 0);

  data.placement = getOppositePlacement(placement);
  data.offsets.popper = getClientRect(popper);

  return data;
}

/**
 * Modifier function, each modifier can have a function of this type assigned
 * to its `fn` property.<br />
 * These functions will be called on each update, this means that you must
 * make sure they are performant enough to avoid performance bottlenecks.
 *
 * @function ModifierFn
 * @argument {dataObject} data - The data object generated by `update` method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {dataObject} The data object, properly modified
 */

/**
 * Modifiers are plugins used to alter the behavior of your poppers.<br />
 * Popper.js uses a set of 9 modifiers to provide all the basic functionalities
 * needed by the library.
 *
 * Usually you don't want to override the `order`, `fn` and `onLoad` props.
 * All the other properties are configurations that could be tweaked.
 * @namespace modifiers
 */
var modifiers = {
  /**
   * Modifier used to shift the popper on the start or end of its reference
   * element.<br />
   * It will read the variation of the `placement` property.<br />
   * It can be one either `-end` or `-start`.
   * @memberof modifiers
   * @inner
   */
  shift: {
    /** @prop {number} order=100 - Index used to define the order of execution */
    order: 100,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: shift
  },

  /**
   * The `offset` modifier can shift your popper on both its axis.
   *
   * It accepts the following units:
   * - `px` or unit-less, interpreted as pixels
   * - `%` or `%r`, percentage relative to the length of the reference element
   * - `%p`, percentage relative to the length of the popper element
   * - `vw`, CSS viewport width unit
   * - `vh`, CSS viewport height unit
   *
   * For length is intended the main axis relative to the placement of the popper.<br />
   * This means that if the placement is `top` or `bottom`, the length will be the
   * `width`. In case of `left` or `right`, it will be the `height`.
   *
   * You can provide a single value (as `Number` or `String`), or a pair of values
   * as `String` divided by a comma or one (or more) white spaces.<br />
   * The latter is a deprecated method because it leads to confusion and will be
   * removed in v2.<br />
   * Additionally, it accepts additions and subtractions between different units.
   * Note that multiplications and divisions aren't supported.
   *
   * Valid examples are:
   * ```
   * 10
   * '10%'
   * '10, 10'
   * '10%, 10'
   * '10 + 10%'
   * '10 - 5vh + 3%'
   * '-10px + 5vh, 5px - 6%'
   * ```
   * > **NB**: If you desire to apply offsets to your poppers in a way that may make them overlap
   * > with their reference element, unfortunately, you will have to disable the `flip` modifier.
   * > You can read more on this at this [issue](https://github.com/FezVrasta/popper.js/issues/373).
   *
   * @memberof modifiers
   * @inner
   */
  offset: {
    /** @prop {number} order=200 - Index used to define the order of execution */
    order: 200,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: offset,
    /** @prop {Number|String} offset=0
     * The offset value as described in the modifier description
     */
    offset: 0
  },

  /**
   * Modifier used to prevent the popper from being positioned outside the boundary.
   *
   * A scenario exists where the reference itself is not within the boundaries.<br />
   * We can say it has "escaped the boundaries" — or just "escaped".<br />
   * In this case we need to decide whether the popper should either:
   *
   * - detach from the reference and remain "trapped" in the boundaries, or
   * - if it should ignore the boundary and "escape with its reference"
   *
   * When `escapeWithReference` is set to`true` and reference is completely
   * outside its boundaries, the popper will overflow (or completely leave)
   * the boundaries in order to remain attached to the edge of the reference.
   *
   * @memberof modifiers
   * @inner
   */
  preventOverflow: {
    /** @prop {number} order=300 - Index used to define the order of execution */
    order: 300,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: preventOverflow,
    /**
     * @prop {Array} [priority=['left','right','top','bottom']]
     * Popper will try to prevent overflow following these priorities by default,
     * then, it could overflow on the left and on top of the `boundariesElement`
     */
    priority: ['left', 'right', 'top', 'bottom'],
    /**
     * @prop {number} padding=5
     * Amount of pixel used to define a minimum distance between the boundaries
     * and the popper. This makes sure the popper always has a little padding
     * between the edges of its container
     */
    padding: 5,
    /**
     * @prop {String|HTMLElement} boundariesElement='scrollParent'
     * Boundaries used by the modifier. Can be `scrollParent`, `window`,
     * `viewport` or any DOM element.
     */
    boundariesElement: 'scrollParent'
  },

  /**
   * Modifier used to make sure the reference and its popper stay near each other
   * without leaving any gap between the two. Especially useful when the arrow is
   * enabled and you want to ensure that it points to its reference element.
   * It cares only about the first axis. You can still have poppers with margin
   * between the popper and its reference element.
   * @memberof modifiers
   * @inner
   */
  keepTogether: {
    /** @prop {number} order=400 - Index used to define the order of execution */
    order: 400,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: keepTogether
  },

  /**
   * This modifier is used to move the `arrowElement` of the popper to make
   * sure it is positioned between the reference element and its popper element.
   * It will read the outer size of the `arrowElement` node to detect how many
   * pixels of conjunction are needed.
   *
   * It has no effect if no `arrowElement` is provided.
   * @memberof modifiers
   * @inner
   */
  arrow: {
    /** @prop {number} order=500 - Index used to define the order of execution */
    order: 500,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: arrow,
    /** @prop {String|HTMLElement} element='[x-arrow]' - Selector or node used as arrow */
    element: '[x-arrow]'
  },

  /**
   * Modifier used to flip the popper's placement when it starts to overlap its
   * reference element.
   *
   * Requires the `preventOverflow` modifier before it in order to work.
   *
   * **NOTE:** this modifier will interrupt the current update cycle and will
   * restart it if it detects the need to flip the placement.
   * @memberof modifiers
   * @inner
   */
  flip: {
    /** @prop {number} order=600 - Index used to define the order of execution */
    order: 600,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: flip,
    /**
     * @prop {String|Array} behavior='flip'
     * The behavior used to change the popper's placement. It can be one of
     * `flip`, `clockwise`, `counterclockwise` or an array with a list of valid
     * placements (with optional variations)
     */
    behavior: 'flip',
    /**
     * @prop {number} padding=5
     * The popper will flip if it hits the edges of the `boundariesElement`
     */
    padding: 5,
    /**
     * @prop {String|HTMLElement} boundariesElement='viewport'
     * The element which will define the boundaries of the popper position.
     * The popper will never be placed outside of the defined boundaries
     * (except if `keepTogether` is enabled)
     */
    boundariesElement: 'viewport',
    /**
     * @prop {Boolean} flipVariations=false
     * The popper will switch placement variation between `-start` and `-end` when
     * the reference element overlaps its boundaries.
     *
     * The original placement should have a set variation.
     */
    flipVariations: false,
    /**
     * @prop {Boolean} flipVariationsByContent=false
     * The popper will switch placement variation between `-start` and `-end` when
     * the popper element overlaps its reference boundaries.
     *
     * The original placement should have a set variation.
     */
    flipVariationsByContent: false
  },

  /**
   * Modifier used to make the popper flow toward the inner of the reference element.
   * By default, when this modifier is disabled, the popper will be placed outside
   * the reference element.
   * @memberof modifiers
   * @inner
   */
  inner: {
    /** @prop {number} order=700 - Index used to define the order of execution */
    order: 700,
    /** @prop {Boolean} enabled=false - Whether the modifier is enabled or not */
    enabled: false,
    /** @prop {ModifierFn} */
    fn: inner
  },

  /**
   * Modifier used to hide the popper when its reference element is outside of the
   * popper boundaries. It will set a `x-out-of-boundaries` attribute which can
   * be used to hide with a CSS selector the popper when its reference is
   * out of boundaries.
   *
   * Requires the `preventOverflow` modifier before it in order to work.
   * @memberof modifiers
   * @inner
   */
  hide: {
    /** @prop {number} order=800 - Index used to define the order of execution */
    order: 800,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: hide
  },

  /**
   * Computes the style that will be applied to the popper element to gets
   * properly positioned.
   *
   * Note that this modifier will not touch the DOM, it just prepares the styles
   * so that `applyStyle` modifier can apply it. This separation is useful
   * in case you need to replace `applyStyle` with a custom implementation.
   *
   * This modifier has `850` as `order` value to maintain backward compatibility
   * with previous versions of Popper.js. Expect the modifiers ordering method
   * to change in future major versions of the library.
   *
   * @memberof modifiers
   * @inner
   */
  computeStyle: {
    /** @prop {number} order=850 - Index used to define the order of execution */
    order: 850,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: computeStyle,
    /**
     * @prop {Boolean} gpuAcceleration=true
     * If true, it uses the CSS 3D transformation to position the popper.
     * Otherwise, it will use the `top` and `left` properties
     */
    gpuAcceleration: true,
    /**
     * @prop {string} [x='bottom']
     * Where to anchor the X axis (`bottom` or `top`). AKA X offset origin.
     * Change this if your popper should grow in a direction different from `bottom`
     */
    x: 'bottom',
    /**
     * @prop {string} [x='left']
     * Where to anchor the Y axis (`left` or `right`). AKA Y offset origin.
     * Change this if your popper should grow in a direction different from `right`
     */
    y: 'right'
  },

  /**
   * Applies the computed styles to the popper element.
   *
   * All the DOM manipulations are limited to this modifier. This is useful in case
   * you want to integrate Popper.js inside a framework or view library and you
   * want to delegate all the DOM manipulations to it.
   *
   * Note that if you disable this modifier, you must make sure the popper element
   * has its position set to `absolute` before Popper.js can do its work!
   *
   * Just disable this modifier and define your own to achieve the desired effect.
   *
   * @memberof modifiers
   * @inner
   */
  applyStyle: {
    /** @prop {number} order=900 - Index used to define the order of execution */
    order: 900,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: applyStyle,
    /** @prop {Function} */
    onLoad: applyStyleOnLoad,
    /**
     * @deprecated since version 1.10.0, the property moved to `computeStyle` modifier
     * @prop {Boolean} gpuAcceleration=true
     * If true, it uses the CSS 3D transformation to position the popper.
     * Otherwise, it will use the `top` and `left` properties
     */
    gpuAcceleration: undefined
  }
};

/**
 * The `dataObject` is an object containing all the information used by Popper.js.
 * This object is passed to modifiers and to the `onCreate` and `onUpdate` callbacks.
 * @name dataObject
 * @property {Object} data.instance The Popper.js instance
 * @property {String} data.placement Placement applied to popper
 * @property {String} data.originalPlacement Placement originally defined on init
 * @property {Boolean} data.flipped True if popper has been flipped by flip modifier
 * @property {Boolean} data.hide True if the reference element is out of boundaries, useful to know when to hide the popper
 * @property {HTMLElement} data.arrowElement Node used as arrow by arrow modifier
 * @property {Object} data.styles Any CSS property defined here will be applied to the popper. It expects the JavaScript nomenclature (eg. `marginBottom`)
 * @property {Object} data.arrowStyles Any CSS property defined here will be applied to the popper arrow. It expects the JavaScript nomenclature (eg. `marginBottom`)
 * @property {Object} data.boundaries Offsets of the popper boundaries
 * @property {Object} data.offsets The measurements of popper, reference and arrow elements
 * @property {Object} data.offsets.popper `top`, `left`, `width`, `height` values
 * @property {Object} data.offsets.reference `top`, `left`, `width`, `height` values
 * @property {Object} data.offsets.arrow] `top` and `left` offsets, only one of them will be different from 0
 */

/**
 * Default options provided to Popper.js constructor.<br />
 * These can be overridden using the `options` argument of Popper.js.<br />
 * To override an option, simply pass an object with the same
 * structure of the `options` object, as the 3rd argument. For example:
 * ```
 * new Popper(ref, pop, {
 *   modifiers: {
 *     preventOverflow: { enabled: false }
 *   }
 * })
 * ```
 * @type {Object}
 * @static
 * @memberof Popper
 */
var Defaults = {
  /**
   * Popper's placement.
   * @prop {Popper.placements} placement='bottom'
   */
  placement: 'bottom',

  /**
   * Set this to true if you want popper to position it self in 'fixed' mode
   * @prop {Boolean} positionFixed=false
   */
  positionFixed: false,

  /**
   * Whether events (resize, scroll) are initially enabled.
   * @prop {Boolean} eventsEnabled=true
   */
  eventsEnabled: true,

  /**
   * Set to true if you want to automatically remove the popper when
   * you call the `destroy` method.
   * @prop {Boolean} removeOnDestroy=false
   */
  removeOnDestroy: false,

  /**
   * Callback called when the popper is created.<br />
   * By default, it is set to no-op.<br />
   * Access Popper.js instance with `data.instance`.
   * @prop {onCreate}
   */
  onCreate: function onCreate() {},

  /**
   * Callback called when the popper is updated. This callback is not called
   * on the initialization/creation of the popper, but only on subsequent
   * updates.<br />
   * By default, it is set to no-op.<br />
   * Access Popper.js instance with `data.instance`.
   * @prop {onUpdate}
   */
  onUpdate: function onUpdate() {},

  /**
   * List of modifiers used to modify the offsets before they are applied to the popper.
   * They provide most of the functionalities of Popper.js.
   * @prop {modifiers}
   */
  modifiers: modifiers
};

/**
 * @callback onCreate
 * @param {dataObject} data
 */

/**
 * @callback onUpdate
 * @param {dataObject} data
 */

// Utils
// Methods
var Popper = function () {
  /**
   * Creates a new Popper.js instance.
   * @class Popper
   * @param {Element|referenceObject} reference - The reference element used to position the popper
   * @param {Element} popper - The HTML / XML element used as the popper
   * @param {Object} options - Your custom options to override the ones defined in [Defaults](#defaults)
   * @return {Object} instance - The generated Popper.js instance
   */
  function Popper(reference, popper) {
    var _this = this;

    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    classCallCheck(this, Popper);

    this.scheduleUpdate = function () {
      return requestAnimationFrame(_this.update);
    };

    // make update() debounced, so that it only runs at most once-per-tick
    this.update = debounce(this.update.bind(this));

    // with {} we create a new object with the options inside it
    this.options = _extends({}, Popper.Defaults, options);

    // init state
    this.state = {
      isDestroyed: false,
      isCreated: false,
      scrollParents: []
    };

    // get reference and popper elements (allow jQuery wrappers)
    this.reference = reference && reference.jquery ? reference[0] : reference;
    this.popper = popper && popper.jquery ? popper[0] : popper;

    // Deep merge modifiers options
    this.options.modifiers = {};
    Object.keys(_extends({}, Popper.Defaults.modifiers, options.modifiers)).forEach(function (name) {
      _this.options.modifiers[name] = _extends({}, Popper.Defaults.modifiers[name] || {}, options.modifiers ? options.modifiers[name] : {});
    });

    // Refactoring modifiers' list (Object => Array)
    this.modifiers = Object.keys(this.options.modifiers).map(function (name) {
      return _extends({
        name: name
      }, _this.options.modifiers[name]);
    })
    // sort the modifiers by order
    .sort(function (a, b) {
      return a.order - b.order;
    });

    // modifiers have the ability to execute arbitrary code when Popper.js get inited
    // such code is executed in the same order of its modifier
    // they could add new properties to their options configuration
    // BE AWARE: don't add options to `options.modifiers.name` but to `modifierOptions`!
    this.modifiers.forEach(function (modifierOptions) {
      if (modifierOptions.enabled && isFunction(modifierOptions.onLoad)) {
        modifierOptions.onLoad(_this.reference, _this.popper, _this.options, modifierOptions, _this.state);
      }
    });

    // fire the first update to position the popper in the right place
    this.update();

    var eventsEnabled = this.options.eventsEnabled;
    if (eventsEnabled) {
      // setup event listeners, they will take care of update the position in specific situations
      this.enableEventListeners();
    }

    this.state.eventsEnabled = eventsEnabled;
  }

  // We can't use class properties because they don't get listed in the
  // class prototype and break stuff like Sinon stubs


  createClass(Popper, [{
    key: 'update',
    value: function update$$1() {
      return update.call(this);
    }
  }, {
    key: 'destroy',
    value: function destroy$$1() {
      return destroy.call(this);
    }
  }, {
    key: 'enableEventListeners',
    value: function enableEventListeners$$1() {
      return enableEventListeners.call(this);
    }
  }, {
    key: 'disableEventListeners',
    value: function disableEventListeners$$1() {
      return disableEventListeners.call(this);
    }

    /**
     * Schedules an update. It will run on the next UI update available.
     * @method scheduleUpdate
     * @memberof Popper
     */


    /**
     * Collection of utilities useful when writing custom modifiers.
     * Starting from version 1.7, this method is available only if you
     * include `popper-utils.js` before `popper.js`.
     *
     * **DEPRECATION**: This way to access PopperUtils is deprecated
     * and will be removed in v2! Use the PopperUtils module directly instead.
     * Due to the high instability of the methods contained in Utils, we can't
     * guarantee them to follow semver. Use them at your own risk!
     * @static
     * @private
     * @type {Object}
     * @deprecated since version 1.8
     * @member Utils
     * @memberof Popper
     */

  }]);
  return Popper;
}();

/**
 * The `referenceObject` is an object that provides an interface compatible with Popper.js
 * and lets you use it as replacement of a real DOM node.<br />
 * You can use this method to position a popper relatively to a set of coordinates
 * in case you don't have a DOM node to use as reference.
 *
 * ```
 * new Popper(referenceObject, popperNode);
 * ```
 *
 * NB: This feature isn't supported in Internet Explorer 10.
 * @name referenceObject
 * @property {Function} data.getBoundingClientRect
 * A function that returns a set of coordinates compatible with the native `getBoundingClientRect` method.
 * @property {number} data.clientWidth
 * An ES6 getter that will return the width of the virtual reference element.
 * @property {number} data.clientHeight
 * An ES6 getter that will return the height of the virtual reference element.
 */


Popper.Utils = (typeof window !== 'undefined' ? window : global).PopperUtils;
Popper.placements = placements;
Popper.Defaults = Defaults;

/* harmony default export */ __webpack_exports__["default"] = (Popper);
//# sourceMappingURL=popper.js.map

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./node_modules/webpack/buildin/module.js":
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(module) {
	if (!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ "./public/alert/dist/sweetalert2.all.min.js":
/*!**************************************************!*\
  !*** ./public/alert/dist/sweetalert2.all.min.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (t, e) {
  "object" == ( false ? undefined : _typeof(exports)) && "undefined" != typeof module ? module.exports = e() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function () {
  "use strict";

  function r(t) {
    return (r = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (t) {
      return _typeof(t);
    } : function (t) {
      return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : _typeof(t);
    })(t);
  }

  function a(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
  }

  function o(t, e) {
    for (var n = 0; n < e.length; n++) {
      var o = e[n];
      o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
    }
  }

  function c(t, e, n) {
    return e && o(t.prototype, e), n && o(t, n), t;
  }

  function s() {
    return (s = Object.assign || function (t) {
      for (var e = 1; e < arguments.length; e++) {
        var n = arguments[e];

        for (var o in n) {
          Object.prototype.hasOwnProperty.call(n, o) && (t[o] = n[o]);
        }
      }

      return t;
    }).apply(this, arguments);
  }

  function u(t) {
    return (u = Object.setPrototypeOf ? Object.getPrototypeOf : function (t) {
      return t.__proto__ || Object.getPrototypeOf(t);
    })(t);
  }

  function l(t, e) {
    return (l = Object.setPrototypeOf || function (t, e) {
      return t.__proto__ = e, t;
    })(t, e);
  }

  function d() {
    if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
    if (Reflect.construct.sham) return !1;
    if ("function" == typeof Proxy) return !0;

    try {
      return Date.prototype.toString.call(Reflect.construct(Date, [], function () {})), !0;
    } catch (t) {
      return !1;
    }
  }

  function i(t, e, n) {
    return (i = d() ? Reflect.construct : function (t, e, n) {
      var o = [null];
      o.push.apply(o, e);
      var i = new (Function.bind.apply(t, o))();
      return n && l(i, n.prototype), i;
    }).apply(null, arguments);
  }

  function p(t, e) {
    return !e || "object" != _typeof(e) && "function" != typeof e ? function (t) {
      if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
      return t;
    }(t) : e;
  }

  function f(t, e, n) {
    return (f = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (t, e, n) {
      var o = function (t, e) {
        for (; !Object.prototype.hasOwnProperty.call(t, e) && null !== (t = u(t));) {
          ;
        }

        return t;
      }(t, e);

      if (o) {
        var i = Object.getOwnPropertyDescriptor(o, e);
        return i.get ? i.get.call(n) : i.value;
      }
    })(t, e, n || t);
  }

  function m(e) {
    return Object.keys(e).map(function (t) {
      return e[t];
    });
  }

  function h(t) {
    return Array.prototype.slice.call(t);
  }

  function v(t) {
    console.error("".concat(N, " ").concat(t));
  }

  function g(t, e) {
    var n;
    n = '"'.concat(t, '" is deprecated and will be removed in the next major release. Please use "').concat(e, '" instead.'), -1 === _.indexOf(n) && (_.push(n), U(n));
  }

  function b(t) {
    return t && Promise.resolve(t) === t;
  }

  function y(t) {
    return t instanceof Element || "object" === r(e = t) && e.jquery;
    var e;
  }

  function t(t) {
    var e = {};

    for (var n in t) {
      e[t[n]] = "swal2-" + t[n];
    }

    return e;
  }

  function w(t) {
    var e = Y();
    return e ? e.querySelector(t) : null;
  }

  function e(t) {
    return w(".".concat(t));
  }

  function n() {
    var t = Z();
    return h(t.querySelectorAll(".".concat(W.icon)));
  }

  function C() {
    var t = n().filter(function (t) {
      return ht(t);
    });
    return t.length ? t[0] : null;
  }

  function k() {
    return e(W.title);
  }

  function x() {
    return e(W.content);
  }

  function P() {
    return e(W.image);
  }

  function A() {
    return e(W["progress-steps"]);
  }

  function B() {
    return e(W["validation-message"]);
  }

  function S() {
    return w(".".concat(W.actions, " .").concat(W.confirm));
  }

  function E() {
    return w(".".concat(W.actions, " .").concat(W.cancel));
  }

  function O() {
    return e(W.actions);
  }

  function T() {
    return e(W.header);
  }

  function L() {
    return e(W.footer);
  }

  function q() {
    return e(W["timer-progress-bar"]);
  }

  function I() {
    return e(W.close);
  }

  function j() {
    var t = h(Z().querySelectorAll('[tabindex]:not([tabindex="-1"]):not([tabindex="0"])')).sort(function (t, e) {
      return t = parseInt(t.getAttribute("tabindex")), (e = parseInt(e.getAttribute("tabindex"))) < t ? 1 : t < e ? -1 : 0;
    }),
        e = h(Z().querySelectorAll('\n  a[href],\n  area[href],\n  input:not([disabled]),\n  select:not([disabled]),\n  textarea:not([disabled]),\n  button:not([disabled]),\n  iframe,\n  object,\n  embed,\n  [tabindex="0"],\n  [contenteditable],\n  audio[controls],\n  video[controls],\n  summary\n')).filter(function (t) {
      return "-1" !== t.getAttribute("tabindex");
    });
    return function (t) {
      for (var e = [], n = 0; n < t.length; n++) {
        -1 === e.indexOf(t[n]) && e.push(t[n]);
      }

      return e;
    }(t.concat(e)).filter(function (t) {
      return ht(t);
    });
  }

  function M() {
    return !Q() && !document.body.classList.contains(W["no-backdrop"]);
  }

  function V() {
    return Z().hasAttribute("data-loading");
  }

  function R(e, t) {
    var n;
    e.textContent = "", t && (n = new DOMParser().parseFromString(t, "text/html"), h(n.querySelector("head").childNodes).forEach(function (t) {
      e.appendChild(t);
    }), h(n.querySelector("body").childNodes).forEach(function (t) {
      e.appendChild(t);
    }));
  }

  function H(t, e) {
    if (e) {
      for (var n = e.split(/\s+/), o = 0; o < n.length; o++) {
        if (!t.classList.contains(n[o])) return;
      }

      return 1;
    }
  }

  function D(t, e, n) {
    var o, i;

    if (i = e, h((o = t).classList).forEach(function (t) {
      -1 === m(W).indexOf(t) && -1 === m(K).indexOf(t) && -1 === m(i.showClass).indexOf(t) && o.classList.remove(t);
    }), e.customClass && e.customClass[n]) {
      if ("string" != typeof e.customClass[n] && !e.customClass[n].forEach) return U("Invalid type of customClass.".concat(n, '! Expected string or iterable object, got "').concat(r(e.customClass[n]), '"'));
      pt(t, e.customClass[n]);
    }
  }

  var N = "SweetAlert2:",
      U = function U(t) {
    console.warn("".concat(N, " ").concat(t));
  },
      _ = [],
      F = function F(t) {
    return "function" == typeof t ? t() : t;
  },
      z = Object.freeze({
    cancel: "cancel",
    backdrop: "backdrop",
    close: "close",
    esc: "esc",
    timer: "timer"
  }),
      W = t(["container", "shown", "height-auto", "iosfix", "popup", "modal", "no-backdrop", "no-transition", "toast", "toast-shown", "toast-column", "show", "hide", "close", "title", "header", "content", "html-container", "actions", "confirm", "cancel", "footer", "icon", "icon-content", "image", "input", "file", "range", "select", "radio", "checkbox", "label", "textarea", "inputerror", "validation-message", "progress-steps", "active-progress-step", "progress-step", "progress-step-line", "loading", "styled", "top", "top-start", "top-end", "top-left", "top-right", "center", "center-start", "center-end", "center-left", "center-right", "bottom", "bottom-start", "bottom-end", "bottom-left", "bottom-right", "grow-row", "grow-column", "grow-fullscreen", "rtl", "timer-progress-bar", "timer-progress-bar-container", "scrollbar-measure", "icon-success", "icon-warning", "icon-info", "icon-question", "icon-error"]),
      K = t(["success", "warning", "info", "question", "error"]),
      Y = function Y() {
    return document.body.querySelector(".".concat(W.container));
  },
      Z = function Z() {
    return e(W.popup);
  },
      Q = function Q() {
    return document.body.classList.contains(W["toast-shown"]);
  },
      $ = {
    previousBodyPadding: null
  };

  function J(t, e) {
    if (!e) return null;

    switch (e) {
      case "select":
      case "textarea":
      case "file":
        return mt(t, W[e]);

      case "checkbox":
        return t.querySelector(".".concat(W.checkbox, " input"));

      case "radio":
        return t.querySelector(".".concat(W.radio, " input:checked")) || t.querySelector(".".concat(W.radio, " input:first-child"));

      case "range":
        return t.querySelector(".".concat(W.range, " input"));

      default:
        return mt(t, W.input);
    }
  }

  function X(t) {
    var e;
    t.focus(), "file" !== t.type && (e = t.value, t.value = "", t.value = e);
  }

  function G(t, e, n) {
    t && e && ("string" == typeof e && (e = e.split(/\s+/).filter(Boolean)), e.forEach(function (e) {
      t.forEach ? t.forEach(function (t) {
        n ? t.classList.add(e) : t.classList.remove(e);
      }) : n ? t.classList.add(e) : t.classList.remove(e);
    }));
  }

  function tt(t, e, n) {
    n || 0 === parseInt(n) ? t.style[e] = "number" == typeof n ? "".concat(n, "px") : n : t.style.removeProperty(e);
  }

  function et(t, e) {
    var n = 1 < arguments.length && void 0 !== e ? e : "flex";
    t.style.opacity = "", t.style.display = n;
  }

  function nt(t) {
    t.style.opacity = "", t.style.display = "none";
  }

  function ot(t, e, n) {
    e ? et(t, n) : nt(t);
  }

  function it(t) {
    return !!(t.scrollHeight > t.clientHeight);
  }

  function rt(t) {
    var e = window.getComputedStyle(t),
        n = parseFloat(e.getPropertyValue("animation-duration") || "0"),
        o = parseFloat(e.getPropertyValue("transition-duration") || "0");
    return 0 < n || 0 < o;
  }

  function at(t, e) {
    var n = 1 < arguments.length && void 0 !== e && e,
        o = q();
    ht(o) && (n && (o.style.transition = "none", o.style.width = "100%"), setTimeout(function () {
      o.style.transition = "width ".concat(t / 1e3, "s linear"), o.style.width = "0%";
    }, 10));
  }

  function ct() {
    return "undefined" == typeof window || "undefined" == typeof document;
  }

  function st(t) {
    on.isVisible() && dt !== t.target.value && on.resetValidationMessage(), dt = t.target.value;
  }

  function ut(t, e) {
    t instanceof HTMLElement ? e.appendChild(t) : "object" === r(t) ? bt(t, e) : t && R(e, t);
  }

  function lt(t, e) {
    var n = O(),
        o = S(),
        i = E();
    e.showConfirmButton || e.showCancelButton || nt(n), D(n, e, "actions"), Ct(o, "confirm", e), Ct(i, "cancel", e), e.buttonsStyling ? function (t, e, n) {
      pt([t, e], W.styled), n.confirmButtonColor && (t.style.backgroundColor = n.confirmButtonColor);
      n.cancelButtonColor && (e.style.backgroundColor = n.cancelButtonColor);
      {
        var o;
        V() || (o = window.getComputedStyle(t).getPropertyValue("background-color"), t.style.borderLeftColor = o, t.style.borderRightColor = o);
      }
    }(o, i, e) : (ft([o, i], W.styled), o.style.backgroundColor = o.style.borderLeftColor = o.style.borderRightColor = "", i.style.backgroundColor = i.style.borderLeftColor = i.style.borderRightColor = ""), e.reverseButtons && o.parentNode.insertBefore(i, o);
  }

  var dt,
      pt = function pt(t, e) {
    G(t, e, !0);
  },
      ft = function ft(t, e) {
    G(t, e, !1);
  },
      mt = function mt(t, e) {
    for (var n = 0; n < t.childNodes.length; n++) {
      if (H(t.childNodes[n], e)) return t.childNodes[n];
    }
  },
      ht = function ht(t) {
    return !(!t || !(t.offsetWidth || t.offsetHeight || t.getClientRects().length));
  },
      gt = '\n <div aria-labelledby="'.concat(W.title, '" aria-describedby="').concat(W.content, '" class="').concat(W.popup, '" tabindex="-1">\n   <div class="').concat(W.header, '">\n     <ul class="').concat(W["progress-steps"], '"></ul>\n     <div class="').concat(W.icon, " ").concat(K.error, '"></div>\n     <div class="').concat(W.icon, " ").concat(K.question, '"></div>\n     <div class="').concat(W.icon, " ").concat(K.warning, '"></div>\n     <div class="').concat(W.icon, " ").concat(K.info, '"></div>\n     <div class="').concat(W.icon, " ").concat(K.success, '"></div>\n     <img class="').concat(W.image, '" />\n     <h2 class="').concat(W.title, '" id="').concat(W.title, '"></h2>\n     <button type="button" class="').concat(W.close, '"></button>\n   </div>\n   <div class="').concat(W.content, '">\n     <div id="').concat(W.content, '" class="').concat(W["html-container"], '"></div>\n     <input class="').concat(W.input, '" />\n     <input type="file" class="').concat(W.file, '" />\n     <div class="').concat(W.range, '">\n       <input type="range" />\n       <output></output>\n     </div>\n     <select class="').concat(W.select, '"></select>\n     <div class="').concat(W.radio, '"></div>\n     <label for="').concat(W.checkbox, '" class="').concat(W.checkbox, '">\n       <input type="checkbox" />\n       <span class="').concat(W.label, '"></span>\n     </label>\n     <textarea class="').concat(W.textarea, '"></textarea>\n     <div class="').concat(W["validation-message"], '" id="').concat(W["validation-message"], '"></div>\n   </div>\n   <div class="').concat(W.actions, '">\n     <button type="button" class="').concat(W.confirm, '">OK</button>\n     <button type="button" class="').concat(W.cancel, '">Cancel</button>\n   </div>\n   <div class="').concat(W.footer, '"></div>\n   <div class="').concat(W["timer-progress-bar-container"], '">\n     <div class="').concat(W["timer-progress-bar"], '"></div>\n   </div>\n </div>\n').replace(/(^|\n)\s*/g, ""),
      vt = function vt(t) {
    var e,
        n,
        o,
        i,
        r,
        a,
        c,
        s,
        u,
        l,
        d,
        p,
        f,
        m,
        h,
        g = !!(e = Y()) && (e.parentNode.removeChild(e), ft([document.documentElement, document.body], [W["no-backdrop"], W["toast-shown"], W["has-column"]]), !0);
    ct() ? v("SweetAlert2 requires document to initialize") : ((n = document.createElement("div")).className = W.container, g && pt(n, W["no-transition"]), R(n, gt), (o = "string" == typeof (i = t.target) ? document.querySelector(i) : i).appendChild(n), r = t, (a = Z()).setAttribute("role", r.toast ? "alert" : "dialog"), a.setAttribute("aria-live", r.toast ? "polite" : "assertive"), r.toast || a.setAttribute("aria-modal", "true"), c = o, "rtl" === window.getComputedStyle(c).direction && pt(Y(), W.rtl), s = x(), u = mt(s, W.input), l = mt(s, W.file), d = s.querySelector(".".concat(W.range, " input")), p = s.querySelector(".".concat(W.range, " output")), f = mt(s, W.select), m = s.querySelector(".".concat(W.checkbox, " input")), h = mt(s, W.textarea), u.oninput = st, l.onchange = st, f.onchange = st, m.onchange = st, h.oninput = st, d.oninput = function (t) {
      st(t), p.value = d.value;
    }, d.onchange = function (t) {
      st(t), d.nextSibling.value = d.value;
    });
  },
      bt = function bt(t, e) {
    t.jquery ? yt(e, t) : R(e, t.toString());
  },
      yt = function yt(t, e) {
    if (t.textContent = "", 0 in e) for (var n = 0; (n in e); n++) {
      t.appendChild(e[n].cloneNode(!0));
    } else t.appendChild(e.cloneNode(!0));
  },
      wt = function () {
    if (ct()) return !1;
    var t = document.createElement("div"),
        e = {
      WebkitAnimation: "webkitAnimationEnd",
      OAnimation: "oAnimationEnd oanimationend",
      animation: "animationend"
    };

    for (var n in e) {
      if (Object.prototype.hasOwnProperty.call(e, n) && void 0 !== t.style[n]) return e[n];
    }

    return !1;
  }();

  function Ct(t, e, n) {
    var o;
    ot(t, n["show".concat((o = e).charAt(0).toUpperCase() + o.slice(1), "Button")], "inline-block"), R(t, n["".concat(e, "ButtonText")]), t.setAttribute("aria-label", n["".concat(e, "ButtonAriaLabel")]), t.className = W[e], D(t, n, "".concat(e, "Button")), pt(t, n["".concat(e, "ButtonClass")]);
  }

  function kt(t, e) {
    var n,
        o,
        i,
        r,
        a,
        c,
        s,
        u,
        l = Y();
    l && (n = l, "string" == typeof (o = e.backdrop) ? n.style.background = o : o || pt([document.documentElement, document.body], W["no-backdrop"]), !e.backdrop && e.allowOutsideClick && U('"allowOutsideClick" parameter requires `backdrop` parameter to be set to `true`'), i = l, (r = e.position) in W ? pt(i, W[r]) : (U('The "position" parameter is not valid, defaulting to "center"'), pt(i, W.center)), a = l, !(c = e.grow) || "string" != typeof c || (s = "grow-".concat(c)) in W && pt(a, W[s]), D(l, e, "container"), (u = document.body.getAttribute("data-swal2-queue-step")) && (l.setAttribute("data-queue-step", u), document.body.removeAttribute("data-swal2-queue-step")));
  }

  function xt(t, e) {
    t.placeholder && !e.inputPlaceholder || (t.placeholder = e.inputPlaceholder);
  }

  var Pt = {
    promise: new WeakMap(),
    innerParams: new WeakMap(),
    domCache: new WeakMap()
  },
      At = ["input", "file", "range", "select", "radio", "checkbox", "textarea"],
      Bt = function Bt(t) {
    if (!Tt[t.input]) return v('Unexpected type of input! Expected "text", "email", "password", "number", "tel", "select", "radio", "checkbox", "textarea", "file" or "url", got "'.concat(t.input, '"'));
    var e = Ot(t.input),
        n = Tt[t.input](e, t);
    et(n), setTimeout(function () {
      X(n);
    });
  },
      St = function St(t, e) {
    var n = J(x(), t);
    if (n) for (var o in !function (t) {
      for (var e = 0; e < t.attributes.length; e++) {
        var n = t.attributes[e].name;
        -1 === ["type", "value", "style"].indexOf(n) && t.removeAttribute(n);
      }
    }(n), e) {
      "range" === t && "placeholder" === o || n.setAttribute(o, e[o]);
    }
  },
      Et = function Et(t) {
    var e = Ot(t.input);
    t.customClass && pt(e, t.customClass.input);
  },
      Ot = function Ot(t) {
    var e = W[t] ? W[t] : W.input;
    return mt(x(), e);
  },
      Tt = {};

  Tt.text = Tt.email = Tt.password = Tt.number = Tt.tel = Tt.url = function (t, e) {
    return "string" == typeof e.inputValue || "number" == typeof e.inputValue ? t.value = e.inputValue : b(e.inputValue) || U('Unexpected type of inputValue! Expected "string", "number" or "Promise", got "'.concat(r(e.inputValue), '"')), xt(t, e), t.type = e.input, t;
  }, Tt.file = function (t, e) {
    return xt(t, e), t;
  }, Tt.range = function (t, e) {
    var n = t.querySelector("input"),
        o = t.querySelector("output");
    return n.value = e.inputValue, n.type = e.input, o.value = e.inputValue, t;
  }, Tt.select = function (t, e) {
    var n;
    return t.textContent = "", e.inputPlaceholder && (n = document.createElement("option"), R(n, e.inputPlaceholder), n.value = "", n.disabled = !0, n.selected = !0, t.appendChild(n)), t;
  }, Tt.radio = function (t) {
    return t.textContent = "", t;
  }, Tt.checkbox = function (t, e) {
    var n = J(x(), "checkbox");
    n.value = 1, n.id = W.checkbox, n.checked = Boolean(e.inputValue);
    var o = t.querySelector("span");
    return R(o, e.inputPlaceholder), t;
  }, Tt.textarea = function (e, t) {
    var n, o;
    return e.value = t.inputValue, xt(e, t), "MutationObserver" in window && (n = parseInt(window.getComputedStyle(Z()).width), o = parseInt(window.getComputedStyle(Z()).paddingLeft) + parseInt(window.getComputedStyle(Z()).paddingRight), new MutationObserver(function () {
      var t = e.offsetWidth + o;
      Z().style.width = n < t ? "".concat(t, "px") : null;
    }).observe(e, {
      attributes: !0,
      attributeFilter: ["style"]
    })), e;
  };

  function Lt(t, e) {
    var n,
        o,
        i,
        r,
        a,
        c = x().querySelector("#".concat(W.content));
    e.html ? (ut(e.html, c), et(c, "block")) : e.text ? (c.textContent = e.text, et(c, "block")) : nt(c), n = t, o = e, i = x(), r = Pt.innerParams.get(n), a = !r || o.input !== r.input, At.forEach(function (t) {
      var e = W[t],
          n = mt(i, e);
      St(t, o.inputAttributes), n.className = e, a && nt(n);
    }), o.input && (a && Bt(o), Et(o)), D(x(), e, "content");
  }

  function qt() {
    return Y() && Y().getAttribute("data-queue-step");
  }

  function It(t, s) {
    var u = A();
    if (!s.progressSteps || 0 === s.progressSteps.length) return nt(u);
    et(u), u.textContent = "";
    var l = parseInt(void 0 === s.currentProgressStep ? qt() : s.currentProgressStep);
    l >= s.progressSteps.length && U("Invalid currentProgressStep parameter, it should be less than progressSteps.length (currentProgressStep like JS arrays starts from 0)"), s.progressSteps.forEach(function (t, e) {
      var n,
          o,
          i,
          r,
          a,
          c = (n = t, o = document.createElement("li"), pt(o, W["progress-step"]), R(o, n), o);
      u.appendChild(c), e === l && pt(c, W["active-progress-step"]), e !== s.progressSteps.length - 1 && (r = t, a = document.createElement("li"), pt(a, W["progress-step-line"]), r.progressStepsDistance && (a.style.width = r.progressStepsDistance), i = a, u.appendChild(i));
    });
  }

  function jt(t, e) {
    var n,
        o,
        i,
        r,
        a,
        c,
        s,
        u,
        l = T();
    D(l, e, "header"), It(0, e), n = t, o = e, (r = Pt.innerParams.get(n)) && o.icon === r.icon && C() ? D(C(), o, "icon") : (Rt(), o.icon && (-1 !== Object.keys(K).indexOf(o.icon) ? (i = w(".".concat(W.icon, ".").concat(K[o.icon])), et(i), Dt(i, o), Ht(), D(i, o, "icon"), pt(i, o.showClass.icon)) : v('Unknown icon! Expected "success", "error", "warning", "info" or "question", got "'.concat(o.icon, '"')))), function (t) {
      var e = P();
      if (!t.imageUrl) return nt(e);
      et(e), e.setAttribute("src", t.imageUrl), e.setAttribute("alt", t.imageAlt), tt(e, "width", t.imageWidth), tt(e, "height", t.imageHeight), e.className = W.image, D(e, t, "image");
    }(e), a = e, c = k(), ot(c, a.title || a.titleText), a.title && ut(a.title, c), a.titleText && (c.innerText = a.titleText), D(c, a, "title"), s = e, u = I(), R(u, s.closeButtonHtml), D(u, s, "closeButton"), ot(u, s.showCloseButton), u.setAttribute("aria-label", s.closeButtonAriaLabel);
  }

  function Mt(t, e) {
    var n, o, i, r;
    n = e, o = Z(), tt(o, "width", n.width), tt(o, "padding", n.padding), n.background && (o.style.background = n.background), _t(o, n), kt(0, e), jt(t, e), Lt(t, e), lt(0, e), i = e, r = L(), ot(r, i.footer), i.footer && ut(i.footer, r), D(r, i, "footer"), "function" == typeof e.onRender && e.onRender(Z());
  }

  function Vt() {
    return S() && S().click();
  }

  var Rt = function Rt() {
    for (var t = n(), e = 0; e < t.length; e++) {
      nt(t[e]);
    }
  },
      Ht = function Ht() {
    for (var t = Z(), e = window.getComputedStyle(t).getPropertyValue("background-color"), n = t.querySelectorAll("[class^=swal2-success-circular-line], .swal2-success-fix"), o = 0; o < n.length; o++) {
      n[o].style.backgroundColor = e;
    }
  },
      Dt = function Dt(t, e) {
    t.textContent = "", e.iconHtml ? R(t, Nt(e.iconHtml)) : "success" === e.icon ? R(t, '\n      <div class="swal2-success-circular-line-left"></div>\n      <span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span>\n      <div class="swal2-success-ring"></div> <div class="swal2-success-fix"></div>\n      <div class="swal2-success-circular-line-right"></div>\n    ') : "error" === e.icon ? R(t, '\n      <span class="swal2-x-mark">\n        <span class="swal2-x-mark-line-left"></span>\n        <span class="swal2-x-mark-line-right"></span>\n      </span>\n    ') : R(t, Nt({
      question: "?",
      warning: "!",
      info: "i"
    }[e.icon]));
  },
      Nt = function Nt(t) {
    return '<div class="'.concat(W["icon-content"], '">').concat(t, "</div>");
  },
      Ut = [],
      _t = function _t(t, e) {
    t.className = "".concat(W.popup, " ").concat(ht(t) ? e.showClass.popup : ""), e.toast ? (pt([document.documentElement, document.body], W["toast-shown"]), pt(t, W.toast)) : pt(t, W.modal), D(t, e, "popup"), "string" == typeof e.customClass && pt(t, e.customClass), e.icon && pt(t, W["icon-".concat(e.icon)]);
  };

  function Ft() {
    var t = Z();
    t || on.fire(), t = Z();
    var e = O(),
        n = S();
    et(e), et(n, "inline-block"), pt([t, e], W.loading), n.disabled = !0, t.setAttribute("data-loading", !0), t.setAttribute("aria-busy", !0), t.focus();
  }

  function zt() {
    return new Promise(function (t) {
      var e = window.scrollX,
          n = window.scrollY;
      Qt.restoreFocusTimeout = setTimeout(function () {
        Qt.previousActiveElement && Qt.previousActiveElement.focus ? (Qt.previousActiveElement.focus(), Qt.previousActiveElement = null) : document.body && document.body.focus(), t();
      }, 100), void 0 !== e && void 0 !== n && window.scrollTo(e, n);
    });
  }

  function Wt() {
    if (Qt.timeout) return function () {
      var t = q(),
          e = parseInt(window.getComputedStyle(t).width);
      t.style.removeProperty("transition"), t.style.width = "100%";
      var n = parseInt(window.getComputedStyle(t).width),
          o = parseInt(e / n * 100);
      t.style.removeProperty("transition"), t.style.width = "".concat(o, "%");
    }(), Qt.timeout.stop();
  }

  function Kt() {
    if (Qt.timeout) {
      var t = Qt.timeout.start();
      return at(t), t;
    }
  }

  function Yt(t) {
    return Object.prototype.hasOwnProperty.call($t, t);
  }

  function Zt(t) {
    return Xt[t];
  }

  var Qt = {},
      $t = {
    title: "",
    titleText: "",
    text: "",
    html: "",
    footer: "",
    icon: void 0,
    iconHtml: void 0,
    toast: !1,
    animation: !0,
    showClass: {
      popup: "swal2-show",
      backdrop: "swal2-backdrop-show",
      icon: "swal2-icon-show"
    },
    hideClass: {
      popup: "swal2-hide",
      backdrop: "swal2-backdrop-hide",
      icon: "swal2-icon-hide"
    },
    customClass: void 0,
    target: "body",
    backdrop: !0,
    heightAuto: !0,
    allowOutsideClick: !0,
    allowEscapeKey: !0,
    allowEnterKey: !0,
    stopKeydownPropagation: !0,
    keydownListenerCapture: !1,
    showConfirmButton: !0,
    showCancelButton: !1,
    preConfirm: void 0,
    confirmButtonText: "OK",
    confirmButtonAriaLabel: "",
    confirmButtonColor: void 0,
    cancelButtonText: "Cancel",
    cancelButtonAriaLabel: "",
    cancelButtonColor: void 0,
    buttonsStyling: !0,
    reverseButtons: !1,
    focusConfirm: !0,
    focusCancel: !1,
    showCloseButton: !1,
    closeButtonHtml: "&times;",
    closeButtonAriaLabel: "Close this dialog",
    showLoaderOnConfirm: !1,
    imageUrl: void 0,
    imageWidth: void 0,
    imageHeight: void 0,
    imageAlt: "",
    timer: void 0,
    timerProgressBar: !1,
    width: void 0,
    padding: void 0,
    background: void 0,
    input: void 0,
    inputPlaceholder: "",
    inputValue: "",
    inputOptions: {},
    inputAutoTrim: !0,
    inputAttributes: {},
    inputValidator: void 0,
    validationMessage: void 0,
    grow: !1,
    position: "center",
    progressSteps: [],
    currentProgressStep: void 0,
    progressStepsDistance: void 0,
    onBeforeOpen: void 0,
    onOpen: void 0,
    onRender: void 0,
    onClose: void 0,
    onAfterClose: void 0,
    onDestroy: void 0,
    scrollbarPadding: !0
  },
      Jt = ["title", "titleText", "text", "html", "icon", "hideClass", "customClass", "allowOutsideClick", "allowEscapeKey", "showConfirmButton", "showCancelButton", "confirmButtonText", "confirmButtonAriaLabel", "confirmButtonColor", "cancelButtonText", "cancelButtonAriaLabel", "cancelButtonColor", "buttonsStyling", "reverseButtons", "imageUrl", "imageWidth", "imageHeight", "imageAlt", "progressSteps", "currentProgressStep"],
      Xt = {
    animation: 'showClass" and "hideClass'
  },
      Gt = ["allowOutsideClick", "allowEnterKey", "backdrop", "focusConfirm", "focusCancel", "heightAuto", "keydownListenerCapture"],
      te = Object.freeze({
    isValidParameter: Yt,
    isUpdatableParameter: function isUpdatableParameter(t) {
      return -1 !== Jt.indexOf(t);
    },
    isDeprecatedParameter: Zt,
    argsToParams: function argsToParams(o) {
      var i = {};
      return "object" !== r(o[0]) || y(o[0]) ? ["title", "html", "icon"].forEach(function (t, e) {
        var n = o[e];
        "string" == typeof n || y(n) ? i[t] = n : void 0 !== n && v("Unexpected type of ".concat(t, '! Expected "string" or "Element", got ').concat(r(n)));
      }) : s(i, o[0]), i;
    },
    isVisible: function isVisible() {
      return ht(Z());
    },
    clickConfirm: Vt,
    clickCancel: function clickCancel() {
      return E() && E().click();
    },
    getContainer: Y,
    getPopup: Z,
    getTitle: k,
    getContent: x,
    getHtmlContainer: function getHtmlContainer() {
      return e(W["html-container"]);
    },
    getImage: P,
    getIcon: C,
    getIcons: n,
    getCloseButton: I,
    getActions: O,
    getConfirmButton: S,
    getCancelButton: E,
    getHeader: T,
    getFooter: L,
    getTimerProgressBar: q,
    getFocusableElements: j,
    getValidationMessage: B,
    isLoading: V,
    fire: function fire() {
      for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) {
        e[n] = arguments[n];
      }

      return i(this, e);
    },
    mixin: function mixin(r) {
      return function (t) {
        !function (t, e) {
          if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
          t.prototype = Object.create(e && e.prototype, {
            constructor: {
              value: t,
              writable: !0,
              configurable: !0
            }
          }), e && l(t, e);
        }(i, t);
        var n,
            o,
            e = (n = i, o = d(), function () {
          var t,
              e = u(n);
          return p(this, o ? (t = u(this).constructor, Reflect.construct(e, arguments, t)) : e.apply(this, arguments));
        });

        function i() {
          return a(this, i), e.apply(this, arguments);
        }

        return c(i, [{
          key: "_main",
          value: function value(t) {
            return f(u(i.prototype), "_main", this).call(this, s({}, r, t));
          }
        }]), i;
      }(this);
    },
    queue: function queue(t) {
      var r = this;
      Ut = t;

      function a(t, e) {
        Ut = [], t(e);
      }

      var c = [];
      return new Promise(function (i) {
        !function e(n, o) {
          n < Ut.length ? (document.body.setAttribute("data-swal2-queue-step", n), r.fire(Ut[n]).then(function (t) {
            void 0 !== t.value ? (c.push(t.value), e(n + 1, o)) : a(i, {
              dismiss: t.dismiss
            });
          })) : a(i, {
            value: c
          });
        }(0);
      });
    },
    getQueueStep: qt,
    insertQueueStep: function insertQueueStep(t, e) {
      return e && e < Ut.length ? Ut.splice(e, 0, t) : Ut.push(t);
    },
    deleteQueueStep: function deleteQueueStep(t) {
      void 0 !== Ut[t] && Ut.splice(t, 1);
    },
    showLoading: Ft,
    enableLoading: Ft,
    getTimerLeft: function getTimerLeft() {
      return Qt.timeout && Qt.timeout.getTimerLeft();
    },
    stopTimer: Wt,
    resumeTimer: Kt,
    toggleTimer: function toggleTimer() {
      var t = Qt.timeout;
      return t && (t.running ? Wt : Kt)();
    },
    increaseTimer: function increaseTimer(t) {
      if (Qt.timeout) {
        var e = Qt.timeout.increase(t);
        return at(e, !0), e;
      }
    },
    isTimerRunning: function isTimerRunning() {
      return Qt.timeout && Qt.timeout.isRunning();
    }
  });

  function ee() {
    var t,
        e = Pt.innerParams.get(this);
    e && (t = Pt.domCache.get(this), e.showConfirmButton || (nt(t.confirmButton), e.showCancelButton || nt(t.actions)), ft([t.popup, t.actions], W.loading), t.popup.removeAttribute("aria-busy"), t.popup.removeAttribute("data-loading"), t.confirmButton.disabled = !1, t.cancelButton.disabled = !1);
  }

  function ne() {
    null === $.previousBodyPadding && document.body.scrollHeight > window.innerHeight && ($.previousBodyPadding = parseInt(window.getComputedStyle(document.body).getPropertyValue("padding-right")), document.body.style.paddingRight = "".concat($.previousBodyPadding + function () {
      var t = document.createElement("div");
      t.className = W["scrollbar-measure"], document.body.appendChild(t);
      var e = t.getBoundingClientRect().width - t.clientWidth;
      return document.body.removeChild(t), e;
    }(), "px"));
  }

  function oe() {
    return !!window.MSInputMethodContext && !!document.documentMode;
  }

  function ie() {
    var t = Y(),
        e = Z();
    t.style.removeProperty("align-items"), e.offsetTop < 0 && (t.style.alignItems = "flex-start");
  }

  var re = function re() {
    var e,
        t = Y();
    t.ontouchstart = function (t) {
      e = ae(t.target);
    }, t.ontouchmove = function (t) {
      e && (t.preventDefault(), t.stopPropagation());
    };
  },
      ae = function ae(t) {
    var e = Y();
    return t === e || !(it(e) || "INPUT" === t.tagName || it(x()) && x().contains(t));
  },
      ce = {
    swalPromiseResolve: new WeakMap()
  };

  function se(t, e, n, o) {
    var i;
    n ? de(t, o) : (zt().then(function () {
      return de(t, o);
    }), Qt.keydownTarget.removeEventListener("keydown", Qt.keydownHandler, {
      capture: Qt.keydownListenerCapture
    }), Qt.keydownHandlerAdded = !1), e.parentNode && !document.body.getAttribute("data-swal2-queue-step") && e.parentNode.removeChild(e), M() && (null !== $.previousBodyPadding && (document.body.style.paddingRight = "".concat($.previousBodyPadding, "px"), $.previousBodyPadding = null), H(document.body, W.iosfix) && (i = parseInt(document.body.style.top, 10), ft(document.body, W.iosfix), document.body.style.top = "", document.body.scrollTop = -1 * i), "undefined" != typeof window && oe() && window.removeEventListener("resize", ie), h(document.body.children).forEach(function (t) {
      t.hasAttribute("data-previous-aria-hidden") ? (t.setAttribute("aria-hidden", t.getAttribute("data-previous-aria-hidden")), t.removeAttribute("data-previous-aria-hidden")) : t.removeAttribute("aria-hidden");
    })), ft([document.documentElement, document.body], [W.shown, W["height-auto"], W["no-backdrop"], W["toast-shown"], W["toast-column"]]);
  }

  function ue(t) {
    var e,
        n,
        o,
        i = Z();
    i && (e = Pt.innerParams.get(this)) && !H(i, e.hideClass.popup) && (n = ce.swalPromiseResolve.get(this), ft(i, e.showClass.popup), pt(i, e.hideClass.popup), o = Y(), ft(o, e.showClass.backdrop), pt(o, e.hideClass.backdrop), function (t, e, n) {
      var o = Y(),
          i = wt && rt(e),
          r = n.onClose,
          a = n.onAfterClose;

      if (r !== null && typeof r === "function") {
        r(e);
      }

      if (i) {
        le(t, e, o, a);
      } else {
        se(t, o, Q(), a);
      }
    }(this, i, e), n(t || {}));
  }

  var le = function le(t, e, n, o) {
    Qt.swalCloseEventFinishedCallback = se.bind(null, t, n, Q(), o), e.addEventListener(wt, function (t) {
      t.target === e && (Qt.swalCloseEventFinishedCallback(), delete Qt.swalCloseEventFinishedCallback);
    });
  },
      de = function de(t, e) {
    setTimeout(function () {
      "function" == typeof e && e(), t._destroy();
    });
  };

  function pe(t, e, n) {
    var o = Pt.domCache.get(t);
    e.forEach(function (t) {
      o[t].disabled = n;
    });
  }

  function fe(t, e) {
    if (!t) return !1;
    if ("radio" === t.type) for (var n = t.parentNode.parentNode.querySelectorAll("input"), o = 0; o < n.length; o++) {
      n[o].disabled = e;
    } else t.disabled = e;
  }

  var me = function () {
    function n(t, e) {
      a(this, n), this.callback = t, this.remaining = e, this.running = !1, this.start();
    }

    return c(n, [{
      key: "start",
      value: function value() {
        return this.running || (this.running = !0, this.started = new Date(), this.id = setTimeout(this.callback, this.remaining)), this.remaining;
      }
    }, {
      key: "stop",
      value: function value() {
        return this.running && (this.running = !1, clearTimeout(this.id), this.remaining -= new Date() - this.started), this.remaining;
      }
    }, {
      key: "increase",
      value: function value(t) {
        var e = this.running;
        return e && this.stop(), this.remaining += t, e && this.start(), this.remaining;
      }
    }, {
      key: "getTimerLeft",
      value: function value() {
        return this.running && (this.stop(), this.start()), this.remaining;
      }
    }, {
      key: "isRunning",
      value: function value() {
        return this.running;
      }
    }]), n;
  }(),
      he = {
    email: function email(t, e) {
      return /^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9-]{2,24}$/.test(t) ? Promise.resolve() : Promise.resolve(e || "Invalid email address");
    },
    url: function url(t, e) {
      return /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,63}\b([-a-zA-Z0-9@:%_+.~#?&/=]*)$/.test(t) ? Promise.resolve() : Promise.resolve(e || "Invalid URL");
    }
  };

  function ge(t) {
    var e, n;
    (e = t).inputValidator || Object.keys(he).forEach(function (t) {
      e.input === t && (e.inputValidator = he[t]);
    }), t.showLoaderOnConfirm && !t.preConfirm && U("showLoaderOnConfirm is set to true, but preConfirm is not defined.\nshowLoaderOnConfirm should be used together with preConfirm, see usage example:\nhttps://sweetalert2.github.io/#ajax-request"), t.animation = F(t.animation), (n = t).target && ("string" != typeof n.target || document.querySelector(n.target)) && ("string" == typeof n.target || n.target.appendChild) || (U('Target parameter is not valid, defaulting to "body"'), n.target = "body"), "string" == typeof t.title && (t.title = t.title.split("\n").join("<br />")), vt(t);
  }

  function ve(t) {
    var e = Y(),
        n = Z();
    "function" == typeof t.onBeforeOpen && t.onBeforeOpen(n), Ee(e, n, t), Be(e, n), M() && Se(e, t.scrollbarPadding), Q() || Qt.previousActiveElement || (Qt.previousActiveElement = document.activeElement), "function" == typeof t.onOpen && setTimeout(function () {
      return t.onOpen(n);
    }), ft(e, W["no-transition"]);
  }

  function be(t) {
    var e,
        n = Z();
    t.target === n && (e = Y(), n.removeEventListener(wt, be), e.style.overflowY = "auto");
  }

  function ye(t, e) {
    "select" === e.input || "radio" === e.input ? qe(t, e) : -1 !== ["text", "email", "number", "tel", "textarea"].indexOf(e.input) && b(e.inputValue) && Ie(t, e);
  }

  function we(t, e) {
    t.disableButtons(), e.input ? Ve(t, e) : Re(t, e, !0);
  }

  function Ce(t, e) {
    t.disableButtons(), e(z.cancel);
  }

  function ke(t, e) {
    t.closePopup({
      value: e
    });
  }

  function xe(e, t, n, o) {
    t.keydownTarget && t.keydownHandlerAdded && (t.keydownTarget.removeEventListener("keydown", t.keydownHandler, {
      capture: t.keydownListenerCapture
    }), t.keydownHandlerAdded = !1), n.toast || (t.keydownHandler = function (t) {
      return Ne(e, t, o);
    }, t.keydownTarget = n.keydownListenerCapture ? window : Z(), t.keydownListenerCapture = n.keydownListenerCapture, t.keydownTarget.addEventListener("keydown", t.keydownHandler, {
      capture: t.keydownListenerCapture
    }), t.keydownHandlerAdded = !0);
  }

  function Pe(t, e, n) {
    var o = j(),
        i = 0;
    if (i < o.length) return (e += n) === o.length ? e = 0 : -1 === e && (e = o.length - 1), o[e].focus();
    Z().focus();
  }

  function Ae(t, e, n) {
    Pt.innerParams.get(t).toast ? We(t, e, n) : (Ye(e), Ze(e), Qe(t, e, n));
  }

  var Be = function Be(t, e) {
    wt && rt(e) ? (t.style.overflowY = "hidden", e.addEventListener(wt, be)) : t.style.overflowY = "auto";
  },
      Se = function Se(t, e) {
    var n;
    (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream || "MacIntel" === navigator.platform && 1 < navigator.maxTouchPoints) && !H(document.body, W.iosfix) && (n = document.body.scrollTop, document.body.style.top = "".concat(-1 * n, "px"), pt(document.body, W.iosfix), re()), "undefined" != typeof window && oe() && (ie(), window.addEventListener("resize", ie)), h(document.body.children).forEach(function (t) {
      t === Y() || function (t, e) {
        if ("function" == typeof t.contains) return t.contains(e);
      }(t, Y()) || (t.hasAttribute("aria-hidden") && t.setAttribute("data-previous-aria-hidden", t.getAttribute("aria-hidden")), t.setAttribute("aria-hidden", "true"));
    }), e && ne(), setTimeout(function () {
      t.scrollTop = 0;
    });
  },
      Ee = function Ee(t, e, n) {
    pt(t, n.showClass.backdrop), et(e), pt(e, n.showClass.popup), pt([document.documentElement, document.body], W.shown), n.heightAuto && n.backdrop && !n.toast && pt([document.documentElement, document.body], W["height-auto"]);
  },
      Oe = function Oe(t) {
    return t.checked ? 1 : 0;
  },
      Te = function Te(t) {
    return t.checked ? t.value : null;
  },
      Le = function Le(t) {
    return t.files.length ? null !== t.getAttribute("multiple") ? t.files : t.files[0] : null;
  },
      qe = function qe(e, n) {
    function o(t) {
      return je[n.input](i, Me(t), n);
    }

    var i = x();
    b(n.inputOptions) ? (Ft(), n.inputOptions.then(function (t) {
      e.hideLoading(), o(t);
    })) : "object" === r(n.inputOptions) ? o(n.inputOptions) : v("Unexpected type of inputOptions! Expected object, Map or Promise, got ".concat(r(n.inputOptions)));
  },
      Ie = function Ie(e, n) {
    var o = e.getInput();
    nt(o), n.inputValue.then(function (t) {
      o.value = "number" === n.input ? parseFloat(t) || 0 : "".concat(t), et(o), o.focus(), e.hideLoading();
    })["catch"](function (t) {
      v("Error in inputValue promise: ".concat(t)), o.value = "", et(o), o.focus(), e.hideLoading();
    });
  },
      je = {
    select: function select(t, e, i) {
      var r = mt(t, W.select);
      e.forEach(function (t) {
        var e = t[0],
            n = t[1],
            o = document.createElement("option");
        o.value = e, R(o, n), i.inputValue.toString() === e.toString() && (o.selected = !0), r.appendChild(o);
      }), r.focus();
    },
    radio: function radio(t, e, a) {
      var c = mt(t, W.radio);
      e.forEach(function (t) {
        var e = t[0],
            n = t[1],
            o = document.createElement("input"),
            i = document.createElement("label");
        o.type = "radio", o.name = W.radio, o.value = e, a.inputValue.toString() === e.toString() && (o.checked = !0);
        var r = document.createElement("span");
        R(r, n), r.className = W.label, i.appendChild(o), i.appendChild(r), c.appendChild(i);
      });
      var n = c.querySelectorAll("input");
      n.length && n[0].focus();
    }
  },
      Me = function Me(e) {
    var n = [];
    return "undefined" != typeof Map && e instanceof Map ? e.forEach(function (t, e) {
      n.push([e, t]);
    }) : Object.keys(e).forEach(function (t) {
      n.push([t, e[t]]);
    }), n;
  },
      Ve = function Ve(e, n) {
    var o = function (t, e) {
      var n = t.getInput();
      if (!n) return null;

      switch (e.input) {
        case "checkbox":
          return Oe(n);

        case "radio":
          return Te(n);

        case "file":
          return Le(n);

        default:
          return e.inputAutoTrim ? n.value.trim() : n.value;
      }
    }(e, n);

    n.inputValidator ? (e.disableInput(), Promise.resolve().then(function () {
      return n.inputValidator(o, n.validationMessage);
    }).then(function (t) {
      e.enableButtons(), e.enableInput(), t ? e.showValidationMessage(t) : Re(e, n, o);
    })) : e.getInput().checkValidity() ? Re(e, n, o) : (e.enableButtons(), e.showValidationMessage(n.validationMessage));
  },
      Re = function Re(e, t, n) {
    t.showLoaderOnConfirm && Ft(), t.preConfirm ? (e.resetValidationMessage(), Promise.resolve().then(function () {
      return t.preConfirm(n, t.validationMessage);
    }).then(function (t) {
      ht(B()) || !1 === t ? e.hideLoading() : ke(e, void 0 === t ? n : t);
    })) : ke(e, n);
  },
      He = ["ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown", "Left", "Right", "Up", "Down"],
      De = ["Escape", "Esc"],
      Ne = function Ne(t, e, n) {
    var o = Pt.innerParams.get(t);
    o.stopKeydownPropagation && e.stopPropagation(), "Enter" === e.key ? Ue(t, e, o) : "Tab" === e.key ? _e(e, o) : -1 !== He.indexOf(e.key) ? Fe() : -1 !== De.indexOf(e.key) && ze(e, o, n);
  },
      Ue = function Ue(t, e, n) {
    if (!e.isComposing && e.target && t.getInput() && e.target.outerHTML === t.getInput().outerHTML) {
      if (-1 !== ["textarea", "file"].indexOf(n.input)) return;
      Vt(), e.preventDefault();
    }
  },
      _e = function _e(t) {
    for (var e = t.target, n = j(), o = -1, i = 0; i < n.length; i++) {
      if (e === n[i]) {
        o = i;
        break;
      }
    }

    t.shiftKey ? Pe(0, o, -1) : Pe(0, o, 1), t.stopPropagation(), t.preventDefault();
  },
      Fe = function Fe() {
    var t = S(),
        e = E();
    document.activeElement === t && ht(e) ? e.focus() : document.activeElement === e && ht(t) && t.focus();
  },
      ze = function ze(t, e, n) {
    F(e.allowEscapeKey) && (t.preventDefault(), n(z.esc));
  },
      We = function We(e, t, n) {
    t.popup.onclick = function () {
      var t = Pt.innerParams.get(e);
      t.showConfirmButton || t.showCancelButton || t.showCloseButton || t.input || n(z.close);
    };
  },
      Ke = !1,
      Ye = function Ye(e) {
    e.popup.onmousedown = function () {
      e.container.onmouseup = function (t) {
        e.container.onmouseup = void 0, t.target === e.container && (Ke = !0);
      };
    };
  },
      Ze = function Ze(e) {
    e.container.onmousedown = function () {
      e.popup.onmouseup = function (t) {
        e.popup.onmouseup = void 0, t.target !== e.popup && !e.popup.contains(t.target) || (Ke = !0);
      };
    };
  },
      Qe = function Qe(n, o, i) {
    o.container.onclick = function (t) {
      var e = Pt.innerParams.get(n);
      Ke ? Ke = !1 : t.target === o.container && F(e.allowOutsideClick) && i(z.backdrop);
    };
  };

  var $e = function $e(t, e, n) {
    var o = q();
    nt(o), e.timer && (t.timeout = new me(function () {
      n("timer"), delete t.timeout;
    }, e.timer), e.timerProgressBar && (et(o), setTimeout(function () {
      t.timeout.running && at(e.timer);
    })));
  },
      Je = function Je(t, e) {
    if (!e.toast) return F(e.allowEnterKey) ? e.focusCancel && ht(t.cancelButton) ? t.cancelButton.focus() : e.focusConfirm && ht(t.confirmButton) ? t.confirmButton.focus() : void Pe(0, -1, 1) : Xe();
  },
      Xe = function Xe() {
    document.activeElement && "function" == typeof document.activeElement.blur && document.activeElement.blur();
  };

  var Ge,
      tn = function tn(t) {
    for (var e in t) {
      t[e] = new WeakMap();
    }
  },
      en = Object.freeze({
    hideLoading: ee,
    disableLoading: ee,
    getInput: function getInput(t) {
      var e = Pt.innerParams.get(t || this),
          n = Pt.domCache.get(t || this);
      return n ? J(n.content, e.input) : null;
    },
    close: ue,
    closePopup: ue,
    closeModal: ue,
    closeToast: ue,
    enableButtons: function enableButtons() {
      pe(this, ["confirmButton", "cancelButton"], !1);
    },
    disableButtons: function disableButtons() {
      pe(this, ["confirmButton", "cancelButton"], !0);
    },
    enableInput: function enableInput() {
      return fe(this.getInput(), !1);
    },
    disableInput: function disableInput() {
      return fe(this.getInput(), !0);
    },
    showValidationMessage: function showValidationMessage(t) {
      var e = Pt.domCache.get(this);
      R(e.validationMessage, t);
      var n = window.getComputedStyle(e.popup);
      e.validationMessage.style.marginLeft = "-".concat(n.getPropertyValue("padding-left")), e.validationMessage.style.marginRight = "-".concat(n.getPropertyValue("padding-right")), et(e.validationMessage);
      var o = this.getInput();
      o && (o.setAttribute("aria-invalid", !0), o.setAttribute("aria-describedBy", W["validation-message"]), X(o), pt(o, W.inputerror));
    },
    resetValidationMessage: function resetValidationMessage() {
      var t = Pt.domCache.get(this);
      t.validationMessage && nt(t.validationMessage);
      var e = this.getInput();
      e && (e.removeAttribute("aria-invalid"), e.removeAttribute("aria-describedBy"), ft(e, W.inputerror));
    },
    getProgressSteps: function getProgressSteps() {
      return Pt.domCache.get(this).progressSteps;
    },
    _main: function _main(t) {
      !function (t) {
        for (var e in t) {
          Yt(i = e) || U('Unknown parameter "'.concat(i, '"')), t.toast && (o = e, -1 !== Gt.indexOf(o) && U('The parameter "'.concat(o, '" is incompatible with toasts'))), Zt(n = e) && g(n, Zt(n));
        }

        var n, o, i;
      }(t), Qt.currentInstance && Qt.currentInstance._destroy(), Qt.currentInstance = this;

      var e = function (t) {
        var e = s({}, $t.showClass, t.showClass),
            n = s({}, $t.hideClass, t.hideClass),
            o = s({}, $t, t);

        if (o.showClass = e, o.hideClass = n, t.animation === false) {
          o.showClass = {
            popup: "swal2-noanimation",
            backdrop: "swal2-noanimation"
          };
          o.hideClass = {};
        }

        return o;
      }(t);

      ge(e), Object.freeze(e), Qt.timeout && (Qt.timeout.stop(), delete Qt.timeout), clearTimeout(Qt.restoreFocusTimeout);

      var n = function (t) {
        var e = {
          popup: Z(),
          container: Y(),
          content: x(),
          actions: O(),
          confirmButton: S(),
          cancelButton: E(),
          closeButton: I(),
          validationMessage: B(),
          progressSteps: A()
        };
        return Pt.domCache.set(t, e), e;
      }(this);

      return Mt(this, e), Pt.innerParams.set(this, e), function (n, o, i) {
        return new Promise(function (t) {
          var e = function t(e) {
            n.closePopup({
              dismiss: e
            });
          };

          ce.swalPromiseResolve.set(n, t);

          o.confirmButton.onclick = function () {
            return we(n, i);
          };

          o.cancelButton.onclick = function () {
            return Ce(n, e);
          };

          o.closeButton.onclick = function () {
            return e(z.close);
          };

          Ae(n, o, e);
          xe(n, Qt, i, e);

          if (i.toast && (i.input || i.footer || i.showCloseButton)) {
            pt(document.body, W["toast-column"]);
          } else {
            ft(document.body, W["toast-column"]);
          }

          ye(n, i);
          ve(i);
          $e(Qt, i, e);
          Je(o, i);
          setTimeout(function () {
            o.container.scrollTop = 0;
          });
        });
      }(this, n, e);
    },
    update: function update(e) {
      var t = Z(),
          n = Pt.innerParams.get(this);
      if (!t || H(t, n.hideClass.popup)) return U("You're trying to update the closed or closing popup, that won't work. Use the update() method in preConfirm parameter or show a new popup.");
      var o = {};
      Object.keys(e).forEach(function (t) {
        on.isUpdatableParameter(t) ? o[t] = e[t] : U('Invalid parameter to update: "'.concat(t, '". Updatable params are listed here: https://github.com/sweetalert2/sweetalert2/blob/master/src/utils/params.js'));
      });
      var i = s({}, n, o);
      Mt(this, i), Pt.innerParams.set(this, i), Object.defineProperties(this, {
        params: {
          value: s({}, this.params, e),
          writable: !1,
          enumerable: !0
        }
      });
    },
    _destroy: function _destroy() {
      var t = Pt.domCache.get(this),
          e = Pt.innerParams.get(this);
      e && (t.popup && Qt.swalCloseEventFinishedCallback && (Qt.swalCloseEventFinishedCallback(), delete Qt.swalCloseEventFinishedCallback), Qt.deferDisposalTimer && (clearTimeout(Qt.deferDisposalTimer), delete Qt.deferDisposalTimer), "function" == typeof e.onDestroy && e.onDestroy(), delete this.params, delete Qt.keydownHandler, delete Qt.keydownTarget, tn(Pt), tn(ce));
    }
  }),
      nn = function () {
    function r() {
      if (a(this, r), "undefined" != typeof window) {
        "undefined" == typeof Promise && v("This package requires a Promise library, please include a shim to enable it in this browser (See: https://github.com/sweetalert2/sweetalert2/wiki/Migration-from-SweetAlert-to-SweetAlert2#1-ie-support)"), Ge = this;

        for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) {
          e[n] = arguments[n];
        }

        var o = Object.freeze(this.constructor.argsToParams(e));
        Object.defineProperties(this, {
          params: {
            value: o,
            writable: !1,
            enumerable: !0,
            configurable: !0
          }
        });

        var i = this._main(this.params);

        Pt.promise.set(this, i);
      }
    }

    return c(r, [{
      key: "then",
      value: function value(t) {
        return Pt.promise.get(this).then(t);
      }
    }, {
      key: "finally",
      value: function value(t) {
        return Pt.promise.get(this)["finally"](t);
      }
    }]), r;
  }();

  s(nn.prototype, en), s(nn, te), Object.keys(en).forEach(function (t) {
    nn[t] = function () {
      if (Ge) return Ge[t].apply(Ge, arguments);
    };
  }), nn.DismissReason = z, nn.version = "9.10.13";
  var on = nn;
  return on["default"] = on;
}), void 0 !== this && this.Sweetalert2 && (this.swal = this.sweetAlert = this.Swal = this.SweetAlert = this.Sweetalert2);
"undefined" != typeof document && function (e, t) {
  var n = e.createElement("style");
  if (e.getElementsByTagName("head")[0].appendChild(n), n.styleSheet) n.styleSheet.disabled || (n.styleSheet.cssText = t);else try {
    n.innerHTML = t;
  } catch (e) {
    n.innerText = t;
  }
}(document, ".swal2-popup.swal2-toast{flex-direction:row;align-items:center;width:auto;padding:.625em;overflow-y:hidden;background:#fff;box-shadow:0 0 .625em #d9d9d9}.swal2-popup.swal2-toast .swal2-header{flex-direction:row}.swal2-popup.swal2-toast .swal2-title{flex-grow:1;justify-content:flex-start;margin:0 .6em;font-size:1em}.swal2-popup.swal2-toast .swal2-footer{margin:.5em 0 0;padding:.5em 0 0;font-size:.8em}.swal2-popup.swal2-toast .swal2-close{position:static;width:.8em;height:.8em;line-height:.8}.swal2-popup.swal2-toast .swal2-content{justify-content:flex-start;font-size:1em}.swal2-popup.swal2-toast .swal2-icon{width:2em;min-width:2em;height:2em;margin:0}.swal2-popup.swal2-toast .swal2-icon .swal2-icon-content{display:flex;align-items:center;font-size:1.8em;font-weight:700}@media all and (-ms-high-contrast:none),(-ms-high-contrast:active){.swal2-popup.swal2-toast .swal2-icon .swal2-icon-content{font-size:.25em}}.swal2-popup.swal2-toast .swal2-icon.swal2-success .swal2-success-ring{width:2em;height:2em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line]{top:.875em;width:1.375em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=left]{left:.3125em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=right]{right:.3125em}.swal2-popup.swal2-toast .swal2-actions{flex-basis:auto!important;width:auto;height:auto;margin:0 .3125em}.swal2-popup.swal2-toast .swal2-styled{margin:0 .3125em;padding:.3125em .625em;font-size:1em}.swal2-popup.swal2-toast .swal2-styled:focus{box-shadow:0 0 0 1px #fff,0 0 0 3px rgba(50,100,150,.4)}.swal2-popup.swal2-toast .swal2-success{border-color:#a5dc86}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line]{position:absolute;width:1.6em;height:3em;transform:rotate(45deg);border-radius:50%}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line][class$=left]{top:-.8em;left:-.5em;transform:rotate(-45deg);transform-origin:2em 2em;border-radius:4em 0 0 4em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line][class$=right]{top:-.25em;left:.9375em;transform-origin:0 1.5em;border-radius:0 4em 4em 0}.swal2-popup.swal2-toast .swal2-success .swal2-success-ring{width:2em;height:2em}.swal2-popup.swal2-toast .swal2-success .swal2-success-fix{top:0;left:.4375em;width:.4375em;height:2.6875em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line]{height:.3125em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line][class$=tip]{top:1.125em;left:.1875em;width:.75em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line][class$=long]{top:.9375em;right:.1875em;width:1.375em}.swal2-popup.swal2-toast .swal2-success.swal2-icon-show .swal2-success-line-tip{-webkit-animation:swal2-toast-animate-success-line-tip .75s;animation:swal2-toast-animate-success-line-tip .75s}.swal2-popup.swal2-toast .swal2-success.swal2-icon-show .swal2-success-line-long{-webkit-animation:swal2-toast-animate-success-line-long .75s;animation:swal2-toast-animate-success-line-long .75s}.swal2-popup.swal2-toast.swal2-show{-webkit-animation:swal2-toast-show .5s;animation:swal2-toast-show .5s}.swal2-popup.swal2-toast.swal2-hide{-webkit-animation:swal2-toast-hide .1s forwards;animation:swal2-toast-hide .1s forwards}.swal2-container{display:flex;position:fixed;z-index:1060;top:0;right:0;bottom:0;left:0;flex-direction:row;align-items:center;justify-content:center;padding:.625em;overflow-x:hidden;transition:background-color .1s;-webkit-overflow-scrolling:touch}.swal2-container.swal2-backdrop-show,.swal2-container.swal2-noanimation{background:rgba(0,0,0,.4)}.swal2-container.swal2-backdrop-hide{background:0 0!important}.swal2-container.swal2-top{align-items:flex-start}.swal2-container.swal2-top-left,.swal2-container.swal2-top-start{align-items:flex-start;justify-content:flex-start}.swal2-container.swal2-top-end,.swal2-container.swal2-top-right{align-items:flex-start;justify-content:flex-end}.swal2-container.swal2-center{align-items:center}.swal2-container.swal2-center-left,.swal2-container.swal2-center-start{align-items:center;justify-content:flex-start}.swal2-container.swal2-center-end,.swal2-container.swal2-center-right{align-items:center;justify-content:flex-end}.swal2-container.swal2-bottom{align-items:flex-end}.swal2-container.swal2-bottom-left,.swal2-container.swal2-bottom-start{align-items:flex-end;justify-content:flex-start}.swal2-container.swal2-bottom-end,.swal2-container.swal2-bottom-right{align-items:flex-end;justify-content:flex-end}.swal2-container.swal2-bottom-end>:first-child,.swal2-container.swal2-bottom-left>:first-child,.swal2-container.swal2-bottom-right>:first-child,.swal2-container.swal2-bottom-start>:first-child,.swal2-container.swal2-bottom>:first-child{margin-top:auto}.swal2-container.swal2-grow-fullscreen>.swal2-modal{display:flex!important;flex:1;align-self:stretch;justify-content:center}.swal2-container.swal2-grow-row>.swal2-modal{display:flex!important;flex:1;align-content:center;justify-content:center}.swal2-container.swal2-grow-column{flex:1;flex-direction:column}.swal2-container.swal2-grow-column.swal2-bottom,.swal2-container.swal2-grow-column.swal2-center,.swal2-container.swal2-grow-column.swal2-top{align-items:center}.swal2-container.swal2-grow-column.swal2-bottom-left,.swal2-container.swal2-grow-column.swal2-bottom-start,.swal2-container.swal2-grow-column.swal2-center-left,.swal2-container.swal2-grow-column.swal2-center-start,.swal2-container.swal2-grow-column.swal2-top-left,.swal2-container.swal2-grow-column.swal2-top-start{align-items:flex-start}.swal2-container.swal2-grow-column.swal2-bottom-end,.swal2-container.swal2-grow-column.swal2-bottom-right,.swal2-container.swal2-grow-column.swal2-center-end,.swal2-container.swal2-grow-column.swal2-center-right,.swal2-container.swal2-grow-column.swal2-top-end,.swal2-container.swal2-grow-column.swal2-top-right{align-items:flex-end}.swal2-container.swal2-grow-column>.swal2-modal{display:flex!important;flex:1;align-content:center;justify-content:center}.swal2-container.swal2-no-transition{transition:none!important}.swal2-container:not(.swal2-top):not(.swal2-top-start):not(.swal2-top-end):not(.swal2-top-left):not(.swal2-top-right):not(.swal2-center-start):not(.swal2-center-end):not(.swal2-center-left):not(.swal2-center-right):not(.swal2-bottom):not(.swal2-bottom-start):not(.swal2-bottom-end):not(.swal2-bottom-left):not(.swal2-bottom-right):not(.swal2-grow-fullscreen)>.swal2-modal{margin:auto}@media all and (-ms-high-contrast:none),(-ms-high-contrast:active){.swal2-container .swal2-modal{margin:0!important}}.swal2-popup{display:none;position:relative;box-sizing:border-box;flex-direction:column;justify-content:center;width:32em;max-width:100%;padding:1.25em;border:none;border-radius:.3125em;background:#fff;font-family:inherit;font-size:1rem}.swal2-popup:focus{outline:0}.swal2-popup.swal2-loading{overflow-y:hidden}.swal2-header{display:flex;flex-direction:column;align-items:center}.swal2-title{position:relative;max-width:100%;margin:0 0 .4em;padding:0;color:#595959;font-size:1.875em;font-weight:600;text-align:center;text-transform:none;word-wrap:break-word}.swal2-actions{display:flex;z-index:1;flex-wrap:wrap;align-items:center;justify-content:center;width:100%;margin:1.25em auto 0}.swal2-actions:not(.swal2-loading) .swal2-styled[disabled]{opacity:.4}.swal2-actions:not(.swal2-loading) .swal2-styled:hover{background-image:linear-gradient(rgba(0,0,0,.1),rgba(0,0,0,.1))}.swal2-actions:not(.swal2-loading) .swal2-styled:active{background-image:linear-gradient(rgba(0,0,0,.2),rgba(0,0,0,.2))}.swal2-actions.swal2-loading .swal2-styled.swal2-confirm{box-sizing:border-box;width:2.5em;height:2.5em;margin:.46875em;padding:0;-webkit-animation:swal2-rotate-loading 1.5s linear 0s infinite normal;animation:swal2-rotate-loading 1.5s linear 0s infinite normal;border:.25em solid transparent;border-radius:100%;border-color:transparent;background-color:transparent!important;color:transparent!important;cursor:default;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.swal2-actions.swal2-loading .swal2-styled.swal2-cancel{margin-right:30px;margin-left:30px}.swal2-actions.swal2-loading :not(.swal2-styled).swal2-confirm::after{content:\"\";display:inline-block;width:15px;height:15px;margin-left:5px;-webkit-animation:swal2-rotate-loading 1.5s linear 0s infinite normal;animation:swal2-rotate-loading 1.5s linear 0s infinite normal;border:3px solid #999;border-radius:50%;border-right-color:transparent;box-shadow:1px 1px 1px #fff}.swal2-styled{margin:.3125em;padding:.625em 2em;box-shadow:none;font-weight:500}.swal2-styled:not([disabled]){cursor:pointer}.swal2-styled.swal2-confirm{border:0;border-radius:.25em;background:initial;background-color:#3085d6;color:#fff;font-size:1.0625em}.swal2-styled.swal2-cancel{border:0;border-radius:.25em;background:initial;background-color:#aaa;color:#fff;font-size:1.0625em}.swal2-styled:focus{outline:0;box-shadow:0 0 0 1px #fff,0 0 0 3px rgba(50,100,150,.4)}.swal2-styled::-moz-focus-inner{border:0}.swal2-footer{justify-content:center;margin:1.25em 0 0;padding:1em 0 0;border-top:1px solid #eee;color:#545454;font-size:1em}.swal2-timer-progress-bar-container{position:absolute;right:0;bottom:0;left:0;height:.25em;overflow:hidden;border-bottom-right-radius:.3125em;border-bottom-left-radius:.3125em}.swal2-timer-progress-bar{width:100%;height:.25em;background:rgba(0,0,0,.2)}.swal2-image{max-width:100%;margin:1.25em auto}.swal2-close{position:absolute;z-index:2;top:0;right:0;align-items:center;justify-content:center;width:1.2em;height:1.2em;padding:0;overflow:hidden;transition:color .1s ease-out;border:none;border-radius:0;background:0 0;color:#ccc;font-family:serif;font-size:2.5em;line-height:1.2;cursor:pointer}.swal2-close:hover{transform:none;background:0 0;color:#f27474}.swal2-close::-moz-focus-inner{border:0}.swal2-content{z-index:1;justify-content:center;margin:0;padding:0;color:#545454;font-size:1.125em;font-weight:400;line-height:normal;text-align:center;word-wrap:break-word}.swal2-checkbox,.swal2-file,.swal2-input,.swal2-radio,.swal2-select,.swal2-textarea{margin:1em auto}.swal2-file,.swal2-input,.swal2-textarea{box-sizing:border-box;width:100%;transition:border-color .3s,box-shadow .3s;border:1px solid #d9d9d9;border-radius:.1875em;background:inherit;box-shadow:inset 0 1px 1px rgba(0,0,0,.06);color:inherit;font-size:1.125em}.swal2-file.swal2-inputerror,.swal2-input.swal2-inputerror,.swal2-textarea.swal2-inputerror{border-color:#f27474!important;box-shadow:0 0 2px #f27474!important}.swal2-file:focus,.swal2-input:focus,.swal2-textarea:focus{border:1px solid #b4dbed;outline:0;box-shadow:0 0 3px #c4e6f5}.swal2-file::-webkit-input-placeholder,.swal2-input::-webkit-input-placeholder,.swal2-textarea::-webkit-input-placeholder{color:#ccc}.swal2-file::-moz-placeholder,.swal2-input::-moz-placeholder,.swal2-textarea::-moz-placeholder{color:#ccc}.swal2-file:-ms-input-placeholder,.swal2-input:-ms-input-placeholder,.swal2-textarea:-ms-input-placeholder{color:#ccc}.swal2-file::-ms-input-placeholder,.swal2-input::-ms-input-placeholder,.swal2-textarea::-ms-input-placeholder{color:#ccc}.swal2-file::placeholder,.swal2-input::placeholder,.swal2-textarea::placeholder{color:#ccc}.swal2-range{margin:1em auto;background:#fff}.swal2-range input{width:80%}.swal2-range output{width:20%;color:inherit;font-weight:600;text-align:center}.swal2-range input,.swal2-range output{height:2.625em;padding:0;font-size:1.125em;line-height:2.625em}.swal2-input{height:2.625em;padding:0 .75em}.swal2-input[type=number]{max-width:10em}.swal2-file{background:inherit;font-size:1.125em}.swal2-textarea{height:6.75em;padding:.75em}.swal2-select{min-width:50%;max-width:100%;padding:.375em .625em;background:inherit;color:inherit;font-size:1.125em}.swal2-checkbox,.swal2-radio{align-items:center;justify-content:center;background:#fff;color:inherit}.swal2-checkbox label,.swal2-radio label{margin:0 .6em;font-size:1.125em}.swal2-checkbox input,.swal2-radio input{margin:0 .4em}.swal2-validation-message{display:none;align-items:center;justify-content:center;padding:.625em;overflow:hidden;background:#f0f0f0;color:#666;font-size:1em;font-weight:300}.swal2-validation-message::before{content:\"!\";display:inline-block;width:1.5em;min-width:1.5em;height:1.5em;margin:0 .625em;border-radius:50%;background-color:#f27474;color:#fff;font-weight:600;line-height:1.5em;text-align:center}.swal2-icon{position:relative;box-sizing:content-box;justify-content:center;width:5em;height:5em;margin:1.25em auto 1.875em;border:.25em solid transparent;border-radius:50%;font-family:inherit;line-height:5em;cursor:default;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.swal2-icon .swal2-icon-content{display:flex;align-items:center;font-size:3.75em}.swal2-icon.swal2-error{border-color:#f27474;color:#f27474}.swal2-icon.swal2-error .swal2-x-mark{position:relative;flex-grow:1}.swal2-icon.swal2-error [class^=swal2-x-mark-line]{display:block;position:absolute;top:2.3125em;width:2.9375em;height:.3125em;border-radius:.125em;background-color:#f27474}.swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=left]{left:1.0625em;transform:rotate(45deg)}.swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=right]{right:1em;transform:rotate(-45deg)}.swal2-icon.swal2-error.swal2-icon-show{-webkit-animation:swal2-animate-error-icon .5s;animation:swal2-animate-error-icon .5s}.swal2-icon.swal2-error.swal2-icon-show .swal2-x-mark{-webkit-animation:swal2-animate-error-x-mark .5s;animation:swal2-animate-error-x-mark .5s}.swal2-icon.swal2-warning{border-color:#facea8;color:#f8bb86}.swal2-icon.swal2-info{border-color:#9de0f6;color:#3fc3ee}.swal2-icon.swal2-question{border-color:#c9dae1;color:#87adbd}.swal2-icon.swal2-success{border-color:#a5dc86;color:#a5dc86}.swal2-icon.swal2-success [class^=swal2-success-circular-line]{position:absolute;width:3.75em;height:7.5em;transform:rotate(45deg);border-radius:50%}.swal2-icon.swal2-success [class^=swal2-success-circular-line][class$=left]{top:-.4375em;left:-2.0635em;transform:rotate(-45deg);transform-origin:3.75em 3.75em;border-radius:7.5em 0 0 7.5em}.swal2-icon.swal2-success [class^=swal2-success-circular-line][class$=right]{top:-.6875em;left:1.875em;transform:rotate(-45deg);transform-origin:0 3.75em;border-radius:0 7.5em 7.5em 0}.swal2-icon.swal2-success .swal2-success-ring{position:absolute;z-index:2;top:-.25em;left:-.25em;box-sizing:content-box;width:100%;height:100%;border:.25em solid rgba(165,220,134,.3);border-radius:50%}.swal2-icon.swal2-success .swal2-success-fix{position:absolute;z-index:1;top:.5em;left:1.625em;width:.4375em;height:5.625em;transform:rotate(-45deg)}.swal2-icon.swal2-success [class^=swal2-success-line]{display:block;position:absolute;z-index:2;height:.3125em;border-radius:.125em;background-color:#a5dc86}.swal2-icon.swal2-success [class^=swal2-success-line][class$=tip]{top:2.875em;left:.8125em;width:1.5625em;transform:rotate(45deg)}.swal2-icon.swal2-success [class^=swal2-success-line][class$=long]{top:2.375em;right:.5em;width:2.9375em;transform:rotate(-45deg)}.swal2-icon.swal2-success.swal2-icon-show .swal2-success-line-tip{-webkit-animation:swal2-animate-success-line-tip .75s;animation:swal2-animate-success-line-tip .75s}.swal2-icon.swal2-success.swal2-icon-show .swal2-success-line-long{-webkit-animation:swal2-animate-success-line-long .75s;animation:swal2-animate-success-line-long .75s}.swal2-icon.swal2-success.swal2-icon-show .swal2-success-circular-line-right{-webkit-animation:swal2-rotate-success-circular-line 4.25s ease-in;animation:swal2-rotate-success-circular-line 4.25s ease-in}.swal2-progress-steps{align-items:center;margin:0 0 1.25em;padding:0;background:inherit;font-weight:600}.swal2-progress-steps li{display:inline-block;position:relative}.swal2-progress-steps .swal2-progress-step{z-index:20;width:2em;height:2em;border-radius:2em;background:#3085d6;color:#fff;line-height:2em;text-align:center}.swal2-progress-steps .swal2-progress-step.swal2-active-progress-step{background:#3085d6}.swal2-progress-steps .swal2-progress-step.swal2-active-progress-step~.swal2-progress-step{background:#add8e6;color:#fff}.swal2-progress-steps .swal2-progress-step.swal2-active-progress-step~.swal2-progress-step-line{background:#add8e6}.swal2-progress-steps .swal2-progress-step-line{z-index:10;width:2.5em;height:.4em;margin:0 -1px;background:#3085d6}[class^=swal2]{-webkit-tap-highlight-color:transparent}.swal2-show{-webkit-animation:swal2-show .3s;animation:swal2-show .3s}.swal2-hide{-webkit-animation:swal2-hide .15s forwards;animation:swal2-hide .15s forwards}.swal2-noanimation{transition:none}.swal2-scrollbar-measure{position:absolute;top:-9999px;width:50px;height:50px;overflow:scroll}.swal2-rtl .swal2-close{right:auto;left:0}.swal2-rtl .swal2-timer-progress-bar{right:0;left:auto}@supports (-ms-accelerator:true){.swal2-range input{width:100%!important}.swal2-range output{display:none}}@media all and (-ms-high-contrast:none),(-ms-high-contrast:active){.swal2-range input{width:100%!important}.swal2-range output{display:none}}@-moz-document url-prefix(){.swal2-close:focus{outline:2px solid rgba(50,100,150,.4)}}@-webkit-keyframes swal2-toast-show{0%{transform:translateY(-.625em) rotateZ(2deg)}33%{transform:translateY(0) rotateZ(-2deg)}66%{transform:translateY(.3125em) rotateZ(2deg)}100%{transform:translateY(0) rotateZ(0)}}@keyframes swal2-toast-show{0%{transform:translateY(-.625em) rotateZ(2deg)}33%{transform:translateY(0) rotateZ(-2deg)}66%{transform:translateY(.3125em) rotateZ(2deg)}100%{transform:translateY(0) rotateZ(0)}}@-webkit-keyframes swal2-toast-hide{100%{transform:rotateZ(1deg);opacity:0}}@keyframes swal2-toast-hide{100%{transform:rotateZ(1deg);opacity:0}}@-webkit-keyframes swal2-toast-animate-success-line-tip{0%{top:.5625em;left:.0625em;width:0}54%{top:.125em;left:.125em;width:0}70%{top:.625em;left:-.25em;width:1.625em}84%{top:1.0625em;left:.75em;width:.5em}100%{top:1.125em;left:.1875em;width:.75em}}@keyframes swal2-toast-animate-success-line-tip{0%{top:.5625em;left:.0625em;width:0}54%{top:.125em;left:.125em;width:0}70%{top:.625em;left:-.25em;width:1.625em}84%{top:1.0625em;left:.75em;width:.5em}100%{top:1.125em;left:.1875em;width:.75em}}@-webkit-keyframes swal2-toast-animate-success-line-long{0%{top:1.625em;right:1.375em;width:0}65%{top:1.25em;right:.9375em;width:0}84%{top:.9375em;right:0;width:1.125em}100%{top:.9375em;right:.1875em;width:1.375em}}@keyframes swal2-toast-animate-success-line-long{0%{top:1.625em;right:1.375em;width:0}65%{top:1.25em;right:.9375em;width:0}84%{top:.9375em;right:0;width:1.125em}100%{top:.9375em;right:.1875em;width:1.375em}}@-webkit-keyframes swal2-show{0%{transform:scale(.7)}45%{transform:scale(1.05)}80%{transform:scale(.95)}100%{transform:scale(1)}}@keyframes swal2-show{0%{transform:scale(.7)}45%{transform:scale(1.05)}80%{transform:scale(.95)}100%{transform:scale(1)}}@-webkit-keyframes swal2-hide{0%{transform:scale(1);opacity:1}100%{transform:scale(.5);opacity:0}}@keyframes swal2-hide{0%{transform:scale(1);opacity:1}100%{transform:scale(.5);opacity:0}}@-webkit-keyframes swal2-animate-success-line-tip{0%{top:1.1875em;left:.0625em;width:0}54%{top:1.0625em;left:.125em;width:0}70%{top:2.1875em;left:-.375em;width:3.125em}84%{top:3em;left:1.3125em;width:1.0625em}100%{top:2.8125em;left:.8125em;width:1.5625em}}@keyframes swal2-animate-success-line-tip{0%{top:1.1875em;left:.0625em;width:0}54%{top:1.0625em;left:.125em;width:0}70%{top:2.1875em;left:-.375em;width:3.125em}84%{top:3em;left:1.3125em;width:1.0625em}100%{top:2.8125em;left:.8125em;width:1.5625em}}@-webkit-keyframes swal2-animate-success-line-long{0%{top:3.375em;right:2.875em;width:0}65%{top:3.375em;right:2.875em;width:0}84%{top:2.1875em;right:0;width:3.4375em}100%{top:2.375em;right:.5em;width:2.9375em}}@keyframes swal2-animate-success-line-long{0%{top:3.375em;right:2.875em;width:0}65%{top:3.375em;right:2.875em;width:0}84%{top:2.1875em;right:0;width:3.4375em}100%{top:2.375em;right:.5em;width:2.9375em}}@-webkit-keyframes swal2-rotate-success-circular-line{0%{transform:rotate(-45deg)}5%{transform:rotate(-45deg)}12%{transform:rotate(-405deg)}100%{transform:rotate(-405deg)}}@keyframes swal2-rotate-success-circular-line{0%{transform:rotate(-45deg)}5%{transform:rotate(-45deg)}12%{transform:rotate(-405deg)}100%{transform:rotate(-405deg)}}@-webkit-keyframes swal2-animate-error-x-mark{0%{margin-top:1.625em;transform:scale(.4);opacity:0}50%{margin-top:1.625em;transform:scale(.4);opacity:0}80%{margin-top:-.375em;transform:scale(1.15)}100%{margin-top:0;transform:scale(1);opacity:1}}@keyframes swal2-animate-error-x-mark{0%{margin-top:1.625em;transform:scale(.4);opacity:0}50%{margin-top:1.625em;transform:scale(.4);opacity:0}80%{margin-top:-.375em;transform:scale(1.15)}100%{margin-top:0;transform:scale(1);opacity:1}}@-webkit-keyframes swal2-animate-error-icon{0%{transform:rotateX(100deg);opacity:0}100%{transform:rotateX(0);opacity:1}}@keyframes swal2-animate-error-icon{0%{transform:rotateX(100deg);opacity:0}100%{transform:rotateX(0);opacity:1}}@-webkit-keyframes swal2-rotate-loading{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}@keyframes swal2-rotate-loading{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){overflow:hidden}body.swal2-height-auto{height:auto!important}body.swal2-no-backdrop .swal2-container{top:auto;right:auto;bottom:auto;left:auto;max-width:calc(100% - .625em * 2);background-color:transparent!important}body.swal2-no-backdrop .swal2-container>.swal2-modal{box-shadow:0 0 10px rgba(0,0,0,.4)}body.swal2-no-backdrop .swal2-container.swal2-top{top:0;left:50%;transform:translateX(-50%)}body.swal2-no-backdrop .swal2-container.swal2-top-left,body.swal2-no-backdrop .swal2-container.swal2-top-start{top:0;left:0}body.swal2-no-backdrop .swal2-container.swal2-top-end,body.swal2-no-backdrop .swal2-container.swal2-top-right{top:0;right:0}body.swal2-no-backdrop .swal2-container.swal2-center{top:50%;left:50%;transform:translate(-50%,-50%)}body.swal2-no-backdrop .swal2-container.swal2-center-left,body.swal2-no-backdrop .swal2-container.swal2-center-start{top:50%;left:0;transform:translateY(-50%)}body.swal2-no-backdrop .swal2-container.swal2-center-end,body.swal2-no-backdrop .swal2-container.swal2-center-right{top:50%;right:0;transform:translateY(-50%)}body.swal2-no-backdrop .swal2-container.swal2-bottom{bottom:0;left:50%;transform:translateX(-50%)}body.swal2-no-backdrop .swal2-container.swal2-bottom-left,body.swal2-no-backdrop .swal2-container.swal2-bottom-start{bottom:0;left:0}body.swal2-no-backdrop .swal2-container.swal2-bottom-end,body.swal2-no-backdrop .swal2-container.swal2-bottom-right{right:0;bottom:0}@media print{body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){overflow-y:scroll!important}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown)>[aria-hidden=true]{display:none}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown) .swal2-container{position:static!important}}body.swal2-toast-shown .swal2-container{background-color:transparent}body.swal2-toast-shown .swal2-container.swal2-top{top:0;right:auto;bottom:auto;left:50%;transform:translateX(-50%)}body.swal2-toast-shown .swal2-container.swal2-top-end,body.swal2-toast-shown .swal2-container.swal2-top-right{top:0;right:0;bottom:auto;left:auto}body.swal2-toast-shown .swal2-container.swal2-top-left,body.swal2-toast-shown .swal2-container.swal2-top-start{top:0;right:auto;bottom:auto;left:0}body.swal2-toast-shown .swal2-container.swal2-center-left,body.swal2-toast-shown .swal2-container.swal2-center-start{top:50%;right:auto;bottom:auto;left:0;transform:translateY(-50%)}body.swal2-toast-shown .swal2-container.swal2-center{top:50%;right:auto;bottom:auto;left:50%;transform:translate(-50%,-50%)}body.swal2-toast-shown .swal2-container.swal2-center-end,body.swal2-toast-shown .swal2-container.swal2-center-right{top:50%;right:0;bottom:auto;left:auto;transform:translateY(-50%)}body.swal2-toast-shown .swal2-container.swal2-bottom-left,body.swal2-toast-shown .swal2-container.swal2-bottom-start{top:auto;right:auto;bottom:0;left:0}body.swal2-toast-shown .swal2-container.swal2-bottom{top:auto;right:auto;bottom:0;left:50%;transform:translateX(-50%)}body.swal2-toast-shown .swal2-container.swal2-bottom-end,body.swal2-toast-shown .swal2-container.swal2-bottom-right{top:auto;right:0;bottom:0;left:auto}body.swal2-toast-column .swal2-toast{flex-direction:column;align-items:stretch}body.swal2-toast-column .swal2-toast .swal2-actions{flex:1;align-self:stretch;height:2.2em;margin-top:.3125em}body.swal2-toast-column .swal2-toast .swal2-loading{justify-content:center}body.swal2-toast-column .swal2-toast .swal2-input{height:2em;margin:.3125em auto;font-size:1em}body.swal2-toast-column .swal2-toast .swal2-validation-message{font-size:1em}");

/***/ }),

/***/ "./public/assets/js/app.js":
/*!*********************************!*\
  !*** ./public/assets/js/app.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var App = function () {
  var MediaSize = {
    xl: 1200,
    lg: 992,
    md: 991,
    sm: 576
  };
  var ToggleClasses = {
    headerhamburger: '.toggle-sidebar'
  };
  var Selector = {
    mainHeader: '.header.navbar',
    headerhamburger: '.toggle-sidebar',
    fixed: '.fixed-top',
    mainContainer: '.main-container',
    sidebar: '#sidebar',
    sidebarContent: '#sidebar-content',
    sidebarStickyContent: '.sticky-sidebar-content',
    ariaExpandedTrue: '#sidebar [aria-expanded="true"]',
    ariaExpandedFalse: '#sidebar [aria-expanded="false"]',
    contentWrapper: '#content',
    contentWrapperContent: '.container',
    mainContentArea: '.main-content',
    mainFooter: '.theme-footer'
  };
  var categoryScroll = {
    onLoad: function onLoad(onLoadResolution) {
      if (onLoadResolution === 'def') {
        var scrollToCategory = $(Selector.ariaExpandedTrue).parent().offset().top;
        var liMenuOuterHeight = $(Selector.ariaExpandedTrue).parent().children().closest('a').outerHeight();
        var body = $("html, body");
        body.stop().animate({
          scrollTop: scrollToCategory - liMenuOuterHeight
        }, 500, 'swing', function () {});
      }

      if (onLoadResolution === 'mob') {
        var childPos = $(Selector.ariaExpandedTrue).parent().offset();
        var parentPos = $('#sidebar').offset();
        var childOffset = {
          top: childPos.top - parentPos.top,
          left: childPos.left - parentPos.left
        };
        $("#sidebar").mCustomScrollbar('scrollTo', '-=' + childOffset.top);
      }
    },
    "default": function _default() {
      $('.menu-categories li > .collapse').not(':first').on('shown.bs.collapse', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var scrollToCategory = $(this).parent().offset().top;
        var liMenuOuterHeight = $(this).parent().children().closest('a').outerHeight();
        var body = $("html, body");
        body.stop().animate({
          scrollTop: scrollToCategory - liMenuOuterHeight
        }, 500, 'swing', function () {});
      });
    },
    mobile: function mobile() {
      $('.menu-categories li > .collapse').on('shown.bs.collapse', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var childPos = $(this).parent().offset();
        var parentPos = $('#sidebar').offset();
        var childOffset = {
          top: childPos.top - parentPos.top,
          left: childPos.left - parentPos.left
        };
        $("#sidebar").mCustomScrollbar('scrollTo', '-=' + childOffset.top);
      });
    }
  }; // Default Enabled

  var toggleFunction = {
    sidebar: function sidebar() {
      $('.sidebarCollapse').on('click', function (sidebar) {
        sidebar.preventDefault();
        $(Selector.mainContainer).toggleClass("sidebar-closed");
        $(Selector.mainHeader).toggleClass('expand-header');
        $(Selector.mainContainer).toggleClass("sbar-open");
        $('.overlay').toggleClass('show');
        $('html,body').toggleClass('sidebar-noneoverflow');
        $('footer .footer-section-1').toggleClass('f-close');
      });
    },
    CS: function CS() {
      $(".toggle-control-sidebar").click(function (B) {
        B.preventDefault();
        $(".control-sidebar").toggleClass("control-sidebar-open");
        $('.cs-overlay').toggleClass('show');
        $('html,body').toggleClass('cs-noneoverflow');
      });
    },
    overlay: function overlay() {
      $('#dismiss, .overlay, cs-overlay').on('click', function () {
        // hide sidebar
        $(Selector.mainContainer).addClass('sidebar-closed');
        $(Selector.mainContainer).removeClass('sbar-open'); // hide overlay

        $('.overlay').removeClass('show');
        $('html,body').removeClass('sidebar-noneoverflow');
      });
    },
    CSoverlay: function CSoverlay() {
      $('.cs-overlay').on('click', function () {
        $(this).removeClass('show');
        $('html,body').removeClass('cs-noneoverflow');
        $('.control-sidebar').removeClass('control-sidebar-open');
      });
    },
    deactivateScroll: function deactivateScroll() {
      $('#sidebar').mCustomScrollbar("destroy");
    }
  };
  var mobileFunctions = {
    activateScroll: function activateScroll() {
      $("#sidebar").mCustomScrollbar({
        theme: "minimal",
        scrollInertia: 1000
      });
    }
  };
  var controlSidebar = {
    chk: function chk() {
      $(".chb").change(function () {
        $(".chb").prop('checked', false);
        $(this).prop('checked', true);
      });
    }
  };
  var _mobileResolution = {
    onRefresh: function onRefresh() {
      var windowWidth = window.innerWidth;

      if (windowWidth <= MediaSize.md) {
        categoryScroll.mobile();
        mobileFunctions.activateScroll();
      }
    },
    onResize: function onResize() {
      $(window).on('resize', function (event) {
        event.preventDefault();
        var windowWidth = window.innerWidth;

        if (windowWidth <= MediaSize.md) {
          mobileFunctions.activateScroll();
        }
      });
    }
  };
  var _desktopResolution = {
    onRefresh: function onRefresh() {
      var windowWidth = window.innerWidth;

      if (windowWidth > MediaSize.md) {
        toggleFunction.deactivateScroll();
        categoryScroll["default"]();
      }
    },
    onResize: function onResize() {
      $(window).on('resize', function (event) {
        event.preventDefault();
        var windowWidth = window.innerWidth;

        if (windowWidth > MediaSize.md) {
          $('footer .footer-section-1').removeClass('f-close');
          toggleFunction.deactivateScroll();
        }
      });
    }
  };

  function sidebarFunctionality() {
    function sidebarCloser() {
      if (window.innerWidth <= 991) {
        $("#container").addClass("sidebar-closed");
        $('.overlay').removeClass('show');
      } else if (window.innerWidth > 991) {
        $("#container").removeClass("sidebar-closed");
        $(".navbar").removeClass("expand-header");
        $('.overlay').removeClass('show');
        $('#container').removeClass('sbar-open');
      }
    }

    function sidebarMobCheck() {
      if (window.innerWidth <= 991) {
        if ($('.main-container').hasClass('sbar-open')) {
          return;
        } else {
          sidebarCloser();
        }
      } else if (window.innerWidth > 991) {
        sidebarCloser();
      }
    }

    sidebarCloser();
    $(window).resize(function (event) {
      sidebarMobCheck();
    });
  }

  return {
    init: function init() {
      controlSidebar.chk(); // Sidebar fn

      toggleFunction.sidebar(); // Control Sidebar fn

      toggleFunction.CS(); // Overlay fn

      toggleFunction.overlay(); // CSoverlay

      toggleFunction.CSoverlay(); // Desktop Resoltion fn

      _desktopResolution.onRefresh();

      _desktopResolution.onResize(); // Mobile Resoltion fn


      _mobileResolution.onRefresh();

      _mobileResolution.onResize();

      sidebarFunctionality();
    }
  };
}();

/***/ }),

/***/ "./public/assets/js/libs/jquery-3.1.1.min.js":
/*!***************************************************!*\
  !*** ./public/assets/js/libs/jquery-3.1.1.min.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*! jQuery v3.1.1 | (c) jQuery Foundation | jquery.org/license */
!function (a, b) {
  "use strict";

  "object" == ( false ? undefined : _typeof(module)) && "object" == _typeof(module.exports) ? module.exports = a.document ? b(a, !0) : function (a) {
    if (!a.document) throw new Error("jQuery requires a window with a document");
    return b(a);
  } : b(a);
}("undefined" != typeof window ? window : this, function (a, b) {
  "use strict";

  var c = [],
      d = a.document,
      e = Object.getPrototypeOf,
      f = c.slice,
      g = c.concat,
      h = c.push,
      i = c.indexOf,
      j = {},
      k = j.toString,
      l = j.hasOwnProperty,
      m = l.toString,
      n = m.call(Object),
      o = {};

  function p(a, b) {
    b = b || d;
    var c = b.createElement("script");
    c.text = a, b.head.appendChild(c).parentNode.removeChild(c);
  }

  var q = "3.1.1",
      r = function r(a, b) {
    return new r.fn.init(a, b);
  },
      s = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
      t = /^-ms-/,
      u = /-([a-z])/g,
      v = function v(a, b) {
    return b.toUpperCase();
  };

  r.fn = r.prototype = {
    jquery: q,
    constructor: r,
    length: 0,
    toArray: function toArray() {
      return f.call(this);
    },
    get: function get(a) {
      return null == a ? f.call(this) : a < 0 ? this[a + this.length] : this[a];
    },
    pushStack: function pushStack(a) {
      var b = r.merge(this.constructor(), a);
      return b.prevObject = this, b;
    },
    each: function each(a) {
      return r.each(this, a);
    },
    map: function map(a) {
      return this.pushStack(r.map(this, function (b, c) {
        return a.call(b, c, b);
      }));
    },
    slice: function slice() {
      return this.pushStack(f.apply(this, arguments));
    },
    first: function first() {
      return this.eq(0);
    },
    last: function last() {
      return this.eq(-1);
    },
    eq: function eq(a) {
      var b = this.length,
          c = +a + (a < 0 ? b : 0);
      return this.pushStack(c >= 0 && c < b ? [this[c]] : []);
    },
    end: function end() {
      return this.prevObject || this.constructor();
    },
    push: h,
    sort: c.sort,
    splice: c.splice
  }, r.extend = r.fn.extend = function () {
    var a,
        b,
        c,
        d,
        e,
        f,
        g = arguments[0] || {},
        h = 1,
        i = arguments.length,
        j = !1;

    for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == _typeof(g) || r.isFunction(g) || (g = {}), h === i && (g = this, h--); h < i; h++) {
      if (null != (a = arguments[h])) for (b in a) {
        c = g[b], d = a[b], g !== d && (j && d && (r.isPlainObject(d) || (e = r.isArray(d))) ? (e ? (e = !1, f = c && r.isArray(c) ? c : []) : f = c && r.isPlainObject(c) ? c : {}, g[b] = r.extend(j, f, d)) : void 0 !== d && (g[b] = d));
      }
    }

    return g;
  }, r.extend({
    expando: "jQuery" + (q + Math.random()).replace(/\D/g, ""),
    isReady: !0,
    error: function error(a) {
      throw new Error(a);
    },
    noop: function noop() {},
    isFunction: function isFunction(a) {
      return "function" === r.type(a);
    },
    isArray: Array.isArray,
    isWindow: function isWindow(a) {
      return null != a && a === a.window;
    },
    isNumeric: function isNumeric(a) {
      var b = r.type(a);
      return ("number" === b || "string" === b) && !isNaN(a - parseFloat(a));
    },
    isPlainObject: function isPlainObject(a) {
      var b, c;
      return !(!a || "[object Object]" !== k.call(a)) && (!(b = e(a)) || (c = l.call(b, "constructor") && b.constructor, "function" == typeof c && m.call(c) === n));
    },
    isEmptyObject: function isEmptyObject(a) {
      var b;

      for (b in a) {
        return !1;
      }

      return !0;
    },
    type: function type(a) {
      return null == a ? a + "" : "object" == _typeof(a) || "function" == typeof a ? j[k.call(a)] || "object" : _typeof(a);
    },
    globalEval: function globalEval(a) {
      p(a);
    },
    camelCase: function camelCase(a) {
      return a.replace(t, "ms-").replace(u, v);
    },
    nodeName: function nodeName(a, b) {
      return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase();
    },
    each: function each(a, b) {
      var c,
          d = 0;

      if (w(a)) {
        for (c = a.length; d < c; d++) {
          if (b.call(a[d], d, a[d]) === !1) break;
        }
      } else for (d in a) {
        if (b.call(a[d], d, a[d]) === !1) break;
      }

      return a;
    },
    trim: function trim(a) {
      return null == a ? "" : (a + "").replace(s, "");
    },
    makeArray: function makeArray(a, b) {
      var c = b || [];
      return null != a && (w(Object(a)) ? r.merge(c, "string" == typeof a ? [a] : a) : h.call(c, a)), c;
    },
    inArray: function inArray(a, b, c) {
      return null == b ? -1 : i.call(b, a, c);
    },
    merge: function merge(a, b) {
      for (var c = +b.length, d = 0, e = a.length; d < c; d++) {
        a[e++] = b[d];
      }

      return a.length = e, a;
    },
    grep: function grep(a, b, c) {
      for (var d, e = [], f = 0, g = a.length, h = !c; f < g; f++) {
        d = !b(a[f], f), d !== h && e.push(a[f]);
      }

      return e;
    },
    map: function map(a, b, c) {
      var d,
          e,
          f = 0,
          h = [];
      if (w(a)) for (d = a.length; f < d; f++) {
        e = b(a[f], f, c), null != e && h.push(e);
      } else for (f in a) {
        e = b(a[f], f, c), null != e && h.push(e);
      }
      return g.apply([], h);
    },
    guid: 1,
    proxy: function proxy(a, b) {
      var c, d, e;
      if ("string" == typeof b && (c = a[b], b = a, a = c), r.isFunction(a)) return d = f.call(arguments, 2), e = function e() {
        return a.apply(b || this, d.concat(f.call(arguments)));
      }, e.guid = a.guid = a.guid || r.guid++, e;
    },
    now: Date.now,
    support: o
  }), "function" == typeof Symbol && (r.fn[Symbol.iterator] = c[Symbol.iterator]), r.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (a, b) {
    j["[object " + b + "]"] = b.toLowerCase();
  });

  function w(a) {
    var b = !!a && "length" in a && a.length,
        c = r.type(a);
    return "function" !== c && !r.isWindow(a) && ("array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a);
  }

  var x = function (a) {
    var b,
        c,
        d,
        e,
        f,
        g,
        h,
        i,
        j,
        k,
        l,
        m,
        n,
        o,
        p,
        q,
        r,
        s,
        t,
        u = "sizzle" + 1 * new Date(),
        v = a.document,
        w = 0,
        x = 0,
        y = ha(),
        z = ha(),
        A = ha(),
        B = function B(a, b) {
      return a === b && (l = !0), 0;
    },
        C = {}.hasOwnProperty,
        D = [],
        E = D.pop,
        F = D.push,
        G = D.push,
        H = D.slice,
        I = function I(a, b) {
      for (var c = 0, d = a.length; c < d; c++) {
        if (a[c] === b) return c;
      }

      return -1;
    },
        J = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
        K = "[\\x20\\t\\r\\n\\f]",
        L = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
        M = "\\[" + K + "*(" + L + ")(?:" + K + "*([*^$|!~]?=)" + K + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + L + "))|)" + K + "*\\]",
        N = ":(" + L + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + M + ")*)|.*)\\)|)",
        O = new RegExp(K + "+", "g"),
        P = new RegExp("^" + K + "+|((?:^|[^\\\\])(?:\\\\.)*)" + K + "+$", "g"),
        Q = new RegExp("^" + K + "*," + K + "*"),
        R = new RegExp("^" + K + "*([>+~]|" + K + ")" + K + "*"),
        S = new RegExp("=" + K + "*([^\\]'\"]*?)" + K + "*\\]", "g"),
        T = new RegExp(N),
        U = new RegExp("^" + L + "$"),
        V = {
      ID: new RegExp("^#(" + L + ")"),
      CLASS: new RegExp("^\\.(" + L + ")"),
      TAG: new RegExp("^(" + L + "|[*])"),
      ATTR: new RegExp("^" + M),
      PSEUDO: new RegExp("^" + N),
      CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + K + "*(even|odd|(([+-]|)(\\d*)n|)" + K + "*(?:([+-]|)" + K + "*(\\d+)|))" + K + "*\\)|)", "i"),
      bool: new RegExp("^(?:" + J + ")$", "i"),
      needsContext: new RegExp("^" + K + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + K + "*((?:-\\d)?\\d*)" + K + "*\\)|)(?=[^-]|$)", "i")
    },
        W = /^(?:input|select|textarea|button)$/i,
        X = /^h\d$/i,
        Y = /^[^{]+\{\s*\[native \w/,
        Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
        $ = /[+~]/,
        _ = new RegExp("\\\\([\\da-f]{1,6}" + K + "?|(" + K + ")|.)", "ig"),
        aa = function aa(a, b, c) {
      var d = "0x" + b - 65536;
      return d !== d || c ? b : d < 0 ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320);
    },
        ba = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
        ca = function ca(a, b) {
      return b ? "\0" === a ? "\uFFFD" : a.slice(0, -1) + "\\" + a.charCodeAt(a.length - 1).toString(16) + " " : "\\" + a;
    },
        da = function da() {
      m();
    },
        ea = ta(function (a) {
      return a.disabled === !0 && ("form" in a || "label" in a);
    }, {
      dir: "parentNode",
      next: "legend"
    });

    try {
      G.apply(D = H.call(v.childNodes), v.childNodes), D[v.childNodes.length].nodeType;
    } catch (fa) {
      G = {
        apply: D.length ? function (a, b) {
          F.apply(a, H.call(b));
        } : function (a, b) {
          var c = a.length,
              d = 0;

          while (a[c++] = b[d++]) {
            ;
          }

          a.length = c - 1;
        }
      };
    }

    function ga(a, b, d, e) {
      var f,
          h,
          j,
          k,
          l,
          o,
          r,
          s = b && b.ownerDocument,
          w = b ? b.nodeType : 9;
      if (d = d || [], "string" != typeof a || !a || 1 !== w && 9 !== w && 11 !== w) return d;

      if (!e && ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, p)) {
        if (11 !== w && (l = Z.exec(a))) if (f = l[1]) {
          if (9 === w) {
            if (!(j = b.getElementById(f))) return d;
            if (j.id === f) return d.push(j), d;
          } else if (s && (j = s.getElementById(f)) && t(b, j) && j.id === f) return d.push(j), d;
        } else {
          if (l[2]) return G.apply(d, b.getElementsByTagName(a)), d;
          if ((f = l[3]) && c.getElementsByClassName && b.getElementsByClassName) return G.apply(d, b.getElementsByClassName(f)), d;
        }

        if (c.qsa && !A[a + " "] && (!q || !q.test(a))) {
          if (1 !== w) s = b, r = a;else if ("object" !== b.nodeName.toLowerCase()) {
            (k = b.getAttribute("id")) ? k = k.replace(ba, ca) : b.setAttribute("id", k = u), o = g(a), h = o.length;

            while (h--) {
              o[h] = "#" + k + " " + sa(o[h]);
            }

            r = o.join(","), s = $.test(a) && qa(b.parentNode) || b;
          }
          if (r) try {
            return G.apply(d, s.querySelectorAll(r)), d;
          } catch (x) {} finally {
            k === u && b.removeAttribute("id");
          }
        }
      }

      return i(a.replace(P, "$1"), b, d, e);
    }

    function ha() {
      var a = [];

      function b(c, e) {
        return a.push(c + " ") > d.cacheLength && delete b[a.shift()], b[c + " "] = e;
      }

      return b;
    }

    function ia(a) {
      return a[u] = !0, a;
    }

    function ja(a) {
      var b = n.createElement("fieldset");

      try {
        return !!a(b);
      } catch (c) {
        return !1;
      } finally {
        b.parentNode && b.parentNode.removeChild(b), b = null;
      }
    }

    function ka(a, b) {
      var c = a.split("|"),
          e = c.length;

      while (e--) {
        d.attrHandle[c[e]] = b;
      }
    }

    function la(a, b) {
      var c = b && a,
          d = c && 1 === a.nodeType && 1 === b.nodeType && a.sourceIndex - b.sourceIndex;
      if (d) return d;
      if (c) while (c = c.nextSibling) {
        if (c === b) return -1;
      }
      return a ? 1 : -1;
    }

    function ma(a) {
      return function (b) {
        var c = b.nodeName.toLowerCase();
        return "input" === c && b.type === a;
      };
    }

    function na(a) {
      return function (b) {
        var c = b.nodeName.toLowerCase();
        return ("input" === c || "button" === c) && b.type === a;
      };
    }

    function oa(a) {
      return function (b) {
        return "form" in b ? b.parentNode && b.disabled === !1 ? "label" in b ? "label" in b.parentNode ? b.parentNode.disabled === a : b.disabled === a : b.isDisabled === a || b.isDisabled !== !a && ea(b) === a : b.disabled === a : "label" in b && b.disabled === a;
      };
    }

    function pa(a) {
      return ia(function (b) {
        return b = +b, ia(function (c, d) {
          var e,
              f = a([], c.length, b),
              g = f.length;

          while (g--) {
            c[e = f[g]] && (c[e] = !(d[e] = c[e]));
          }
        });
      });
    }

    function qa(a) {
      return a && "undefined" != typeof a.getElementsByTagName && a;
    }

    c = ga.support = {}, f = ga.isXML = function (a) {
      var b = a && (a.ownerDocument || a).documentElement;
      return !!b && "HTML" !== b.nodeName;
    }, m = ga.setDocument = function (a) {
      var b,
          e,
          g = a ? a.ownerDocument || a : v;
      return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = n.documentElement, p = !f(n), v !== n && (e = n.defaultView) && e.top !== e && (e.addEventListener ? e.addEventListener("unload", da, !1) : e.attachEvent && e.attachEvent("onunload", da)), c.attributes = ja(function (a) {
        return a.className = "i", !a.getAttribute("className");
      }), c.getElementsByTagName = ja(function (a) {
        return a.appendChild(n.createComment("")), !a.getElementsByTagName("*").length;
      }), c.getElementsByClassName = Y.test(n.getElementsByClassName), c.getById = ja(function (a) {
        return o.appendChild(a).id = u, !n.getElementsByName || !n.getElementsByName(u).length;
      }), c.getById ? (d.filter.ID = function (a) {
        var b = a.replace(_, aa);
        return function (a) {
          return a.getAttribute("id") === b;
        };
      }, d.find.ID = function (a, b) {
        if ("undefined" != typeof b.getElementById && p) {
          var c = b.getElementById(a);
          return c ? [c] : [];
        }
      }) : (d.filter.ID = function (a) {
        var b = a.replace(_, aa);
        return function (a) {
          var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode("id");
          return c && c.value === b;
        };
      }, d.find.ID = function (a, b) {
        if ("undefined" != typeof b.getElementById && p) {
          var c,
              d,
              e,
              f = b.getElementById(a);

          if (f) {
            if (c = f.getAttributeNode("id"), c && c.value === a) return [f];
            e = b.getElementsByName(a), d = 0;

            while (f = e[d++]) {
              if (c = f.getAttributeNode("id"), c && c.value === a) return [f];
            }
          }

          return [];
        }
      }), d.find.TAG = c.getElementsByTagName ? function (a, b) {
        return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0;
      } : function (a, b) {
        var c,
            d = [],
            e = 0,
            f = b.getElementsByTagName(a);

        if ("*" === a) {
          while (c = f[e++]) {
            1 === c.nodeType && d.push(c);
          }

          return d;
        }

        return f;
      }, d.find.CLASS = c.getElementsByClassName && function (a, b) {
        if ("undefined" != typeof b.getElementsByClassName && p) return b.getElementsByClassName(a);
      }, r = [], q = [], (c.qsa = Y.test(n.querySelectorAll)) && (ja(function (a) {
        o.appendChild(a).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\r\\' msallowcapture=''><option selected=''></option></select>", a.querySelectorAll("[msallowcapture^='']").length && q.push("[*^$]=" + K + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || q.push("\\[" + K + "*(?:value|" + J + ")"), a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="), a.querySelectorAll(":checked").length || q.push(":checked"), a.querySelectorAll("a#" + u + "+*").length || q.push(".#.+[+~]");
      }), ja(function (a) {
        a.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
        var b = n.createElement("input");
        b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && q.push("name" + K + "*[*^$|!~]?="), 2 !== a.querySelectorAll(":enabled").length && q.push(":enabled", ":disabled"), o.appendChild(a).disabled = !0, 2 !== a.querySelectorAll(":disabled").length && q.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), q.push(",.*:");
      })), (c.matchesSelector = Y.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ja(function (a) {
        c.disconnectedMatch = s.call(a, "*"), s.call(a, "[s!='']:x"), r.push("!=", N);
      }), q = q.length && new RegExp(q.join("|")), r = r.length && new RegExp(r.join("|")), b = Y.test(o.compareDocumentPosition), t = b || Y.test(o.contains) ? function (a, b) {
        var c = 9 === a.nodeType ? a.documentElement : a,
            d = b && b.parentNode;
        return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)));
      } : function (a, b) {
        if (b) while (b = b.parentNode) {
          if (b === a) return !0;
        }
        return !1;
      }, B = b ? function (a, b) {
        if (a === b) return l = !0, 0;
        var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
        return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === n || a.ownerDocument === v && t(v, a) ? -1 : b === n || b.ownerDocument === v && t(v, b) ? 1 : k ? I(k, a) - I(k, b) : 0 : 4 & d ? -1 : 1);
      } : function (a, b) {
        if (a === b) return l = !0, 0;
        var c,
            d = 0,
            e = a.parentNode,
            f = b.parentNode,
            g = [a],
            h = [b];
        if (!e || !f) return a === n ? -1 : b === n ? 1 : e ? -1 : f ? 1 : k ? I(k, a) - I(k, b) : 0;
        if (e === f) return la(a, b);
        c = a;

        while (c = c.parentNode) {
          g.unshift(c);
        }

        c = b;

        while (c = c.parentNode) {
          h.unshift(c);
        }

        while (g[d] === h[d]) {
          d++;
        }

        return d ? la(g[d], h[d]) : g[d] === v ? -1 : h[d] === v ? 1 : 0;
      }, n) : n;
    }, ga.matches = function (a, b) {
      return ga(a, null, null, b);
    }, ga.matchesSelector = function (a, b) {
      if ((a.ownerDocument || a) !== n && m(a), b = b.replace(S, "='$1']"), c.matchesSelector && p && !A[b + " "] && (!r || !r.test(b)) && (!q || !q.test(b))) try {
        var d = s.call(a, b);
        if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d;
      } catch (e) {}
      return ga(b, n, null, [a]).length > 0;
    }, ga.contains = function (a, b) {
      return (a.ownerDocument || a) !== n && m(a), t(a, b);
    }, ga.attr = function (a, b) {
      (a.ownerDocument || a) !== n && m(a);
      var e = d.attrHandle[b.toLowerCase()],
          f = e && C.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;
      return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null;
    }, ga.escape = function (a) {
      return (a + "").replace(ba, ca);
    }, ga.error = function (a) {
      throw new Error("Syntax error, unrecognized expression: " + a);
    }, ga.uniqueSort = function (a) {
      var b,
          d = [],
          e = 0,
          f = 0;

      if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) {
        while (b = a[f++]) {
          b === a[f] && (e = d.push(f));
        }

        while (e--) {
          a.splice(d[e], 1);
        }
      }

      return k = null, a;
    }, e = ga.getText = function (a) {
      var b,
          c = "",
          d = 0,
          f = a.nodeType;

      if (f) {
        if (1 === f || 9 === f || 11 === f) {
          if ("string" == typeof a.textContent) return a.textContent;

          for (a = a.firstChild; a; a = a.nextSibling) {
            c += e(a);
          }
        } else if (3 === f || 4 === f) return a.nodeValue;
      } else while (b = a[d++]) {
        c += e(b);
      }

      return c;
    }, d = ga.selectors = {
      cacheLength: 50,
      createPseudo: ia,
      match: V,
      attrHandle: {},
      find: {},
      relative: {
        ">": {
          dir: "parentNode",
          first: !0
        },
        " ": {
          dir: "parentNode"
        },
        "+": {
          dir: "previousSibling",
          first: !0
        },
        "~": {
          dir: "previousSibling"
        }
      },
      preFilter: {
        ATTR: function ATTR(a) {
          return a[1] = a[1].replace(_, aa), a[3] = (a[3] || a[4] || a[5] || "").replace(_, aa), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4);
        },
        CHILD: function CHILD(a) {
          return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || ga.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && ga.error(a[0]), a;
        },
        PSEUDO: function PSEUDO(a) {
          var b,
              c = !a[6] && a[2];
          return V.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && T.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3));
        }
      },
      filter: {
        TAG: function TAG(a) {
          var b = a.replace(_, aa).toLowerCase();
          return "*" === a ? function () {
            return !0;
          } : function (a) {
            return a.nodeName && a.nodeName.toLowerCase() === b;
          };
        },
        CLASS: function CLASS(a) {
          var b = y[a + " "];
          return b || (b = new RegExp("(^|" + K + ")" + a + "(" + K + "|$)")) && y(a, function (a) {
            return b.test("string" == typeof a.className && a.className || "undefined" != typeof a.getAttribute && a.getAttribute("class") || "");
          });
        },
        ATTR: function ATTR(a, b, c) {
          return function (d) {
            var e = ga.attr(d, a);
            return null == e ? "!=" === b : !b || (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e.replace(O, " ") + " ").indexOf(c) > -1 : "|=" === b && (e === c || e.slice(0, c.length + 1) === c + "-"));
          };
        },
        CHILD: function CHILD(a, b, c, d, e) {
          var f = "nth" !== a.slice(0, 3),
              g = "last" !== a.slice(-4),
              h = "of-type" === b;
          return 1 === d && 0 === e ? function (a) {
            return !!a.parentNode;
          } : function (b, c, i) {
            var j,
                k,
                l,
                m,
                n,
                o,
                p = f !== g ? "nextSibling" : "previousSibling",
                q = b.parentNode,
                r = h && b.nodeName.toLowerCase(),
                s = !i && !h,
                t = !1;

            if (q) {
              if (f) {
                while (p) {
                  m = b;

                  while (m = m[p]) {
                    if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
                  }

                  o = p = "only" === a && !o && "nextSibling";
                }

                return !0;
              }

              if (o = [g ? q.firstChild : q.lastChild], g && s) {
                m = q, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n && j[2], m = n && q.childNodes[n];

                while (m = ++n && m && m[p] || (t = n = 0) || o.pop()) {
                  if (1 === m.nodeType && ++t && m === b) {
                    k[a] = [w, n, t];
                    break;
                  }
                }
              } else if (s && (m = b, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n), t === !1) while (m = ++n && m && m[p] || (t = n = 0) || o.pop()) {
                if ((h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) && ++t && (s && (l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), k[a] = [w, t]), m === b)) break;
              }

              return t -= e, t === d || t % d === 0 && t / d >= 0;
            }
          };
        },
        PSEUDO: function PSEUDO(a, b) {
          var c,
              e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || ga.error("unsupported pseudo: " + a);
          return e[u] ? e(b) : e.length > 1 ? (c = [a, a, "", b], d.setFilters.hasOwnProperty(a.toLowerCase()) ? ia(function (a, c) {
            var d,
                f = e(a, b),
                g = f.length;

            while (g--) {
              d = I(a, f[g]), a[d] = !(c[d] = f[g]);
            }
          }) : function (a) {
            return e(a, 0, c);
          }) : e;
        }
      },
      pseudos: {
        not: ia(function (a) {
          var b = [],
              c = [],
              d = h(a.replace(P, "$1"));
          return d[u] ? ia(function (a, b, c, e) {
            var f,
                g = d(a, null, e, []),
                h = a.length;

            while (h--) {
              (f = g[h]) && (a[h] = !(b[h] = f));
            }
          }) : function (a, e, f) {
            return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop();
          };
        }),
        has: ia(function (a) {
          return function (b) {
            return ga(a, b).length > 0;
          };
        }),
        contains: ia(function (a) {
          return a = a.replace(_, aa), function (b) {
            return (b.textContent || b.innerText || e(b)).indexOf(a) > -1;
          };
        }),
        lang: ia(function (a) {
          return U.test(a || "") || ga.error("unsupported lang: " + a), a = a.replace(_, aa).toLowerCase(), function (b) {
            var c;

            do {
              if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-");
            } while ((b = b.parentNode) && 1 === b.nodeType);

            return !1;
          };
        }),
        target: function target(b) {
          var c = a.location && a.location.hash;
          return c && c.slice(1) === b.id;
        },
        root: function root(a) {
          return a === o;
        },
        focus: function focus(a) {
          return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex);
        },
        enabled: oa(!1),
        disabled: oa(!0),
        checked: function checked(a) {
          var b = a.nodeName.toLowerCase();
          return "input" === b && !!a.checked || "option" === b && !!a.selected;
        },
        selected: function selected(a) {
          return a.parentNode && a.parentNode.selectedIndex, a.selected === !0;
        },
        empty: function empty(a) {
          for (a = a.firstChild; a; a = a.nextSibling) {
            if (a.nodeType < 6) return !1;
          }

          return !0;
        },
        parent: function parent(a) {
          return !d.pseudos.empty(a);
        },
        header: function header(a) {
          return X.test(a.nodeName);
        },
        input: function input(a) {
          return W.test(a.nodeName);
        },
        button: function button(a) {
          var b = a.nodeName.toLowerCase();
          return "input" === b && "button" === a.type || "button" === b;
        },
        text: function text(a) {
          var b;
          return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase());
        },
        first: pa(function () {
          return [0];
        }),
        last: pa(function (a, b) {
          return [b - 1];
        }),
        eq: pa(function (a, b, c) {
          return [c < 0 ? c + b : c];
        }),
        even: pa(function (a, b) {
          for (var c = 0; c < b; c += 2) {
            a.push(c);
          }

          return a;
        }),
        odd: pa(function (a, b) {
          for (var c = 1; c < b; c += 2) {
            a.push(c);
          }

          return a;
        }),
        lt: pa(function (a, b, c) {
          for (var d = c < 0 ? c + b : c; --d >= 0;) {
            a.push(d);
          }

          return a;
        }),
        gt: pa(function (a, b, c) {
          for (var d = c < 0 ? c + b : c; ++d < b;) {
            a.push(d);
          }

          return a;
        })
      }
    }, d.pseudos.nth = d.pseudos.eq;

    for (b in {
      radio: !0,
      checkbox: !0,
      file: !0,
      password: !0,
      image: !0
    }) {
      d.pseudos[b] = ma(b);
    }

    for (b in {
      submit: !0,
      reset: !0
    }) {
      d.pseudos[b] = na(b);
    }

    function ra() {}

    ra.prototype = d.filters = d.pseudos, d.setFilters = new ra(), g = ga.tokenize = function (a, b) {
      var c,
          e,
          f,
          g,
          h,
          i,
          j,
          k = z[a + " "];
      if (k) return b ? 0 : k.slice(0);
      h = a, i = [], j = d.preFilter;

      while (h) {
        c && !(e = Q.exec(h)) || (e && (h = h.slice(e[0].length) || h), i.push(f = [])), c = !1, (e = R.exec(h)) && (c = e.shift(), f.push({
          value: c,
          type: e[0].replace(P, " ")
        }), h = h.slice(c.length));

        for (g in d.filter) {
          !(e = V[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), f.push({
            value: c,
            type: g,
            matches: e
          }), h = h.slice(c.length));
        }

        if (!c) break;
      }

      return b ? h.length : h ? ga.error(a) : z(a, i).slice(0);
    };

    function sa(a) {
      for (var b = 0, c = a.length, d = ""; b < c; b++) {
        d += a[b].value;
      }

      return d;
    }

    function ta(a, b, c) {
      var d = b.dir,
          e = b.next,
          f = e || d,
          g = c && "parentNode" === f,
          h = x++;
      return b.first ? function (b, c, e) {
        while (b = b[d]) {
          if (1 === b.nodeType || g) return a(b, c, e);
        }

        return !1;
      } : function (b, c, i) {
        var j,
            k,
            l,
            m = [w, h];

        if (i) {
          while (b = b[d]) {
            if ((1 === b.nodeType || g) && a(b, c, i)) return !0;
          }
        } else while (b = b[d]) {
          if (1 === b.nodeType || g) if (l = b[u] || (b[u] = {}), k = l[b.uniqueID] || (l[b.uniqueID] = {}), e && e === b.nodeName.toLowerCase()) b = b[d] || b;else {
            if ((j = k[f]) && j[0] === w && j[1] === h) return m[2] = j[2];
            if (k[f] = m, m[2] = a(b, c, i)) return !0;
          }
        }

        return !1;
      };
    }

    function ua(a) {
      return a.length > 1 ? function (b, c, d) {
        var e = a.length;

        while (e--) {
          if (!a[e](b, c, d)) return !1;
        }

        return !0;
      } : a[0];
    }

    function va(a, b, c) {
      for (var d = 0, e = b.length; d < e; d++) {
        ga(a, b[d], c);
      }

      return c;
    }

    function wa(a, b, c, d, e) {
      for (var f, g = [], h = 0, i = a.length, j = null != b; h < i; h++) {
        (f = a[h]) && (c && !c(f, d, e) || (g.push(f), j && b.push(h)));
      }

      return g;
    }

    function xa(a, b, c, d, e, f) {
      return d && !d[u] && (d = xa(d)), e && !e[u] && (e = xa(e, f)), ia(function (f, g, h, i) {
        var j,
            k,
            l,
            m = [],
            n = [],
            o = g.length,
            p = f || va(b || "*", h.nodeType ? [h] : h, []),
            q = !a || !f && b ? p : wa(p, m, a, h, i),
            r = c ? e || (f ? a : o || d) ? [] : g : q;

        if (c && c(q, r, h, i), d) {
          j = wa(r, n), d(j, [], h, i), k = j.length;

          while (k--) {
            (l = j[k]) && (r[n[k]] = !(q[n[k]] = l));
          }
        }

        if (f) {
          if (e || a) {
            if (e) {
              j = [], k = r.length;

              while (k--) {
                (l = r[k]) && j.push(q[k] = l);
              }

              e(null, r = [], j, i);
            }

            k = r.length;

            while (k--) {
              (l = r[k]) && (j = e ? I(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l));
            }
          }
        } else r = wa(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : G.apply(g, r);
      });
    }

    function ya(a) {
      for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, k = ta(function (a) {
        return a === b;
      }, h, !0), l = ta(function (a) {
        return I(b, a) > -1;
      }, h, !0), m = [function (a, c, d) {
        var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
        return b = null, e;
      }]; i < f; i++) {
        if (c = d.relative[a[i].type]) m = [ta(ua(m), c)];else {
          if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) {
            for (e = ++i; e < f; e++) {
              if (d.relative[a[e].type]) break;
            }

            return xa(i > 1 && ua(m), i > 1 && sa(a.slice(0, i - 1).concat({
              value: " " === a[i - 2].type ? "*" : ""
            })).replace(P, "$1"), c, i < e && ya(a.slice(i, e)), e < f && ya(a = a.slice(e)), e < f && sa(a));
          }

          m.push(c);
        }
      }

      return ua(m);
    }

    function za(a, b) {
      var c = b.length > 0,
          e = a.length > 0,
          f = function f(_f, g, h, i, k) {
        var l,
            o,
            q,
            r = 0,
            s = "0",
            t = _f && [],
            u = [],
            v = j,
            x = _f || e && d.find.TAG("*", k),
            y = w += null == v ? 1 : Math.random() || .1,
            z = x.length;

        for (k && (j = g === n || g || k); s !== z && null != (l = x[s]); s++) {
          if (e && l) {
            o = 0, g || l.ownerDocument === n || (m(l), h = !p);

            while (q = a[o++]) {
              if (q(l, g || n, h)) {
                i.push(l);
                break;
              }
            }

            k && (w = y);
          }

          c && ((l = !q && l) && r--, _f && t.push(l));
        }

        if (r += s, c && s !== r) {
          o = 0;

          while (q = b[o++]) {
            q(t, u, g, h);
          }

          if (_f) {
            if (r > 0) while (s--) {
              t[s] || u[s] || (u[s] = E.call(i));
            }
            u = wa(u);
          }

          G.apply(i, u), k && !_f && u.length > 0 && r + b.length > 1 && ga.uniqueSort(i);
        }

        return k && (w = y, j = v), t;
      };

      return c ? ia(f) : f;
    }

    return h = ga.compile = function (a, b) {
      var c,
          d = [],
          e = [],
          f = A[a + " "];

      if (!f) {
        b || (b = g(a)), c = b.length;

        while (c--) {
          f = ya(b[c]), f[u] ? d.push(f) : e.push(f);
        }

        f = A(a, za(e, d)), f.selector = a;
      }

      return f;
    }, i = ga.select = function (a, b, c, e) {
      var f,
          i,
          j,
          k,
          l,
          m = "function" == typeof a && a,
          n = !e && g(a = m.selector || a);

      if (c = c || [], 1 === n.length) {
        if (i = n[0] = n[0].slice(0), i.length > 2 && "ID" === (j = i[0]).type && 9 === b.nodeType && p && d.relative[i[1].type]) {
          if (b = (d.find.ID(j.matches[0].replace(_, aa), b) || [])[0], !b) return c;
          m && (b = b.parentNode), a = a.slice(i.shift().value.length);
        }

        f = V.needsContext.test(a) ? 0 : i.length;

        while (f--) {
          if (j = i[f], d.relative[k = j.type]) break;

          if ((l = d.find[k]) && (e = l(j.matches[0].replace(_, aa), $.test(i[0].type) && qa(b.parentNode) || b))) {
            if (i.splice(f, 1), a = e.length && sa(i), !a) return G.apply(c, e), c;
            break;
          }
        }
      }

      return (m || h(a, n))(e, b, !p, c, !b || $.test(a) && qa(b.parentNode) || b), c;
    }, c.sortStable = u.split("").sort(B).join("") === u, c.detectDuplicates = !!l, m(), c.sortDetached = ja(function (a) {
      return 1 & a.compareDocumentPosition(n.createElement("fieldset"));
    }), ja(function (a) {
      return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href");
    }) || ka("type|href|height|width", function (a, b, c) {
      if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2);
    }), c.attributes && ja(function (a) {
      return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value");
    }) || ka("value", function (a, b, c) {
      if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue;
    }), ja(function (a) {
      return null == a.getAttribute("disabled");
    }) || ka(J, function (a, b, c) {
      var d;
      if (!c) return a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null;
    }), ga;
  }(a);

  r.find = x, r.expr = x.selectors, r.expr[":"] = r.expr.pseudos, r.uniqueSort = r.unique = x.uniqueSort, r.text = x.getText, r.isXMLDoc = x.isXML, r.contains = x.contains, r.escapeSelector = x.escape;

  var y = function y(a, b, c) {
    var d = [],
        e = void 0 !== c;

    while ((a = a[b]) && 9 !== a.nodeType) {
      if (1 === a.nodeType) {
        if (e && r(a).is(c)) break;
        d.push(a);
      }
    }

    return d;
  },
      z = function z(a, b) {
    for (var c = []; a; a = a.nextSibling) {
      1 === a.nodeType && a !== b && c.push(a);
    }

    return c;
  },
      A = r.expr.match.needsContext,
      B = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
      C = /^.[^:#\[\.,]*$/;

  function D(a, b, c) {
    return r.isFunction(b) ? r.grep(a, function (a, d) {
      return !!b.call(a, d, a) !== c;
    }) : b.nodeType ? r.grep(a, function (a) {
      return a === b !== c;
    }) : "string" != typeof b ? r.grep(a, function (a) {
      return i.call(b, a) > -1 !== c;
    }) : C.test(b) ? r.filter(b, a, c) : (b = r.filter(b, a), r.grep(a, function (a) {
      return i.call(b, a) > -1 !== c && 1 === a.nodeType;
    }));
  }

  r.filter = function (a, b, c) {
    var d = b[0];
    return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? r.find.matchesSelector(d, a) ? [d] : [] : r.find.matches(a, r.grep(b, function (a) {
      return 1 === a.nodeType;
    }));
  }, r.fn.extend({
    find: function find(a) {
      var b,
          c,
          d = this.length,
          e = this;
      if ("string" != typeof a) return this.pushStack(r(a).filter(function () {
        for (b = 0; b < d; b++) {
          if (r.contains(e[b], this)) return !0;
        }
      }));

      for (c = this.pushStack([]), b = 0; b < d; b++) {
        r.find(a, e[b], c);
      }

      return d > 1 ? r.uniqueSort(c) : c;
    },
    filter: function filter(a) {
      return this.pushStack(D(this, a || [], !1));
    },
    not: function not(a) {
      return this.pushStack(D(this, a || [], !0));
    },
    is: function is(a) {
      return !!D(this, "string" == typeof a && A.test(a) ? r(a) : a || [], !1).length;
    }
  });

  var E,
      F = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
      G = r.fn.init = function (a, b, c) {
    var e, f;
    if (!a) return this;

    if (c = c || E, "string" == typeof a) {
      if (e = "<" === a[0] && ">" === a[a.length - 1] && a.length >= 3 ? [null, a, null] : F.exec(a), !e || !e[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);

      if (e[1]) {
        if (b = b instanceof r ? b[0] : b, r.merge(this, r.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)), B.test(e[1]) && r.isPlainObject(b)) for (e in b) {
          r.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);
        }
        return this;
      }

      return f = d.getElementById(e[2]), f && (this[0] = f, this.length = 1), this;
    }

    return a.nodeType ? (this[0] = a, this.length = 1, this) : r.isFunction(a) ? void 0 !== c.ready ? c.ready(a) : a(r) : r.makeArray(a, this);
  };

  G.prototype = r.fn, E = r(d);
  var H = /^(?:parents|prev(?:Until|All))/,
      I = {
    children: !0,
    contents: !0,
    next: !0,
    prev: !0
  };
  r.fn.extend({
    has: function has(a) {
      var b = r(a, this),
          c = b.length;
      return this.filter(function () {
        for (var a = 0; a < c; a++) {
          if (r.contains(this, b[a])) return !0;
        }
      });
    },
    closest: function closest(a, b) {
      var c,
          d = 0,
          e = this.length,
          f = [],
          g = "string" != typeof a && r(a);
      if (!A.test(a)) for (; d < e; d++) {
        for (c = this[d]; c && c !== b; c = c.parentNode) {
          if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && r.find.matchesSelector(c, a))) {
            f.push(c);
            break;
          }
        }
      }
      return this.pushStack(f.length > 1 ? r.uniqueSort(f) : f);
    },
    index: function index(a) {
      return a ? "string" == typeof a ? i.call(r(a), this[0]) : i.call(this, a.jquery ? a[0] : a) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
    },
    add: function add(a, b) {
      return this.pushStack(r.uniqueSort(r.merge(this.get(), r(a, b))));
    },
    addBack: function addBack(a) {
      return this.add(null == a ? this.prevObject : this.prevObject.filter(a));
    }
  });

  function J(a, b) {
    while ((a = a[b]) && 1 !== a.nodeType) {
      ;
    }

    return a;
  }

  r.each({
    parent: function parent(a) {
      var b = a.parentNode;
      return b && 11 !== b.nodeType ? b : null;
    },
    parents: function parents(a) {
      return y(a, "parentNode");
    },
    parentsUntil: function parentsUntil(a, b, c) {
      return y(a, "parentNode", c);
    },
    next: function next(a) {
      return J(a, "nextSibling");
    },
    prev: function prev(a) {
      return J(a, "previousSibling");
    },
    nextAll: function nextAll(a) {
      return y(a, "nextSibling");
    },
    prevAll: function prevAll(a) {
      return y(a, "previousSibling");
    },
    nextUntil: function nextUntil(a, b, c) {
      return y(a, "nextSibling", c);
    },
    prevUntil: function prevUntil(a, b, c) {
      return y(a, "previousSibling", c);
    },
    siblings: function siblings(a) {
      return z((a.parentNode || {}).firstChild, a);
    },
    children: function children(a) {
      return z(a.firstChild);
    },
    contents: function contents(a) {
      return a.contentDocument || r.merge([], a.childNodes);
    }
  }, function (a, b) {
    r.fn[a] = function (c, d) {
      var e = r.map(this, b, c);
      return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = r.filter(d, e)), this.length > 1 && (I[a] || r.uniqueSort(e), H.test(a) && e.reverse()), this.pushStack(e);
    };
  });
  var K = /[^\x20\t\r\n\f]+/g;

  function L(a) {
    var b = {};
    return r.each(a.match(K) || [], function (a, c) {
      b[c] = !0;
    }), b;
  }

  r.Callbacks = function (a) {
    a = "string" == typeof a ? L(a) : r.extend({}, a);

    var b,
        c,
        d,
        e,
        f = [],
        g = [],
        h = -1,
        i = function i() {
      for (e = a.once, d = b = !0; g.length; h = -1) {
        c = g.shift();

        while (++h < f.length) {
          f[h].apply(c[0], c[1]) === !1 && a.stopOnFalse && (h = f.length, c = !1);
        }
      }

      a.memory || (c = !1), b = !1, e && (f = c ? [] : "");
    },
        j = {
      add: function add() {
        return f && (c && !b && (h = f.length - 1, g.push(c)), function d(b) {
          r.each(b, function (b, c) {
            r.isFunction(c) ? a.unique && j.has(c) || f.push(c) : c && c.length && "string" !== r.type(c) && d(c);
          });
        }(arguments), c && !b && i()), this;
      },
      remove: function remove() {
        return r.each(arguments, function (a, b) {
          var c;

          while ((c = r.inArray(b, f, c)) > -1) {
            f.splice(c, 1), c <= h && h--;
          }
        }), this;
      },
      has: function has(a) {
        return a ? r.inArray(a, f) > -1 : f.length > 0;
      },
      empty: function empty() {
        return f && (f = []), this;
      },
      disable: function disable() {
        return e = g = [], f = c = "", this;
      },
      disabled: function disabled() {
        return !f;
      },
      lock: function lock() {
        return e = g = [], c || b || (f = c = ""), this;
      },
      locked: function locked() {
        return !!e;
      },
      fireWith: function fireWith(a, c) {
        return e || (c = c || [], c = [a, c.slice ? c.slice() : c], g.push(c), b || i()), this;
      },
      fire: function fire() {
        return j.fireWith(this, arguments), this;
      },
      fired: function fired() {
        return !!d;
      }
    };

    return j;
  };

  function M(a) {
    return a;
  }

  function N(a) {
    throw a;
  }

  function O(a, b, c) {
    var d;

    try {
      a && r.isFunction(d = a.promise) ? d.call(a).done(b).fail(c) : a && r.isFunction(d = a.then) ? d.call(a, b, c) : b.call(void 0, a);
    } catch (a) {
      c.call(void 0, a);
    }
  }

  r.extend({
    Deferred: function Deferred(b) {
      var c = [["notify", "progress", r.Callbacks("memory"), r.Callbacks("memory"), 2], ["resolve", "done", r.Callbacks("once memory"), r.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", r.Callbacks("once memory"), r.Callbacks("once memory"), 1, "rejected"]],
          d = "pending",
          e = {
        state: function state() {
          return d;
        },
        always: function always() {
          return f.done(arguments).fail(arguments), this;
        },
        "catch": function _catch(a) {
          return e.then(null, a);
        },
        pipe: function pipe() {
          var a = arguments;
          return r.Deferred(function (b) {
            r.each(c, function (c, d) {
              var e = r.isFunction(a[d[4]]) && a[d[4]];
              f[d[1]](function () {
                var a = e && e.apply(this, arguments);
                a && r.isFunction(a.promise) ? a.promise().progress(b.notify).done(b.resolve).fail(b.reject) : b[d[0] + "With"](this, e ? [a] : arguments);
              });
            }), a = null;
          }).promise();
        },
        then: function then(b, d, e) {
          var f = 0;

          function g(b, c, d, e) {
            return function () {
              var h = this,
                  i = arguments,
                  j = function j() {
                var a, j;

                if (!(b < f)) {
                  if (a = d.apply(h, i), a === c.promise()) throw new TypeError("Thenable self-resolution");
                  j = a && ("object" == _typeof(a) || "function" == typeof a) && a.then, r.isFunction(j) ? e ? j.call(a, g(f, c, M, e), g(f, c, N, e)) : (f++, j.call(a, g(f, c, M, e), g(f, c, N, e), g(f, c, M, c.notifyWith))) : (d !== M && (h = void 0, i = [a]), (e || c.resolveWith)(h, i));
                }
              },
                  k = e ? j : function () {
                try {
                  j();
                } catch (a) {
                  r.Deferred.exceptionHook && r.Deferred.exceptionHook(a, k.stackTrace), b + 1 >= f && (d !== N && (h = void 0, i = [a]), c.rejectWith(h, i));
                }
              };

              b ? k() : (r.Deferred.getStackHook && (k.stackTrace = r.Deferred.getStackHook()), a.setTimeout(k));
            };
          }

          return r.Deferred(function (a) {
            c[0][3].add(g(0, a, r.isFunction(e) ? e : M, a.notifyWith)), c[1][3].add(g(0, a, r.isFunction(b) ? b : M)), c[2][3].add(g(0, a, r.isFunction(d) ? d : N));
          }).promise();
        },
        promise: function promise(a) {
          return null != a ? r.extend(a, e) : e;
        }
      },
          f = {};
      return r.each(c, function (a, b) {
        var g = b[2],
            h = b[5];
        e[b[1]] = g.add, h && g.add(function () {
          d = h;
        }, c[3 - a][2].disable, c[0][2].lock), g.add(b[3].fire), f[b[0]] = function () {
          return f[b[0] + "With"](this === f ? void 0 : this, arguments), this;
        }, f[b[0] + "With"] = g.fireWith;
      }), e.promise(f), b && b.call(f, f), f;
    },
    when: function when(a) {
      var b = arguments.length,
          c = b,
          d = Array(c),
          e = f.call(arguments),
          g = r.Deferred(),
          h = function h(a) {
        return function (c) {
          d[a] = this, e[a] = arguments.length > 1 ? f.call(arguments) : c, --b || g.resolveWith(d, e);
        };
      };

      if (b <= 1 && (O(a, g.done(h(c)).resolve, g.reject), "pending" === g.state() || r.isFunction(e[c] && e[c].then))) return g.then();

      while (c--) {
        O(e[c], h(c), g.reject);
      }

      return g.promise();
    }
  });
  var P = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
  r.Deferred.exceptionHook = function (b, c) {
    a.console && a.console.warn && b && P.test(b.name) && a.console.warn("jQuery.Deferred exception: " + b.message, b.stack, c);
  }, r.readyException = function (b) {
    a.setTimeout(function () {
      throw b;
    });
  };
  var Q = r.Deferred();
  r.fn.ready = function (a) {
    return Q.then(a)["catch"](function (a) {
      r.readyException(a);
    }), this;
  }, r.extend({
    isReady: !1,
    readyWait: 1,
    holdReady: function holdReady(a) {
      a ? r.readyWait++ : r.ready(!0);
    },
    ready: function ready(a) {
      (a === !0 ? --r.readyWait : r.isReady) || (r.isReady = !0, a !== !0 && --r.readyWait > 0 || Q.resolveWith(d, [r]));
    }
  }), r.ready.then = Q.then;

  function R() {
    d.removeEventListener("DOMContentLoaded", R), a.removeEventListener("load", R), r.ready();
  }

  "complete" === d.readyState || "loading" !== d.readyState && !d.documentElement.doScroll ? a.setTimeout(r.ready) : (d.addEventListener("DOMContentLoaded", R), a.addEventListener("load", R));

  var S = function S(a, b, c, d, e, f, g) {
    var h = 0,
        i = a.length,
        j = null == c;

    if ("object" === r.type(c)) {
      e = !0;

      for (h in c) {
        S(a, b, h, c[h], !0, f, g);
      }
    } else if (void 0 !== d && (e = !0, r.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function b(a, _b, c) {
      return j.call(r(a), c);
    })), b)) for (; h < i; h++) {
      b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
    }

    return e ? a : j ? b.call(a) : i ? b(a[0], c) : f;
  },
      T = function T(a) {
    return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType;
  };

  function U() {
    this.expando = r.expando + U.uid++;
  }

  U.uid = 1, U.prototype = {
    cache: function cache(a) {
      var b = a[this.expando];
      return b || (b = {}, T(a) && (a.nodeType ? a[this.expando] = b : Object.defineProperty(a, this.expando, {
        value: b,
        configurable: !0
      }))), b;
    },
    set: function set(a, b, c) {
      var d,
          e = this.cache(a);
      if ("string" == typeof b) e[r.camelCase(b)] = c;else for (d in b) {
        e[r.camelCase(d)] = b[d];
      }
      return e;
    },
    get: function get(a, b) {
      return void 0 === b ? this.cache(a) : a[this.expando] && a[this.expando][r.camelCase(b)];
    },
    access: function access(a, b, c) {
      return void 0 === b || b && "string" == typeof b && void 0 === c ? this.get(a, b) : (this.set(a, b, c), void 0 !== c ? c : b);
    },
    remove: function remove(a, b) {
      var c,
          d = a[this.expando];

      if (void 0 !== d) {
        if (void 0 !== b) {
          r.isArray(b) ? b = b.map(r.camelCase) : (b = r.camelCase(b), b = b in d ? [b] : b.match(K) || []), c = b.length;

          while (c--) {
            delete d[b[c]];
          }
        }

        (void 0 === b || r.isEmptyObject(d)) && (a.nodeType ? a[this.expando] = void 0 : delete a[this.expando]);
      }
    },
    hasData: function hasData(a) {
      var b = a[this.expando];
      return void 0 !== b && !r.isEmptyObject(b);
    }
  };
  var V = new U(),
      W = new U(),
      X = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
      Y = /[A-Z]/g;

  function Z(a) {
    return "true" === a || "false" !== a && ("null" === a ? null : a === +a + "" ? +a : X.test(a) ? JSON.parse(a) : a);
  }

  function $(a, b, c) {
    var d;
    if (void 0 === c && 1 === a.nodeType) if (d = "data-" + b.replace(Y, "-$&").toLowerCase(), c = a.getAttribute(d), "string" == typeof c) {
      try {
        c = Z(c);
      } catch (e) {}

      W.set(a, b, c);
    } else c = void 0;
    return c;
  }

  r.extend({
    hasData: function hasData(a) {
      return W.hasData(a) || V.hasData(a);
    },
    data: function data(a, b, c) {
      return W.access(a, b, c);
    },
    removeData: function removeData(a, b) {
      W.remove(a, b);
    },
    _data: function _data(a, b, c) {
      return V.access(a, b, c);
    },
    _removeData: function _removeData(a, b) {
      V.remove(a, b);
    }
  }), r.fn.extend({
    data: function data(a, b) {
      var c,
          d,
          e,
          f = this[0],
          g = f && f.attributes;

      if (void 0 === a) {
        if (this.length && (e = W.get(f), 1 === f.nodeType && !V.get(f, "hasDataAttrs"))) {
          c = g.length;

          while (c--) {
            g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = r.camelCase(d.slice(5)), $(f, d, e[d])));
          }

          V.set(f, "hasDataAttrs", !0);
        }

        return e;
      }

      return "object" == _typeof(a) ? this.each(function () {
        W.set(this, a);
      }) : S(this, function (b) {
        var c;

        if (f && void 0 === b) {
          if (c = W.get(f, a), void 0 !== c) return c;
          if (c = $(f, a), void 0 !== c) return c;
        } else this.each(function () {
          W.set(this, a, b);
        });
      }, null, b, arguments.length > 1, null, !0);
    },
    removeData: function removeData(a) {
      return this.each(function () {
        W.remove(this, a);
      });
    }
  }), r.extend({
    queue: function queue(a, b, c) {
      var d;
      if (a) return b = (b || "fx") + "queue", d = V.get(a, b), c && (!d || r.isArray(c) ? d = V.access(a, b, r.makeArray(c)) : d.push(c)), d || [];
    },
    dequeue: function dequeue(a, b) {
      b = b || "fx";

      var c = r.queue(a, b),
          d = c.length,
          e = c.shift(),
          f = r._queueHooks(a, b),
          g = function g() {
        r.dequeue(a, b);
      };

      "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire();
    },
    _queueHooks: function _queueHooks(a, b) {
      var c = b + "queueHooks";
      return V.get(a, c) || V.access(a, c, {
        empty: r.Callbacks("once memory").add(function () {
          V.remove(a, [b + "queue", c]);
        })
      });
    }
  }), r.fn.extend({
    queue: function queue(a, b) {
      var c = 2;
      return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? r.queue(this[0], a) : void 0 === b ? this : this.each(function () {
        var c = r.queue(this, a, b);
        r._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && r.dequeue(this, a);
      });
    },
    dequeue: function dequeue(a) {
      return this.each(function () {
        r.dequeue(this, a);
      });
    },
    clearQueue: function clearQueue(a) {
      return this.queue(a || "fx", []);
    },
    promise: function promise(a, b) {
      var c,
          d = 1,
          e = r.Deferred(),
          f = this,
          g = this.length,
          h = function h() {
        --d || e.resolveWith(f, [f]);
      };

      "string" != typeof a && (b = a, a = void 0), a = a || "fx";

      while (g--) {
        c = V.get(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
      }

      return h(), e.promise(b);
    }
  });

  var _ = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
      aa = new RegExp("^(?:([+-])=|)(" + _ + ")([a-z%]*)$", "i"),
      ba = ["Top", "Right", "Bottom", "Left"],
      ca = function ca(a, b) {
    return a = b || a, "none" === a.style.display || "" === a.style.display && r.contains(a.ownerDocument, a) && "none" === r.css(a, "display");
  },
      da = function da(a, b, c, d) {
    var e,
        f,
        g = {};

    for (f in b) {
      g[f] = a.style[f], a.style[f] = b[f];
    }

    e = c.apply(a, d || []);

    for (f in b) {
      a.style[f] = g[f];
    }

    return e;
  };

  function ea(a, b, c, d) {
    var e,
        f = 1,
        g = 20,
        h = d ? function () {
      return d.cur();
    } : function () {
      return r.css(a, b, "");
    },
        i = h(),
        j = c && c[3] || (r.cssNumber[b] ? "" : "px"),
        k = (r.cssNumber[b] || "px" !== j && +i) && aa.exec(r.css(a, b));

    if (k && k[3] !== j) {
      j = j || k[3], c = c || [], k = +i || 1;

      do {
        f = f || ".5", k /= f, r.style(a, b, k + j);
      } while (f !== (f = h() / i) && 1 !== f && --g);
    }

    return c && (k = +k || +i || 0, e = c[1] ? k + (c[1] + 1) * c[2] : +c[2], d && (d.unit = j, d.start = k, d.end = e)), e;
  }

  var fa = {};

  function ga(a) {
    var b,
        c = a.ownerDocument,
        d = a.nodeName,
        e = fa[d];
    return e ? e : (b = c.body.appendChild(c.createElement(d)), e = r.css(b, "display"), b.parentNode.removeChild(b), "none" === e && (e = "block"), fa[d] = e, e);
  }

  function ha(a, b) {
    for (var c, d, e = [], f = 0, g = a.length; f < g; f++) {
      d = a[f], d.style && (c = d.style.display, b ? ("none" === c && (e[f] = V.get(d, "display") || null, e[f] || (d.style.display = "")), "" === d.style.display && ca(d) && (e[f] = ga(d))) : "none" !== c && (e[f] = "none", V.set(d, "display", c)));
    }

    for (f = 0; f < g; f++) {
      null != e[f] && (a[f].style.display = e[f]);
    }

    return a;
  }

  r.fn.extend({
    show: function show() {
      return ha(this, !0);
    },
    hide: function hide() {
      return ha(this);
    },
    toggle: function toggle(a) {
      return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function () {
        ca(this) ? r(this).show() : r(this).hide();
      });
    }
  });
  var ia = /^(?:checkbox|radio)$/i,
      ja = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
      ka = /^$|\/(?:java|ecma)script/i,
      la = {
    option: [1, "<select multiple='multiple'>", "</select>"],
    thead: [1, "<table>", "</table>"],
    col: [2, "<table><colgroup>", "</colgroup></table>"],
    tr: [2, "<table><tbody>", "</tbody></table>"],
    td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
    _default: [0, "", ""]
  };
  la.optgroup = la.option, la.tbody = la.tfoot = la.colgroup = la.caption = la.thead, la.th = la.td;

  function ma(a, b) {
    var c;
    return c = "undefined" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || "*") : "undefined" != typeof a.querySelectorAll ? a.querySelectorAll(b || "*") : [], void 0 === b || b && r.nodeName(a, b) ? r.merge([a], c) : c;
  }

  function na(a, b) {
    for (var c = 0, d = a.length; c < d; c++) {
      V.set(a[c], "globalEval", !b || V.get(b[c], "globalEval"));
    }
  }

  var oa = /<|&#?\w+;/;

  function pa(a, b, c, d, e) {
    for (var f, g, h, i, j, k, l = b.createDocumentFragment(), m = [], n = 0, o = a.length; n < o; n++) {
      if (f = a[n], f || 0 === f) if ("object" === r.type(f)) r.merge(m, f.nodeType ? [f] : f);else if (oa.test(f)) {
        g = g || l.appendChild(b.createElement("div")), h = (ja.exec(f) || ["", ""])[1].toLowerCase(), i = la[h] || la._default, g.innerHTML = i[1] + r.htmlPrefilter(f) + i[2], k = i[0];

        while (k--) {
          g = g.lastChild;
        }

        r.merge(m, g.childNodes), g = l.firstChild, g.textContent = "";
      } else m.push(b.createTextNode(f));
    }

    l.textContent = "", n = 0;

    while (f = m[n++]) {
      if (d && r.inArray(f, d) > -1) e && e.push(f);else if (j = r.contains(f.ownerDocument, f), g = ma(l.appendChild(f), "script"), j && na(g), c) {
        k = 0;

        while (f = g[k++]) {
          ka.test(f.type || "") && c.push(f);
        }
      }
    }

    return l;
  }

  !function () {
    var a = d.createDocumentFragment(),
        b = a.appendChild(d.createElement("div")),
        c = d.createElement("input");
    c.setAttribute("type", "radio"), c.setAttribute("checked", "checked"), c.setAttribute("name", "t"), b.appendChild(c), o.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, b.innerHTML = "<textarea>x</textarea>", o.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue;
  }();
  var qa = d.documentElement,
      ra = /^key/,
      sa = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
      ta = /^([^.]*)(?:\.(.+)|)/;

  function ua() {
    return !0;
  }

  function va() {
    return !1;
  }

  function wa() {
    try {
      return d.activeElement;
    } catch (a) {}
  }

  function xa(a, b, c, d, e, f) {
    var g, h;

    if ("object" == _typeof(b)) {
      "string" != typeof c && (d = d || c, c = void 0);

      for (h in b) {
        xa(a, h, c, d, b[h], f);
      }

      return a;
    }

    if (null == d && null == e ? (e = c, d = c = void 0) : null == e && ("string" == typeof c ? (e = d, d = void 0) : (e = d, d = c, c = void 0)), e === !1) e = va;else if (!e) return a;
    return 1 === f && (g = e, e = function e(a) {
      return r().off(a), g.apply(this, arguments);
    }, e.guid = g.guid || (g.guid = r.guid++)), a.each(function () {
      r.event.add(this, b, e, d, c);
    });
  }

  r.event = {
    global: {},
    add: function add(a, b, c, d, e) {
      var f,
          g,
          h,
          i,
          j,
          k,
          l,
          m,
          n,
          o,
          p,
          q = V.get(a);

      if (q) {
        c.handler && (f = c, c = f.handler, e = f.selector), e && r.find.matchesSelector(qa, e), c.guid || (c.guid = r.guid++), (i = q.events) || (i = q.events = {}), (g = q.handle) || (g = q.handle = function (b) {
          return "undefined" != typeof r && r.event.triggered !== b.type ? r.event.dispatch.apply(a, arguments) : void 0;
        }), b = (b || "").match(K) || [""], j = b.length;

        while (j--) {
          h = ta.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n && (l = r.event.special[n] || {}, n = (e ? l.delegateType : l.bindType) || n, l = r.event.special[n] || {}, k = r.extend({
            type: n,
            origType: p,
            data: d,
            handler: c,
            guid: c.guid,
            selector: e,
            needsContext: e && r.expr.match.needsContext.test(e),
            namespace: o.join(".")
          }, f), (m = i[n]) || (m = i[n] = [], m.delegateCount = 0, l.setup && l.setup.call(a, d, o, g) !== !1 || a.addEventListener && a.addEventListener(n, g)), l.add && (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, k) : m.push(k), r.event.global[n] = !0);
        }
      }
    },
    remove: function remove(a, b, c, d, e) {
      var f,
          g,
          h,
          i,
          j,
          k,
          l,
          m,
          n,
          o,
          p,
          q = V.hasData(a) && V.get(a);

      if (q && (i = q.events)) {
        b = (b || "").match(K) || [""], j = b.length;

        while (j--) {
          if (h = ta.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n) {
            l = r.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = i[n] || [], h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), g = f = m.length;

            while (f--) {
              k = m[f], !e && p !== k.origType || c && c.guid !== k.guid || h && !h.test(k.namespace) || d && d !== k.selector && ("**" !== d || !k.selector) || (m.splice(f, 1), k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));
            }

            g && !m.length && (l.teardown && l.teardown.call(a, o, q.handle) !== !1 || r.removeEvent(a, n, q.handle), delete i[n]);
          } else for (n in i) {
            r.event.remove(a, n + b[j], c, d, !0);
          }
        }

        r.isEmptyObject(i) && V.remove(a, "handle events");
      }
    },
    dispatch: function dispatch(a) {
      var b = r.event.fix(a),
          c,
          d,
          e,
          f,
          g,
          h,
          i = new Array(arguments.length),
          j = (V.get(this, "events") || {})[b.type] || [],
          k = r.event.special[b.type] || {};

      for (i[0] = b, c = 1; c < arguments.length; c++) {
        i[c] = arguments[c];
      }

      if (b.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, b) !== !1) {
        h = r.event.handlers.call(this, b, j), c = 0;

        while ((f = h[c++]) && !b.isPropagationStopped()) {
          b.currentTarget = f.elem, d = 0;

          while ((g = f.handlers[d++]) && !b.isImmediatePropagationStopped()) {
            b.rnamespace && !b.rnamespace.test(g.namespace) || (b.handleObj = g, b.data = g.data, e = ((r.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i), void 0 !== e && (b.result = e) === !1 && (b.preventDefault(), b.stopPropagation()));
          }
        }

        return k.postDispatch && k.postDispatch.call(this, b), b.result;
      }
    },
    handlers: function handlers(a, b) {
      var c,
          d,
          e,
          f,
          g,
          h = [],
          i = b.delegateCount,
          j = a.target;
      if (i && j.nodeType && !("click" === a.type && a.button >= 1)) for (; j !== this; j = j.parentNode || this) {
        if (1 === j.nodeType && ("click" !== a.type || j.disabled !== !0)) {
          for (f = [], g = {}, c = 0; c < i; c++) {
            d = b[c], e = d.selector + " ", void 0 === g[e] && (g[e] = d.needsContext ? r(e, this).index(j) > -1 : r.find(e, this, null, [j]).length), g[e] && f.push(d);
          }

          f.length && h.push({
            elem: j,
            handlers: f
          });
        }
      }
      return j = this, i < b.length && h.push({
        elem: j,
        handlers: b.slice(i)
      }), h;
    },
    addProp: function addProp(a, b) {
      Object.defineProperty(r.Event.prototype, a, {
        enumerable: !0,
        configurable: !0,
        get: r.isFunction(b) ? function () {
          if (this.originalEvent) return b(this.originalEvent);
        } : function () {
          if (this.originalEvent) return this.originalEvent[a];
        },
        set: function set(b) {
          Object.defineProperty(this, a, {
            enumerable: !0,
            configurable: !0,
            writable: !0,
            value: b
          });
        }
      });
    },
    fix: function fix(a) {
      return a[r.expando] ? a : new r.Event(a);
    },
    special: {
      load: {
        noBubble: !0
      },
      focus: {
        trigger: function trigger() {
          if (this !== wa() && this.focus) return this.focus(), !1;
        },
        delegateType: "focusin"
      },
      blur: {
        trigger: function trigger() {
          if (this === wa() && this.blur) return this.blur(), !1;
        },
        delegateType: "focusout"
      },
      click: {
        trigger: function trigger() {
          if ("checkbox" === this.type && this.click && r.nodeName(this, "input")) return this.click(), !1;
        },
        _default: function _default(a) {
          return r.nodeName(a.target, "a");
        }
      },
      beforeunload: {
        postDispatch: function postDispatch(a) {
          void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result);
        }
      }
    }
  }, r.removeEvent = function (a, b, c) {
    a.removeEventListener && a.removeEventListener(b, c);
  }, r.Event = function (a, b) {
    return this instanceof r.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? ua : va, this.target = a.target && 3 === a.target.nodeType ? a.target.parentNode : a.target, this.currentTarget = a.currentTarget, this.relatedTarget = a.relatedTarget) : this.type = a, b && r.extend(this, b), this.timeStamp = a && a.timeStamp || r.now(), void (this[r.expando] = !0)) : new r.Event(a, b);
  }, r.Event.prototype = {
    constructor: r.Event,
    isDefaultPrevented: va,
    isPropagationStopped: va,
    isImmediatePropagationStopped: va,
    isSimulated: !1,
    preventDefault: function preventDefault() {
      var a = this.originalEvent;
      this.isDefaultPrevented = ua, a && !this.isSimulated && a.preventDefault();
    },
    stopPropagation: function stopPropagation() {
      var a = this.originalEvent;
      this.isPropagationStopped = ua, a && !this.isSimulated && a.stopPropagation();
    },
    stopImmediatePropagation: function stopImmediatePropagation() {
      var a = this.originalEvent;
      this.isImmediatePropagationStopped = ua, a && !this.isSimulated && a.stopImmediatePropagation(), this.stopPropagation();
    }
  }, r.each({
    altKey: !0,
    bubbles: !0,
    cancelable: !0,
    changedTouches: !0,
    ctrlKey: !0,
    detail: !0,
    eventPhase: !0,
    metaKey: !0,
    pageX: !0,
    pageY: !0,
    shiftKey: !0,
    view: !0,
    "char": !0,
    charCode: !0,
    key: !0,
    keyCode: !0,
    button: !0,
    buttons: !0,
    clientX: !0,
    clientY: !0,
    offsetX: !0,
    offsetY: !0,
    pointerId: !0,
    pointerType: !0,
    screenX: !0,
    screenY: !0,
    targetTouches: !0,
    toElement: !0,
    touches: !0,
    which: function which(a) {
      var b = a.button;
      return null == a.which && ra.test(a.type) ? null != a.charCode ? a.charCode : a.keyCode : !a.which && void 0 !== b && sa.test(a.type) ? 1 & b ? 1 : 2 & b ? 3 : 4 & b ? 2 : 0 : a.which;
    }
  }, r.event.addProp), r.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout",
    pointerenter: "pointerover",
    pointerleave: "pointerout"
  }, function (a, b) {
    r.event.special[a] = {
      delegateType: b,
      bindType: b,
      handle: function handle(a) {
        var c,
            d = this,
            e = a.relatedTarget,
            f = a.handleObj;
        return e && (e === d || r.contains(d, e)) || (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c;
      }
    };
  }), r.fn.extend({
    on: function on(a, b, c, d) {
      return xa(this, a, b, c, d);
    },
    one: function one(a, b, c, d) {
      return xa(this, a, b, c, d, 1);
    },
    off: function off(a, b, c) {
      var d, e;
      if (a && a.preventDefault && a.handleObj) return d = a.handleObj, r(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;

      if ("object" == _typeof(a)) {
        for (e in a) {
          this.off(e, b, a[e]);
        }

        return this;
      }

      return b !== !1 && "function" != typeof b || (c = b, b = void 0), c === !1 && (c = va), this.each(function () {
        r.event.remove(this, a, c, b);
      });
    }
  });
  var ya = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
      za = /<script|<style|<link/i,
      Aa = /checked\s*(?:[^=]|=\s*.checked.)/i,
      Ba = /^true\/(.*)/,
      Ca = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

  function Da(a, b) {
    return r.nodeName(a, "table") && r.nodeName(11 !== b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a : a;
  }

  function Ea(a) {
    return a.type = (null !== a.getAttribute("type")) + "/" + a.type, a;
  }

  function Fa(a) {
    var b = Ba.exec(a.type);
    return b ? a.type = b[1] : a.removeAttribute("type"), a;
  }

  function Ga(a, b) {
    var c, d, e, f, g, h, i, j;

    if (1 === b.nodeType) {
      if (V.hasData(a) && (f = V.access(a), g = V.set(b, f), j = f.events)) {
        delete g.handle, g.events = {};

        for (e in j) {
          for (c = 0, d = j[e].length; c < d; c++) {
            r.event.add(b, e, j[e][c]);
          }
        }
      }

      W.hasData(a) && (h = W.access(a), i = r.extend({}, h), W.set(b, i));
    }
  }

  function Ha(a, b) {
    var c = b.nodeName.toLowerCase();
    "input" === c && ia.test(a.type) ? b.checked = a.checked : "input" !== c && "textarea" !== c || (b.defaultValue = a.defaultValue);
  }

  function Ia(a, b, c, d) {
    b = g.apply([], b);
    var e,
        f,
        h,
        i,
        j,
        k,
        l = 0,
        m = a.length,
        n = m - 1,
        q = b[0],
        s = r.isFunction(q);
    if (s || m > 1 && "string" == typeof q && !o.checkClone && Aa.test(q)) return a.each(function (e) {
      var f = a.eq(e);
      s && (b[0] = q.call(this, e, f.html())), Ia(f, b, c, d);
    });

    if (m && (e = pa(b, a[0].ownerDocument, !1, a, d), f = e.firstChild, 1 === e.childNodes.length && (e = f), f || d)) {
      for (h = r.map(ma(e, "script"), Ea), i = h.length; l < m; l++) {
        j = e, l !== n && (j = r.clone(j, !0, !0), i && r.merge(h, ma(j, "script"))), c.call(a[l], j, l);
      }

      if (i) for (k = h[h.length - 1].ownerDocument, r.map(h, Fa), l = 0; l < i; l++) {
        j = h[l], ka.test(j.type || "") && !V.access(j, "globalEval") && r.contains(k, j) && (j.src ? r._evalUrl && r._evalUrl(j.src) : p(j.textContent.replace(Ca, ""), k));
      }
    }

    return a;
  }

  function Ja(a, b, c) {
    for (var d, e = b ? r.filter(b, a) : a, f = 0; null != (d = e[f]); f++) {
      c || 1 !== d.nodeType || r.cleanData(ma(d)), d.parentNode && (c && r.contains(d.ownerDocument, d) && na(ma(d, "script")), d.parentNode.removeChild(d));
    }

    return a;
  }

  r.extend({
    htmlPrefilter: function htmlPrefilter(a) {
      return a.replace(ya, "<$1></$2>");
    },
    clone: function clone(a, b, c) {
      var d,
          e,
          f,
          g,
          h = a.cloneNode(!0),
          i = r.contains(a.ownerDocument, a);
      if (!(o.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || r.isXMLDoc(a))) for (g = ma(h), f = ma(a), d = 0, e = f.length; d < e; d++) {
        Ha(f[d], g[d]);
      }
      if (b) if (c) for (f = f || ma(a), g = g || ma(h), d = 0, e = f.length; d < e; d++) {
        Ga(f[d], g[d]);
      } else Ga(a, h);
      return g = ma(h, "script"), g.length > 0 && na(g, !i && ma(a, "script")), h;
    },
    cleanData: function cleanData(a) {
      for (var b, c, d, e = r.event.special, f = 0; void 0 !== (c = a[f]); f++) {
        if (T(c)) {
          if (b = c[V.expando]) {
            if (b.events) for (d in b.events) {
              e[d] ? r.event.remove(c, d) : r.removeEvent(c, d, b.handle);
            }
            c[V.expando] = void 0;
          }

          c[W.expando] && (c[W.expando] = void 0);
        }
      }
    }
  }), r.fn.extend({
    detach: function detach(a) {
      return Ja(this, a, !0);
    },
    remove: function remove(a) {
      return Ja(this, a);
    },
    text: function text(a) {
      return S(this, function (a) {
        return void 0 === a ? r.text(this) : this.empty().each(function () {
          1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = a);
        });
      }, null, a, arguments.length);
    },
    append: function append() {
      return Ia(this, arguments, function (a) {
        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
          var b = Da(this, a);
          b.appendChild(a);
        }
      });
    },
    prepend: function prepend() {
      return Ia(this, arguments, function (a) {
        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
          var b = Da(this, a);
          b.insertBefore(a, b.firstChild);
        }
      });
    },
    before: function before() {
      return Ia(this, arguments, function (a) {
        this.parentNode && this.parentNode.insertBefore(a, this);
      });
    },
    after: function after() {
      return Ia(this, arguments, function (a) {
        this.parentNode && this.parentNode.insertBefore(a, this.nextSibling);
      });
    },
    empty: function empty() {
      for (var a, b = 0; null != (a = this[b]); b++) {
        1 === a.nodeType && (r.cleanData(ma(a, !1)), a.textContent = "");
      }

      return this;
    },
    clone: function clone(a, b) {
      return a = null != a && a, b = null == b ? a : b, this.map(function () {
        return r.clone(this, a, b);
      });
    },
    html: function html(a) {
      return S(this, function (a) {
        var b = this[0] || {},
            c = 0,
            d = this.length;
        if (void 0 === a && 1 === b.nodeType) return b.innerHTML;

        if ("string" == typeof a && !za.test(a) && !la[(ja.exec(a) || ["", ""])[1].toLowerCase()]) {
          a = r.htmlPrefilter(a);

          try {
            for (; c < d; c++) {
              b = this[c] || {}, 1 === b.nodeType && (r.cleanData(ma(b, !1)), b.innerHTML = a);
            }

            b = 0;
          } catch (e) {}
        }

        b && this.empty().append(a);
      }, null, a, arguments.length);
    },
    replaceWith: function replaceWith() {
      var a = [];
      return Ia(this, arguments, function (b) {
        var c = this.parentNode;
        r.inArray(this, a) < 0 && (r.cleanData(ma(this)), c && c.replaceChild(b, this));
      }, a);
    }
  }), r.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function (a, b) {
    r.fn[a] = function (a) {
      for (var c, d = [], e = r(a), f = e.length - 1, g = 0; g <= f; g++) {
        c = g === f ? this : this.clone(!0), r(e[g])[b](c), h.apply(d, c.get());
      }

      return this.pushStack(d);
    };
  });

  var Ka = /^margin/,
      La = new RegExp("^(" + _ + ")(?!px)[a-z%]+$", "i"),
      Ma = function Ma(b) {
    var c = b.ownerDocument.defaultView;
    return c && c.opener || (c = a), c.getComputedStyle(b);
  };

  !function () {
    function b() {
      if (i) {
        i.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", i.innerHTML = "", qa.appendChild(h);
        var b = a.getComputedStyle(i);
        c = "1%" !== b.top, g = "2px" === b.marginLeft, e = "4px" === b.width, i.style.marginRight = "50%", f = "4px" === b.marginRight, qa.removeChild(h), i = null;
      }
    }

    var c,
        e,
        f,
        g,
        h = d.createElement("div"),
        i = d.createElement("div");
    i.style && (i.style.backgroundClip = "content-box", i.cloneNode(!0).style.backgroundClip = "", o.clearCloneStyle = "content-box" === i.style.backgroundClip, h.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", h.appendChild(i), r.extend(o, {
      pixelPosition: function pixelPosition() {
        return b(), c;
      },
      boxSizingReliable: function boxSizingReliable() {
        return b(), e;
      },
      pixelMarginRight: function pixelMarginRight() {
        return b(), f;
      },
      reliableMarginLeft: function reliableMarginLeft() {
        return b(), g;
      }
    }));
  }();

  function Na(a, b, c) {
    var d,
        e,
        f,
        g,
        h = a.style;
    return c = c || Ma(a), c && (g = c.getPropertyValue(b) || c[b], "" !== g || r.contains(a.ownerDocument, a) || (g = r.style(a, b)), !o.pixelMarginRight() && La.test(g) && Ka.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 !== g ? g + "" : g;
  }

  function Oa(a, b) {
    return {
      get: function get() {
        return a() ? void delete this.get : (this.get = b).apply(this, arguments);
      }
    };
  }

  var Pa = /^(none|table(?!-c[ea]).+)/,
      Qa = {
    position: "absolute",
    visibility: "hidden",
    display: "block"
  },
      Ra = {
    letterSpacing: "0",
    fontWeight: "400"
  },
      Sa = ["Webkit", "Moz", "ms"],
      Ta = d.createElement("div").style;

  function Ua(a) {
    if (a in Ta) return a;
    var b = a[0].toUpperCase() + a.slice(1),
        c = Sa.length;

    while (c--) {
      if (a = Sa[c] + b, a in Ta) return a;
    }
  }

  function Va(a, b, c) {
    var d = aa.exec(b);
    return d ? Math.max(0, d[2] - (c || 0)) + (d[3] || "px") : b;
  }

  function Wa(a, b, c, d, e) {
    var f,
        g = 0;

    for (f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0; f < 4; f += 2) {
      "margin" === c && (g += r.css(a, c + ba[f], !0, e)), d ? ("content" === c && (g -= r.css(a, "padding" + ba[f], !0, e)), "margin" !== c && (g -= r.css(a, "border" + ba[f] + "Width", !0, e))) : (g += r.css(a, "padding" + ba[f], !0, e), "padding" !== c && (g += r.css(a, "border" + ba[f] + "Width", !0, e)));
    }

    return g;
  }

  function Xa(a, b, c) {
    var d,
        e = !0,
        f = Ma(a),
        g = "border-box" === r.css(a, "boxSizing", !1, f);

    if (a.getClientRects().length && (d = a.getBoundingClientRect()[b]), d <= 0 || null == d) {
      if (d = Na(a, b, f), (d < 0 || null == d) && (d = a.style[b]), La.test(d)) return d;
      e = g && (o.boxSizingReliable() || d === a.style[b]), d = parseFloat(d) || 0;
    }

    return d + Wa(a, b, c || (g ? "border" : "content"), e, f) + "px";
  }

  r.extend({
    cssHooks: {
      opacity: {
        get: function get(a, b) {
          if (b) {
            var c = Na(a, "opacity");
            return "" === c ? "1" : c;
          }
        }
      }
    },
    cssNumber: {
      animationIterationCount: !0,
      columnCount: !0,
      fillOpacity: !0,
      flexGrow: !0,
      flexShrink: !0,
      fontWeight: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0
    },
    cssProps: {
      "float": "cssFloat"
    },
    style: function style(a, b, c, d) {
      if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
        var e,
            f,
            g,
            h = r.camelCase(b),
            i = a.style;
        return b = r.cssProps[h] || (r.cssProps[h] = Ua(h) || h), g = r.cssHooks[b] || r.cssHooks[h], void 0 === c ? g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b] : (f = _typeof(c), "string" === f && (e = aa.exec(c)) && e[1] && (c = ea(a, b, e), f = "number"), null != c && c === c && ("number" === f && (c += e && e[3] || (r.cssNumber[h] ? "" : "px")), o.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (i[b] = "inherit"), g && "set" in g && void 0 === (c = g.set(a, c, d)) || (i[b] = c)), void 0);
      }
    },
    css: function css(a, b, c, d) {
      var e,
          f,
          g,
          h = r.camelCase(b);
      return b = r.cssProps[h] || (r.cssProps[h] = Ua(h) || h), g = r.cssHooks[b] || r.cssHooks[h], g && "get" in g && (e = g.get(a, !0, c)), void 0 === e && (e = Na(a, b, d)), "normal" === e && b in Ra && (e = Ra[b]), "" === c || c ? (f = parseFloat(e), c === !0 || isFinite(f) ? f || 0 : e) : e;
    }
  }), r.each(["height", "width"], function (a, b) {
    r.cssHooks[b] = {
      get: function get(a, c, d) {
        if (c) return !Pa.test(r.css(a, "display")) || a.getClientRects().length && a.getBoundingClientRect().width ? Xa(a, b, d) : da(a, Qa, function () {
          return Xa(a, b, d);
        });
      },
      set: function set(a, c, d) {
        var e,
            f = d && Ma(a),
            g = d && Wa(a, b, d, "border-box" === r.css(a, "boxSizing", !1, f), f);
        return g && (e = aa.exec(c)) && "px" !== (e[3] || "px") && (a.style[b] = c, c = r.css(a, b)), Va(a, c, g);
      }
    };
  }), r.cssHooks.marginLeft = Oa(o.reliableMarginLeft, function (a, b) {
    if (b) return (parseFloat(Na(a, "marginLeft")) || a.getBoundingClientRect().left - da(a, {
      marginLeft: 0
    }, function () {
      return a.getBoundingClientRect().left;
    })) + "px";
  }), r.each({
    margin: "",
    padding: "",
    border: "Width"
  }, function (a, b) {
    r.cssHooks[a + b] = {
      expand: function expand(c) {
        for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; d < 4; d++) {
          e[a + ba[d] + b] = f[d] || f[d - 2] || f[0];
        }

        return e;
      }
    }, Ka.test(a) || (r.cssHooks[a + b].set = Va);
  }), r.fn.extend({
    css: function css(a, b) {
      return S(this, function (a, b, c) {
        var d,
            e,
            f = {},
            g = 0;

        if (r.isArray(b)) {
          for (d = Ma(a), e = b.length; g < e; g++) {
            f[b[g]] = r.css(a, b[g], !1, d);
          }

          return f;
        }

        return void 0 !== c ? r.style(a, b, c) : r.css(a, b);
      }, a, b, arguments.length > 1);
    }
  });

  function Ya(a, b, c, d, e) {
    return new Ya.prototype.init(a, b, c, d, e);
  }

  r.Tween = Ya, Ya.prototype = {
    constructor: Ya,
    init: function init(a, b, c, d, e, f) {
      this.elem = a, this.prop = c, this.easing = e || r.easing._default, this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (r.cssNumber[c] ? "" : "px");
    },
    cur: function cur() {
      var a = Ya.propHooks[this.prop];
      return a && a.get ? a.get(this) : Ya.propHooks._default.get(this);
    },
    run: function run(a) {
      var b,
          c = Ya.propHooks[this.prop];
      return this.options.duration ? this.pos = b = r.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : Ya.propHooks._default.set(this), this;
    }
  }, Ya.prototype.init.prototype = Ya.prototype, Ya.propHooks = {
    _default: {
      get: function get(a) {
        var b;
        return 1 !== a.elem.nodeType || null != a.elem[a.prop] && null == a.elem.style[a.prop] ? a.elem[a.prop] : (b = r.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0);
      },
      set: function set(a) {
        r.fx.step[a.prop] ? r.fx.step[a.prop](a) : 1 !== a.elem.nodeType || null == a.elem.style[r.cssProps[a.prop]] && !r.cssHooks[a.prop] ? a.elem[a.prop] = a.now : r.style(a.elem, a.prop, a.now + a.unit);
      }
    }
  }, Ya.propHooks.scrollTop = Ya.propHooks.scrollLeft = {
    set: function set(a) {
      a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now);
    }
  }, r.easing = {
    linear: function linear(a) {
      return a;
    },
    swing: function swing(a) {
      return .5 - Math.cos(a * Math.PI) / 2;
    },
    _default: "swing"
  }, r.fx = Ya.prototype.init, r.fx.step = {};
  var Za,
      $a,
      _a = /^(?:toggle|show|hide)$/,
      ab = /queueHooks$/;

  function bb() {
    $a && (a.requestAnimationFrame(bb), r.fx.tick());
  }

  function cb() {
    return a.setTimeout(function () {
      Za = void 0;
    }), Za = r.now();
  }

  function db(a, b) {
    var c,
        d = 0,
        e = {
      height: a
    };

    for (b = b ? 1 : 0; d < 4; d += 2 - b) {
      c = ba[d], e["margin" + c] = e["padding" + c] = a;
    }

    return b && (e.opacity = e.width = a), e;
  }

  function eb(a, b, c) {
    for (var d, e = (hb.tweeners[b] || []).concat(hb.tweeners["*"]), f = 0, g = e.length; f < g; f++) {
      if (d = e[f].call(c, b, a)) return d;
    }
  }

  function fb(a, b, c) {
    var d,
        e,
        f,
        g,
        h,
        i,
        j,
        k,
        l = "width" in b || "height" in b,
        m = this,
        n = {},
        o = a.style,
        p = a.nodeType && ca(a),
        q = V.get(a, "fxshow");
    c.queue || (g = r._queueHooks(a, "fx"), null == g.unqueued && (g.unqueued = 0, h = g.empty.fire, g.empty.fire = function () {
      g.unqueued || h();
    }), g.unqueued++, m.always(function () {
      m.always(function () {
        g.unqueued--, r.queue(a, "fx").length || g.empty.fire();
      });
    }));

    for (d in b) {
      if (e = b[d], _a.test(e)) {
        if (delete b[d], f = f || "toggle" === e, e === (p ? "hide" : "show")) {
          if ("show" !== e || !q || void 0 === q[d]) continue;
          p = !0;
        }

        n[d] = q && q[d] || r.style(a, d);
      }
    }

    if (i = !r.isEmptyObject(b), i || !r.isEmptyObject(n)) {
      l && 1 === a.nodeType && (c.overflow = [o.overflow, o.overflowX, o.overflowY], j = q && q.display, null == j && (j = V.get(a, "display")), k = r.css(a, "display"), "none" === k && (j ? k = j : (ha([a], !0), j = a.style.display || j, k = r.css(a, "display"), ha([a]))), ("inline" === k || "inline-block" === k && null != j) && "none" === r.css(a, "float") && (i || (m.done(function () {
        o.display = j;
      }), null == j && (k = o.display, j = "none" === k ? "" : k)), o.display = "inline-block")), c.overflow && (o.overflow = "hidden", m.always(function () {
        o.overflow = c.overflow[0], o.overflowX = c.overflow[1], o.overflowY = c.overflow[2];
      })), i = !1;

      for (d in n) {
        i || (q ? "hidden" in q && (p = q.hidden) : q = V.access(a, "fxshow", {
          display: j
        }), f && (q.hidden = !p), p && ha([a], !0), m.done(function () {
          p || ha([a]), V.remove(a, "fxshow");

          for (d in n) {
            r.style(a, d, n[d]);
          }
        })), i = eb(p ? q[d] : 0, d, m), d in q || (q[d] = i.start, p && (i.end = i.start, i.start = 0));
      }
    }
  }

  function gb(a, b) {
    var c, d, e, f, g;

    for (c in a) {
      if (d = r.camelCase(c), e = b[d], f = a[c], r.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = r.cssHooks[d], g && "expand" in g) {
        f = g.expand(f), delete a[d];

        for (c in f) {
          c in a || (a[c] = f[c], b[c] = e);
        }
      } else b[d] = e;
    }
  }

  function hb(a, b, c) {
    var d,
        e,
        f = 0,
        g = hb.prefilters.length,
        h = r.Deferred().always(function () {
      delete i.elem;
    }),
        i = function i() {
      if (e) return !1;

      for (var b = Za || cb(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; g < i; g++) {
        j.tweens[g].run(f);
      }

      return h.notifyWith(a, [j, f, c]), f < 1 && i ? c : (h.resolveWith(a, [j]), !1);
    },
        j = h.promise({
      elem: a,
      props: r.extend({}, b),
      opts: r.extend(!0, {
        specialEasing: {},
        easing: r.easing._default
      }, c),
      originalProperties: b,
      originalOptions: c,
      startTime: Za || cb(),
      duration: c.duration,
      tweens: [],
      createTween: function createTween(b, c) {
        var d = r.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
        return j.tweens.push(d), d;
      },
      stop: function stop(b) {
        var c = 0,
            d = b ? j.tweens.length : 0;
        if (e) return this;

        for (e = !0; c < d; c++) {
          j.tweens[c].run(1);
        }

        return b ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b])) : h.rejectWith(a, [j, b]), this;
      }
    }),
        k = j.props;

    for (gb(k, j.opts.specialEasing); f < g; f++) {
      if (d = hb.prefilters[f].call(j, a, k, j.opts)) return r.isFunction(d.stop) && (r._queueHooks(j.elem, j.opts.queue).stop = r.proxy(d.stop, d)), d;
    }

    return r.map(k, eb, j), r.isFunction(j.opts.start) && j.opts.start.call(a, j), r.fx.timer(r.extend(i, {
      elem: a,
      anim: j,
      queue: j.opts.queue
    })), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always);
  }

  r.Animation = r.extend(hb, {
    tweeners: {
      "*": [function (a, b) {
        var c = this.createTween(a, b);
        return ea(c.elem, a, aa.exec(b), c), c;
      }]
    },
    tweener: function tweener(a, b) {
      r.isFunction(a) ? (b = a, a = ["*"]) : a = a.match(K);

      for (var c, d = 0, e = a.length; d < e; d++) {
        c = a[d], hb.tweeners[c] = hb.tweeners[c] || [], hb.tweeners[c].unshift(b);
      }
    },
    prefilters: [fb],
    prefilter: function prefilter(a, b) {
      b ? hb.prefilters.unshift(a) : hb.prefilters.push(a);
    }
  }), r.speed = function (a, b, c) {
    var e = a && "object" == _typeof(a) ? r.extend({}, a) : {
      complete: c || !c && b || r.isFunction(a) && a,
      duration: a,
      easing: c && b || b && !r.isFunction(b) && b
    };
    return r.fx.off || d.hidden ? e.duration = 0 : "number" != typeof e.duration && (e.duration in r.fx.speeds ? e.duration = r.fx.speeds[e.duration] : e.duration = r.fx.speeds._default), null != e.queue && e.queue !== !0 || (e.queue = "fx"), e.old = e.complete, e.complete = function () {
      r.isFunction(e.old) && e.old.call(this), e.queue && r.dequeue(this, e.queue);
    }, e;
  }, r.fn.extend({
    fadeTo: function fadeTo(a, b, c, d) {
      return this.filter(ca).css("opacity", 0).show().end().animate({
        opacity: b
      }, a, c, d);
    },
    animate: function animate(a, b, c, d) {
      var e = r.isEmptyObject(a),
          f = r.speed(b, c, d),
          g = function g() {
        var b = hb(this, r.extend({}, a), f);
        (e || V.get(this, "finish")) && b.stop(!0);
      };

      return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g);
    },
    stop: function stop(a, b, c) {
      var d = function d(a) {
        var b = a.stop;
        delete a.stop, b(c);
      };

      return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), this.each(function () {
        var b = !0,
            e = null != a && a + "queueHooks",
            f = r.timers,
            g = V.get(this);
        if (e) g[e] && g[e].stop && d(g[e]);else for (e in g) {
          g[e] && g[e].stop && ab.test(e) && d(g[e]);
        }

        for (e = f.length; e--;) {
          f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));
        }

        !b && c || r.dequeue(this, a);
      });
    },
    finish: function finish(a) {
      return a !== !1 && (a = a || "fx"), this.each(function () {
        var b,
            c = V.get(this),
            d = c[a + "queue"],
            e = c[a + "queueHooks"],
            f = r.timers,
            g = d ? d.length : 0;

        for (c.finish = !0, r.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) {
          f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
        }

        for (b = 0; b < g; b++) {
          d[b] && d[b].finish && d[b].finish.call(this);
        }

        delete c.finish;
      });
    }
  }), r.each(["toggle", "show", "hide"], function (a, b) {
    var c = r.fn[b];

    r.fn[b] = function (a, d, e) {
      return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(db(b, !0), a, d, e);
    };
  }), r.each({
    slideDown: db("show"),
    slideUp: db("hide"),
    slideToggle: db("toggle"),
    fadeIn: {
      opacity: "show"
    },
    fadeOut: {
      opacity: "hide"
    },
    fadeToggle: {
      opacity: "toggle"
    }
  }, function (a, b) {
    r.fn[a] = function (a, c, d) {
      return this.animate(b, a, c, d);
    };
  }), r.timers = [], r.fx.tick = function () {
    var a,
        b = 0,
        c = r.timers;

    for (Za = r.now(); b < c.length; b++) {
      a = c[b], a() || c[b] !== a || c.splice(b--, 1);
    }

    c.length || r.fx.stop(), Za = void 0;
  }, r.fx.timer = function (a) {
    r.timers.push(a), a() ? r.fx.start() : r.timers.pop();
  }, r.fx.interval = 13, r.fx.start = function () {
    $a || ($a = a.requestAnimationFrame ? a.requestAnimationFrame(bb) : a.setInterval(r.fx.tick, r.fx.interval));
  }, r.fx.stop = function () {
    a.cancelAnimationFrame ? a.cancelAnimationFrame($a) : a.clearInterval($a), $a = null;
  }, r.fx.speeds = {
    slow: 600,
    fast: 200,
    _default: 400
  }, r.fn.delay = function (b, c) {
    return b = r.fx ? r.fx.speeds[b] || b : b, c = c || "fx", this.queue(c, function (c, d) {
      var e = a.setTimeout(c, b);

      d.stop = function () {
        a.clearTimeout(e);
      };
    });
  }, function () {
    var a = d.createElement("input"),
        b = d.createElement("select"),
        c = b.appendChild(d.createElement("option"));
    a.type = "checkbox", o.checkOn = "" !== a.value, o.optSelected = c.selected, a = d.createElement("input"), a.value = "t", a.type = "radio", o.radioValue = "t" === a.value;
  }();
  var ib,
      jb = r.expr.attrHandle;
  r.fn.extend({
    attr: function attr(a, b) {
      return S(this, r.attr, a, b, arguments.length > 1);
    },
    removeAttr: function removeAttr(a) {
      return this.each(function () {
        r.removeAttr(this, a);
      });
    }
  }), r.extend({
    attr: function attr(a, b, c) {
      var d,
          e,
          f = a.nodeType;
      if (3 !== f && 8 !== f && 2 !== f) return "undefined" == typeof a.getAttribute ? r.prop(a, b, c) : (1 === f && r.isXMLDoc(a) || (e = r.attrHooks[b.toLowerCase()] || (r.expr.match.bool.test(b) ? ib : void 0)), void 0 !== c ? null === c ? void r.removeAttr(a, b) : e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + ""), c) : e && "get" in e && null !== (d = e.get(a, b)) ? d : (d = r.find.attr(a, b), null == d ? void 0 : d));
    },
    attrHooks: {
      type: {
        set: function set(a, b) {
          if (!o.radioValue && "radio" === b && r.nodeName(a, "input")) {
            var c = a.value;
            return a.setAttribute("type", b), c && (a.value = c), b;
          }
        }
      }
    },
    removeAttr: function removeAttr(a, b) {
      var c,
          d = 0,
          e = b && b.match(K);
      if (e && 1 === a.nodeType) while (c = e[d++]) {
        a.removeAttribute(c);
      }
    }
  }), ib = {
    set: function set(a, b, c) {
      return b === !1 ? r.removeAttr(a, c) : a.setAttribute(c, c), c;
    }
  }, r.each(r.expr.match.bool.source.match(/\w+/g), function (a, b) {
    var c = jb[b] || r.find.attr;

    jb[b] = function (a, b, d) {
      var e,
          f,
          g = b.toLowerCase();
      return d || (f = jb[g], jb[g] = e, e = null != c(a, b, d) ? g : null, jb[g] = f), e;
    };
  });
  var kb = /^(?:input|select|textarea|button)$/i,
      lb = /^(?:a|area)$/i;
  r.fn.extend({
    prop: function prop(a, b) {
      return S(this, r.prop, a, b, arguments.length > 1);
    },
    removeProp: function removeProp(a) {
      return this.each(function () {
        delete this[r.propFix[a] || a];
      });
    }
  }), r.extend({
    prop: function prop(a, b, c) {
      var d,
          e,
          f = a.nodeType;
      if (3 !== f && 8 !== f && 2 !== f) return 1 === f && r.isXMLDoc(a) || (b = r.propFix[b] || b, e = r.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b];
    },
    propHooks: {
      tabIndex: {
        get: function get(a) {
          var b = r.find.attr(a, "tabindex");
          return b ? parseInt(b, 10) : kb.test(a.nodeName) || lb.test(a.nodeName) && a.href ? 0 : -1;
        }
      }
    },
    propFix: {
      "for": "htmlFor",
      "class": "className"
    }
  }), o.optSelected || (r.propHooks.selected = {
    get: function get(a) {
      var b = a.parentNode;
      return b && b.parentNode && b.parentNode.selectedIndex, null;
    },
    set: function set(a) {
      var b = a.parentNode;
      b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex);
    }
  }), r.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
    r.propFix[this.toLowerCase()] = this;
  });

  function mb(a) {
    var b = a.match(K) || [];
    return b.join(" ");
  }

  function nb(a) {
    return a.getAttribute && a.getAttribute("class") || "";
  }

  r.fn.extend({
    addClass: function addClass(a) {
      var b,
          c,
          d,
          e,
          f,
          g,
          h,
          i = 0;
      if (r.isFunction(a)) return this.each(function (b) {
        r(this).addClass(a.call(this, b, nb(this)));
      });

      if ("string" == typeof a && a) {
        b = a.match(K) || [];

        while (c = this[i++]) {
          if (e = nb(c), d = 1 === c.nodeType && " " + mb(e) + " ") {
            g = 0;

            while (f = b[g++]) {
              d.indexOf(" " + f + " ") < 0 && (d += f + " ");
            }

            h = mb(d), e !== h && c.setAttribute("class", h);
          }
        }
      }

      return this;
    },
    removeClass: function removeClass(a) {
      var b,
          c,
          d,
          e,
          f,
          g,
          h,
          i = 0;
      if (r.isFunction(a)) return this.each(function (b) {
        r(this).removeClass(a.call(this, b, nb(this)));
      });
      if (!arguments.length) return this.attr("class", "");

      if ("string" == typeof a && a) {
        b = a.match(K) || [];

        while (c = this[i++]) {
          if (e = nb(c), d = 1 === c.nodeType && " " + mb(e) + " ") {
            g = 0;

            while (f = b[g++]) {
              while (d.indexOf(" " + f + " ") > -1) {
                d = d.replace(" " + f + " ", " ");
              }
            }

            h = mb(d), e !== h && c.setAttribute("class", h);
          }
        }
      }

      return this;
    },
    toggleClass: function toggleClass(a, b) {
      var c = _typeof(a);

      return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : r.isFunction(a) ? this.each(function (c) {
        r(this).toggleClass(a.call(this, c, nb(this), b), b);
      }) : this.each(function () {
        var b, d, e, f;

        if ("string" === c) {
          d = 0, e = r(this), f = a.match(K) || [];

          while (b = f[d++]) {
            e.hasClass(b) ? e.removeClass(b) : e.addClass(b);
          }
        } else void 0 !== a && "boolean" !== c || (b = nb(this), b && V.set(this, "__className__", b), this.setAttribute && this.setAttribute("class", b || a === !1 ? "" : V.get(this, "__className__") || ""));
      });
    },
    hasClass: function hasClass(a) {
      var b,
          c,
          d = 0;
      b = " " + a + " ";

      while (c = this[d++]) {
        if (1 === c.nodeType && (" " + mb(nb(c)) + " ").indexOf(b) > -1) return !0;
      }

      return !1;
    }
  });
  var ob = /\r/g;
  r.fn.extend({
    val: function val(a) {
      var b,
          c,
          d,
          e = this[0];
      {
        if (arguments.length) return d = r.isFunction(a), this.each(function (c) {
          var e;
          1 === this.nodeType && (e = d ? a.call(this, c, r(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : r.isArray(e) && (e = r.map(e, function (a) {
            return null == a ? "" : a + "";
          })), b = r.valHooks[this.type] || r.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e));
        });
        if (e) return b = r.valHooks[e.type] || r.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(ob, "") : null == c ? "" : c);
      }
    }
  }), r.extend({
    valHooks: {
      option: {
        get: function get(a) {
          var b = r.find.attr(a, "value");
          return null != b ? b : mb(r.text(a));
        }
      },
      select: {
        get: function get(a) {
          var b,
              c,
              d,
              e = a.options,
              f = a.selectedIndex,
              g = "select-one" === a.type,
              h = g ? null : [],
              i = g ? f + 1 : e.length;

          for (d = f < 0 ? i : g ? f : 0; d < i; d++) {
            if (c = e[d], (c.selected || d === f) && !c.disabled && (!c.parentNode.disabled || !r.nodeName(c.parentNode, "optgroup"))) {
              if (b = r(c).val(), g) return b;
              h.push(b);
            }
          }

          return h;
        },
        set: function set(a, b) {
          var c,
              d,
              e = a.options,
              f = r.makeArray(b),
              g = e.length;

          while (g--) {
            d = e[g], (d.selected = r.inArray(r.valHooks.option.get(d), f) > -1) && (c = !0);
          }

          return c || (a.selectedIndex = -1), f;
        }
      }
    }
  }), r.each(["radio", "checkbox"], function () {
    r.valHooks[this] = {
      set: function set(a, b) {
        if (r.isArray(b)) return a.checked = r.inArray(r(a).val(), b) > -1;
      }
    }, o.checkOn || (r.valHooks[this].get = function (a) {
      return null === a.getAttribute("value") ? "on" : a.value;
    });
  });
  var pb = /^(?:focusinfocus|focusoutblur)$/;
  r.extend(r.event, {
    trigger: function trigger(b, c, e, f) {
      var g,
          h,
          i,
          j,
          k,
          m,
          n,
          o = [e || d],
          p = l.call(b, "type") ? b.type : b,
          q = l.call(b, "namespace") ? b.namespace.split(".") : [];

      if (h = i = e = e || d, 3 !== e.nodeType && 8 !== e.nodeType && !pb.test(p + r.event.triggered) && (p.indexOf(".") > -1 && (q = p.split("."), p = q.shift(), q.sort()), k = p.indexOf(":") < 0 && "on" + p, b = b[r.expando] ? b : new r.Event(p, "object" == _typeof(b) && b), b.isTrigger = f ? 2 : 3, b.namespace = q.join("."), b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + q.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = e), c = null == c ? [b] : r.makeArray(c, [b]), n = r.event.special[p] || {}, f || !n.trigger || n.trigger.apply(e, c) !== !1)) {
        if (!f && !n.noBubble && !r.isWindow(e)) {
          for (j = n.delegateType || p, pb.test(j + p) || (h = h.parentNode); h; h = h.parentNode) {
            o.push(h), i = h;
          }

          i === (e.ownerDocument || d) && o.push(i.defaultView || i.parentWindow || a);
        }

        g = 0;

        while ((h = o[g++]) && !b.isPropagationStopped()) {
          b.type = g > 1 ? j : n.bindType || p, m = (V.get(h, "events") || {})[b.type] && V.get(h, "handle"), m && m.apply(h, c), m = k && h[k], m && m.apply && T(h) && (b.result = m.apply(h, c), b.result === !1 && b.preventDefault());
        }

        return b.type = p, f || b.isDefaultPrevented() || n._default && n._default.apply(o.pop(), c) !== !1 || !T(e) || k && r.isFunction(e[p]) && !r.isWindow(e) && (i = e[k], i && (e[k] = null), r.event.triggered = p, e[p](), r.event.triggered = void 0, i && (e[k] = i)), b.result;
      }
    },
    simulate: function simulate(a, b, c) {
      var d = r.extend(new r.Event(), c, {
        type: a,
        isSimulated: !0
      });
      r.event.trigger(d, null, b);
    }
  }), r.fn.extend({
    trigger: function trigger(a, b) {
      return this.each(function () {
        r.event.trigger(a, b, this);
      });
    },
    triggerHandler: function triggerHandler(a, b) {
      var c = this[0];
      if (c) return r.event.trigger(a, b, c, !0);
    }
  }), r.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (a, b) {
    r.fn[b] = function (a, c) {
      return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b);
    };
  }), r.fn.extend({
    hover: function hover(a, b) {
      return this.mouseenter(a).mouseleave(b || a);
    }
  }), o.focusin = "onfocusin" in a, o.focusin || r.each({
    focus: "focusin",
    blur: "focusout"
  }, function (a, b) {
    var c = function c(a) {
      r.event.simulate(b, a.target, r.event.fix(a));
    };

    r.event.special[b] = {
      setup: function setup() {
        var d = this.ownerDocument || this,
            e = V.access(d, b);
        e || d.addEventListener(a, c, !0), V.access(d, b, (e || 0) + 1);
      },
      teardown: function teardown() {
        var d = this.ownerDocument || this,
            e = V.access(d, b) - 1;
        e ? V.access(d, b, e) : (d.removeEventListener(a, c, !0), V.remove(d, b));
      }
    };
  });
  var qb = a.location,
      rb = r.now(),
      sb = /\?/;

  r.parseXML = function (b) {
    var c;
    if (!b || "string" != typeof b) return null;

    try {
      c = new a.DOMParser().parseFromString(b, "text/xml");
    } catch (d) {
      c = void 0;
    }

    return c && !c.getElementsByTagName("parsererror").length || r.error("Invalid XML: " + b), c;
  };

  var tb = /\[\]$/,
      ub = /\r?\n/g,
      vb = /^(?:submit|button|image|reset|file)$/i,
      wb = /^(?:input|select|textarea|keygen)/i;

  function xb(a, b, c, d) {
    var e;
    if (r.isArray(b)) r.each(b, function (b, e) {
      c || tb.test(a) ? d(a, e) : xb(a + "[" + ("object" == _typeof(e) && null != e ? b : "") + "]", e, c, d);
    });else if (c || "object" !== r.type(b)) d(a, b);else for (e in b) {
      xb(a + "[" + e + "]", b[e], c, d);
    }
  }

  r.param = function (a, b) {
    var c,
        d = [],
        e = function e(a, b) {
      var c = r.isFunction(b) ? b() : b;
      d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(null == c ? "" : c);
    };

    if (r.isArray(a) || a.jquery && !r.isPlainObject(a)) r.each(a, function () {
      e(this.name, this.value);
    });else for (c in a) {
      xb(c, a[c], b, e);
    }
    return d.join("&");
  }, r.fn.extend({
    serialize: function serialize() {
      return r.param(this.serializeArray());
    },
    serializeArray: function serializeArray() {
      return this.map(function () {
        var a = r.prop(this, "elements");
        return a ? r.makeArray(a) : this;
      }).filter(function () {
        var a = this.type;
        return this.name && !r(this).is(":disabled") && wb.test(this.nodeName) && !vb.test(a) && (this.checked || !ia.test(a));
      }).map(function (a, b) {
        var c = r(this).val();
        return null == c ? null : r.isArray(c) ? r.map(c, function (a) {
          return {
            name: b.name,
            value: a.replace(ub, "\r\n")
          };
        }) : {
          name: b.name,
          value: c.replace(ub, "\r\n")
        };
      }).get();
    }
  });
  var yb = /%20/g,
      zb = /#.*$/,
      Ab = /([?&])_=[^&]*/,
      Bb = /^(.*?):[ \t]*([^\r\n]*)$/gm,
      Cb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
      Db = /^(?:GET|HEAD)$/,
      Eb = /^\/\//,
      Fb = {},
      Gb = {},
      Hb = "*/".concat("*"),
      Ib = d.createElement("a");
  Ib.href = qb.href;

  function Jb(a) {
    return function (b, c) {
      "string" != typeof b && (c = b, b = "*");
      var d,
          e = 0,
          f = b.toLowerCase().match(K) || [];
      if (r.isFunction(c)) while (d = f[e++]) {
        "+" === d[0] ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c);
      }
    };
  }

  function Kb(a, b, c, d) {
    var e = {},
        f = a === Gb;

    function g(h) {
      var i;
      return e[h] = !0, r.each(a[h] || [], function (a, h) {
        var j = h(b, c, d);
        return "string" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), g(j), !1);
      }), i;
    }

    return g(b.dataTypes[0]) || !e["*"] && g("*");
  }

  function Lb(a, b) {
    var c,
        d,
        e = r.ajaxSettings.flatOptions || {};

    for (c in b) {
      void 0 !== b[c] && ((e[c] ? a : d || (d = {}))[c] = b[c]);
    }

    return d && r.extend(!0, a, d), a;
  }

  function Mb(a, b, c) {
    var d,
        e,
        f,
        g,
        h = a.contents,
        i = a.dataTypes;

    while ("*" === i[0]) {
      i.shift(), void 0 === d && (d = a.mimeType || b.getResponseHeader("Content-Type"));
    }

    if (d) for (e in h) {
      if (h[e] && h[e].test(d)) {
        i.unshift(e);
        break;
      }
    }
    if (i[0] in c) f = i[0];else {
      for (e in c) {
        if (!i[0] || a.converters[e + " " + i[0]]) {
          f = e;
          break;
        }

        g || (g = e);
      }

      f = f || g;
    }
    if (f) return f !== i[0] && i.unshift(f), c[f];
  }

  function Nb(a, b, c, d) {
    var e,
        f,
        g,
        h,
        i,
        j = {},
        k = a.dataTypes.slice();
    if (k[1]) for (g in a.converters) {
      j[g.toLowerCase()] = a.converters[g];
    }
    f = k.shift();

    while (f) {
      if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift()) if ("*" === f) f = i;else if ("*" !== i && i !== f) {
        if (g = j[i + " " + f] || j["* " + f], !g) for (e in j) {
          if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
            g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
            break;
          }
        }
        if (g !== !0) if (g && a["throws"]) b = g(b);else try {
          b = g(b);
        } catch (l) {
          return {
            state: "parsererror",
            error: g ? l : "No conversion from " + i + " to " + f
          };
        }
      }
    }

    return {
      state: "success",
      data: b
    };
  }

  r.extend({
    active: 0,
    lastModified: {},
    etag: {},
    ajaxSettings: {
      url: qb.href,
      type: "GET",
      isLocal: Cb.test(qb.protocol),
      global: !0,
      processData: !0,
      async: !0,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      accepts: {
        "*": Hb,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript"
      },
      contents: {
        xml: /\bxml\b/,
        html: /\bhtml/,
        json: /\bjson\b/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON"
      },
      converters: {
        "* text": String,
        "text html": !0,
        "text json": JSON.parse,
        "text xml": r.parseXML
      },
      flatOptions: {
        url: !0,
        context: !0
      }
    },
    ajaxSetup: function ajaxSetup(a, b) {
      return b ? Lb(Lb(a, r.ajaxSettings), b) : Lb(r.ajaxSettings, a);
    },
    ajaxPrefilter: Jb(Fb),
    ajaxTransport: Jb(Gb),
    ajax: function ajax(b, c) {
      "object" == _typeof(b) && (c = b, b = void 0), c = c || {};
      var e,
          f,
          g,
          h,
          i,
          j,
          k,
          l,
          m,
          n,
          o = r.ajaxSetup({}, c),
          p = o.context || o,
          q = o.context && (p.nodeType || p.jquery) ? r(p) : r.event,
          s = r.Deferred(),
          t = r.Callbacks("once memory"),
          u = o.statusCode || {},
          v = {},
          w = {},
          x = "canceled",
          y = {
        readyState: 0,
        getResponseHeader: function getResponseHeader(a) {
          var b;

          if (k) {
            if (!h) {
              h = {};

              while (b = Bb.exec(g)) {
                h[b[1].toLowerCase()] = b[2];
              }
            }

            b = h[a.toLowerCase()];
          }

          return null == b ? null : b;
        },
        getAllResponseHeaders: function getAllResponseHeaders() {
          return k ? g : null;
        },
        setRequestHeader: function setRequestHeader(a, b) {
          return null == k && (a = w[a.toLowerCase()] = w[a.toLowerCase()] || a, v[a] = b), this;
        },
        overrideMimeType: function overrideMimeType(a) {
          return null == k && (o.mimeType = a), this;
        },
        statusCode: function statusCode(a) {
          var b;
          if (a) if (k) y.always(a[y.status]);else for (b in a) {
            u[b] = [u[b], a[b]];
          }
          return this;
        },
        abort: function abort(a) {
          var b = a || x;
          return e && e.abort(b), A(0, b), this;
        }
      };

      if (s.promise(y), o.url = ((b || o.url || qb.href) + "").replace(Eb, qb.protocol + "//"), o.type = c.method || c.type || o.method || o.type, o.dataTypes = (o.dataType || "*").toLowerCase().match(K) || [""], null == o.crossDomain) {
        j = d.createElement("a");

        try {
          j.href = o.url, j.href = j.href, o.crossDomain = Ib.protocol + "//" + Ib.host != j.protocol + "//" + j.host;
        } catch (z) {
          o.crossDomain = !0;
        }
      }

      if (o.data && o.processData && "string" != typeof o.data && (o.data = r.param(o.data, o.traditional)), Kb(Fb, o, c, y), k) return y;
      l = r.event && o.global, l && 0 === r.active++ && r.event.trigger("ajaxStart"), o.type = o.type.toUpperCase(), o.hasContent = !Db.test(o.type), f = o.url.replace(zb, ""), o.hasContent ? o.data && o.processData && 0 === (o.contentType || "").indexOf("application/x-www-form-urlencoded") && (o.data = o.data.replace(yb, "+")) : (n = o.url.slice(f.length), o.data && (f += (sb.test(f) ? "&" : "?") + o.data, delete o.data), o.cache === !1 && (f = f.replace(Ab, "$1"), n = (sb.test(f) ? "&" : "?") + "_=" + rb++ + n), o.url = f + n), o.ifModified && (r.lastModified[f] && y.setRequestHeader("If-Modified-Since", r.lastModified[f]), r.etag[f] && y.setRequestHeader("If-None-Match", r.etag[f])), (o.data && o.hasContent && o.contentType !== !1 || c.contentType) && y.setRequestHeader("Content-Type", o.contentType), y.setRequestHeader("Accept", o.dataTypes[0] && o.accepts[o.dataTypes[0]] ? o.accepts[o.dataTypes[0]] + ("*" !== o.dataTypes[0] ? ", " + Hb + "; q=0.01" : "") : o.accepts["*"]);

      for (m in o.headers) {
        y.setRequestHeader(m, o.headers[m]);
      }

      if (o.beforeSend && (o.beforeSend.call(p, y, o) === !1 || k)) return y.abort();

      if (x = "abort", t.add(o.complete), y.done(o.success), y.fail(o.error), e = Kb(Gb, o, c, y)) {
        if (y.readyState = 1, l && q.trigger("ajaxSend", [y, o]), k) return y;
        o.async && o.timeout > 0 && (i = a.setTimeout(function () {
          y.abort("timeout");
        }, o.timeout));

        try {
          k = !1, e.send(v, A);
        } catch (z) {
          if (k) throw z;
          A(-1, z);
        }
      } else A(-1, "No Transport");

      function A(b, c, d, h) {
        var j,
            m,
            n,
            v,
            w,
            x = c;
        k || (k = !0, i && a.clearTimeout(i), e = void 0, g = h || "", y.readyState = b > 0 ? 4 : 0, j = b >= 200 && b < 300 || 304 === b, d && (v = Mb(o, y, d)), v = Nb(o, v, y, j), j ? (o.ifModified && (w = y.getResponseHeader("Last-Modified"), w && (r.lastModified[f] = w), w = y.getResponseHeader("etag"), w && (r.etag[f] = w)), 204 === b || "HEAD" === o.type ? x = "nocontent" : 304 === b ? x = "notmodified" : (x = v.state, m = v.data, n = v.error, j = !n)) : (n = x, !b && x || (x = "error", b < 0 && (b = 0))), y.status = b, y.statusText = (c || x) + "", j ? s.resolveWith(p, [m, x, y]) : s.rejectWith(p, [y, x, n]), y.statusCode(u), u = void 0, l && q.trigger(j ? "ajaxSuccess" : "ajaxError", [y, o, j ? m : n]), t.fireWith(p, [y, x]), l && (q.trigger("ajaxComplete", [y, o]), --r.active || r.event.trigger("ajaxStop")));
      }

      return y;
    },
    getJSON: function getJSON(a, b, c) {
      return r.get(a, b, c, "json");
    },
    getScript: function getScript(a, b) {
      return r.get(a, void 0, b, "script");
    }
  }), r.each(["get", "post"], function (a, b) {
    r[b] = function (a, c, d, e) {
      return r.isFunction(c) && (e = e || d, d = c, c = void 0), r.ajax(r.extend({
        url: a,
        type: b,
        dataType: e,
        data: c,
        success: d
      }, r.isPlainObject(a) && a));
    };
  }), r._evalUrl = function (a) {
    return r.ajax({
      url: a,
      type: "GET",
      dataType: "script",
      cache: !0,
      async: !1,
      global: !1,
      "throws": !0
    });
  }, r.fn.extend({
    wrapAll: function wrapAll(a) {
      var b;
      return this[0] && (r.isFunction(a) && (a = a.call(this[0])), b = r(a, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && b.insertBefore(this[0]), b.map(function () {
        var a = this;

        while (a.firstElementChild) {
          a = a.firstElementChild;
        }

        return a;
      }).append(this)), this;
    },
    wrapInner: function wrapInner(a) {
      return r.isFunction(a) ? this.each(function (b) {
        r(this).wrapInner(a.call(this, b));
      }) : this.each(function () {
        var b = r(this),
            c = b.contents();
        c.length ? c.wrapAll(a) : b.append(a);
      });
    },
    wrap: function wrap(a) {
      var b = r.isFunction(a);
      return this.each(function (c) {
        r(this).wrapAll(b ? a.call(this, c) : a);
      });
    },
    unwrap: function unwrap(a) {
      return this.parent(a).not("body").each(function () {
        r(this).replaceWith(this.childNodes);
      }), this;
    }
  }), r.expr.pseudos.hidden = function (a) {
    return !r.expr.pseudos.visible(a);
  }, r.expr.pseudos.visible = function (a) {
    return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length);
  }, r.ajaxSettings.xhr = function () {
    try {
      return new a.XMLHttpRequest();
    } catch (b) {}
  };
  var Ob = {
    0: 200,
    1223: 204
  },
      Pb = r.ajaxSettings.xhr();
  o.cors = !!Pb && "withCredentials" in Pb, o.ajax = Pb = !!Pb, r.ajaxTransport(function (b) {
    var _c, d;

    if (o.cors || Pb && !b.crossDomain) return {
      send: function send(e, f) {
        var g,
            h = b.xhr();
        if (h.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields) for (g in b.xhrFields) {
          h[g] = b.xhrFields[g];
        }
        b.mimeType && h.overrideMimeType && h.overrideMimeType(b.mimeType), b.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest");

        for (g in e) {
          h.setRequestHeader(g, e[g]);
        }

        _c = function c(a) {
          return function () {
            _c && (_c = d = h.onload = h.onerror = h.onabort = h.onreadystatechange = null, "abort" === a ? h.abort() : "error" === a ? "number" != typeof h.status ? f(0, "error") : f(h.status, h.statusText) : f(Ob[h.status] || h.status, h.statusText, "text" !== (h.responseType || "text") || "string" != typeof h.responseText ? {
              binary: h.response
            } : {
              text: h.responseText
            }, h.getAllResponseHeaders()));
          };
        }, h.onload = _c(), d = h.onerror = _c("error"), void 0 !== h.onabort ? h.onabort = d : h.onreadystatechange = function () {
          4 === h.readyState && a.setTimeout(function () {
            _c && d();
          });
        }, _c = _c("abort");

        try {
          h.send(b.hasContent && b.data || null);
        } catch (i) {
          if (_c) throw i;
        }
      },
      abort: function abort() {
        _c && _c();
      }
    };
  }), r.ajaxPrefilter(function (a) {
    a.crossDomain && (a.contents.script = !1);
  }), r.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /\b(?:java|ecma)script\b/
    },
    converters: {
      "text script": function textScript(a) {
        return r.globalEval(a), a;
      }
    }
  }), r.ajaxPrefilter("script", function (a) {
    void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET");
  }), r.ajaxTransport("script", function (a) {
    if (a.crossDomain) {
      var b, _c2;

      return {
        send: function send(e, f) {
          b = r("<script>").prop({
            charset: a.scriptCharset,
            src: a.url
          }).on("load error", _c2 = function c(a) {
            b.remove(), _c2 = null, a && f("error" === a.type ? 404 : 200, a.type);
          }), d.head.appendChild(b[0]);
        },
        abort: function abort() {
          _c2 && _c2();
        }
      };
    }
  });
  var Qb = [],
      Rb = /(=)\?(?=&|$)|\?\?/;
  r.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function jsonpCallback() {
      var a = Qb.pop() || r.expando + "_" + rb++;
      return this[a] = !0, a;
    }
  }), r.ajaxPrefilter("json jsonp", function (b, c, d) {
    var e,
        f,
        g,
        h = b.jsonp !== !1 && (Rb.test(b.url) ? "url" : "string" == typeof b.data && 0 === (b.contentType || "").indexOf("application/x-www-form-urlencoded") && Rb.test(b.data) && "data");
    if (h || "jsonp" === b.dataTypes[0]) return e = b.jsonpCallback = r.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(Rb, "$1" + e) : b.jsonp !== !1 && (b.url += (sb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function () {
      return g || r.error(e + " was not called"), g[0];
    }, b.dataTypes[0] = "json", f = a[e], a[e] = function () {
      g = arguments;
    }, d.always(function () {
      void 0 === f ? r(a).removeProp(e) : a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, Qb.push(e)), g && r.isFunction(f) && f(g[0]), g = f = void 0;
    }), "script";
  }), o.createHTMLDocument = function () {
    var a = d.implementation.createHTMLDocument("").body;
    return a.innerHTML = "<form></form><form></form>", 2 === a.childNodes.length;
  }(), r.parseHTML = function (a, b, c) {
    if ("string" != typeof a) return [];
    "boolean" == typeof b && (c = b, b = !1);
    var e, f, g;
    return b || (o.createHTMLDocument ? (b = d.implementation.createHTMLDocument(""), e = b.createElement("base"), e.href = d.location.href, b.head.appendChild(e)) : b = d), f = B.exec(a), g = !c && [], f ? [b.createElement(f[1])] : (f = pa([a], b, g), g && g.length && r(g).remove(), r.merge([], f.childNodes));
  }, r.fn.load = function (a, b, c) {
    var d,
        e,
        f,
        g = this,
        h = a.indexOf(" ");
    return h > -1 && (d = mb(a.slice(h)), a = a.slice(0, h)), r.isFunction(b) ? (c = b, b = void 0) : b && "object" == _typeof(b) && (e = "POST"), g.length > 0 && r.ajax({
      url: a,
      type: e || "GET",
      dataType: "html",
      data: b
    }).done(function (a) {
      f = arguments, g.html(d ? r("<div>").append(r.parseHTML(a)).find(d) : a);
    }).always(c && function (a, b) {
      g.each(function () {
        c.apply(this, f || [a.responseText, b, a]);
      });
    }), this;
  }, r.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (a, b) {
    r.fn[b] = function (a) {
      return this.on(b, a);
    };
  }), r.expr.pseudos.animated = function (a) {
    return r.grep(r.timers, function (b) {
      return a === b.elem;
    }).length;
  };

  function Sb(a) {
    return r.isWindow(a) ? a : 9 === a.nodeType && a.defaultView;
  }

  r.offset = {
    setOffset: function setOffset(a, b, c) {
      var d,
          e,
          f,
          g,
          h,
          i,
          j,
          k = r.css(a, "position"),
          l = r(a),
          m = {};
      "static" === k && (a.style.position = "relative"), h = l.offset(), f = r.css(a, "top"), i = r.css(a, "left"), j = ("absolute" === k || "fixed" === k) && (f + i).indexOf("auto") > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), r.isFunction(b) && (b = b.call(a, c, r.extend({}, h))), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m);
    }
  }, r.fn.extend({
    offset: function offset(a) {
      if (arguments.length) return void 0 === a ? this : this.each(function (b) {
        r.offset.setOffset(this, a, b);
      });
      var b,
          c,
          d,
          e,
          f = this[0];
      if (f) return f.getClientRects().length ? (d = f.getBoundingClientRect(), d.width || d.height ? (e = f.ownerDocument, c = Sb(e), b = e.documentElement, {
        top: d.top + c.pageYOffset - b.clientTop,
        left: d.left + c.pageXOffset - b.clientLeft
      }) : d) : {
        top: 0,
        left: 0
      };
    },
    position: function position() {
      if (this[0]) {
        var a,
            b,
            c = this[0],
            d = {
          top: 0,
          left: 0
        };
        return "fixed" === r.css(c, "position") ? b = c.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), r.nodeName(a[0], "html") || (d = a.offset()), d = {
          top: d.top + r.css(a[0], "borderTopWidth", !0),
          left: d.left + r.css(a[0], "borderLeftWidth", !0)
        }), {
          top: b.top - d.top - r.css(c, "marginTop", !0),
          left: b.left - d.left - r.css(c, "marginLeft", !0)
        };
      }
    },
    offsetParent: function offsetParent() {
      return this.map(function () {
        var a = this.offsetParent;

        while (a && "static" === r.css(a, "position")) {
          a = a.offsetParent;
        }

        return a || qa;
      });
    }
  }), r.each({
    scrollLeft: "pageXOffset",
    scrollTop: "pageYOffset"
  }, function (a, b) {
    var c = "pageYOffset" === b;

    r.fn[a] = function (d) {
      return S(this, function (a, d, e) {
        var f = Sb(a);
        return void 0 === e ? f ? f[b] : a[d] : void (f ? f.scrollTo(c ? f.pageXOffset : e, c ? e : f.pageYOffset) : a[d] = e);
      }, a, d, arguments.length);
    };
  }), r.each(["top", "left"], function (a, b) {
    r.cssHooks[b] = Oa(o.pixelPosition, function (a, c) {
      if (c) return c = Na(a, b), La.test(c) ? r(a).position()[b] + "px" : c;
    });
  }), r.each({
    Height: "height",
    Width: "width"
  }, function (a, b) {
    r.each({
      padding: "inner" + a,
      content: b,
      "": "outer" + a
    }, function (c, d) {
      r.fn[d] = function (e, f) {
        var g = arguments.length && (c || "boolean" != typeof e),
            h = c || (e === !0 || f === !0 ? "margin" : "border");
        return S(this, function (b, c, e) {
          var f;
          return r.isWindow(b) ? 0 === d.indexOf("outer") ? b["inner" + a] : b.document.documentElement["client" + a] : 9 === b.nodeType ? (f = b.documentElement, Math.max(b.body["scroll" + a], f["scroll" + a], b.body["offset" + a], f["offset" + a], f["client" + a])) : void 0 === e ? r.css(b, c, h) : r.style(b, c, e, h);
        }, b, g ? e : void 0, g);
      };
    });
  }), r.fn.extend({
    bind: function bind(a, b, c) {
      return this.on(a, null, b, c);
    },
    unbind: function unbind(a, b) {
      return this.off(a, null, b);
    },
    delegate: function delegate(a, b, c, d) {
      return this.on(b, a, c, d);
    },
    undelegate: function undelegate(a, b, c) {
      return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c);
    }
  }), r.parseJSON = JSON.parse,  true && !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
    return r;
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  var Tb = a.jQuery,
      Ub = a.$;
  return r.noConflict = function (b) {
    return a.$ === r && (a.$ = Ub), b && a.jQuery === r && (a.jQuery = Tb), r;
  }, b || (a.jQuery = a.$ = r), r;
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/module.js */ "./node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "./public/bootstrap/js/bootstrap.min.js":
/*!**********************************************!*\
  !*** ./public/bootstrap/js/bootstrap.min.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
  * Bootstrap v4.3.1 (https://getbootstrap.com/)
  * Copyright 2011-2019 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
!function (t, e) {
  "object" == ( false ? undefined : _typeof(exports)) && "undefined" != typeof module ? e(exports, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! popper.js */ "./node_modules/popper.js/dist/esm/popper.js")) :  true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! popper.js */ "./node_modules/popper.js/dist/esm/popper.js")], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function (t, g, u) {
  "use strict";

  function i(t, e) {
    for (var n = 0; n < e.length; n++) {
      var i = e[n];
      i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
    }
  }

  function s(t, e, n) {
    return e && i(t.prototype, e), n && i(t, n), t;
  }

  function l(o) {
    for (var t = 1; t < arguments.length; t++) {
      var r = null != arguments[t] ? arguments[t] : {},
          e = Object.keys(r);
      "function" == typeof Object.getOwnPropertySymbols && (e = e.concat(Object.getOwnPropertySymbols(r).filter(function (t) {
        return Object.getOwnPropertyDescriptor(r, t).enumerable;
      }))), e.forEach(function (t) {
        var e, n, i;
        e = o, i = r[n = t], n in e ? Object.defineProperty(e, n, {
          value: i,
          enumerable: !0,
          configurable: !0,
          writable: !0
        }) : e[n] = i;
      });
    }

    return o;
  }

  g = g && g.hasOwnProperty("default") ? g["default"] : g, u = u && u.hasOwnProperty("default") ? u["default"] : u;
  var e = "transitionend";

  function n(t) {
    var e = this,
        n = !1;
    return g(this).one(_.TRANSITION_END, function () {
      n = !0;
    }), setTimeout(function () {
      n || _.triggerTransitionEnd(e);
    }, t), this;
  }

  var _ = {
    TRANSITION_END: "bsTransitionEnd",
    getUID: function getUID(t) {
      for (; t += ~~(1e6 * Math.random()), document.getElementById(t);) {
        ;
      }

      return t;
    },
    getSelectorFromElement: function getSelectorFromElement(t) {
      var e = t.getAttribute("data-target");

      if (!e || "#" === e) {
        var n = t.getAttribute("href");
        e = n && "#" !== n ? n.trim() : "";
      }

      try {
        return document.querySelector(e) ? e : null;
      } catch (t) {
        return null;
      }
    },
    getTransitionDurationFromElement: function getTransitionDurationFromElement(t) {
      if (!t) return 0;
      var e = g(t).css("transition-duration"),
          n = g(t).css("transition-delay"),
          i = parseFloat(e),
          o = parseFloat(n);
      return i || o ? (e = e.split(",")[0], n = n.split(",")[0], 1e3 * (parseFloat(e) + parseFloat(n))) : 0;
    },
    reflow: function reflow(t) {
      return t.offsetHeight;
    },
    triggerTransitionEnd: function triggerTransitionEnd(t) {
      g(t).trigger(e);
    },
    supportsTransitionEnd: function supportsTransitionEnd() {
      return Boolean(e);
    },
    isElement: function isElement(t) {
      return (t[0] || t).nodeType;
    },
    typeCheckConfig: function typeCheckConfig(t, e, n) {
      for (var i in n) {
        if (Object.prototype.hasOwnProperty.call(n, i)) {
          var o = n[i],
              r = e[i],
              s = r && _.isElement(r) ? "element" : (a = r, {}.toString.call(a).match(/\s([a-z]+)/i)[1].toLowerCase());
          if (!new RegExp(o).test(s)) throw new Error(t.toUpperCase() + ': Option "' + i + '" provided type "' + s + '" but expected type "' + o + '".');
        }
      }

      var a;
    },
    findShadowRoot: function findShadowRoot(t) {
      if (!document.documentElement.attachShadow) return null;
      if ("function" != typeof t.getRootNode) return t instanceof ShadowRoot ? t : t.parentNode ? _.findShadowRoot(t.parentNode) : null;
      var e = t.getRootNode();
      return e instanceof ShadowRoot ? e : null;
    }
  };
  g.fn.emulateTransitionEnd = n, g.event.special[_.TRANSITION_END] = {
    bindType: e,
    delegateType: e,
    handle: function handle(t) {
      if (g(t.target).is(this)) return t.handleObj.handler.apply(this, arguments);
    }
  };

  var o = "alert",
      r = "bs.alert",
      a = "." + r,
      c = g.fn[o],
      h = {
    CLOSE: "close" + a,
    CLOSED: "closed" + a,
    CLICK_DATA_API: "click" + a + ".data-api"
  },
      f = "alert",
      d = "fade",
      m = "show",
      p = function () {
    function i(t) {
      this._element = t;
    }

    var t = i.prototype;
    return t.close = function (t) {
      var e = this._element;
      t && (e = this._getRootElement(t)), this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e);
    }, t.dispose = function () {
      g.removeData(this._element, r), this._element = null;
    }, t._getRootElement = function (t) {
      var e = _.getSelectorFromElement(t),
          n = !1;

      return e && (n = document.querySelector(e)), n || (n = g(t).closest("." + f)[0]), n;
    }, t._triggerCloseEvent = function (t) {
      var e = g.Event(h.CLOSE);
      return g(t).trigger(e), e;
    }, t._removeElement = function (e) {
      var n = this;

      if (g(e).removeClass(m), g(e).hasClass(d)) {
        var t = _.getTransitionDurationFromElement(e);

        g(e).one(_.TRANSITION_END, function (t) {
          return n._destroyElement(e, t);
        }).emulateTransitionEnd(t);
      } else this._destroyElement(e);
    }, t._destroyElement = function (t) {
      g(t).detach().trigger(h.CLOSED).remove();
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = g(this),
            e = t.data(r);
        e || (e = new i(this), t.data(r, e)), "close" === n && e[n](this);
      });
    }, i._handleDismiss = function (e) {
      return function (t) {
        t && t.preventDefault(), e.close(this);
      };
    }, s(i, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }]), i;
  }();

  g(document).on(h.CLICK_DATA_API, '[data-dismiss="alert"]', p._handleDismiss(new p())), g.fn[o] = p._jQueryInterface, g.fn[o].Constructor = p, g.fn[o].noConflict = function () {
    return g.fn[o] = c, p._jQueryInterface;
  };

  var v = "button",
      y = "bs.button",
      E = "." + y,
      C = ".data-api",
      T = g.fn[v],
      S = "active",
      b = "btn",
      I = "focus",
      D = '[data-toggle^="button"]',
      w = '[data-toggle="buttons"]',
      A = 'input:not([type="hidden"])',
      N = ".active",
      O = ".btn",
      k = {
    CLICK_DATA_API: "click" + E + C,
    FOCUS_BLUR_DATA_API: "focus" + E + C + " blur" + E + C
  },
      P = function () {
    function n(t) {
      this._element = t;
    }

    var t = n.prototype;
    return t.toggle = function () {
      var t = !0,
          e = !0,
          n = g(this._element).closest(w)[0];

      if (n) {
        var i = this._element.querySelector(A);

        if (i) {
          if ("radio" === i.type) if (i.checked && this._element.classList.contains(S)) t = !1;else {
            var o = n.querySelector(N);
            o && g(o).removeClass(S);
          }

          if (t) {
            if (i.hasAttribute("disabled") || n.hasAttribute("disabled") || i.classList.contains("disabled") || n.classList.contains("disabled")) return;
            i.checked = !this._element.classList.contains(S), g(i).trigger("change");
          }

          i.focus(), e = !1;
        }
      }

      e && this._element.setAttribute("aria-pressed", !this._element.classList.contains(S)), t && g(this._element).toggleClass(S);
    }, t.dispose = function () {
      g.removeData(this._element, y), this._element = null;
    }, n._jQueryInterface = function (e) {
      return this.each(function () {
        var t = g(this).data(y);
        t || (t = new n(this), g(this).data(y, t)), "toggle" === e && t[e]();
      });
    }, s(n, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }]), n;
  }();

  g(document).on(k.CLICK_DATA_API, D, function (t) {
    t.preventDefault();
    var e = t.target;
    g(e).hasClass(b) || (e = g(e).closest(O)), P._jQueryInterface.call(g(e), "toggle");
  }).on(k.FOCUS_BLUR_DATA_API, D, function (t) {
    var e = g(t.target).closest(O)[0];
    g(e).toggleClass(I, /^focus(in)?$/.test(t.type));
  }), g.fn[v] = P._jQueryInterface, g.fn[v].Constructor = P, g.fn[v].noConflict = function () {
    return g.fn[v] = T, P._jQueryInterface;
  };

  var L = "carousel",
      j = "bs.carousel",
      H = "." + j,
      R = ".data-api",
      x = g.fn[L],
      F = {
    interval: 5e3,
    keyboard: !0,
    slide: !1,
    pause: "hover",
    wrap: !0,
    touch: !0
  },
      U = {
    interval: "(number|boolean)",
    keyboard: "boolean",
    slide: "(boolean|string)",
    pause: "(string|boolean)",
    wrap: "boolean",
    touch: "boolean"
  },
      W = "next",
      q = "prev",
      M = "left",
      K = "right",
      Q = {
    SLIDE: "slide" + H,
    SLID: "slid" + H,
    KEYDOWN: "keydown" + H,
    MOUSEENTER: "mouseenter" + H,
    MOUSELEAVE: "mouseleave" + H,
    TOUCHSTART: "touchstart" + H,
    TOUCHMOVE: "touchmove" + H,
    TOUCHEND: "touchend" + H,
    POINTERDOWN: "pointerdown" + H,
    POINTERUP: "pointerup" + H,
    DRAG_START: "dragstart" + H,
    LOAD_DATA_API: "load" + H + R,
    CLICK_DATA_API: "click" + H + R
  },
      B = "carousel",
      V = "active",
      Y = "slide",
      z = "carousel-item-right",
      X = "carousel-item-left",
      $ = "carousel-item-next",
      G = "carousel-item-prev",
      J = "pointer-event",
      Z = ".active",
      tt = ".active.carousel-item",
      et = ".carousel-item",
      nt = ".carousel-item img",
      it = ".carousel-item-next, .carousel-item-prev",
      ot = ".carousel-indicators",
      rt = "[data-slide], [data-slide-to]",
      st = '[data-ride="carousel"]',
      at = {
    TOUCH: "touch",
    PEN: "pen"
  },
      lt = function () {
    function r(t, e) {
      this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this.touchStartX = 0, this.touchDeltaX = 0, this._config = this._getConfig(e), this._element = t, this._indicatorsElement = this._element.querySelector(ot), this._touchSupported = "ontouchstart" in document.documentElement || 0 < navigator.maxTouchPoints, this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent), this._addEventListeners();
    }

    var t = r.prototype;
    return t.next = function () {
      this._isSliding || this._slide(W);
    }, t.nextWhenVisible = function () {
      !document.hidden && g(this._element).is(":visible") && "hidden" !== g(this._element).css("visibility") && this.next();
    }, t.prev = function () {
      this._isSliding || this._slide(q);
    }, t.pause = function (t) {
      t || (this._isPaused = !0), this._element.querySelector(it) && (_.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null;
    }, t.cycle = function (t) {
      t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval));
    }, t.to = function (t) {
      var e = this;
      this._activeElement = this._element.querySelector(tt);

      var n = this._getItemIndex(this._activeElement);

      if (!(t > this._items.length - 1 || t < 0)) if (this._isSliding) g(this._element).one(Q.SLID, function () {
        return e.to(t);
      });else {
        if (n === t) return this.pause(), void this.cycle();
        var i = n < t ? W : q;

        this._slide(i, this._items[t]);
      }
    }, t.dispose = function () {
      g(this._element).off(H), g.removeData(this._element, j), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null;
    }, t._getConfig = function (t) {
      return t = l({}, F, t), _.typeCheckConfig(L, t, U), t;
    }, t._handleSwipe = function () {
      var t = Math.abs(this.touchDeltaX);

      if (!(t <= 40)) {
        var e = t / this.touchDeltaX;
        0 < e && this.prev(), e < 0 && this.next();
      }
    }, t._addEventListeners = function () {
      var e = this;
      this._config.keyboard && g(this._element).on(Q.KEYDOWN, function (t) {
        return e._keydown(t);
      }), "hover" === this._config.pause && g(this._element).on(Q.MOUSEENTER, function (t) {
        return e.pause(t);
      }).on(Q.MOUSELEAVE, function (t) {
        return e.cycle(t);
      }), this._config.touch && this._addTouchEventListeners();
    }, t._addTouchEventListeners = function () {
      var n = this;

      if (this._touchSupported) {
        var e = function e(t) {
          n._pointerEvent && at[t.originalEvent.pointerType.toUpperCase()] ? n.touchStartX = t.originalEvent.clientX : n._pointerEvent || (n.touchStartX = t.originalEvent.touches[0].clientX);
        },
            i = function i(t) {
          n._pointerEvent && at[t.originalEvent.pointerType.toUpperCase()] && (n.touchDeltaX = t.originalEvent.clientX - n.touchStartX), n._handleSwipe(), "hover" === n._config.pause && (n.pause(), n.touchTimeout && clearTimeout(n.touchTimeout), n.touchTimeout = setTimeout(function (t) {
            return n.cycle(t);
          }, 500 + n._config.interval));
        };

        g(this._element.querySelectorAll(nt)).on(Q.DRAG_START, function (t) {
          return t.preventDefault();
        }), this._pointerEvent ? (g(this._element).on(Q.POINTERDOWN, function (t) {
          return e(t);
        }), g(this._element).on(Q.POINTERUP, function (t) {
          return i(t);
        }), this._element.classList.add(J)) : (g(this._element).on(Q.TOUCHSTART, function (t) {
          return e(t);
        }), g(this._element).on(Q.TOUCHMOVE, function (t) {
          var e;
          (e = t).originalEvent.touches && 1 < e.originalEvent.touches.length ? n.touchDeltaX = 0 : n.touchDeltaX = e.originalEvent.touches[0].clientX - n.touchStartX;
        }), g(this._element).on(Q.TOUCHEND, function (t) {
          return i(t);
        }));
      }
    }, t._keydown = function (t) {
      if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {
        case 37:
          t.preventDefault(), this.prev();
          break;

        case 39:
          t.preventDefault(), this.next();
      }
    }, t._getItemIndex = function (t) {
      return this._items = t && t.parentNode ? [].slice.call(t.parentNode.querySelectorAll(et)) : [], this._items.indexOf(t);
    }, t._getItemByDirection = function (t, e) {
      var n = t === W,
          i = t === q,
          o = this._getItemIndex(e),
          r = this._items.length - 1;

      if ((i && 0 === o || n && o === r) && !this._config.wrap) return e;
      var s = (o + (t === q ? -1 : 1)) % this._items.length;
      return -1 === s ? this._items[this._items.length - 1] : this._items[s];
    }, t._triggerSlideEvent = function (t, e) {
      var n = this._getItemIndex(t),
          i = this._getItemIndex(this._element.querySelector(tt)),
          o = g.Event(Q.SLIDE, {
        relatedTarget: t,
        direction: e,
        from: i,
        to: n
      });

      return g(this._element).trigger(o), o;
    }, t._setActiveIndicatorElement = function (t) {
      if (this._indicatorsElement) {
        var e = [].slice.call(this._indicatorsElement.querySelectorAll(Z));
        g(e).removeClass(V);

        var n = this._indicatorsElement.children[this._getItemIndex(t)];

        n && g(n).addClass(V);
      }
    }, t._slide = function (t, e) {
      var n,
          i,
          o,
          r = this,
          s = this._element.querySelector(tt),
          a = this._getItemIndex(s),
          l = e || s && this._getItemByDirection(t, s),
          c = this._getItemIndex(l),
          h = Boolean(this._interval);

      if (o = t === W ? (n = X, i = $, M) : (n = z, i = G, K), l && g(l).hasClass(V)) this._isSliding = !1;else if (!this._triggerSlideEvent(l, o).isDefaultPrevented() && s && l) {
        this._isSliding = !0, h && this.pause(), this._setActiveIndicatorElement(l);
        var u = g.Event(Q.SLID, {
          relatedTarget: l,
          direction: o,
          from: a,
          to: c
        });

        if (g(this._element).hasClass(Y)) {
          g(l).addClass(i), _.reflow(l), g(s).addClass(n), g(l).addClass(n);
          var f = parseInt(l.getAttribute("data-interval"), 10);
          this._config.interval = f ? (this._config.defaultInterval = this._config.defaultInterval || this._config.interval, f) : this._config.defaultInterval || this._config.interval;

          var d = _.getTransitionDurationFromElement(s);

          g(s).one(_.TRANSITION_END, function () {
            g(l).removeClass(n + " " + i).addClass(V), g(s).removeClass(V + " " + i + " " + n), r._isSliding = !1, setTimeout(function () {
              return g(r._element).trigger(u);
            }, 0);
          }).emulateTransitionEnd(d);
        } else g(s).removeClass(V), g(l).addClass(V), this._isSliding = !1, g(this._element).trigger(u);

        h && this.cycle();
      }
    }, r._jQueryInterface = function (i) {
      return this.each(function () {
        var t = g(this).data(j),
            e = l({}, F, g(this).data());
        "object" == _typeof(i) && (e = l({}, e, i));
        var n = "string" == typeof i ? i : e.slide;
        if (t || (t = new r(this, e), g(this).data(j, t)), "number" == typeof i) t.to(i);else if ("string" == typeof n) {
          if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
          t[n]();
        } else e.interval && e.ride && (t.pause(), t.cycle());
      });
    }, r._dataApiClickHandler = function (t) {
      var e = _.getSelectorFromElement(this);

      if (e) {
        var n = g(e)[0];

        if (n && g(n).hasClass(B)) {
          var i = l({}, g(n).data(), g(this).data()),
              o = this.getAttribute("data-slide-to");
          o && (i.interval = !1), r._jQueryInterface.call(g(n), i), o && g(n).data(j).to(o), t.preventDefault();
        }
      }
    }, s(r, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return F;
      }
    }]), r;
  }();

  g(document).on(Q.CLICK_DATA_API, rt, lt._dataApiClickHandler), g(window).on(Q.LOAD_DATA_API, function () {
    for (var t = [].slice.call(document.querySelectorAll(st)), e = 0, n = t.length; e < n; e++) {
      var i = g(t[e]);

      lt._jQueryInterface.call(i, i.data());
    }
  }), g.fn[L] = lt._jQueryInterface, g.fn[L].Constructor = lt, g.fn[L].noConflict = function () {
    return g.fn[L] = x, lt._jQueryInterface;
  };

  var ct = "collapse",
      ht = "bs.collapse",
      ut = "." + ht,
      ft = g.fn[ct],
      dt = {
    toggle: !0,
    parent: ""
  },
      gt = {
    toggle: "boolean",
    parent: "(string|element)"
  },
      _t = {
    SHOW: "show" + ut,
    SHOWN: "shown" + ut,
    HIDE: "hide" + ut,
    HIDDEN: "hidden" + ut,
    CLICK_DATA_API: "click" + ut + ".data-api"
  },
      mt = "show",
      pt = "collapse",
      vt = "collapsing",
      yt = "collapsed",
      Et = "width",
      Ct = "height",
      Tt = ".show, .collapsing",
      St = '[data-toggle="collapse"]',
      bt = function () {
    function a(e, t) {
      this._isTransitioning = !1, this._element = e, this._config = this._getConfig(t), this._triggerArray = [].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'));

      for (var n = [].slice.call(document.querySelectorAll(St)), i = 0, o = n.length; i < o; i++) {
        var r = n[i],
            s = _.getSelectorFromElement(r),
            a = [].slice.call(document.querySelectorAll(s)).filter(function (t) {
          return t === e;
        });

        null !== s && 0 < a.length && (this._selector = s, this._triggerArray.push(r));
      }

      this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle();
    }

    var t = a.prototype;
    return t.toggle = function () {
      g(this._element).hasClass(mt) ? this.hide() : this.show();
    }, t.show = function () {
      var t,
          e,
          n = this;

      if (!this._isTransitioning && !g(this._element).hasClass(mt) && (this._parent && 0 === (t = [].slice.call(this._parent.querySelectorAll(Tt)).filter(function (t) {
        return "string" == typeof n._config.parent ? t.getAttribute("data-parent") === n._config.parent : t.classList.contains(pt);
      })).length && (t = null), !(t && (e = g(t).not(this._selector).data(ht)) && e._isTransitioning))) {
        var i = g.Event(_t.SHOW);

        if (g(this._element).trigger(i), !i.isDefaultPrevented()) {
          t && (a._jQueryInterface.call(g(t).not(this._selector), "hide"), e || g(t).data(ht, null));

          var o = this._getDimension();

          g(this._element).removeClass(pt).addClass(vt), this._element.style[o] = 0, this._triggerArray.length && g(this._triggerArray).removeClass(yt).attr("aria-expanded", !0), this.setTransitioning(!0);

          var r = "scroll" + (o[0].toUpperCase() + o.slice(1)),
              s = _.getTransitionDurationFromElement(this._element);

          g(this._element).one(_.TRANSITION_END, function () {
            g(n._element).removeClass(vt).addClass(pt).addClass(mt), n._element.style[o] = "", n.setTransitioning(!1), g(n._element).trigger(_t.SHOWN);
          }).emulateTransitionEnd(s), this._element.style[o] = this._element[r] + "px";
        }
      }
    }, t.hide = function () {
      var t = this;

      if (!this._isTransitioning && g(this._element).hasClass(mt)) {
        var e = g.Event(_t.HIDE);

        if (g(this._element).trigger(e), !e.isDefaultPrevented()) {
          var n = this._getDimension();

          this._element.style[n] = this._element.getBoundingClientRect()[n] + "px", _.reflow(this._element), g(this._element).addClass(vt).removeClass(pt).removeClass(mt);
          var i = this._triggerArray.length;
          if (0 < i) for (var o = 0; o < i; o++) {
            var r = this._triggerArray[o],
                s = _.getSelectorFromElement(r);

            if (null !== s) g([].slice.call(document.querySelectorAll(s))).hasClass(mt) || g(r).addClass(yt).attr("aria-expanded", !1);
          }
          this.setTransitioning(!0);
          this._element.style[n] = "";

          var a = _.getTransitionDurationFromElement(this._element);

          g(this._element).one(_.TRANSITION_END, function () {
            t.setTransitioning(!1), g(t._element).removeClass(vt).addClass(pt).trigger(_t.HIDDEN);
          }).emulateTransitionEnd(a);
        }
      }
    }, t.setTransitioning = function (t) {
      this._isTransitioning = t;
    }, t.dispose = function () {
      g.removeData(this._element, ht), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null;
    }, t._getConfig = function (t) {
      return (t = l({}, dt, t)).toggle = Boolean(t.toggle), _.typeCheckConfig(ct, t, gt), t;
    }, t._getDimension = function () {
      return g(this._element).hasClass(Et) ? Et : Ct;
    }, t._getParent = function () {
      var t,
          n = this;
      _.isElement(this._config.parent) ? (t = this._config.parent, "undefined" != typeof this._config.parent.jquery && (t = this._config.parent[0])) : t = document.querySelector(this._config.parent);
      var e = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]',
          i = [].slice.call(t.querySelectorAll(e));
      return g(i).each(function (t, e) {
        n._addAriaAndCollapsedClass(a._getTargetFromElement(e), [e]);
      }), t;
    }, t._addAriaAndCollapsedClass = function (t, e) {
      var n = g(t).hasClass(mt);
      e.length && g(e).toggleClass(yt, !n).attr("aria-expanded", n);
    }, a._getTargetFromElement = function (t) {
      var e = _.getSelectorFromElement(t);

      return e ? document.querySelector(e) : null;
    }, a._jQueryInterface = function (i) {
      return this.each(function () {
        var t = g(this),
            e = t.data(ht),
            n = l({}, dt, t.data(), "object" == _typeof(i) && i ? i : {});

        if (!e && n.toggle && /show|hide/.test(i) && (n.toggle = !1), e || (e = new a(this, n), t.data(ht, e)), "string" == typeof i) {
          if ("undefined" == typeof e[i]) throw new TypeError('No method named "' + i + '"');
          e[i]();
        }
      });
    }, s(a, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return dt;
      }
    }]), a;
  }();

  g(document).on(_t.CLICK_DATA_API, St, function (t) {
    "A" === t.currentTarget.tagName && t.preventDefault();

    var n = g(this),
        e = _.getSelectorFromElement(this),
        i = [].slice.call(document.querySelectorAll(e));

    g(i).each(function () {
      var t = g(this),
          e = t.data(ht) ? "toggle" : n.data();

      bt._jQueryInterface.call(t, e);
    });
  }), g.fn[ct] = bt._jQueryInterface, g.fn[ct].Constructor = bt, g.fn[ct].noConflict = function () {
    return g.fn[ct] = ft, bt._jQueryInterface;
  };

  var It = "dropdown",
      Dt = "bs.dropdown",
      wt = "." + Dt,
      At = ".data-api",
      Nt = g.fn[It],
      Ot = new RegExp("38|40|27"),
      kt = {
    HIDE: "hide" + wt,
    HIDDEN: "hidden" + wt,
    SHOW: "show" + wt,
    SHOWN: "shown" + wt,
    CLICK: "click" + wt,
    CLICK_DATA_API: "click" + wt + At,
    KEYDOWN_DATA_API: "keydown" + wt + At,
    KEYUP_DATA_API: "keyup" + wt + At
  },
      Pt = "disabled",
      Lt = "show",
      jt = "dropup",
      Ht = "dropright",
      Rt = "dropleft",
      xt = "dropdown-menu-right",
      Ft = "position-static",
      Ut = '[data-toggle="dropdown"]',
      Wt = ".dropdown form",
      qt = ".dropdown-menu",
      Mt = ".navbar-nav",
      Kt = ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)",
      Qt = "top-start",
      Bt = "top-end",
      Vt = "bottom-start",
      Yt = "bottom-end",
      zt = "right-start",
      Xt = "left-start",
      $t = {
    offset: 0,
    flip: !0,
    boundary: "scrollParent",
    reference: "toggle",
    display: "dynamic"
  },
      Gt = {
    offset: "(number|string|function)",
    flip: "boolean",
    boundary: "(string|element)",
    reference: "(string|element)",
    display: "string"
  },
      Jt = function () {
    function c(t, e) {
      this._element = t, this._popper = null, this._config = this._getConfig(e), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners();
    }

    var t = c.prototype;
    return t.toggle = function () {
      if (!this._element.disabled && !g(this._element).hasClass(Pt)) {
        var t = c._getParentFromElement(this._element),
            e = g(this._menu).hasClass(Lt);

        if (c._clearMenus(), !e) {
          var n = {
            relatedTarget: this._element
          },
              i = g.Event(kt.SHOW, n);

          if (g(t).trigger(i), !i.isDefaultPrevented()) {
            if (!this._inNavbar) {
              if ("undefined" == typeof u) throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");
              var o = this._element;
              "parent" === this._config.reference ? o = t : _.isElement(this._config.reference) && (o = this._config.reference, "undefined" != typeof this._config.reference.jquery && (o = this._config.reference[0])), "scrollParent" !== this._config.boundary && g(t).addClass(Ft), this._popper = new u(o, this._menu, this._getPopperConfig());
            }

            "ontouchstart" in document.documentElement && 0 === g(t).closest(Mt).length && g(document.body).children().on("mouseover", null, g.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), g(this._menu).toggleClass(Lt), g(t).toggleClass(Lt).trigger(g.Event(kt.SHOWN, n));
          }
        }
      }
    }, t.show = function () {
      if (!(this._element.disabled || g(this._element).hasClass(Pt) || g(this._menu).hasClass(Lt))) {
        var t = {
          relatedTarget: this._element
        },
            e = g.Event(kt.SHOW, t),
            n = c._getParentFromElement(this._element);

        g(n).trigger(e), e.isDefaultPrevented() || (g(this._menu).toggleClass(Lt), g(n).toggleClass(Lt).trigger(g.Event(kt.SHOWN, t)));
      }
    }, t.hide = function () {
      if (!this._element.disabled && !g(this._element).hasClass(Pt) && g(this._menu).hasClass(Lt)) {
        var t = {
          relatedTarget: this._element
        },
            e = g.Event(kt.HIDE, t),
            n = c._getParentFromElement(this._element);

        g(n).trigger(e), e.isDefaultPrevented() || (g(this._menu).toggleClass(Lt), g(n).toggleClass(Lt).trigger(g.Event(kt.HIDDEN, t)));
      }
    }, t.dispose = function () {
      g.removeData(this._element, Dt), g(this._element).off(wt), this._element = null, (this._menu = null) !== this._popper && (this._popper.destroy(), this._popper = null);
    }, t.update = function () {
      this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate();
    }, t._addEventListeners = function () {
      var e = this;
      g(this._element).on(kt.CLICK, function (t) {
        t.preventDefault(), t.stopPropagation(), e.toggle();
      });
    }, t._getConfig = function (t) {
      return t = l({}, this.constructor.Default, g(this._element).data(), t), _.typeCheckConfig(It, t, this.constructor.DefaultType), t;
    }, t._getMenuElement = function () {
      if (!this._menu) {
        var t = c._getParentFromElement(this._element);

        t && (this._menu = t.querySelector(qt));
      }

      return this._menu;
    }, t._getPlacement = function () {
      var t = g(this._element.parentNode),
          e = Vt;
      return t.hasClass(jt) ? (e = Qt, g(this._menu).hasClass(xt) && (e = Bt)) : t.hasClass(Ht) ? e = zt : t.hasClass(Rt) ? e = Xt : g(this._menu).hasClass(xt) && (e = Yt), e;
    }, t._detectNavbar = function () {
      return 0 < g(this._element).closest(".navbar").length;
    }, t._getOffset = function () {
      var e = this,
          t = {};
      return "function" == typeof this._config.offset ? t.fn = function (t) {
        return t.offsets = l({}, t.offsets, e._config.offset(t.offsets, e._element) || {}), t;
      } : t.offset = this._config.offset, t;
    }, t._getPopperConfig = function () {
      var t = {
        placement: this._getPlacement(),
        modifiers: {
          offset: this._getOffset(),
          flip: {
            enabled: this._config.flip
          },
          preventOverflow: {
            boundariesElement: this._config.boundary
          }
        }
      };
      return "static" === this._config.display && (t.modifiers.applyStyle = {
        enabled: !1
      }), t;
    }, c._jQueryInterface = function (e) {
      return this.each(function () {
        var t = g(this).data(Dt);

        if (t || (t = new c(this, "object" == _typeof(e) ? e : null), g(this).data(Dt, t)), "string" == typeof e) {
          if ("undefined" == typeof t[e]) throw new TypeError('No method named "' + e + '"');
          t[e]();
        }
      });
    }, c._clearMenus = function (t) {
      if (!t || 3 !== t.which && ("keyup" !== t.type || 9 === t.which)) for (var e = [].slice.call(document.querySelectorAll(Ut)), n = 0, i = e.length; n < i; n++) {
        var o = c._getParentFromElement(e[n]),
            r = g(e[n]).data(Dt),
            s = {
          relatedTarget: e[n]
        };

        if (t && "click" === t.type && (s.clickEvent = t), r) {
          var a = r._menu;

          if (g(o).hasClass(Lt) && !(t && ("click" === t.type && /input|textarea/i.test(t.target.tagName) || "keyup" === t.type && 9 === t.which) && g.contains(o, t.target))) {
            var l = g.Event(kt.HIDE, s);
            g(o).trigger(l), l.isDefaultPrevented() || ("ontouchstart" in document.documentElement && g(document.body).children().off("mouseover", null, g.noop), e[n].setAttribute("aria-expanded", "false"), g(a).removeClass(Lt), g(o).removeClass(Lt).trigger(g.Event(kt.HIDDEN, s)));
          }
        }
      }
    }, c._getParentFromElement = function (t) {
      var e,
          n = _.getSelectorFromElement(t);

      return n && (e = document.querySelector(n)), e || t.parentNode;
    }, c._dataApiKeydownHandler = function (t) {
      if ((/input|textarea/i.test(t.target.tagName) ? !(32 === t.which || 27 !== t.which && (40 !== t.which && 38 !== t.which || g(t.target).closest(qt).length)) : Ot.test(t.which)) && (t.preventDefault(), t.stopPropagation(), !this.disabled && !g(this).hasClass(Pt))) {
        var e = c._getParentFromElement(this),
            n = g(e).hasClass(Lt);

        if (n && (!n || 27 !== t.which && 32 !== t.which)) {
          var i = [].slice.call(e.querySelectorAll(Kt));

          if (0 !== i.length) {
            var o = i.indexOf(t.target);
            38 === t.which && 0 < o && o--, 40 === t.which && o < i.length - 1 && o++, o < 0 && (o = 0), i[o].focus();
          }
        } else {
          if (27 === t.which) {
            var r = e.querySelector(Ut);
            g(r).trigger("focus");
          }

          g(this).trigger("click");
        }
      }
    }, s(c, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return $t;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return Gt;
      }
    }]), c;
  }();

  g(document).on(kt.KEYDOWN_DATA_API, Ut, Jt._dataApiKeydownHandler).on(kt.KEYDOWN_DATA_API, qt, Jt._dataApiKeydownHandler).on(kt.CLICK_DATA_API + " " + kt.KEYUP_DATA_API, Jt._clearMenus).on(kt.CLICK_DATA_API, Ut, function (t) {
    t.preventDefault(), t.stopPropagation(), Jt._jQueryInterface.call(g(this), "toggle");
  }).on(kt.CLICK_DATA_API, Wt, function (t) {
    t.stopPropagation();
  }), g.fn[It] = Jt._jQueryInterface, g.fn[It].Constructor = Jt, g.fn[It].noConflict = function () {
    return g.fn[It] = Nt, Jt._jQueryInterface;
  };

  var Zt = "modal",
      te = "bs.modal",
      ee = "." + te,
      ne = g.fn[Zt],
      ie = {
    backdrop: !0,
    keyboard: !0,
    focus: !0,
    show: !0
  },
      oe = {
    backdrop: "(boolean|string)",
    keyboard: "boolean",
    focus: "boolean",
    show: "boolean"
  },
      re = {
    HIDE: "hide" + ee,
    HIDDEN: "hidden" + ee,
    SHOW: "show" + ee,
    SHOWN: "shown" + ee,
    FOCUSIN: "focusin" + ee,
    RESIZE: "resize" + ee,
    CLICK_DISMISS: "click.dismiss" + ee,
    KEYDOWN_DISMISS: "keydown.dismiss" + ee,
    MOUSEUP_DISMISS: "mouseup.dismiss" + ee,
    MOUSEDOWN_DISMISS: "mousedown.dismiss" + ee,
    CLICK_DATA_API: "click" + ee + ".data-api"
  },
      se = "modal-dialog-scrollable",
      ae = "modal-scrollbar-measure",
      le = "modal-backdrop",
      ce = "modal-open",
      he = "fade",
      ue = "show",
      fe = ".modal-dialog",
      de = ".modal-body",
      ge = '[data-toggle="modal"]',
      _e = '[data-dismiss="modal"]',
      me = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
      pe = ".sticky-top",
      ve = function () {
    function o(t, e) {
      this._config = this._getConfig(e), this._element = t, this._dialog = t.querySelector(fe), this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._isTransitioning = !1, this._scrollbarWidth = 0;
    }

    var t = o.prototype;
    return t.toggle = function (t) {
      return this._isShown ? this.hide() : this.show(t);
    }, t.show = function (t) {
      var e = this;

      if (!this._isShown && !this._isTransitioning) {
        g(this._element).hasClass(he) && (this._isTransitioning = !0);
        var n = g.Event(re.SHOW, {
          relatedTarget: t
        });
        g(this._element).trigger(n), this._isShown || n.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), this._setEscapeEvent(), this._setResizeEvent(), g(this._element).on(re.CLICK_DISMISS, _e, function (t) {
          return e.hide(t);
        }), g(this._dialog).on(re.MOUSEDOWN_DISMISS, function () {
          g(e._element).one(re.MOUSEUP_DISMISS, function (t) {
            g(t.target).is(e._element) && (e._ignoreBackdropClick = !0);
          });
        }), this._showBackdrop(function () {
          return e._showElement(t);
        }));
      }
    }, t.hide = function (t) {
      var e = this;

      if (t && t.preventDefault(), this._isShown && !this._isTransitioning) {
        var n = g.Event(re.HIDE);

        if (g(this._element).trigger(n), this._isShown && !n.isDefaultPrevented()) {
          this._isShown = !1;
          var i = g(this._element).hasClass(he);

          if (i && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), g(document).off(re.FOCUSIN), g(this._element).removeClass(ue), g(this._element).off(re.CLICK_DISMISS), g(this._dialog).off(re.MOUSEDOWN_DISMISS), i) {
            var o = _.getTransitionDurationFromElement(this._element);

            g(this._element).one(_.TRANSITION_END, function (t) {
              return e._hideModal(t);
            }).emulateTransitionEnd(o);
          } else this._hideModal();
        }
      }
    }, t.dispose = function () {
      [window, this._element, this._dialog].forEach(function (t) {
        return g(t).off(ee);
      }), g(document).off(re.FOCUSIN), g.removeData(this._element, te), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._isTransitioning = null, this._scrollbarWidth = null;
    }, t.handleUpdate = function () {
      this._adjustDialog();
    }, t._getConfig = function (t) {
      return t = l({}, ie, t), _.typeCheckConfig(Zt, t, oe), t;
    }, t._showElement = function (t) {
      var e = this,
          n = g(this._element).hasClass(he);
      this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.setAttribute("aria-modal", !0), g(this._dialog).hasClass(se) ? this._dialog.querySelector(de).scrollTop = 0 : this._element.scrollTop = 0, n && _.reflow(this._element), g(this._element).addClass(ue), this._config.focus && this._enforceFocus();

      var i = g.Event(re.SHOWN, {
        relatedTarget: t
      }),
          o = function o() {
        e._config.focus && e._element.focus(), e._isTransitioning = !1, g(e._element).trigger(i);
      };

      if (n) {
        var r = _.getTransitionDurationFromElement(this._dialog);

        g(this._dialog).one(_.TRANSITION_END, o).emulateTransitionEnd(r);
      } else o();
    }, t._enforceFocus = function () {
      var e = this;
      g(document).off(re.FOCUSIN).on(re.FOCUSIN, function (t) {
        document !== t.target && e._element !== t.target && 0 === g(e._element).has(t.target).length && e._element.focus();
      });
    }, t._setEscapeEvent = function () {
      var e = this;
      this._isShown && this._config.keyboard ? g(this._element).on(re.KEYDOWN_DISMISS, function (t) {
        27 === t.which && (t.preventDefault(), e.hide());
      }) : this._isShown || g(this._element).off(re.KEYDOWN_DISMISS);
    }, t._setResizeEvent = function () {
      var e = this;
      this._isShown ? g(window).on(re.RESIZE, function (t) {
        return e.handleUpdate(t);
      }) : g(window).off(re.RESIZE);
    }, t._hideModal = function () {
      var t = this;
      this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._element.removeAttribute("aria-modal"), this._isTransitioning = !1, this._showBackdrop(function () {
        g(document.body).removeClass(ce), t._resetAdjustments(), t._resetScrollbar(), g(t._element).trigger(re.HIDDEN);
      });
    }, t._removeBackdrop = function () {
      this._backdrop && (g(this._backdrop).remove(), this._backdrop = null);
    }, t._showBackdrop = function (t) {
      var e = this,
          n = g(this._element).hasClass(he) ? he : "";

      if (this._isShown && this._config.backdrop) {
        if (this._backdrop = document.createElement("div"), this._backdrop.className = le, n && this._backdrop.classList.add(n), g(this._backdrop).appendTo(document.body), g(this._element).on(re.CLICK_DISMISS, function (t) {
          e._ignoreBackdropClick ? e._ignoreBackdropClick = !1 : t.target === t.currentTarget && ("static" === e._config.backdrop ? e._element.focus() : e.hide());
        }), n && _.reflow(this._backdrop), g(this._backdrop).addClass(ue), !t) return;
        if (!n) return void t();

        var i = _.getTransitionDurationFromElement(this._backdrop);

        g(this._backdrop).one(_.TRANSITION_END, t).emulateTransitionEnd(i);
      } else if (!this._isShown && this._backdrop) {
        g(this._backdrop).removeClass(ue);

        var o = function o() {
          e._removeBackdrop(), t && t();
        };

        if (g(this._element).hasClass(he)) {
          var r = _.getTransitionDurationFromElement(this._backdrop);

          g(this._backdrop).one(_.TRANSITION_END, o).emulateTransitionEnd(r);
        } else o();
      } else t && t();
    }, t._adjustDialog = function () {
      var t = this._element.scrollHeight > document.documentElement.clientHeight;
      !this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px");
    }, t._resetAdjustments = function () {
      this._element.style.paddingLeft = "", this._element.style.paddingRight = "";
    }, t._checkScrollbar = function () {
      var t = document.body.getBoundingClientRect();
      this._isBodyOverflowing = t.left + t.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth();
    }, t._setScrollbar = function () {
      var o = this;

      if (this._isBodyOverflowing) {
        var t = [].slice.call(document.querySelectorAll(me)),
            e = [].slice.call(document.querySelectorAll(pe));
        g(t).each(function (t, e) {
          var n = e.style.paddingRight,
              i = g(e).css("padding-right");
          g(e).data("padding-right", n).css("padding-right", parseFloat(i) + o._scrollbarWidth + "px");
        }), g(e).each(function (t, e) {
          var n = e.style.marginRight,
              i = g(e).css("margin-right");
          g(e).data("margin-right", n).css("margin-right", parseFloat(i) - o._scrollbarWidth + "px");
        });
        var n = document.body.style.paddingRight,
            i = g(document.body).css("padding-right");
        g(document.body).data("padding-right", n).css("padding-right", parseFloat(i) + this._scrollbarWidth + "px");
      }

      g(document.body).addClass(ce);
    }, t._resetScrollbar = function () {
      var t = [].slice.call(document.querySelectorAll(me));
      g(t).each(function (t, e) {
        var n = g(e).data("padding-right");
        g(e).removeData("padding-right"), e.style.paddingRight = n || "";
      });
      var e = [].slice.call(document.querySelectorAll("" + pe));
      g(e).each(function (t, e) {
        var n = g(e).data("margin-right");
        "undefined" != typeof n && g(e).css("margin-right", n).removeData("margin-right");
      });
      var n = g(document.body).data("padding-right");
      g(document.body).removeData("padding-right"), document.body.style.paddingRight = n || "";
    }, t._getScrollbarWidth = function () {
      var t = document.createElement("div");
      t.className = ae, document.body.appendChild(t);
      var e = t.getBoundingClientRect().width - t.clientWidth;
      return document.body.removeChild(t), e;
    }, o._jQueryInterface = function (n, i) {
      return this.each(function () {
        var t = g(this).data(te),
            e = l({}, ie, g(this).data(), "object" == _typeof(n) && n ? n : {});

        if (t || (t = new o(this, e), g(this).data(te, t)), "string" == typeof n) {
          if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
          t[n](i);
        } else e.show && t.show(i);
      });
    }, s(o, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return ie;
      }
    }]), o;
  }();

  g(document).on(re.CLICK_DATA_API, ge, function (t) {
    var e,
        n = this,
        i = _.getSelectorFromElement(this);

    i && (e = document.querySelector(i));
    var o = g(e).data(te) ? "toggle" : l({}, g(e).data(), g(this).data());
    "A" !== this.tagName && "AREA" !== this.tagName || t.preventDefault();
    var r = g(e).one(re.SHOW, function (t) {
      t.isDefaultPrevented() || r.one(re.HIDDEN, function () {
        g(n).is(":visible") && n.focus();
      });
    });

    ve._jQueryInterface.call(g(e), o, this);
  }), g.fn[Zt] = ve._jQueryInterface, g.fn[Zt].Constructor = ve, g.fn[Zt].noConflict = function () {
    return g.fn[Zt] = ne, ve._jQueryInterface;
  };
  var ye = ["background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href"],
      Ee = {
    "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i],
    a: ["target", "href", "title", "rel"],
    area: [],
    b: [],
    br: [],
    col: [],
    code: [],
    div: [],
    em: [],
    hr: [],
    h1: [],
    h2: [],
    h3: [],
    h4: [],
    h5: [],
    h6: [],
    i: [],
    img: ["src", "alt", "title", "width", "height"],
    li: [],
    ol: [],
    p: [],
    pre: [],
    s: [],
    small: [],
    span: [],
    sub: [],
    sup: [],
    strong: [],
    u: [],
    ul: []
  },
      Ce = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,
      Te = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i;

  function Se(t, s, e) {
    if (0 === t.length) return t;
    if (e && "function" == typeof e) return e(t);

    for (var n = new window.DOMParser().parseFromString(t, "text/html"), a = Object.keys(s), l = [].slice.call(n.body.querySelectorAll("*")), i = function i(t, e) {
      var n = l[t],
          i = n.nodeName.toLowerCase();
      if (-1 === a.indexOf(n.nodeName.toLowerCase())) return n.parentNode.removeChild(n), "continue";
      var o = [].slice.call(n.attributes),
          r = [].concat(s["*"] || [], s[i] || []);
      o.forEach(function (t) {
        (function (t, e) {
          var n = t.nodeName.toLowerCase();
          if (-1 !== e.indexOf(n)) return -1 === ye.indexOf(n) || Boolean(t.nodeValue.match(Ce) || t.nodeValue.match(Te));

          for (var i = e.filter(function (t) {
            return t instanceof RegExp;
          }), o = 0, r = i.length; o < r; o++) {
            if (n.match(i[o])) return !0;
          }

          return !1;
        })(t, r) || n.removeAttribute(t.nodeName);
      });
    }, o = 0, r = l.length; o < r; o++) {
      i(o);
    }

    return n.body.innerHTML;
  }

  var be = "tooltip",
      Ie = "bs.tooltip",
      De = "." + Ie,
      we = g.fn[be],
      Ae = "bs-tooltip",
      Ne = new RegExp("(^|\\s)" + Ae + "\\S+", "g"),
      Oe = ["sanitize", "whiteList", "sanitizeFn"],
      ke = {
    animation: "boolean",
    template: "string",
    title: "(string|element|function)",
    trigger: "string",
    delay: "(number|object)",
    html: "boolean",
    selector: "(string|boolean)",
    placement: "(string|function)",
    offset: "(number|string|function)",
    container: "(string|element|boolean)",
    fallbackPlacement: "(string|array)",
    boundary: "(string|element)",
    sanitize: "boolean",
    sanitizeFn: "(null|function)",
    whiteList: "object"
  },
      Pe = {
    AUTO: "auto",
    TOP: "top",
    RIGHT: "right",
    BOTTOM: "bottom",
    LEFT: "left"
  },
      Le = {
    animation: !0,
    template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: "hover focus",
    title: "",
    delay: 0,
    html: !1,
    selector: !1,
    placement: "top",
    offset: 0,
    container: !1,
    fallbackPlacement: "flip",
    boundary: "scrollParent",
    sanitize: !0,
    sanitizeFn: null,
    whiteList: Ee
  },
      je = "show",
      He = "out",
      Re = {
    HIDE: "hide" + De,
    HIDDEN: "hidden" + De,
    SHOW: "show" + De,
    SHOWN: "shown" + De,
    INSERTED: "inserted" + De,
    CLICK: "click" + De,
    FOCUSIN: "focusin" + De,
    FOCUSOUT: "focusout" + De,
    MOUSEENTER: "mouseenter" + De,
    MOUSELEAVE: "mouseleave" + De
  },
      xe = "fade",
      Fe = "show",
      Ue = ".tooltip-inner",
      We = ".arrow",
      qe = "hover",
      Me = "focus",
      Ke = "click",
      Qe = "manual",
      Be = function () {
    function i(t, e) {
      if ("undefined" == typeof u) throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");
      this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = t, this.config = this._getConfig(e), this.tip = null, this._setListeners();
    }

    var t = i.prototype;
    return t.enable = function () {
      this._isEnabled = !0;
    }, t.disable = function () {
      this._isEnabled = !1;
    }, t.toggleEnabled = function () {
      this._isEnabled = !this._isEnabled;
    }, t.toggle = function (t) {
      if (this._isEnabled) if (t) {
        var e = this.constructor.DATA_KEY,
            n = g(t.currentTarget).data(e);
        n || (n = new this.constructor(t.currentTarget, this._getDelegateConfig()), g(t.currentTarget).data(e, n)), n._activeTrigger.click = !n._activeTrigger.click, n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n);
      } else {
        if (g(this.getTipElement()).hasClass(Fe)) return void this._leave(null, this);

        this._enter(null, this);
      }
    }, t.dispose = function () {
      clearTimeout(this._timeout), g.removeData(this.element, this.constructor.DATA_KEY), g(this.element).off(this.constructor.EVENT_KEY), g(this.element).closest(".modal").off("hide.bs.modal"), this.tip && g(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, (this._activeTrigger = null) !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null;
    }, t.show = function () {
      var e = this;
      if ("none" === g(this.element).css("display")) throw new Error("Please use show on visible elements");
      var t = g.Event(this.constructor.Event.SHOW);

      if (this.isWithContent() && this._isEnabled) {
        g(this.element).trigger(t);

        var n = _.findShadowRoot(this.element),
            i = g.contains(null !== n ? n : this.element.ownerDocument.documentElement, this.element);

        if (t.isDefaultPrevented() || !i) return;

        var o = this.getTipElement(),
            r = _.getUID(this.constructor.NAME);

        o.setAttribute("id", r), this.element.setAttribute("aria-describedby", r), this.setContent(), this.config.animation && g(o).addClass(xe);

        var s = "function" == typeof this.config.placement ? this.config.placement.call(this, o, this.element) : this.config.placement,
            a = this._getAttachment(s);

        this.addAttachmentClass(a);

        var l = this._getContainer();

        g(o).data(this.constructor.DATA_KEY, this), g.contains(this.element.ownerDocument.documentElement, this.tip) || g(o).appendTo(l), g(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new u(this.element, o, {
          placement: a,
          modifiers: {
            offset: this._getOffset(),
            flip: {
              behavior: this.config.fallbackPlacement
            },
            arrow: {
              element: We
            },
            preventOverflow: {
              boundariesElement: this.config.boundary
            }
          },
          onCreate: function onCreate(t) {
            t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t);
          },
          onUpdate: function onUpdate(t) {
            return e._handlePopperPlacementChange(t);
          }
        }), g(o).addClass(Fe), "ontouchstart" in document.documentElement && g(document.body).children().on("mouseover", null, g.noop);

        var c = function c() {
          e.config.animation && e._fixTransition();
          var t = e._hoverState;
          e._hoverState = null, g(e.element).trigger(e.constructor.Event.SHOWN), t === He && e._leave(null, e);
        };

        if (g(this.tip).hasClass(xe)) {
          var h = _.getTransitionDurationFromElement(this.tip);

          g(this.tip).one(_.TRANSITION_END, c).emulateTransitionEnd(h);
        } else c();
      }
    }, t.hide = function (t) {
      var e = this,
          n = this.getTipElement(),
          i = g.Event(this.constructor.Event.HIDE),
          o = function o() {
        e._hoverState !== je && n.parentNode && n.parentNode.removeChild(n), e._cleanTipClass(), e.element.removeAttribute("aria-describedby"), g(e.element).trigger(e.constructor.Event.HIDDEN), null !== e._popper && e._popper.destroy(), t && t();
      };

      if (g(this.element).trigger(i), !i.isDefaultPrevented()) {
        if (g(n).removeClass(Fe), "ontouchstart" in document.documentElement && g(document.body).children().off("mouseover", null, g.noop), this._activeTrigger[Ke] = !1, this._activeTrigger[Me] = !1, this._activeTrigger[qe] = !1, g(this.tip).hasClass(xe)) {
          var r = _.getTransitionDurationFromElement(n);

          g(n).one(_.TRANSITION_END, o).emulateTransitionEnd(r);
        } else o();

        this._hoverState = "";
      }
    }, t.update = function () {
      null !== this._popper && this._popper.scheduleUpdate();
    }, t.isWithContent = function () {
      return Boolean(this.getTitle());
    }, t.addAttachmentClass = function (t) {
      g(this.getTipElement()).addClass(Ae + "-" + t);
    }, t.getTipElement = function () {
      return this.tip = this.tip || g(this.config.template)[0], this.tip;
    }, t.setContent = function () {
      var t = this.getTipElement();
      this.setElementContent(g(t.querySelectorAll(Ue)), this.getTitle()), g(t).removeClass(xe + " " + Fe);
    }, t.setElementContent = function (t, e) {
      "object" != _typeof(e) || !e.nodeType && !e.jquery ? this.config.html ? (this.config.sanitize && (e = Se(e, this.config.whiteList, this.config.sanitizeFn)), t.html(e)) : t.text(e) : this.config.html ? g(e).parent().is(t) || t.empty().append(e) : t.text(g(e).text());
    }, t.getTitle = function () {
      var t = this.element.getAttribute("data-original-title");
      return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t;
    }, t._getOffset = function () {
      var e = this,
          t = {};
      return "function" == typeof this.config.offset ? t.fn = function (t) {
        return t.offsets = l({}, t.offsets, e.config.offset(t.offsets, e.element) || {}), t;
      } : t.offset = this.config.offset, t;
    }, t._getContainer = function () {
      return !1 === this.config.container ? document.body : _.isElement(this.config.container) ? g(this.config.container) : g(document).find(this.config.container);
    }, t._getAttachment = function (t) {
      return Pe[t.toUpperCase()];
    }, t._setListeners = function () {
      var i = this;
      this.config.trigger.split(" ").forEach(function (t) {
        if ("click" === t) g(i.element).on(i.constructor.Event.CLICK, i.config.selector, function (t) {
          return i.toggle(t);
        });else if (t !== Qe) {
          var e = t === qe ? i.constructor.Event.MOUSEENTER : i.constructor.Event.FOCUSIN,
              n = t === qe ? i.constructor.Event.MOUSELEAVE : i.constructor.Event.FOCUSOUT;
          g(i.element).on(e, i.config.selector, function (t) {
            return i._enter(t);
          }).on(n, i.config.selector, function (t) {
            return i._leave(t);
          });
        }
      }), g(this.element).closest(".modal").on("hide.bs.modal", function () {
        i.element && i.hide();
      }), this.config.selector ? this.config = l({}, this.config, {
        trigger: "manual",
        selector: ""
      }) : this._fixTitle();
    }, t._fixTitle = function () {
      var t = _typeof(this.element.getAttribute("data-original-title"));

      (this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""));
    }, t._enter = function (t, e) {
      var n = this.constructor.DATA_KEY;
      (e = e || g(t.currentTarget).data(n)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), g(t.currentTarget).data(n, e)), t && (e._activeTrigger["focusin" === t.type ? Me : qe] = !0), g(e.getTipElement()).hasClass(Fe) || e._hoverState === je ? e._hoverState = je : (clearTimeout(e._timeout), e._hoverState = je, e.config.delay && e.config.delay.show ? e._timeout = setTimeout(function () {
        e._hoverState === je && e.show();
      }, e.config.delay.show) : e.show());
    }, t._leave = function (t, e) {
      var n = this.constructor.DATA_KEY;
      (e = e || g(t.currentTarget).data(n)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), g(t.currentTarget).data(n, e)), t && (e._activeTrigger["focusout" === t.type ? Me : qe] = !1), e._isWithActiveTrigger() || (clearTimeout(e._timeout), e._hoverState = He, e.config.delay && e.config.delay.hide ? e._timeout = setTimeout(function () {
        e._hoverState === He && e.hide();
      }, e.config.delay.hide) : e.hide());
    }, t._isWithActiveTrigger = function () {
      for (var t in this._activeTrigger) {
        if (this._activeTrigger[t]) return !0;
      }

      return !1;
    }, t._getConfig = function (t) {
      var e = g(this.element).data();
      return Object.keys(e).forEach(function (t) {
        -1 !== Oe.indexOf(t) && delete e[t];
      }), "number" == typeof (t = l({}, this.constructor.Default, e, "object" == _typeof(t) && t ? t : {})).delay && (t.delay = {
        show: t.delay,
        hide: t.delay
      }), "number" == typeof t.title && (t.title = t.title.toString()), "number" == typeof t.content && (t.content = t.content.toString()), _.typeCheckConfig(be, t, this.constructor.DefaultType), t.sanitize && (t.template = Se(t.template, t.whiteList, t.sanitizeFn)), t;
    }, t._getDelegateConfig = function () {
      var t = {};
      if (this.config) for (var e in this.config) {
        this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
      }
      return t;
    }, t._cleanTipClass = function () {
      var t = g(this.getTipElement()),
          e = t.attr("class").match(Ne);
      null !== e && e.length && t.removeClass(e.join(""));
    }, t._handlePopperPlacementChange = function (t) {
      var e = t.instance;
      this.tip = e.popper, this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement));
    }, t._fixTransition = function () {
      var t = this.getTipElement(),
          e = this.config.animation;
      null === t.getAttribute("x-placement") && (g(t).removeClass(xe), this.config.animation = !1, this.hide(), this.show(), this.config.animation = e);
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = g(this).data(Ie),
            e = "object" == _typeof(n) && n;

        if ((t || !/dispose|hide/.test(n)) && (t || (t = new i(this, e), g(this).data(Ie, t)), "string" == typeof n)) {
          if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
          t[n]();
        }
      });
    }, s(i, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return Le;
      }
    }, {
      key: "NAME",
      get: function get() {
        return be;
      }
    }, {
      key: "DATA_KEY",
      get: function get() {
        return Ie;
      }
    }, {
      key: "Event",
      get: function get() {
        return Re;
      }
    }, {
      key: "EVENT_KEY",
      get: function get() {
        return De;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return ke;
      }
    }]), i;
  }();

  g.fn[be] = Be._jQueryInterface, g.fn[be].Constructor = Be, g.fn[be].noConflict = function () {
    return g.fn[be] = we, Be._jQueryInterface;
  };

  var Ve = "popover",
      Ye = "bs.popover",
      ze = "." + Ye,
      Xe = g.fn[Ve],
      $e = "bs-popover",
      Ge = new RegExp("(^|\\s)" + $e + "\\S+", "g"),
      Je = l({}, Be.Default, {
    placement: "right",
    trigger: "click",
    content: "",
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
  }),
      Ze = l({}, Be.DefaultType, {
    content: "(string|element|function)"
  }),
      tn = "fade",
      en = "show",
      nn = ".popover-header",
      on = ".popover-body",
      rn = {
    HIDE: "hide" + ze,
    HIDDEN: "hidden" + ze,
    SHOW: "show" + ze,
    SHOWN: "shown" + ze,
    INSERTED: "inserted" + ze,
    CLICK: "click" + ze,
    FOCUSIN: "focusin" + ze,
    FOCUSOUT: "focusout" + ze,
    MOUSEENTER: "mouseenter" + ze,
    MOUSELEAVE: "mouseleave" + ze
  },
      sn = function (t) {
    var e, n;

    function i() {
      return t.apply(this, arguments) || this;
    }

    n = t, (e = i).prototype = Object.create(n.prototype), (e.prototype.constructor = e).__proto__ = n;
    var o = i.prototype;
    return o.isWithContent = function () {
      return this.getTitle() || this._getContent();
    }, o.addAttachmentClass = function (t) {
      g(this.getTipElement()).addClass($e + "-" + t);
    }, o.getTipElement = function () {
      return this.tip = this.tip || g(this.config.template)[0], this.tip;
    }, o.setContent = function () {
      var t = g(this.getTipElement());
      this.setElementContent(t.find(nn), this.getTitle());

      var e = this._getContent();

      "function" == typeof e && (e = e.call(this.element)), this.setElementContent(t.find(on), e), t.removeClass(tn + " " + en);
    }, o._getContent = function () {
      return this.element.getAttribute("data-content") || this.config.content;
    }, o._cleanTipClass = function () {
      var t = g(this.getTipElement()),
          e = t.attr("class").match(Ge);
      null !== e && 0 < e.length && t.removeClass(e.join(""));
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = g(this).data(Ye),
            e = "object" == _typeof(n) ? n : null;

        if ((t || !/dispose|hide/.test(n)) && (t || (t = new i(this, e), g(this).data(Ye, t)), "string" == typeof n)) {
          if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
          t[n]();
        }
      });
    }, s(i, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return Je;
      }
    }, {
      key: "NAME",
      get: function get() {
        return Ve;
      }
    }, {
      key: "DATA_KEY",
      get: function get() {
        return Ye;
      }
    }, {
      key: "Event",
      get: function get() {
        return rn;
      }
    }, {
      key: "EVENT_KEY",
      get: function get() {
        return ze;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return Ze;
      }
    }]), i;
  }(Be);

  g.fn[Ve] = sn._jQueryInterface, g.fn[Ve].Constructor = sn, g.fn[Ve].noConflict = function () {
    return g.fn[Ve] = Xe, sn._jQueryInterface;
  };

  var an = "scrollspy",
      ln = "bs.scrollspy",
      cn = "." + ln,
      hn = g.fn[an],
      un = {
    offset: 10,
    method: "auto",
    target: ""
  },
      fn = {
    offset: "number",
    method: "string",
    target: "(string|element)"
  },
      dn = {
    ACTIVATE: "activate" + cn,
    SCROLL: "scroll" + cn,
    LOAD_DATA_API: "load" + cn + ".data-api"
  },
      gn = "dropdown-item",
      _n = "active",
      mn = '[data-spy="scroll"]',
      pn = ".nav, .list-group",
      vn = ".nav-link",
      yn = ".nav-item",
      En = ".list-group-item",
      Cn = ".dropdown",
      Tn = ".dropdown-item",
      Sn = ".dropdown-toggle",
      bn = "offset",
      In = "position",
      Dn = function () {
    function n(t, e) {
      var n = this;
      this._element = t, this._scrollElement = "BODY" === t.tagName ? window : t, this._config = this._getConfig(e), this._selector = this._config.target + " " + vn + "," + this._config.target + " " + En + "," + this._config.target + " " + Tn, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, g(this._scrollElement).on(dn.SCROLL, function (t) {
        return n._process(t);
      }), this.refresh(), this._process();
    }

    var t = n.prototype;
    return t.refresh = function () {
      var e = this,
          t = this._scrollElement === this._scrollElement.window ? bn : In,
          o = "auto" === this._config.method ? t : this._config.method,
          r = o === In ? this._getScrollTop() : 0;
      this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), [].slice.call(document.querySelectorAll(this._selector)).map(function (t) {
        var e,
            n = _.getSelectorFromElement(t);

        if (n && (e = document.querySelector(n)), e) {
          var i = e.getBoundingClientRect();
          if (i.width || i.height) return [g(e)[o]().top + r, n];
        }

        return null;
      }).filter(function (t) {
        return t;
      }).sort(function (t, e) {
        return t[0] - e[0];
      }).forEach(function (t) {
        e._offsets.push(t[0]), e._targets.push(t[1]);
      });
    }, t.dispose = function () {
      g.removeData(this._element, ln), g(this._scrollElement).off(cn), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null;
    }, t._getConfig = function (t) {
      if ("string" != typeof (t = l({}, un, "object" == _typeof(t) && t ? t : {})).target) {
        var e = g(t.target).attr("id");
        e || (e = _.getUID(an), g(t.target).attr("id", e)), t.target = "#" + e;
      }

      return _.typeCheckConfig(an, t, fn), t;
    }, t._getScrollTop = function () {
      return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
    }, t._getScrollHeight = function () {
      return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
    }, t._getOffsetHeight = function () {
      return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
    }, t._process = function () {
      var t = this._getScrollTop() + this._config.offset,
          e = this._getScrollHeight(),
          n = this._config.offset + e - this._getOffsetHeight();

      if (this._scrollHeight !== e && this.refresh(), n <= t) {
        var i = this._targets[this._targets.length - 1];
        this._activeTarget !== i && this._activate(i);
      } else {
        if (this._activeTarget && t < this._offsets[0] && 0 < this._offsets[0]) return this._activeTarget = null, void this._clear();

        for (var o = this._offsets.length; o--;) {
          this._activeTarget !== this._targets[o] && t >= this._offsets[o] && ("undefined" == typeof this._offsets[o + 1] || t < this._offsets[o + 1]) && this._activate(this._targets[o]);
        }
      }
    }, t._activate = function (e) {
      this._activeTarget = e, this._clear();

      var t = this._selector.split(",").map(function (t) {
        return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]';
      }),
          n = g([].slice.call(document.querySelectorAll(t.join(","))));

      n.hasClass(gn) ? (n.closest(Cn).find(Sn).addClass(_n), n.addClass(_n)) : (n.addClass(_n), n.parents(pn).prev(vn + ", " + En).addClass(_n), n.parents(pn).prev(yn).children(vn).addClass(_n)), g(this._scrollElement).trigger(dn.ACTIVATE, {
        relatedTarget: e
      });
    }, t._clear = function () {
      [].slice.call(document.querySelectorAll(this._selector)).filter(function (t) {
        return t.classList.contains(_n);
      }).forEach(function (t) {
        return t.classList.remove(_n);
      });
    }, n._jQueryInterface = function (e) {
      return this.each(function () {
        var t = g(this).data(ln);

        if (t || (t = new n(this, "object" == _typeof(e) && e), g(this).data(ln, t)), "string" == typeof e) {
          if ("undefined" == typeof t[e]) throw new TypeError('No method named "' + e + '"');
          t[e]();
        }
      });
    }, s(n, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return un;
      }
    }]), n;
  }();

  g(window).on(dn.LOAD_DATA_API, function () {
    for (var t = [].slice.call(document.querySelectorAll(mn)), e = t.length; e--;) {
      var n = g(t[e]);

      Dn._jQueryInterface.call(n, n.data());
    }
  }), g.fn[an] = Dn._jQueryInterface, g.fn[an].Constructor = Dn, g.fn[an].noConflict = function () {
    return g.fn[an] = hn, Dn._jQueryInterface;
  };

  var wn = "bs.tab",
      An = "." + wn,
      Nn = g.fn.tab,
      On = {
    HIDE: "hide" + An,
    HIDDEN: "hidden" + An,
    SHOW: "show" + An,
    SHOWN: "shown" + An,
    CLICK_DATA_API: "click" + An + ".data-api"
  },
      kn = "dropdown-menu",
      Pn = "active",
      Ln = "disabled",
      jn = "fade",
      Hn = "show",
      Rn = ".dropdown",
      xn = ".nav, .list-group",
      Fn = ".active",
      Un = "> li > .active",
      Wn = '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
      qn = ".dropdown-toggle",
      Mn = "> .dropdown-menu .active",
      Kn = function () {
    function i(t) {
      this._element = t;
    }

    var t = i.prototype;
    return t.show = function () {
      var n = this;

      if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && g(this._element).hasClass(Pn) || g(this._element).hasClass(Ln))) {
        var t,
            i,
            e = g(this._element).closest(xn)[0],
            o = _.getSelectorFromElement(this._element);

        if (e) {
          var r = "UL" === e.nodeName || "OL" === e.nodeName ? Un : Fn;
          i = (i = g.makeArray(g(e).find(r)))[i.length - 1];
        }

        var s = g.Event(On.HIDE, {
          relatedTarget: this._element
        }),
            a = g.Event(On.SHOW, {
          relatedTarget: i
        });

        if (i && g(i).trigger(s), g(this._element).trigger(a), !a.isDefaultPrevented() && !s.isDefaultPrevented()) {
          o && (t = document.querySelector(o)), this._activate(this._element, e);

          var l = function l() {
            var t = g.Event(On.HIDDEN, {
              relatedTarget: n._element
            }),
                e = g.Event(On.SHOWN, {
              relatedTarget: i
            });
            g(i).trigger(t), g(n._element).trigger(e);
          };

          t ? this._activate(t, t.parentNode, l) : l();
        }
      }
    }, t.dispose = function () {
      g.removeData(this._element, wn), this._element = null;
    }, t._activate = function (t, e, n) {
      var i = this,
          o = (!e || "UL" !== e.nodeName && "OL" !== e.nodeName ? g(e).children(Fn) : g(e).find(Un))[0],
          r = n && o && g(o).hasClass(jn),
          s = function s() {
        return i._transitionComplete(t, o, n);
      };

      if (o && r) {
        var a = _.getTransitionDurationFromElement(o);

        g(o).removeClass(Hn).one(_.TRANSITION_END, s).emulateTransitionEnd(a);
      } else s();
    }, t._transitionComplete = function (t, e, n) {
      if (e) {
        g(e).removeClass(Pn);
        var i = g(e.parentNode).find(Mn)[0];
        i && g(i).removeClass(Pn), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !1);
      }

      if (g(t).addClass(Pn), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !0), _.reflow(t), t.classList.contains(jn) && t.classList.add(Hn), t.parentNode && g(t.parentNode).hasClass(kn)) {
        var o = g(t).closest(Rn)[0];

        if (o) {
          var r = [].slice.call(o.querySelectorAll(qn));
          g(r).addClass(Pn);
        }

        t.setAttribute("aria-expanded", !0);
      }

      n && n();
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = g(this),
            e = t.data(wn);

        if (e || (e = new i(this), t.data(wn, e)), "string" == typeof n) {
          if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');
          e[n]();
        }
      });
    }, s(i, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }]), i;
  }();

  g(document).on(On.CLICK_DATA_API, Wn, function (t) {
    t.preventDefault(), Kn._jQueryInterface.call(g(this), "show");
  }), g.fn.tab = Kn._jQueryInterface, g.fn.tab.Constructor = Kn, g.fn.tab.noConflict = function () {
    return g.fn.tab = Nn, Kn._jQueryInterface;
  };

  var Qn = "toast",
      Bn = "bs.toast",
      Vn = "." + Bn,
      Yn = g.fn[Qn],
      zn = {
    CLICK_DISMISS: "click.dismiss" + Vn,
    HIDE: "hide" + Vn,
    HIDDEN: "hidden" + Vn,
    SHOW: "show" + Vn,
    SHOWN: "shown" + Vn
  },
      Xn = "fade",
      $n = "hide",
      Gn = "show",
      Jn = "showing",
      Zn = {
    animation: "boolean",
    autohide: "boolean",
    delay: "number"
  },
      ti = {
    animation: !0,
    autohide: !0,
    delay: 500
  },
      ei = '[data-dismiss="toast"]',
      ni = function () {
    function i(t, e) {
      this._element = t, this._config = this._getConfig(e), this._timeout = null, this._setListeners();
    }

    var t = i.prototype;
    return t.show = function () {
      var t = this;
      g(this._element).trigger(zn.SHOW), this._config.animation && this._element.classList.add(Xn);

      var e = function e() {
        t._element.classList.remove(Jn), t._element.classList.add(Gn), g(t._element).trigger(zn.SHOWN), t._config.autohide && t.hide();
      };

      if (this._element.classList.remove($n), this._element.classList.add(Jn), this._config.animation) {
        var n = _.getTransitionDurationFromElement(this._element);

        g(this._element).one(_.TRANSITION_END, e).emulateTransitionEnd(n);
      } else e();
    }, t.hide = function (t) {
      var e = this;
      this._element.classList.contains(Gn) && (g(this._element).trigger(zn.HIDE), t ? this._close() : this._timeout = setTimeout(function () {
        e._close();
      }, this._config.delay));
    }, t.dispose = function () {
      clearTimeout(this._timeout), this._timeout = null, this._element.classList.contains(Gn) && this._element.classList.remove(Gn), g(this._element).off(zn.CLICK_DISMISS), g.removeData(this._element, Bn), this._element = null, this._config = null;
    }, t._getConfig = function (t) {
      return t = l({}, ti, g(this._element).data(), "object" == _typeof(t) && t ? t : {}), _.typeCheckConfig(Qn, t, this.constructor.DefaultType), t;
    }, t._setListeners = function () {
      var t = this;
      g(this._element).on(zn.CLICK_DISMISS, ei, function () {
        return t.hide(!0);
      });
    }, t._close = function () {
      var t = this,
          e = function e() {
        t._element.classList.add($n), g(t._element).trigger(zn.HIDDEN);
      };

      if (this._element.classList.remove(Gn), this._config.animation) {
        var n = _.getTransitionDurationFromElement(this._element);

        g(this._element).one(_.TRANSITION_END, e).emulateTransitionEnd(n);
      } else e();
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = g(this),
            e = t.data(Bn);

        if (e || (e = new i(this, "object" == _typeof(n) && n), t.data(Bn, e)), "string" == typeof n) {
          if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');
          e[n](this);
        }
      });
    }, s(i, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return Zn;
      }
    }, {
      key: "Default",
      get: function get() {
        return ti;
      }
    }]), i;
  }();

  g.fn[Qn] = ni._jQueryInterface, g.fn[Qn].Constructor = ni, g.fn[Qn].noConflict = function () {
    return g.fn[Qn] = Yn, ni._jQueryInterface;
  }, function () {
    if ("undefined" == typeof g) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
    var t = g.fn.jquery.split(" ")[0].split(".");
    if (t[0] < 2 && t[1] < 9 || 1 === t[0] && 9 === t[1] && t[2] < 1 || 4 <= t[0]) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0");
  }(), t.Util = _, t.Alert = p, t.Button = P, t.Carousel = lt, t.Collapse = bt, t.Dropdown = Jt, t.Modal = ve, t.Popover = sn, t.Scrollspy = Dn, t.Tab = Kn, t.Toast = ni, t.Tooltip = Be, Object.defineProperty(t, "__esModule", {
    value: !0
  });
});

/***/ }),

/***/ "./public/bootstrap/js/popper.min.js":
/*!*******************************************!*\
  !*** ./public/bootstrap/js/popper.min.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*
 Copyright (C) Federico Zivolo 2017
 Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */
(function (e, t) {
  'object' == ( false ? undefined : _typeof(exports)) && 'undefined' != typeof module ? module.exports = t() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
})(this, function () {
  'use strict';

  function e(e) {
    return e && '[object Function]' === {}.toString.call(e);
  }

  function t(e, t) {
    if (1 !== e.nodeType) return [];
    var o = window.getComputedStyle(e, null);
    return t ? o[t] : o;
  }

  function o(e) {
    return 'HTML' === e.nodeName ? e : e.parentNode || e.host;
  }

  function n(e) {
    if (!e || -1 !== ['HTML', 'BODY', '#document'].indexOf(e.nodeName)) return window.document.body;
    var i = t(e),
        r = i.overflow,
        p = i.overflowX,
        s = i.overflowY;
    return /(auto|scroll)/.test(r + s + p) ? e : n(o(e));
  }

  function r(e) {
    var o = e && e.offsetParent,
        i = o && o.nodeName;
    return i && 'BODY' !== i && 'HTML' !== i ? -1 !== ['TD', 'TABLE'].indexOf(o.nodeName) && 'static' === t(o, 'position') ? r(o) : o : window.document.documentElement;
  }

  function p(e) {
    var t = e.nodeName;
    return 'BODY' !== t && ('HTML' === t || r(e.firstElementChild) === e);
  }

  function s(e) {
    return null === e.parentNode ? e : s(e.parentNode);
  }

  function d(e, t) {
    if (!e || !e.nodeType || !t || !t.nodeType) return window.document.documentElement;
    var o = e.compareDocumentPosition(t) & Node.DOCUMENT_POSITION_FOLLOWING,
        i = o ? e : t,
        n = o ? t : e,
        a = document.createRange();
    a.setStart(i, 0), a.setEnd(n, 0);
    var f = a.commonAncestorContainer;
    if (e !== f && t !== f || i.contains(n)) return p(f) ? f : r(f);
    var l = s(e);
    return l.host ? d(l.host, t) : d(e, s(t).host);
  }

  function a(e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : 'top',
        o = 'top' === t ? 'scrollTop' : 'scrollLeft',
        i = e.nodeName;

    if ('BODY' === i || 'HTML' === i) {
      var n = window.document.documentElement,
          r = window.document.scrollingElement || n;
      return r[o];
    }

    return e[o];
  }

  function f(e, t) {
    var o = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
        i = a(t, 'top'),
        n = a(t, 'left'),
        r = o ? -1 : 1;
    return e.top += i * r, e.bottom += i * r, e.left += n * r, e.right += n * r, e;
  }

  function l(e, t) {
    var o = 'x' === t ? 'Left' : 'Top',
        i = 'Left' == o ? 'Right' : 'Bottom';
    return +e['border' + o + 'Width'].split('px')[0] + +e['border' + i + 'Width'].split('px')[0];
  }

  function m(e, t, o, i) {
    return _(t['offset' + e], o['client' + e], o['offset' + e], ie() ? o['offset' + e] + i['margin' + ('Height' === e ? 'Top' : 'Left')] + i['margin' + ('Height' === e ? 'Bottom' : 'Right')] : 0);
  }

  function h() {
    var e = window.document.body,
        t = window.document.documentElement,
        o = ie() && window.getComputedStyle(t);
    return {
      height: m('Height', e, t, o),
      width: m('Width', e, t, o)
    };
  }

  function c(e) {
    return se({}, e, {
      right: e.left + e.width,
      bottom: e.top + e.height
    });
  }

  function g(e) {
    var o = {};
    if (ie()) try {
      o = e.getBoundingClientRect();
      var i = a(e, 'top'),
          n = a(e, 'left');
      o.top += i, o.left += n, o.bottom += i, o.right += n;
    } catch (e) {} else o = e.getBoundingClientRect();
    var r = {
      left: o.left,
      top: o.top,
      width: o.right - o.left,
      height: o.bottom - o.top
    },
        p = 'HTML' === e.nodeName ? h() : {},
        s = p.width || e.clientWidth || r.right - r.left,
        d = p.height || e.clientHeight || r.bottom - r.top,
        f = e.offsetWidth - s,
        m = e.offsetHeight - d;

    if (f || m) {
      var g = t(e);
      f -= l(g, 'x'), m -= l(g, 'y'), r.width -= f, r.height -= m;
    }

    return c(r);
  }

  function u(e, o) {
    var i = ie(),
        r = 'HTML' === o.nodeName,
        p = g(e),
        s = g(o),
        d = n(e),
        a = t(o),
        l = +a.borderTopWidth.split('px')[0],
        m = +a.borderLeftWidth.split('px')[0],
        h = c({
      top: p.top - s.top - l,
      left: p.left - s.left - m,
      width: p.width,
      height: p.height
    });

    if (h.marginTop = 0, h.marginLeft = 0, !i && r) {
      var u = +a.marginTop.split('px')[0],
          b = +a.marginLeft.split('px')[0];
      h.top -= l - u, h.bottom -= l - u, h.left -= m - b, h.right -= m - b, h.marginTop = u, h.marginLeft = b;
    }

    return (i ? o.contains(d) : o === d && 'BODY' !== d.nodeName) && (h = f(h, o)), h;
  }

  function b(e) {
    var t = window.document.documentElement,
        o = u(e, t),
        i = _(t.clientWidth, window.innerWidth || 0),
        n = _(t.clientHeight, window.innerHeight || 0),
        r = a(t),
        p = a(t, 'left'),
        s = {
      top: r - o.top + o.marginTop,
      left: p - o.left + o.marginLeft,
      width: i,
      height: n
    };

    return c(s);
  }

  function y(e) {
    var i = e.nodeName;
    return 'BODY' === i || 'HTML' === i ? !1 : 'fixed' === t(e, 'position') || y(o(e));
  }

  function w(e, t, i, r) {
    var p = {
      top: 0,
      left: 0
    },
        s = d(e, t);
    if ('viewport' === r) p = b(s);else {
      var a;
      'scrollParent' === r ? (a = n(o(e)), 'BODY' === a.nodeName && (a = window.document.documentElement)) : 'window' === r ? a = window.document.documentElement : a = r;
      var f = u(a, s);

      if ('HTML' === a.nodeName && !y(s)) {
        var l = h(),
            m = l.height,
            c = l.width;
        p.top += f.top - f.marginTop, p.bottom = m + f.top, p.left += f.left - f.marginLeft, p.right = c + f.left;
      } else p = f;
    }
    return p.left += i, p.top += i, p.right -= i, p.bottom -= i, p;
  }

  function v(e) {
    var t = e.width,
        o = e.height;
    return t * o;
  }

  function E(e, t, o, i, n) {
    var r = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
    if (-1 === e.indexOf('auto')) return e;
    var p = w(o, i, r, n),
        s = {
      top: {
        width: p.width,
        height: t.top - p.top
      },
      right: {
        width: p.right - t.right,
        height: p.height
      },
      bottom: {
        width: p.width,
        height: p.bottom - t.bottom
      },
      left: {
        width: t.left - p.left,
        height: p.height
      }
    },
        d = Object.keys(s).map(function (e) {
      return se({
        key: e
      }, s[e], {
        area: v(s[e])
      });
    }).sort(function (e, t) {
      return t.area - e.area;
    }),
        a = d.filter(function (e) {
      var t = e.width,
          i = e.height;
      return t >= o.clientWidth && i >= o.clientHeight;
    }),
        f = 0 < a.length ? a[0].key : d[0].key,
        l = e.split('-')[1];
    return f + (l ? '-' + l : '');
  }

  function x(e, t, o) {
    var i = d(t, o);
    return u(o, i);
  }

  function O(e) {
    var t = window.getComputedStyle(e),
        o = parseFloat(t.marginTop) + parseFloat(t.marginBottom),
        i = parseFloat(t.marginLeft) + parseFloat(t.marginRight),
        n = {
      width: e.offsetWidth + i,
      height: e.offsetHeight + o
    };
    return n;
  }

  function L(e) {
    var t = {
      left: 'right',
      right: 'left',
      bottom: 'top',
      top: 'bottom'
    };
    return e.replace(/left|right|bottom|top/g, function (e) {
      return t[e];
    });
  }

  function S(e, t, o) {
    o = o.split('-')[0];
    var i = O(e),
        n = {
      width: i.width,
      height: i.height
    },
        r = -1 !== ['right', 'left'].indexOf(o),
        p = r ? 'top' : 'left',
        s = r ? 'left' : 'top',
        d = r ? 'height' : 'width',
        a = r ? 'width' : 'height';
    return n[p] = t[p] + t[d] / 2 - i[d] / 2, n[s] = o === s ? t[s] - i[a] : t[L(s)], n;
  }

  function T(e, t) {
    return Array.prototype.find ? e.find(t) : e.filter(t)[0];
  }

  function C(e, t, o) {
    if (Array.prototype.findIndex) return e.findIndex(function (e) {
      return e[t] === o;
    });
    var i = T(e, function (e) {
      return e[t] === o;
    });
    return e.indexOf(i);
  }

  function N(t, o, i) {
    var n = void 0 === i ? t : t.slice(0, C(t, 'name', i));
    return n.forEach(function (t) {
      t["function"] && console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
      var i = t["function"] || t.fn;
      t.enabled && e(i) && (o.offsets.popper = c(o.offsets.popper), o.offsets.reference = c(o.offsets.reference), o = i(o, t));
    }), o;
  }

  function k() {
    if (!this.state.isDestroyed) {
      var e = {
        instance: this,
        styles: {},
        attributes: {},
        flipped: !1,
        offsets: {}
      };
      e.offsets.reference = x(this.state, this.popper, this.reference), e.placement = E(this.options.placement, e.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), e.originalPlacement = e.placement, e.offsets.popper = S(this.popper, e.offsets.reference, e.placement), e.offsets.popper.position = 'absolute', e = N(this.modifiers, e), this.state.isCreated ? this.options.onUpdate(e) : (this.state.isCreated = !0, this.options.onCreate(e));
    }
  }

  function W(e, t) {
    return e.some(function (e) {
      var o = e.name,
          i = e.enabled;
      return i && o === t;
    });
  }

  function B(e) {
    for (var t = [!1, 'ms', 'Webkit', 'Moz', 'O'], o = e.charAt(0).toUpperCase() + e.slice(1), n = 0; n < t.length - 1; n++) {
      var i = t[n],
          r = i ? '' + i + o : e;
      if ('undefined' != typeof window.document.body.style[r]) return r;
    }

    return null;
  }

  function D() {
    return this.state.isDestroyed = !0, W(this.modifiers, 'applyStyle') && (this.popper.removeAttribute('x-placement'), this.popper.style.left = '', this.popper.style.position = '', this.popper.style.top = '', this.popper.style[B('transform')] = ''), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this;
  }

  function H(e, t, o, i) {
    var r = 'BODY' === e.nodeName,
        p = r ? window : e;
    p.addEventListener(t, o, {
      passive: !0
    }), r || H(n(p.parentNode), t, o, i), i.push(p);
  }

  function P(e, t, o, i) {
    o.updateBound = i, window.addEventListener('resize', o.updateBound, {
      passive: !0
    });
    var r = n(e);
    return H(r, 'scroll', o.updateBound, o.scrollParents), o.scrollElement = r, o.eventsEnabled = !0, o;
  }

  function A() {
    this.state.eventsEnabled || (this.state = P(this.reference, this.options, this.state, this.scheduleUpdate));
  }

  function M(e, t) {
    return window.removeEventListener('resize', t.updateBound), t.scrollParents.forEach(function (e) {
      e.removeEventListener('scroll', t.updateBound);
    }), t.updateBound = null, t.scrollParents = [], t.scrollElement = null, t.eventsEnabled = !1, t;
  }

  function I() {
    this.state.eventsEnabled && (window.cancelAnimationFrame(this.scheduleUpdate), this.state = M(this.reference, this.state));
  }

  function R(e) {
    return '' !== e && !isNaN(parseFloat(e)) && isFinite(e);
  }

  function U(e, t) {
    Object.keys(t).forEach(function (o) {
      var i = '';
      -1 !== ['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(o) && R(t[o]) && (i = 'px'), e.style[o] = t[o] + i;
    });
  }

  function Y(e, t) {
    Object.keys(t).forEach(function (o) {
      var i = t[o];
      !1 === i ? e.removeAttribute(o) : e.setAttribute(o, t[o]);
    });
  }

  function F(e, t, o) {
    var i = T(e, function (e) {
      var o = e.name;
      return o === t;
    }),
        n = !!i && e.some(function (e) {
      return e.name === o && e.enabled && e.order < i.order;
    });

    if (!n) {
      var r = '`' + t + '`';
      console.warn('`' + o + '`' + ' modifier is required by ' + r + ' modifier in order to work, be sure to include it before ' + r + '!');
    }

    return n;
  }

  function j(e) {
    return 'end' === e ? 'start' : 'start' === e ? 'end' : e;
  }

  function K(e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
        o = ae.indexOf(e),
        i = ae.slice(o + 1).concat(ae.slice(0, o));
    return t ? i.reverse() : i;
  }

  function q(e, t, o, i) {
    var n = e.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
        r = +n[1],
        p = n[2];
    if (!r) return e;

    if (0 === p.indexOf('%')) {
      var s;

      switch (p) {
        case '%p':
          s = o;
          break;

        case '%':
        case '%r':
        default:
          s = i;
      }

      var d = c(s);
      return d[t] / 100 * r;
    }

    if ('vh' === p || 'vw' === p) {
      var a;
      return a = 'vh' === p ? _(document.documentElement.clientHeight, window.innerHeight || 0) : _(document.documentElement.clientWidth, window.innerWidth || 0), a / 100 * r;
    }

    return r;
  }

  function G(e, t, o, i) {
    var n = [0, 0],
        r = -1 !== ['right', 'left'].indexOf(i),
        p = e.split(/(\+|\-)/).map(function (e) {
      return e.trim();
    }),
        s = p.indexOf(T(p, function (e) {
      return -1 !== e.search(/,|\s/);
    }));
    p[s] && -1 === p[s].indexOf(',') && console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
    var d = /\s*,\s*|\s+/,
        a = -1 === s ? [p] : [p.slice(0, s).concat([p[s].split(d)[0]]), [p[s].split(d)[1]].concat(p.slice(s + 1))];
    return a = a.map(function (e, i) {
      var n = (1 === i ? !r : r) ? 'height' : 'width',
          p = !1;
      return e.reduce(function (e, t) {
        return '' === e[e.length - 1] && -1 !== ['+', '-'].indexOf(t) ? (e[e.length - 1] = t, p = !0, e) : p ? (e[e.length - 1] += t, p = !1, e) : e.concat(t);
      }, []).map(function (e) {
        return q(e, n, t, o);
      });
    }), a.forEach(function (e, t) {
      e.forEach(function (o, i) {
        R(o) && (n[t] += o * ('-' === e[i - 1] ? -1 : 1));
      });
    }), n;
  }

  for (var z = Math.min, V = Math.floor, _ = Math.max, X = ['native code', '[object MutationObserverConstructor]'], Q = function Q(e) {
    return X.some(function (t) {
      return -1 < (e || '').toString().indexOf(t);
    });
  }, J = 'undefined' != typeof window, Z = ['Edge', 'Trident', 'Firefox'], $ = 0, ee = 0; ee < Z.length; ee += 1) {
    if (J && 0 <= navigator.userAgent.indexOf(Z[ee])) {
      $ = 1;
      break;
    }
  }

  var i,
      te = J && Q(window.MutationObserver),
      oe = te ? function (e) {
    var t = !1,
        o = 0,
        i = document.createElement('span'),
        n = new MutationObserver(function () {
      e(), t = !1;
    });
    return n.observe(i, {
      attributes: !0
    }), function () {
      t || (t = !0, i.setAttribute('x-index', o), ++o);
    };
  } : function (e) {
    var t = !1;
    return function () {
      t || (t = !0, setTimeout(function () {
        t = !1, e();
      }, $));
    };
  },
      ie = function ie() {
    return void 0 == i && (i = -1 !== navigator.appVersion.indexOf('MSIE 10')), i;
  },
      ne = function ne(e, t) {
    if (!(e instanceof t)) throw new TypeError('Cannot call a class as a function');
  },
      re = function () {
    function e(e, t) {
      for (var o, n = 0; n < t.length; n++) {
        o = t[n], o.enumerable = o.enumerable || !1, o.configurable = !0, 'value' in o && (o.writable = !0), Object.defineProperty(e, o.key, o);
      }
    }

    return function (t, o, i) {
      return o && e(t.prototype, o), i && e(t, i), t;
    };
  }(),
      pe = function pe(e, t, o) {
    return t in e ? Object.defineProperty(e, t, {
      value: o,
      enumerable: !0,
      configurable: !0,
      writable: !0
    }) : e[t] = o, e;
  },
      se = Object.assign || function (e) {
    for (var t, o = 1; o < arguments.length; o++) {
      for (var i in t = arguments[o], t) {
        Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
      }
    }

    return e;
  },
      de = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'],
      ae = de.slice(3),
      fe = {
    FLIP: 'flip',
    CLOCKWISE: 'clockwise',
    COUNTERCLOCKWISE: 'counterclockwise'
  },
      le = function () {
    function t(o, i) {
      var n = this,
          r = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
      ne(this, t), this.scheduleUpdate = function () {
        return requestAnimationFrame(n.update);
      }, this.update = oe(this.update.bind(this)), this.options = se({}, t.Defaults, r), this.state = {
        isDestroyed: !1,
        isCreated: !1,
        scrollParents: []
      }, this.reference = o.jquery ? o[0] : o, this.popper = i.jquery ? i[0] : i, this.options.modifiers = {}, Object.keys(se({}, t.Defaults.modifiers, r.modifiers)).forEach(function (e) {
        n.options.modifiers[e] = se({}, t.Defaults.modifiers[e] || {}, r.modifiers ? r.modifiers[e] : {});
      }), this.modifiers = Object.keys(this.options.modifiers).map(function (e) {
        return se({
          name: e
        }, n.options.modifiers[e]);
      }).sort(function (e, t) {
        return e.order - t.order;
      }), this.modifiers.forEach(function (t) {
        t.enabled && e(t.onLoad) && t.onLoad(n.reference, n.popper, n.options, t, n.state);
      }), this.update();
      var p = this.options.eventsEnabled;
      p && this.enableEventListeners(), this.state.eventsEnabled = p;
    }

    return re(t, [{
      key: 'update',
      value: function value() {
        return k.call(this);
      }
    }, {
      key: 'destroy',
      value: function value() {
        return D.call(this);
      }
    }, {
      key: 'enableEventListeners',
      value: function value() {
        return A.call(this);
      }
    }, {
      key: 'disableEventListeners',
      value: function value() {
        return I.call(this);
      }
    }]), t;
  }();

  return le.Utils = ('undefined' == typeof window ? global : window).PopperUtils, le.placements = de, le.Defaults = {
    placement: 'bottom',
    eventsEnabled: !0,
    removeOnDestroy: !1,
    onCreate: function onCreate() {},
    onUpdate: function onUpdate() {},
    modifiers: {
      shift: {
        order: 100,
        enabled: !0,
        fn: function fn(e) {
          var t = e.placement,
              o = t.split('-')[0],
              i = t.split('-')[1];

          if (i) {
            var n = e.offsets,
                r = n.reference,
                p = n.popper,
                s = -1 !== ['bottom', 'top'].indexOf(o),
                d = s ? 'left' : 'top',
                a = s ? 'width' : 'height',
                f = {
              start: pe({}, d, r[d]),
              end: pe({}, d, r[d] + r[a] - p[a])
            };
            e.offsets.popper = se({}, p, f[i]);
          }

          return e;
        }
      },
      offset: {
        order: 200,
        enabled: !0,
        fn: function fn(e, t) {
          var o,
              i = t.offset,
              n = e.placement,
              r = e.offsets,
              p = r.popper,
              s = r.reference,
              d = n.split('-')[0];
          return o = R(+i) ? [+i, 0] : G(i, p, s, d), 'left' === d ? (p.top += o[0], p.left -= o[1]) : 'right' === d ? (p.top += o[0], p.left += o[1]) : 'top' === d ? (p.left += o[0], p.top -= o[1]) : 'bottom' === d && (p.left += o[0], p.top += o[1]), e.popper = p, e;
        },
        offset: 0
      },
      preventOverflow: {
        order: 300,
        enabled: !0,
        fn: function fn(e, t) {
          var o = t.boundariesElement || r(e.instance.popper);
          e.instance.reference === o && (o = r(o));
          var i = w(e.instance.popper, e.instance.reference, t.padding, o);
          t.boundaries = i;
          var n = t.priority,
              p = e.offsets.popper,
              s = {
            primary: function primary(e) {
              var o = p[e];
              return p[e] < i[e] && !t.escapeWithReference && (o = _(p[e], i[e])), pe({}, e, o);
            },
            secondary: function secondary(e) {
              var o = 'right' === e ? 'left' : 'top',
                  n = p[o];
              return p[e] > i[e] && !t.escapeWithReference && (n = z(p[o], i[e] - ('right' === e ? p.width : p.height))), pe({}, o, n);
            }
          };
          return n.forEach(function (e) {
            var t = -1 === ['left', 'top'].indexOf(e) ? 'secondary' : 'primary';
            p = se({}, p, s[t](e));
          }), e.offsets.popper = p, e;
        },
        priority: ['left', 'right', 'top', 'bottom'],
        padding: 5,
        boundariesElement: 'scrollParent'
      },
      keepTogether: {
        order: 400,
        enabled: !0,
        fn: function fn(e) {
          var t = e.offsets,
              o = t.popper,
              i = t.reference,
              n = e.placement.split('-')[0],
              r = V,
              p = -1 !== ['top', 'bottom'].indexOf(n),
              s = p ? 'right' : 'bottom',
              d = p ? 'left' : 'top',
              a = p ? 'width' : 'height';
          return o[s] < r(i[d]) && (e.offsets.popper[d] = r(i[d]) - o[a]), o[d] > r(i[s]) && (e.offsets.popper[d] = r(i[s])), e;
        }
      },
      arrow: {
        order: 500,
        enabled: !0,
        fn: function fn(e, t) {
          if (!F(e.instance.modifiers, 'arrow', 'keepTogether')) return e;
          var o = t.element;

          if ('string' == typeof o) {
            if (o = e.instance.popper.querySelector(o), !o) return e;
          } else if (!e.instance.popper.contains(o)) return console.warn('WARNING: `arrow.element` must be child of its popper element!'), e;

          var i = e.placement.split('-')[0],
              n = e.offsets,
              r = n.popper,
              p = n.reference,
              s = -1 !== ['left', 'right'].indexOf(i),
              d = s ? 'height' : 'width',
              a = s ? 'top' : 'left',
              f = s ? 'left' : 'top',
              l = s ? 'bottom' : 'right',
              m = O(o)[d];
          p[l] - m < r[a] && (e.offsets.popper[a] -= r[a] - (p[l] - m)), p[a] + m > r[l] && (e.offsets.popper[a] += p[a] + m - r[l]);
          var h = p[a] + p[d] / 2 - m / 2,
              g = h - c(e.offsets.popper)[a];
          return g = _(z(r[d] - m, g), 0), e.arrowElement = o, e.offsets.arrow = {}, e.offsets.arrow[a] = Math.round(g), e.offsets.arrow[f] = '', e;
        },
        element: '[x-arrow]'
      },
      flip: {
        order: 600,
        enabled: !0,
        fn: function fn(e, t) {
          if (W(e.instance.modifiers, 'inner')) return e;
          if (e.flipped && e.placement === e.originalPlacement) return e;
          var o = w(e.instance.popper, e.instance.reference, t.padding, t.boundariesElement),
              i = e.placement.split('-')[0],
              n = L(i),
              r = e.placement.split('-')[1] || '',
              p = [];

          switch (t.behavior) {
            case fe.FLIP:
              p = [i, n];
              break;

            case fe.CLOCKWISE:
              p = K(i);
              break;

            case fe.COUNTERCLOCKWISE:
              p = K(i, !0);
              break;

            default:
              p = t.behavior;
          }

          return p.forEach(function (s, d) {
            if (i !== s || p.length === d + 1) return e;
            i = e.placement.split('-')[0], n = L(i);
            var a = e.offsets.popper,
                f = e.offsets.reference,
                l = V,
                m = 'left' === i && l(a.right) > l(f.left) || 'right' === i && l(a.left) < l(f.right) || 'top' === i && l(a.bottom) > l(f.top) || 'bottom' === i && l(a.top) < l(f.bottom),
                h = l(a.left) < l(o.left),
                c = l(a.right) > l(o.right),
                g = l(a.top) < l(o.top),
                u = l(a.bottom) > l(o.bottom),
                b = 'left' === i && h || 'right' === i && c || 'top' === i && g || 'bottom' === i && u,
                y = -1 !== ['top', 'bottom'].indexOf(i),
                w = !!t.flipVariations && (y && 'start' === r && h || y && 'end' === r && c || !y && 'start' === r && g || !y && 'end' === r && u);
            (m || b || w) && (e.flipped = !0, (m || b) && (i = p[d + 1]), w && (r = j(r)), e.placement = i + (r ? '-' + r : ''), e.offsets.popper = se({}, e.offsets.popper, S(e.instance.popper, e.offsets.reference, e.placement)), e = N(e.instance.modifiers, e, 'flip'));
          }), e;
        },
        behavior: 'flip',
        padding: 5,
        boundariesElement: 'viewport'
      },
      inner: {
        order: 700,
        enabled: !1,
        fn: function fn(e) {
          var t = e.placement,
              o = t.split('-')[0],
              i = e.offsets,
              n = i.popper,
              r = i.reference,
              p = -1 !== ['left', 'right'].indexOf(o),
              s = -1 === ['top', 'left'].indexOf(o);
          return n[p ? 'left' : 'top'] = r[t] - (s ? n[p ? 'width' : 'height'] : 0), e.placement = L(t), e.offsets.popper = c(n), e;
        }
      },
      hide: {
        order: 800,
        enabled: !0,
        fn: function fn(e) {
          if (!F(e.instance.modifiers, 'hide', 'preventOverflow')) return e;
          var t = e.offsets.reference,
              o = T(e.instance.modifiers, function (e) {
            return 'preventOverflow' === e.name;
          }).boundaries;

          if (t.bottom < o.top || t.left > o.right || t.top > o.bottom || t.right < o.left) {
            if (!0 === e.hide) return e;
            e.hide = !0, e.attributes['x-out-of-boundaries'] = '';
          } else {
            if (!1 === e.hide) return e;
            e.hide = !1, e.attributes['x-out-of-boundaries'] = !1;
          }

          return e;
        }
      },
      computeStyle: {
        order: 850,
        enabled: !0,
        fn: function fn(e, t) {
          var o = t.x,
              i = t.y,
              n = e.offsets.popper,
              p = T(e.instance.modifiers, function (e) {
            return 'applyStyle' === e.name;
          }).gpuAcceleration;
          void 0 !== p && console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');
          var s,
              d,
              a = void 0 === p ? t.gpuAcceleration : p,
              f = r(e.instance.popper),
              l = g(f),
              m = {
            position: n.position
          },
              h = {
            left: V(n.left),
            top: V(n.top),
            bottom: V(n.bottom),
            right: V(n.right)
          },
              c = 'bottom' === o ? 'top' : 'bottom',
              u = 'right' === i ? 'left' : 'right',
              b = B('transform');
          if (d = 'bottom' == c ? -l.height + h.bottom : h.top, s = 'right' == u ? -l.width + h.right : h.left, a && b) m[b] = 'translate3d(' + s + 'px, ' + d + 'px, 0)', m[c] = 0, m[u] = 0, m.willChange = 'transform';else {
            var y = 'bottom' == c ? -1 : 1,
                w = 'right' == u ? -1 : 1;
            m[c] = d * y, m[u] = s * w, m.willChange = c + ', ' + u;
          }
          var v = {
            "x-placement": e.placement
          };
          return e.attributes = se({}, v, e.attributes), e.styles = se({}, m, e.styles), e;
        },
        gpuAcceleration: !0,
        x: 'bottom',
        y: 'right'
      },
      applyStyle: {
        order: 900,
        enabled: !0,
        fn: function fn(e) {
          return U(e.instance.popper, e.styles), Y(e.instance.popper, e.attributes), e.offsets.arrow && U(e.arrowElement, e.offsets.arrow), e;
        },
        onLoad: function onLoad(e, t, o, i, n) {
          var r = x(n, t, e),
              p = E(o.placement, r, t, e, o.modifiers.flip.boundariesElement, o.modifiers.flip.padding);
          return t.setAttribute('x-placement', p), U(t, {
            position: 'absolute'
          }), o;
        },
        gpuAcceleration: void 0
      }
    }
  }, le;
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./public/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js":
/*!************************************************************************!*\
  !*** ./public/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

throw new Error("Module build failed (from ./node_modules/babel-loader/lib/index.js):\nSyntaxError: /home/casudin/www/micsimbawa/public/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js: Unexpected token, expected \":\" (4:334)\n\n\u001b[0m \u001b[90m 2 | \u001b[39m\u001b[33m!\u001b[39m\u001b[36mfunction\u001b[39m(a){\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m define\u001b[33m&&\u001b[39mdefine\u001b[33m.\u001b[39mamd\u001b[33m?\u001b[39mdefine([\u001b[32m\"jquery\"\u001b[39m]\u001b[33m,\u001b[39ma)\u001b[33m:\u001b[39m\u001b[32m\"object\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m exports\u001b[33m?\u001b[39mmodule\u001b[33m.\u001b[39mexports\u001b[33m=\u001b[39ma\u001b[33m:\u001b[39ma(jQuery)}(\u001b[36mfunction\u001b[39m(a){\u001b[36mfunction\u001b[39m b(b){\u001b[36mvar\u001b[39m g\u001b[33m=\u001b[39mb\u001b[33m||\u001b[39mwindow\u001b[33m.\u001b[39mevent\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mcall(arguments\u001b[33m,\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39mj\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(b\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mfix(g)\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39mtype\u001b[33m=\u001b[39m\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"detail\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(m\u001b[33m=\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m*\u001b[39mg\u001b[33m.\u001b[39mdetail)\u001b[33m,\u001b[39m\u001b[32m\"wheelDelta\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(m\u001b[33m=\u001b[39mg\u001b[33m.\u001b[39mwheelDelta)\u001b[33m,\u001b[39m\u001b[32m\"wheelDeltaY\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(m\u001b[33m=\u001b[39mg\u001b[33m.\u001b[39mwheelDeltaY)\u001b[33m,\u001b[39m\u001b[32m\"wheelDeltaX\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(l\u001b[33m=\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m*\u001b[39mg\u001b[33m.\u001b[39mwheelDeltaX)\u001b[33m,\u001b[39m\u001b[32m\"axis\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39mg\u001b[33m.\u001b[39maxis\u001b[33m===\u001b[39mg\u001b[33m.\u001b[39m\u001b[33mHORIZONTAL_AXIS\u001b[39m\u001b[33m&&\u001b[39m(l\u001b[33m=\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m*\u001b[39mm\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m)\u001b[33m,\u001b[39mj\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m===\u001b[39mm\u001b[33m?\u001b[39ml\u001b[33m:\u001b[39mm\u001b[33m,\u001b[39m\u001b[32m\"deltaY\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(m\u001b[33m=\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m*\u001b[39mg\u001b[33m.\u001b[39mdeltaY\u001b[33m,\u001b[39mj\u001b[33m=\u001b[39mm)\u001b[33m,\u001b[39m\u001b[32m\"deltaX\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(l\u001b[33m=\u001b[39mg\u001b[33m.\u001b[39mdeltaX\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m\u001b[33m===\u001b[39mm\u001b[33m&&\u001b[39m(j\u001b[33m=\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m*\u001b[39ml))\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m\u001b[33m!==\u001b[39mm\u001b[33m||\u001b[39m\u001b[35m0\u001b[39m\u001b[33m!==\u001b[39ml){\u001b[36mif\u001b[39m(\u001b[35m1\u001b[39m\u001b[33m===\u001b[39mg\u001b[33m.\u001b[39mdeltaMode){\u001b[36mvar\u001b[39m q\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mdata(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-line-height\"\u001b[39m)\u001b[33m;\u001b[39mj\u001b[33m*=\u001b[39mq\u001b[33m,\u001b[39mm\u001b[33m*=\u001b[39mq\u001b[33m,\u001b[39ml\u001b[33m*=\u001b[39mq}\u001b[36melse\u001b[39m \u001b[36mif\u001b[39m(\u001b[35m2\u001b[39m\u001b[33m===\u001b[39mg\u001b[33m.\u001b[39mdeltaMode){\u001b[36mvar\u001b[39m r\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mdata(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-page-height\"\u001b[39m)\u001b[33m;\u001b[39mj\u001b[33m*=\u001b[39mr\u001b[33m,\u001b[39mm\u001b[33m*=\u001b[39mr\u001b[33m,\u001b[39ml\u001b[33m*=\u001b[39mr}\u001b[36mif\u001b[39m(n\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mmax(\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(m)\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(l))\u001b[33m,\u001b[39m(\u001b[33m!\u001b[39mf\u001b[33m||\u001b[39mf\u001b[33m>\u001b[39mn)\u001b[33m&&\u001b[39m(f\u001b[33m=\u001b[39mn\u001b[33m,\u001b[39md(g\u001b[33m,\u001b[39mn)\u001b[33m&&\u001b[39m(f\u001b[35m/=40)),d(g,n)&&(j/\u001b[39m\u001b[33m=\u001b[39m\u001b[35m40\u001b[39m\u001b[33m,\u001b[39ml\u001b[35m/=40,m/\u001b[39m\u001b[33m=\u001b[39m\u001b[35m40\u001b[39m)\u001b[33m,\u001b[39mj\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m[j\u001b[33m>=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[32m\"floor\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"ceil\"\u001b[39m](j\u001b[33m/\u001b[39mf)\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m[l\u001b[33m>=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[32m\"floor\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"ceil\"\u001b[39m](l\u001b[33m/\u001b[39mf)\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m[m\u001b[33m>=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[32m\"floor\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"ceil\"\u001b[39m](m\u001b[33m/\u001b[39mf)\u001b[33m,\u001b[39mk\u001b[33m.\u001b[39msettings\u001b[33m.\u001b[39mnormalizeOffset\u001b[33m&&\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mgetBoundingClientRect){\u001b[36mvar\u001b[39m s\u001b[33m=\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mgetBoundingClientRect()\u001b[33m;\u001b[39mo\u001b[33m=\u001b[39mb\u001b[33m.\u001b[39mclientX\u001b[33m-\u001b[39ms\u001b[33m.\u001b[39mleft\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39mb\u001b[33m.\u001b[39mclientY\u001b[33m-\u001b[39ms\u001b[33m.\u001b[39mtop}\u001b[36mreturn\u001b[39m b\u001b[33m.\u001b[39mdeltaX\u001b[33m=\u001b[39ml\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39mdeltaY\u001b[33m=\u001b[39mm\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39mdeltaFactor\u001b[33m=\u001b[39mf\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39moffsetX\u001b[33m=\u001b[39mo\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39moffsetY\u001b[33m=\u001b[39mp\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39mdeltaMode\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mh\u001b[33m.\u001b[39munshift(b\u001b[33m,\u001b[39mj\u001b[33m,\u001b[39ml\u001b[33m,\u001b[39mm)\u001b[33m,\u001b[39me\u001b[33m&&\u001b[39mclearTimeout(e)\u001b[33m,\u001b[39me\u001b[33m=\u001b[39msetTimeout(c\u001b[33m,\u001b[39m\u001b[35m200\u001b[39m)\u001b[33m,\u001b[39m(a\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mdispatch\u001b[33m||\u001b[39ma\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mhandle)\u001b[33m.\u001b[39mapply(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39mh)}}\u001b[36mfunction\u001b[39m c(){f\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m}\u001b[36mfunction\u001b[39m d(a\u001b[33m,\u001b[39mb){\u001b[36mreturn\u001b[39m k\u001b[33m.\u001b[39msettings\u001b[33m.\u001b[39madjustOldDeltas\u001b[33m&&\u001b[39m\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m===\u001b[39ma\u001b[33m.\u001b[39mtype\u001b[33m&&\u001b[39mb\u001b[33m%\u001b[39m\u001b[35m120\u001b[39m\u001b[33m===\u001b[39m\u001b[35m0\u001b[39m}\u001b[36mvar\u001b[39m e\u001b[33m,\u001b[39mf\u001b[33m,\u001b[39mg\u001b[33m=\u001b[39m[\u001b[32m\"wheel\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"DOMMouseScroll\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"MozMousePixelScroll\"\u001b[39m]\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39m\u001b[32m\"onwheel\"\u001b[39m\u001b[36min\u001b[39m document\u001b[33m||\u001b[39mdocument\u001b[33m.\u001b[39mdocumentMode\u001b[33m>=\u001b[39m\u001b[35m9\u001b[39m\u001b[33m?\u001b[39m[\u001b[32m\"wheel\"\u001b[39m]\u001b[33m:\u001b[39m[\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"DomMouseScroll\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"MozMousePixelScroll\"\u001b[39m]\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39m\u001b[33mArray\u001b[39m\u001b[33m.\u001b[39mprototype\u001b[33m.\u001b[39mslice\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(a\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mfixHooks)\u001b[36mfor\u001b[39m(\u001b[36mvar\u001b[39m j\u001b[33m=\u001b[39mg\u001b[33m.\u001b[39mlength\u001b[33m;\u001b[39mj\u001b[33m;\u001b[39m)a\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mfixHooks[g[\u001b[33m--\u001b[39mj]]\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mmouseHooks\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m k\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mspecial\u001b[33m.\u001b[39mmousewheel\u001b[33m=\u001b[39m{version\u001b[33m:\u001b[39m\u001b[32m\"3.1.12\"\u001b[39m\u001b[33m,\u001b[39msetup\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mif\u001b[39m(\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39maddEventListener)\u001b[36mfor\u001b[39m(\u001b[36mvar\u001b[39m c\u001b[33m=\u001b[39mh\u001b[33m.\u001b[39mlength\u001b[33m;\u001b[39mc\u001b[33m;\u001b[39m)\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39maddEventListener(h[\u001b[33m--\u001b[39mc]\u001b[33m,\u001b[39mb\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m;\u001b[39m\u001b[36melse\u001b[39m \u001b[36mthis\u001b[39m\u001b[33m.\u001b[39monmousewheel\u001b[33m=\u001b[39mb\u001b[33m;\u001b[39ma\u001b[33m.\u001b[39mdata(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-line-height\"\u001b[39m\u001b[33m,\u001b[39mk\u001b[33m.\u001b[39mgetLineHeight(\u001b[36mthis\u001b[39m))\u001b[33m,\u001b[39ma\u001b[33m.\u001b[39mdata(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-page-height\"\u001b[39m\u001b[33m,\u001b[39mk\u001b[33m.\u001b[39mgetPageHeight(\u001b[36mthis\u001b[39m))}\u001b[33m,\u001b[39mteardown\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mif\u001b[39m(\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mremoveEventListener)\u001b[36mfor\u001b[39m(\u001b[36mvar\u001b[39m c\u001b[33m=\u001b[39mh\u001b[33m.\u001b[39mlength\u001b[33m;\u001b[39mc\u001b[33m;\u001b[39m)\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mremoveEventListener(h[\u001b[33m--\u001b[39mc]\u001b[33m,\u001b[39mb\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m;\u001b[39m\u001b[36melse\u001b[39m \u001b[36mthis\u001b[39m\u001b[33m.\u001b[39monmousewheel\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m;\u001b[39ma\u001b[33m.\u001b[39mremoveData(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-line-height\"\u001b[39m)\u001b[33m,\u001b[39ma\u001b[33m.\u001b[39mremoveData(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-page-height\"\u001b[39m)}\u001b[33m,\u001b[39mgetLineHeight\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(b){\u001b[36mvar\u001b[39m c\u001b[33m=\u001b[39ma(b)\u001b[33m,\u001b[39md\u001b[33m=\u001b[39mc[\u001b[32m\"offsetParent\"\u001b[39m\u001b[36min\u001b[39m a\u001b[33m.\u001b[39mfn\u001b[33m?\u001b[39m\u001b[32m\"offsetParent\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"parent\"\u001b[39m]()\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m d\u001b[33m.\u001b[39mlength\u001b[33m||\u001b[39m(d\u001b[33m=\u001b[39ma(\u001b[32m\"body\"\u001b[39m))\u001b[33m,\u001b[39mparseInt(d\u001b[33m.\u001b[39mcss(\u001b[32m\"fontSize\"\u001b[39m)\u001b[33m,\u001b[39m\u001b[35m10\u001b[39m)\u001b[33m||\u001b[39mparseInt(c\u001b[33m.\u001b[39mcss(\u001b[32m\"fontSize\"\u001b[39m)\u001b[33m,\u001b[39m\u001b[35m10\u001b[39m)\u001b[33m||\u001b[39m\u001b[35m16\u001b[39m}\u001b[33m,\u001b[39mgetPageHeight\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(b){\u001b[36mreturn\u001b[39m a(b)\u001b[33m.\u001b[39mheight()}\u001b[33m,\u001b[39msettings\u001b[33m:\u001b[39m{adjustOldDeltas\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mnormalizeOffset\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m}}\u001b[33m;\u001b[39ma\u001b[33m.\u001b[39mfn\u001b[33m.\u001b[39mextend({mousewheel\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(a){\u001b[36mreturn\u001b[39m a\u001b[33m?\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mbind(\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m,\u001b[39ma)\u001b[33m:\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mtrigger(\u001b[32m\"mousewheel\"\u001b[39m)}\u001b[33m,\u001b[39munmousewheel\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(a){\u001b[36mreturn\u001b[39m \u001b[36mthis\u001b[39m\u001b[33m.\u001b[39munbind(\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m,\u001b[39ma)}})})\u001b[33m;\u001b[39m\u001b[33m!\u001b[39m\u001b[36mfunction\u001b[39m(a){\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m define\u001b[33m&&\u001b[39mdefine\u001b[33m.\u001b[39mamd\u001b[33m?\u001b[39mdefine([\u001b[32m\"jquery\"\u001b[39m]\u001b[33m,\u001b[39ma)\u001b[33m:\u001b[39m\u001b[32m\"object\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m exports\u001b[33m?\u001b[39mmodule\u001b[33m.\u001b[39mexports\u001b[33m=\u001b[39ma\u001b[33m:\u001b[39ma(jQuery)}(\u001b[36mfunction\u001b[39m(a){\u001b[36mfunction\u001b[39m b(b){\u001b[36mvar\u001b[39m g\u001b[33m=\u001b[39mb\u001b[33m||\u001b[39mwindow\u001b[33m.\u001b[39mevent\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mcall(arguments\u001b[33m,\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39mj\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(b\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mfix(g)\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39mtype\u001b[33m=\u001b[39m\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"detail\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(m\u001b[33m=\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m*\u001b[39mg\u001b[33m.\u001b[39mdetail)\u001b[33m,\u001b[39m\u001b[32m\"wheelDelta\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(m\u001b[33m=\u001b[39mg\u001b[33m.\u001b[39mwheelDelta)\u001b[33m,\u001b[39m\u001b[32m\"wheelDeltaY\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(m\u001b[33m=\u001b[39mg\u001b[33m.\u001b[39mwheelDeltaY)\u001b[33m,\u001b[39m\u001b[32m\"wheelDeltaX\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(l\u001b[33m=\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m*\u001b[39mg\u001b[33m.\u001b[39mwheelDeltaX)\u001b[33m,\u001b[39m\u001b[32m\"axis\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39mg\u001b[33m.\u001b[39maxis\u001b[33m===\u001b[39mg\u001b[33m.\u001b[39m\u001b[33mHORIZONTAL_AXIS\u001b[39m\u001b[33m&&\u001b[39m(l\u001b[33m=\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m*\u001b[39mm\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m)\u001b[33m,\u001b[39mj\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m===\u001b[39mm\u001b[33m?\u001b[39ml\u001b[33m:\u001b[39mm\u001b[33m,\u001b[39m\u001b[32m\"deltaY\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(m\u001b[33m=\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m*\u001b[39mg\u001b[33m.\u001b[39mdeltaY\u001b[33m,\u001b[39mj\u001b[33m=\u001b[39mm)\u001b[33m,\u001b[39m\u001b[32m\"deltaX\"\u001b[39m\u001b[36min\u001b[39m g\u001b[33m&&\u001b[39m(l\u001b[33m=\u001b[39mg\u001b[33m.\u001b[39mdeltaX\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m\u001b[33m===\u001b[39mm\u001b[33m&&\u001b[39m(j\u001b[33m=\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m*\u001b[39ml))\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m\u001b[33m!==\u001b[39mm\u001b[33m||\u001b[39m\u001b[35m0\u001b[39m\u001b[33m!==\u001b[39ml){\u001b[36mif\u001b[39m(\u001b[35m1\u001b[39m\u001b[33m===\u001b[39mg\u001b[33m.\u001b[39mdeltaMode){\u001b[36mvar\u001b[39m q\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mdata(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-line-height\"\u001b[39m)\u001b[33m;\u001b[39mj\u001b[33m*=\u001b[39mq\u001b[33m,\u001b[39mm\u001b[33m*=\u001b[39mq\u001b[33m,\u001b[39ml\u001b[33m*=\u001b[39mq}\u001b[36melse\u001b[39m \u001b[36mif\u001b[39m(\u001b[35m2\u001b[39m\u001b[33m===\u001b[39mg\u001b[33m.\u001b[39mdeltaMode){\u001b[36mvar\u001b[39m r\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mdata(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-page-height\"\u001b[39m)\u001b[33m;\u001b[39mj\u001b[33m*=\u001b[39mr\u001b[33m,\u001b[39mm\u001b[33m*=\u001b[39mr\u001b[33m,\u001b[39ml\u001b[33m*=\u001b[39mr}\u001b[36mif\u001b[39m(n\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mmax(\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(m)\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(l))\u001b[33m,\u001b[39m(\u001b[33m!\u001b[39mf\u001b[33m||\u001b[39mf\u001b[33m>\u001b[39mn)\u001b[33m&&\u001b[39m(f\u001b[33m=\u001b[39mn\u001b[33m,\u001b[39md(g\u001b[33m,\u001b[39mn)\u001b[33m&&\u001b[39m(f\u001b[35m/=40)),d(g,n)&&(j/\u001b[39m\u001b[33m=\u001b[39m\u001b[35m40\u001b[39m\u001b[33m,\u001b[39ml\u001b[35m/=40,m/\u001b[39m\u001b[33m=\u001b[39m\u001b[35m40\u001b[39m)\u001b[33m,\u001b[39mj\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m[j\u001b[33m>=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[32m\"floor\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"ceil\"\u001b[39m](j\u001b[33m/\u001b[39mf)\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m[l\u001b[33m>=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[32m\"floor\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"ceil\"\u001b[39m](l\u001b[33m/\u001b[39mf)\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m[m\u001b[33m>=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[32m\"floor\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"ceil\"\u001b[39m](m\u001b[33m/\u001b[39mf)\u001b[33m,\u001b[39mk\u001b[33m.\u001b[39msettings\u001b[33m.\u001b[39mnormalizeOffset\u001b[33m&&\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mgetBoundingClientRect){\u001b[36mvar\u001b[39m s\u001b[33m=\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mgetBoundingClientRect()\u001b[33m;\u001b[39mo\u001b[33m=\u001b[39mb\u001b[33m.\u001b[39mclientX\u001b[33m-\u001b[39ms\u001b[33m.\u001b[39mleft\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39mb\u001b[33m.\u001b[39mclientY\u001b[33m-\u001b[39ms\u001b[33m.\u001b[39mtop}\u001b[36mreturn\u001b[39m b\u001b[33m.\u001b[39mdeltaX\u001b[33m=\u001b[39ml\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39mdeltaY\u001b[33m=\u001b[39mm\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39mdeltaFactor\u001b[33m=\u001b[39mf\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39moffsetX\u001b[33m=\u001b[39mo\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39moffsetY\u001b[33m=\u001b[39mp\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39mdeltaMode\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mh\u001b[33m.\u001b[39munshift(b\u001b[33m,\u001b[39mj\u001b[33m,\u001b[39ml\u001b[33m,\u001b[39mm)\u001b[33m,\u001b[39me\u001b[33m&&\u001b[39mclearTimeout(e)\u001b[33m,\u001b[39me\u001b[33m=\u001b[39msetTimeout(c\u001b[33m,\u001b[39m\u001b[35m200\u001b[39m)\u001b[33m,\u001b[39m(a\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mdispatch\u001b[33m||\u001b[39ma\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mhandle)\u001b[33m.\u001b[39mapply(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39mh)}}\u001b[36mfunction\u001b[39m c(){f\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m}\u001b[36mfunction\u001b[39m d(a\u001b[33m,\u001b[39mb){\u001b[36mreturn\u001b[39m k\u001b[33m.\u001b[39msettings\u001b[33m.\u001b[39madjustOldDeltas\u001b[33m&&\u001b[39m\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m===\u001b[39ma\u001b[33m.\u001b[39mtype\u001b[33m&&\u001b[39mb\u001b[33m%\u001b[39m\u001b[35m120\u001b[39m\u001b[33m===\u001b[39m\u001b[35m0\u001b[39m}\u001b[36mvar\u001b[39m e\u001b[33m,\u001b[39mf\u001b[33m,\u001b[39mg\u001b[33m=\u001b[39m[\u001b[32m\"wheel\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"DOMMouseScroll\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"MozMousePixelScroll\"\u001b[39m]\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39m\u001b[32m\"onwheel\"\u001b[39m\u001b[36min\u001b[39m document\u001b[33m||\u001b[39mdocument\u001b[33m.\u001b[39mdocumentMode\u001b[33m>=\u001b[39m\u001b[35m9\u001b[39m\u001b[33m?\u001b[39m[\u001b[32m\"wheel\"\u001b[39m]\u001b[33m:\u001b[39m[\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"DomMouseScroll\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"MozMousePixelScroll\"\u001b[39m]\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39m\u001b[33mArray\u001b[39m\u001b[33m.\u001b[39mprototype\u001b[33m.\u001b[39mslice\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(a\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mfixHooks)\u001b[36mfor\u001b[39m(\u001b[36mvar\u001b[39m j\u001b[33m=\u001b[39mg\u001b[33m.\u001b[39mlength\u001b[33m;\u001b[39mj\u001b[33m;\u001b[39m)a\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mfixHooks[g[\u001b[33m--\u001b[39mj]]\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mmouseHooks\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m k\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mspecial\u001b[33m.\u001b[39mmousewheel\u001b[33m=\u001b[39m{version\u001b[33m:\u001b[39m\u001b[32m\"3.1.12\"\u001b[39m\u001b[33m,\u001b[39msetup\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mif\u001b[39m(\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39maddEventListener)\u001b[36mfor\u001b[39m(\u001b[36mvar\u001b[39m c\u001b[33m=\u001b[39mh\u001b[33m.\u001b[39mlength\u001b[33m;\u001b[39mc\u001b[33m;\u001b[39m)\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39maddEventListener(h[\u001b[33m--\u001b[39mc]\u001b[33m,\u001b[39mb\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m;\u001b[39m\u001b[36melse\u001b[39m \u001b[36mthis\u001b[39m\u001b[33m.\u001b[39monmousewheel\u001b[33m=\u001b[39mb\u001b[33m;\u001b[39ma\u001b[33m.\u001b[39mdata(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-line-height\"\u001b[39m\u001b[33m,\u001b[39mk\u001b[33m.\u001b[39mgetLineHeight(\u001b[36mthis\u001b[39m))\u001b[33m,\u001b[39ma\u001b[33m.\u001b[39mdata(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-page-height\"\u001b[39m\u001b[33m,\u001b[39mk\u001b[33m.\u001b[39mgetPageHeight(\u001b[36mthis\u001b[39m))}\u001b[33m,\u001b[39mteardown\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mif\u001b[39m(\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mremoveEventListener)\u001b[36mfor\u001b[39m(\u001b[36mvar\u001b[39m c\u001b[33m=\u001b[39mh\u001b[33m.\u001b[39mlength\u001b[33m;\u001b[39mc\u001b[33m;\u001b[39m)\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mremoveEventListener(h[\u001b[33m--\u001b[39mc]\u001b[33m,\u001b[39mb\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m;\u001b[39m\u001b[36melse\u001b[39m \u001b[36mthis\u001b[39m\u001b[33m.\u001b[39monmousewheel\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m;\u001b[39ma\u001b[33m.\u001b[39mremoveData(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-line-height\"\u001b[39m)\u001b[33m,\u001b[39ma\u001b[33m.\u001b[39mremoveData(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mousewheel-page-height\"\u001b[39m)}\u001b[33m,\u001b[39mgetLineHeight\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(b){\u001b[36mvar\u001b[39m c\u001b[33m=\u001b[39ma(b)\u001b[33m,\u001b[39md\u001b[33m=\u001b[39mc[\u001b[32m\"offsetParent\"\u001b[39m\u001b[36min\u001b[39m a\u001b[33m.\u001b[39mfn\u001b[33m?\u001b[39m\u001b[32m\"offsetParent\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"parent\"\u001b[39m]()\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m d\u001b[33m.\u001b[39mlength\u001b[33m||\u001b[39m(d\u001b[33m=\u001b[39ma(\u001b[32m\"body\"\u001b[39m))\u001b[33m,\u001b[39mparseInt(d\u001b[33m.\u001b[39mcss(\u001b[32m\"fontSize\"\u001b[39m)\u001b[33m,\u001b[39m\u001b[35m10\u001b[39m)\u001b[33m||\u001b[39mparseInt(c\u001b[33m.\u001b[39mcss(\u001b[32m\"fontSize\"\u001b[39m)\u001b[33m,\u001b[39m\u001b[35m10\u001b[39m)\u001b[33m||\u001b[39m\u001b[35m16\u001b[39m}\u001b[33m,\u001b[39mgetPageHeight\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(b){\u001b[36mreturn\u001b[39m a(b)\u001b[33m.\u001b[39mheight()}\u001b[33m,\u001b[39msettings\u001b[33m:\u001b[39m{adjustOldDeltas\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mnormalizeOffset\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m}}\u001b[33m;\u001b[39ma\u001b[33m.\u001b[39mfn\u001b[33m.\u001b[39mextend({mousewheel\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(a){\u001b[36mreturn\u001b[39m a\u001b[33m?\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mbind(\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m,\u001b[39ma)\u001b[33m:\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mtrigger(\u001b[32m\"mousewheel\"\u001b[39m)}\u001b[33m,\u001b[39munmousewheel\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(a){\u001b[36mreturn\u001b[39m \u001b[36mthis\u001b[39m\u001b[33m.\u001b[39munbind(\u001b[32m\"mousewheel\"\u001b[39m\u001b[33m,\u001b[39ma)}})})\u001b[33m;\u001b[39m\u001b[0m\n\u001b[0m \u001b[90m 3 | \u001b[39m\u001b[90m/* == malihu jquery custom scrollbar plugin == Version: 3.1.5, License: MIT License (MIT) */\u001b[39m\u001b[0m\n\u001b[0m\u001b[31m\u001b[1m>\u001b[22m\u001b[39m\u001b[90m 4 | \u001b[39m\u001b[33m!\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m define\u001b[33m&&\u001b[39mdefine\u001b[33m.\u001b[39mamd\u001b[33m?\u001b[39mdefine([\u001b[32m\"jquery\"\u001b[39m]\u001b[33m,\u001b[39me)\u001b[33m:\u001b[39m\u001b[32m\"undefined\"\u001b[39m\u001b[33m!=\u001b[39m\u001b[36mtypeof\u001b[39m module\u001b[33m&&\u001b[39mmodule\u001b[33m.\u001b[39mexports\u001b[33m?\u001b[39mmodule\u001b[33m.\u001b[39mexports\u001b[33m=\u001b[39me\u001b[33m:\u001b[39me(jQuery\u001b[33m,\u001b[39mwindow\u001b[33m,\u001b[39mdocument)}(\u001b[36mfunction\u001b[39m(e){\u001b[33m!\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m define\u001b[33m&&\u001b[39mdefine\u001b[33m.\u001b[39mamd\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39m\u001b[32m\"undefined\"\u001b[39m\u001b[33m!=\u001b[39m\u001b[36mtypeof\u001b[39m module\u001b[33m&&\u001b[39mmodule\u001b[33m.\u001b[39mexports\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39m\u001b[32m\"https:\"\u001b[39m\u001b[33m==\u001b[39mdocument\u001b[33m.\u001b[39mlocation\u001b[33m.\u001b[39mprotocol\u001b[33m?\u001b[39m\u001b[32m\"jquery-mousewheel.js\"\u001b[39m\u001b[33m;\u001b[39mo\u001b[33m||\u001b[39m(a\u001b[33m?\u001b[39mrequire(\u001b[32m\"jquery-mousewheel\"\u001b[39m)(e)\u001b[33m:\u001b[39me\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mspecial\u001b[33m.\u001b[39mmousewheel\u001b[33m||\u001b[39me(\u001b[32m\"head\"\u001b[39m)\u001b[33m.\u001b[39mappend(decodeURI(\u001b[32m\"%3Cscript src=\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m+\u001b[39m\u001b[32m\"//\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m+\u001b[39m\u001b[32m\"%3E%3C/script%3E\"\u001b[39m)))\u001b[33m,\u001b[39mt()}(\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39m\u001b[32m\"mCustomScrollbar\"\u001b[39m\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39m\u001b[32m\"mCS\"\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39m\u001b[32m\".mCustomScrollbar\"\u001b[39m\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39m{setTop\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39msetLeft\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39maxis\u001b[33m:\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mscrollbarPosition\u001b[33m:\u001b[39m\u001b[32m\"inside\"\u001b[39m\u001b[33m,\u001b[39mscrollInertia\u001b[33m:\u001b[39m\u001b[35m950\u001b[39m\u001b[33m,\u001b[39mautoDraggerLength\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39malwaysShowScrollbar\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39msnapOffset\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mmouseWheel\u001b[33m:\u001b[39m{enable\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mscrollAmount\u001b[33m:\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m,\u001b[39maxis\u001b[33m:\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mdeltaFactor\u001b[33m:\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m,\u001b[39mdisableOver\u001b[33m:\u001b[39m[\u001b[32m\"select\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"option\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"keygen\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"datalist\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"textarea\"\u001b[39m]}\u001b[33m,\u001b[39mscrollButtons\u001b[33m:\u001b[39m{scrollType\u001b[33m:\u001b[39m\u001b[32m\"stepless\"\u001b[39m\u001b[33m,\u001b[39mscrollAmount\u001b[33m:\u001b[39m\u001b[32m\"auto\"\u001b[39m}\u001b[33m,\u001b[39mkeyboard\u001b[33m:\u001b[39m{enable\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mscrollType\u001b[33m:\u001b[39m\u001b[32m\"stepless\"\u001b[39m\u001b[33m,\u001b[39mscrollAmount\u001b[33m:\u001b[39m\u001b[32m\"auto\"\u001b[39m}\u001b[33m,\u001b[39mcontentTouchScroll\u001b[33m:\u001b[39m\u001b[35m25\u001b[39m\u001b[33m,\u001b[39mdocumentTouchScroll\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39madvanced\u001b[33m:\u001b[39m{autoScrollOnFocus\u001b[33m:\u001b[39m\u001b[32m\"input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']\"\u001b[39m\u001b[33m,\u001b[39mupdateOnContentResize\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mupdateOnImageLoad\u001b[33m:\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m,\u001b[39mautoUpdateTimeout\u001b[33m:\u001b[39m\u001b[35m60\u001b[39m}\u001b[33m,\u001b[39mtheme\u001b[33m:\u001b[39m\u001b[32m\"light\"\u001b[39m\u001b[33m,\u001b[39mcallbacks\u001b[33m:\u001b[39m{onTotalScrollOffset\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39monTotalScrollBackOffset\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39malwaysTriggerOffsets\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m}}\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m{}\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39mwindow\u001b[33m.\u001b[39mattachEvent\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mwindow\u001b[33m.\u001b[39maddEventListener\u001b[33m?\u001b[39m\u001b[35m1\u001b[39m\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39md\u001b[33m=\u001b[39m[\u001b[32m\"mCSB_dragger_onDrag\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCSB_scrollTools_onDrag\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCS_img_loaded\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCS_disabled\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCS_destroyed\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCS_no_scrollbar\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCS-autoHide\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCS-dir-rtl\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCS_no_scrollbar_y\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCS_no_scrollbar_x\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCS_y_hidden\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCS_x_hidden\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCSB_draggerContainer\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCSB_buttonUp\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCSB_buttonDown\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCSB_buttonLeft\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCSB_buttonRight\"\u001b[39m]\u001b[33m,\u001b[39mu\u001b[33m=\u001b[39m{init\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me\u001b[33m.\u001b[39mextend(\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m{}\u001b[33m,\u001b[39mi\u001b[33m,\u001b[39mt)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mf\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(t\u001b[33m.\u001b[39mlive){\u001b[36mvar\u001b[39m s\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mliveSelector\u001b[33m||\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mselector\u001b[33m||\u001b[39mn\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39me(s)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"off\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m.\u001b[39mlive)\u001b[36mreturn\u001b[39m \u001b[36mvoid\u001b[39m m(s)\u001b[33m;\u001b[39ml[s]\u001b[33m=\u001b[39msetTimeout(\u001b[36mfunction\u001b[39m(){c\u001b[33m.\u001b[39mmCustomScrollbar(t)\u001b[33m,\u001b[39m\u001b[32m\"once\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m.\u001b[39mlive\u001b[33m&&\u001b[39mc\u001b[33m.\u001b[39mlength\u001b[33m&&\u001b[39mm(s)}\u001b[33m,\u001b[39m\u001b[35m500\u001b[39m)}\u001b[36melse\u001b[39m m(s)\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m t\u001b[33m.\u001b[39msetWidth\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mset_width\u001b[33m?\u001b[39mt\u001b[33m.\u001b[39mset_width\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39msetWidth\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39msetHeight\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mset_height\u001b[33m?\u001b[39mt\u001b[33m.\u001b[39mset_height\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39msetHeight\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39maxis\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mhorizontalScroll\u001b[33m?\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m:\u001b[39mp(t\u001b[33m.\u001b[39maxis)\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mscrollInertia\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mscrollInertia\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39mt\u001b[33m.\u001b[39mscrollInertia\u001b[33m<\u001b[39m\u001b[35m17\u001b[39m\u001b[33m?\u001b[39m\u001b[35m17\u001b[39m\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mscrollInertia\u001b[33m,\u001b[39m\u001b[32m\"object\"\u001b[39m\u001b[33m!=\u001b[39m\u001b[36mtypeof\u001b[39m t\u001b[33m.\u001b[39mmouseWheel\u001b[33m&&\u001b[39m\u001b[35m1\u001b[39m\u001b[33m==\u001b[39mt\u001b[33m.\u001b[39mmouseWheel\u001b[33m&&\u001b[39m(t\u001b[33m.\u001b[39mmouseWheel\u001b[33m=\u001b[39m{enable\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mscrollAmount\u001b[33m:\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m,\u001b[39maxis\u001b[33m:\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mpreventDefault\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mdeltaFactor\u001b[33m:\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m,\u001b[39mnormalizeDelta\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39minvert\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m})\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mscrollAmount\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mmouseWheelPixels\u001b[33m?\u001b[39mt\u001b[33m.\u001b[39mmouseWheelPixels\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mscrollAmount\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mnormalizeDelta\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mnormalizeMouseWheelDelta\u001b[33m?\u001b[39mt\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mnormalizeMouseWheelDelta\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mnormalizeDelta\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mscrollButtons\u001b[33m.\u001b[39mscrollType\u001b[33m=\u001b[39mg(t\u001b[33m.\u001b[39mscrollButtons\u001b[33m.\u001b[39mscrollType)\u001b[33m,\u001b[39mh(t)\u001b[33m,\u001b[39me(o)\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33m!\u001b[39mo\u001b[33m.\u001b[39mdata(a)){o\u001b[33m.\u001b[39mdata(a\u001b[33m,\u001b[39m{idx\u001b[33m:\u001b[39m\u001b[33m++\u001b[39mr\u001b[33m,\u001b[39mopt\u001b[33m:\u001b[39mt\u001b[33m,\u001b[39mscrollRatio\u001b[33m:\u001b[39m{y\u001b[33m:\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mx\u001b[33m:\u001b[39m\u001b[36mnull\u001b[39m}\u001b[33m,\u001b[39moverflowed\u001b[33m:\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mcontentReset\u001b[33m:\u001b[39m{y\u001b[33m:\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mx\u001b[33m:\u001b[39m\u001b[36mnull\u001b[39m}\u001b[33m,\u001b[39mbindEvents\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mtweenRunning\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39msequential\u001b[33m:\u001b[39m{}\u001b[33m,\u001b[39mlangDir\u001b[33m:\u001b[39mo\u001b[33m.\u001b[39mcss(\u001b[32m\"direction\"\u001b[39m)\u001b[33m,\u001b[39mcbOffsets\u001b[33m:\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mtrigger\u001b[33m:\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mpoll\u001b[33m:\u001b[39m{size\u001b[33m:\u001b[39m{o\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m}\u001b[33m,\u001b[39mimg\u001b[33m:\u001b[39m{o\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m}\u001b[33m,\u001b[39mchange\u001b[33m:\u001b[39m{o\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m}}})\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m n\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mdata(\u001b[32m\"mcs-axis\"\u001b[39m)\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mdata(\u001b[32m\"mcs-scrollbar-position\"\u001b[39m)\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mdata(\u001b[32m\"mcs-theme\"\u001b[39m)\u001b[33m;\u001b[39ml\u001b[33m&&\u001b[39m(i\u001b[33m.\u001b[39maxis\u001b[33m=\u001b[39ml)\u001b[33m,\u001b[39ms\u001b[33m&&\u001b[39m(i\u001b[33m.\u001b[39mscrollbarPosition\u001b[33m=\u001b[39ms)\u001b[33m,\u001b[39mc\u001b[33m&&\u001b[39m(i\u001b[33m.\u001b[39mtheme\u001b[33m=\u001b[39mc\u001b[33m,\u001b[39mh(i))\u001b[33m,\u001b[39mv\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mn\u001b[33m&&\u001b[39mi\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monCreate\u001b[33m&&\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m i\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monCreate\u001b[33m&&\u001b[39mi\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monCreate\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container img:not(.\"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m2\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\")\"\u001b[39m)\u001b[33m.\u001b[39maddClass(d[\u001b[35m2\u001b[39m])\u001b[33m,\u001b[39mu\u001b[33m.\u001b[39mupdate\u001b[33m.\u001b[39mcall(\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mo)}})}\u001b[33m,\u001b[39mupdate\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(t\u001b[33m,\u001b[39mo){\u001b[36mvar\u001b[39m n\u001b[33m=\u001b[39mt\u001b[33m||\u001b[39mf\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m e(n)\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(t\u001b[33m.\u001b[39mdata(a)){\u001b[36mvar\u001b[39m n\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx)\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39m[e(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical\"\u001b[39m)\u001b[33m,\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal\"\u001b[39m)]\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33m!\u001b[39mr\u001b[33m.\u001b[39mlength)\u001b[36mreturn\u001b[39m\u001b[33m;\u001b[39mn\u001b[33m.\u001b[39mtweenRunning\u001b[33m&&\u001b[39m\u001b[33mQ\u001b[39m(t)\u001b[33m,\u001b[39mo\u001b[33m&&\u001b[39mn\u001b[33m&&\u001b[39mi\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monBeforeUpdate\u001b[33m&&\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m i\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monBeforeUpdate\u001b[33m&&\u001b[39mi\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monBeforeUpdate\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mhasClass(d[\u001b[35m3\u001b[39m])\u001b[33m&&\u001b[39mt\u001b[33m.\u001b[39mremoveClass(d[\u001b[35m3\u001b[39m])\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mhasClass(d[\u001b[35m4\u001b[39m])\u001b[33m&&\u001b[39mt\u001b[33m.\u001b[39mremoveClass(d[\u001b[35m4\u001b[39m])\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39mcss(\u001b[32m\"max-height\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"none\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39mheight()\u001b[33m!==\u001b[39mt\u001b[33m.\u001b[39mheight()\u001b[33m&&\u001b[39ml\u001b[33m.\u001b[39mcss(\u001b[32m\"max-height\"\u001b[39m\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mheight())\u001b[33m,\u001b[39m_\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m||\u001b[39mi\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mautoExpandHorizontalScroll\u001b[33m||\u001b[39mr\u001b[33m.\u001b[39mcss(\u001b[32m\"width\"\u001b[39m\u001b[33m,\u001b[39mx(r))\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39moverflowed\u001b[33m=\u001b[39my\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mM\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39mautoDraggerLength\u001b[33m&&\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mb\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mT\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m c\u001b[33m=\u001b[39m[\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(r[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop)\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(r[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft)]\u001b[33m;\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39m(n\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m?\u001b[39ms[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mheight()\u001b[33m>\u001b[39ms[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mheight()\u001b[33m?\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m:\u001b[39m(\u001b[33mG\u001b[39m(t\u001b[33m,\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mdur\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39moverwrite\u001b[33m:\u001b[39m\u001b[32m\"none\"\u001b[39m})\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39mcontentReset\u001b[33m.\u001b[39my\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m)\u001b[33m:\u001b[39m(\u001b[33mB\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39mk\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m:\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39m\u001b[33mG\u001b[39m(t\u001b[33m,\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39mdur\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39moverwrite\u001b[33m:\u001b[39m\u001b[32m\"none\"\u001b[39m})))\u001b[33m,\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39m(n\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m?\u001b[39ms[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mwidth()\u001b[33m>\u001b[39ms[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mwidth()\u001b[33m?\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m:\u001b[39m(\u001b[33mG\u001b[39m(t\u001b[33m,\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39mdur\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39moverwrite\u001b[33m:\u001b[39m\u001b[32m\"none\"\u001b[39m})\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39mcontentReset\u001b[33m.\u001b[39mx\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m)\u001b[33m:\u001b[39m(\u001b[33mB\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39mk\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m:\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39m\u001b[33mG\u001b[39m(t\u001b[33m,\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mdur\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39moverwrite\u001b[33m:\u001b[39m\u001b[32m\"none\"\u001b[39m})))\u001b[33m,\u001b[39mo\u001b[33m&&\u001b[39mn\u001b[33m&&\u001b[39m(\u001b[35m2\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m&&\u001b[39mi\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monImageLoad\u001b[33m&&\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m i\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monImageLoad\u001b[33m?\u001b[39mi\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monImageLoad\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m:\u001b[39m\u001b[35m3\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m&&\u001b[39mi\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monSelectorChange\u001b[33m&&\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m i\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monSelectorChange\u001b[33m?\u001b[39mi\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monSelectorChange\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m:\u001b[39mi\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monUpdate\u001b[33m&&\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m i\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monUpdate\u001b[33m&&\u001b[39mi\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monUpdate\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m))\u001b[33m,\u001b[39m\u001b[33mN\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)}})}\u001b[33m,\u001b[39mscrollTo\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(t\u001b[33m,\u001b[39mo){\u001b[36mif\u001b[39m(\u001b[32m\"undefined\"\u001b[39m\u001b[33m!=\u001b[39m\u001b[36mtypeof\u001b[39m t\u001b[33m&&\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m!=\u001b[39mt){\u001b[36mvar\u001b[39m n\u001b[33m=\u001b[39mf\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m e(n)\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m n\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(n\u001b[33m.\u001b[39mdata(a)){\u001b[36mvar\u001b[39m i\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m{trigger\u001b[33m:\u001b[39m\u001b[32m\"external\"\u001b[39m\u001b[33m,\u001b[39mscrollInertia\u001b[33m:\u001b[39mr\u001b[33m.\u001b[39mscrollInertia\u001b[33m,\u001b[39mscrollEasing\u001b[33m:\u001b[39m\u001b[32m\"mcsEaseInOut\"\u001b[39m\u001b[33m,\u001b[39mmoveDragger\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mtimeout\u001b[33m:\u001b[39m\u001b[35m60\u001b[39m\u001b[33m,\u001b[39mcallbacks\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39monStart\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39monUpdate\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39monComplete\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m}\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39me\u001b[33m.\u001b[39mextend(\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m{}\u001b[33m,\u001b[39ml\u001b[33m,\u001b[39mo)\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m\u001b[33mY\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39mt)\u001b[33m,\u001b[39md\u001b[33m=\u001b[39ms\u001b[33m.\u001b[39mscrollInertia\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39ms\u001b[33m.\u001b[39mscrollInertia\u001b[33m<\u001b[39m\u001b[35m17\u001b[39m\u001b[33m?\u001b[39m\u001b[35m17\u001b[39m\u001b[33m:\u001b[39ms\u001b[33m.\u001b[39mscrollInertia\u001b[33m;\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m=\u001b[39m\u001b[33mX\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[32m\"y\"\u001b[39m)\u001b[33m,\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m=\u001b[39m\u001b[33mX\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39m\u001b[32m\"x\"\u001b[39m)\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mmoveDragger\u001b[33m&&\u001b[39m(c[\u001b[35m0\u001b[39m]\u001b[33m*=\u001b[39mi\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39my\u001b[33m,\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m*=\u001b[39mi\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39mx)\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mdur\u001b[33m=\u001b[39mne()\u001b[33m?\u001b[39m\u001b[35m0\u001b[39m\u001b[33m:\u001b[39md\u001b[33m,\u001b[39msetTimeout(\u001b[36mfunction\u001b[39m(){\u001b[36mnull\u001b[39m\u001b[33m!==\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39m\u001b[32m\"undefined\"\u001b[39m\u001b[33m!=\u001b[39m\u001b[36mtypeof\u001b[39m c[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m!==\u001b[39mr\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39mi\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39m(s\u001b[33m.\u001b[39mdir\u001b[33m=\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39moverwrite\u001b[33m=\u001b[39m\u001b[32m\"all\"\u001b[39m\u001b[33m,\u001b[39m\u001b[33mG\u001b[39m(n\u001b[33m,\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39ms))\u001b[33m,\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m!==\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39m\u001b[32m\"undefined\"\u001b[39m\u001b[33m!=\u001b[39m\u001b[36mtypeof\u001b[39m c[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m!==\u001b[39mr\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39mi\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39m(s\u001b[33m.\u001b[39mdir\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39moverwrite\u001b[33m=\u001b[39m\u001b[32m\"none\"\u001b[39m\u001b[33m,\u001b[39m\u001b[33mG\u001b[39m(n\u001b[33m,\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39ms))}\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mtimeout)}})}}\u001b[33m,\u001b[39mstop\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39mf\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m e(t)\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m&&\u001b[39m\u001b[33mQ\u001b[39m(t)})}\u001b[33m,\u001b[39mdisable\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39mf\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m e(o)\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(o\u001b[33m.\u001b[39mdata(a)){o\u001b[33m.\u001b[39mdata(a)\u001b[33m;\u001b[39m\u001b[33mN\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"remove\"\u001b[39m)\u001b[33m,\u001b[39mk\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mt\u001b[33m&&\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mM\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m.\u001b[39maddClass(d[\u001b[35m3\u001b[39m])}})}\u001b[33m,\u001b[39mdestroy\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39mf\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m e(t)\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m n\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(n\u001b[33m.\u001b[39mdata(a)){\u001b[36mvar\u001b[39m i\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m.\u001b[39midx)\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39me(\u001b[32m\".mCSB_\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar\"\u001b[39m)\u001b[33m;\u001b[39mr\u001b[33m.\u001b[39mlive\u001b[33m&&\u001b[39mm(r\u001b[33m.\u001b[39mliveSelector\u001b[33m||\u001b[39me(t)\u001b[33m.\u001b[39mselector)\u001b[33m,\u001b[39m\u001b[33mN\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"remove\"\u001b[39m)\u001b[33m,\u001b[39mk\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39mremoveData(a)\u001b[33m,\u001b[39m$(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mcs\"\u001b[39m)\u001b[33m,\u001b[39mc\u001b[33m.\u001b[39mremove()\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mfind(\u001b[32m\"img.\"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m2\u001b[39m])\u001b[33m.\u001b[39mremoveClass(d[\u001b[35m2\u001b[39m])\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39mreplaceWith(s\u001b[33m.\u001b[39mcontents())\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39mremoveClass(o\u001b[33m+\u001b[39m\u001b[32m\" _\"\u001b[39m\u001b[33m+\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m6\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m7\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m5\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m3\u001b[39m])\u001b[33m.\u001b[39maddClass(d[\u001b[35m4\u001b[39m])}})}}\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mreturn\u001b[39m\u001b[32m\"object\"\u001b[39m\u001b[33m!=\u001b[39m\u001b[36mtypeof\u001b[39m e(\u001b[36mthis\u001b[39m)\u001b[33m||\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m.\u001b[39mlength\u001b[33m<\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39mn\u001b[33m:\u001b[39m\u001b[36mthis\u001b[39m}\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39m[\u001b[32m\"rounded\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"rounded-dark\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"rounded-dots\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"rounded-dots-dark\"\u001b[39m]\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39m[\u001b[32m\"rounded-dots\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"rounded-dots-dark\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"3d\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"3d-dark\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"3d-thick\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"3d-thick-dark\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"inset\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"inset-dark\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"inset-2\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"inset-2-dark\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"inset-3\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"inset-3-dark\"\u001b[39m]\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39m[\u001b[32m\"minimal\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"minimal-dark\"\u001b[39m]\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39m[\u001b[32m\"minimal\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"minimal-dark\"\u001b[39m]\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m[\u001b[32m\"minimal\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"minimal-dark\"\u001b[39m]\u001b[33m;\u001b[39mt\u001b[33m.\u001b[39mautoDraggerLength\u001b[33m=\u001b[39me\u001b[33m.\u001b[39minArray(t\u001b[33m.\u001b[39mtheme\u001b[33m,\u001b[39mo)\u001b[33m>\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mautoDraggerLength\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mautoExpandScrollbar\u001b[33m=\u001b[39me\u001b[33m.\u001b[39minArray(t\u001b[33m.\u001b[39mtheme\u001b[33m,\u001b[39ma)\u001b[33m>\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mautoExpandScrollbar\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mscrollButtons\u001b[33m.\u001b[39menable\u001b[33m=\u001b[39me\u001b[33m.\u001b[39minArray(t\u001b[33m.\u001b[39mtheme\u001b[33m,\u001b[39mn)\u001b[33m>\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mscrollButtons\u001b[33m.\u001b[39menable\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mautoHideScrollbar\u001b[33m=\u001b[39me\u001b[33m.\u001b[39minArray(t\u001b[33m.\u001b[39mtheme\u001b[33m,\u001b[39mi)\u001b[33m>\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mautoHideScrollbar\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mscrollbarPosition\u001b[33m=\u001b[39me\u001b[33m.\u001b[39minArray(t\u001b[33m.\u001b[39mtheme\u001b[33m,\u001b[39mr)\u001b[33m>\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[32m\"outside\"\u001b[39m\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mscrollbarPosition}\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e){l[e]\u001b[33m&&\u001b[39m(clearTimeout(l[e])\u001b[33m,\u001b[39m$(l\u001b[33m,\u001b[39me))}\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[36mreturn\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39me\u001b[33m||\u001b[39m\u001b[32m\"xy\"\u001b[39m\u001b[33m===\u001b[39me\u001b[33m||\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m===\u001b[39me\u001b[33m?\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39me\u001b[33m||\u001b[39m\u001b[32m\"horizontal\"\u001b[39m\u001b[33m===\u001b[39me\u001b[33m?\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"y\"\u001b[39m}\u001b[33m,\u001b[39mg\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[36mreturn\u001b[39m\u001b[32m\"stepped\"\u001b[39m\u001b[33m===\u001b[39me\u001b[33m||\u001b[39m\u001b[32m\"pixels\"\u001b[39m\u001b[33m===\u001b[39me\u001b[33m||\u001b[39m\u001b[32m\"step\"\u001b[39m\u001b[33m===\u001b[39me\u001b[33m||\u001b[39m\u001b[32m\"click\"\u001b[39m\u001b[33m===\u001b[39me\u001b[33m?\u001b[39m\u001b[32m\"stepped\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"stepless\"\u001b[39m}\u001b[33m,\u001b[39mv\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mautoExpandScrollbar\u001b[33m?\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m1\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\"_expand\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"\"\u001b[39m\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m[\u001b[32m\"<div id='mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar_vertical' class='mCSB_scrollTools mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar mCS-\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m.\u001b[39mtheme\u001b[33m+\u001b[39m\u001b[32m\" mCSB_scrollTools_vertical\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\"'><div class='\"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m12\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\"'><div id='mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"<div id='mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar_horizontal' class='mCSB_scrollTools mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar mCS-\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m.\u001b[39mtheme\u001b[33m+\u001b[39m\u001b[32m\" mCSB_scrollTools_horizontal\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\"'><div class='\"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m12\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\"'><div id='mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>\"\u001b[39m]\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m\u001b[32m\"mCSB_vertical_horizontal\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m\u001b[32m\"mCSB_horizontal\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"mCSB_vertical\"\u001b[39m\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39ml[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39ml[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39ml[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39ml[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mu\u001b[33m=\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m\u001b[32m\"<div id='mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container_wrapper' class='mCSB_container_wrapper' />\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"\"\u001b[39m\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mautoHideScrollbar\u001b[33m?\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m6\u001b[39m]\u001b[33m:\u001b[39m\u001b[32m\"\"\u001b[39m\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39m\u001b[32m\"rtl\"\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m.\u001b[39mlangDir\u001b[33m?\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m7\u001b[39m]\u001b[33m:\u001b[39m\u001b[32m\"\"\u001b[39m\u001b[33m;\u001b[39mi\u001b[33m.\u001b[39msetWidth\u001b[33m&&\u001b[39mt\u001b[33m.\u001b[39mcss(\u001b[32m\"width\"\u001b[39m\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39msetWidth)\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39msetHeight\u001b[33m&&\u001b[39mt\u001b[33m.\u001b[39mcss(\u001b[32m\"height\"\u001b[39m\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39msetHeight)\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39msetLeft\u001b[33m=\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39m\u001b[32m\"rtl\"\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m.\u001b[39mlangDir\u001b[33m?\u001b[39m\u001b[32m\"989999px\"\u001b[39m\u001b[33m:\u001b[39mi\u001b[33m.\u001b[39msetLeft\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39maddClass(o\u001b[33m+\u001b[39m\u001b[32m\" _\"\u001b[39m\u001b[33m+\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39mf\u001b[33m+\u001b[39mh)\u001b[33m.\u001b[39mwrapInner(\u001b[32m\"<div id='mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"' class='mCustomScrollBox mCS-\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m.\u001b[39mtheme\u001b[33m+\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39ms\u001b[33m+\u001b[39m\u001b[32m\"'><div id='mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container' class='mCSB_container' style='position:relative; top:\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m.\u001b[39msetTop\u001b[33m+\u001b[39m\u001b[32m\"; left:\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m.\u001b[39msetLeft\u001b[33m+\u001b[39m\u001b[32m\";' dir='\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39mlangDir\u001b[33m+\u001b[39m\u001b[32m\"' /></div>\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m m\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx)\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m||\u001b[39mi\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mautoExpandHorizontalScroll\u001b[33m||\u001b[39mp\u001b[33m.\u001b[39mcss(\u001b[32m\"width\"\u001b[39m\u001b[33m,\u001b[39mx(p))\u001b[33m,\u001b[39m\u001b[32m\"outside\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39mscrollbarPosition\u001b[33m?\u001b[39m(\u001b[32m\"static\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m.\u001b[39mcss(\u001b[32m\"position\"\u001b[39m)\u001b[33m&&\u001b[39mt\u001b[33m.\u001b[39mcss(\u001b[32m\"position\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"relative\"\u001b[39m)\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mcss(\u001b[32m\"overflow\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"visible\"\u001b[39m)\u001b[33m,\u001b[39mm\u001b[33m.\u001b[39maddClass(\u001b[32m\"mCSB_outside\"\u001b[39m)\u001b[33m.\u001b[39mafter(c))\u001b[33m:\u001b[39m(m\u001b[33m.\u001b[39maddClass(\u001b[32m\"mCSB_inside\"\u001b[39m)\u001b[33m.\u001b[39mappend(c)\u001b[33m,\u001b[39mp\u001b[33m.\u001b[39mwrap(u))\u001b[33m,\u001b[39mw\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m g\u001b[33m=\u001b[39m[e(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical\"\u001b[39m)\u001b[33m,\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal\"\u001b[39m)]\u001b[33m;\u001b[39mg[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mcss(\u001b[32m\"min-height\"\u001b[39m\u001b[33m,\u001b[39mg[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mheight())\u001b[33m,\u001b[39mg[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mcss(\u001b[32m\"min-width\"\u001b[39m\u001b[33m,\u001b[39mg[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mwidth())}\u001b[33m,\u001b[39mx\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39m[t[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mscrollWidth\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mmax\u001b[33m.\u001b[39mapply(\u001b[33mMath\u001b[39m\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mchildren()\u001b[33m.\u001b[39mmap(\u001b[36mfunction\u001b[39m(){\u001b[36mreturn\u001b[39m e(\u001b[36mthis\u001b[39m)\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m)})\u001b[33m.\u001b[39mget())]\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mwidth()\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m o[\u001b[35m0\u001b[39m]\u001b[33m>\u001b[39ma\u001b[33m?\u001b[39mo[\u001b[35m0\u001b[39m]\u001b[33m:\u001b[39mo[\u001b[35m1\u001b[39m]\u001b[33m>\u001b[39ma\u001b[33m?\u001b[39mo[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39m\u001b[32m\"100%\"\u001b[39m}\u001b[33m,\u001b[39m_\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(n\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mautoExpandHorizontalScroll\u001b[33m&&\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m!==\u001b[39mn\u001b[33m.\u001b[39maxis){i\u001b[33m.\u001b[39mcss({width\u001b[33m:\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"min-width\"\u001b[39m\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"overflow-x\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"scroll\"\u001b[39m})\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m r\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mceil(i[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mscrollWidth)\u001b[33m;\u001b[39m\u001b[35m3\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mautoExpandHorizontalScroll\u001b[33m||\u001b[39m\u001b[35m2\u001b[39m\u001b[33m!==\u001b[39mn\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mautoExpandHorizontalScroll\u001b[33m&&\u001b[39mr\u001b[33m>\u001b[39mi\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mwidth()\u001b[33m?\u001b[39mi\u001b[33m.\u001b[39mcss({width\u001b[33m:\u001b[39mr\u001b[33m,\u001b[39m\u001b[32m\"min-width\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"100%\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"overflow-x\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"inherit\"\u001b[39m})\u001b[33m:\u001b[39mi\u001b[33m.\u001b[39mcss({\u001b[32m\"overflow-x\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"inherit\"\u001b[39m\u001b[33m,\u001b[39mposition\u001b[33m:\u001b[39m\u001b[32m\"absolute\"\u001b[39m})\u001b[33m.\u001b[39mwrap(\u001b[32m\"<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />\"\u001b[39m)\u001b[33m.\u001b[39mcss({width\u001b[33m:\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mceil(i[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mgetBoundingClientRect()\u001b[33m.\u001b[39mright\u001b[33m+\u001b[39m\u001b[35m.4\u001b[39m)\u001b[33m-\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mfloor(i[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mgetBoundingClientRect()\u001b[33m.\u001b[39mleft)\u001b[33m,\u001b[39m\u001b[32m\"min-width\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"100%\"\u001b[39m\u001b[33m,\u001b[39mposition\u001b[33m:\u001b[39m\u001b[32m\"relative\"\u001b[39m})\u001b[33m.\u001b[39munwrap()}}\u001b[33m,\u001b[39mw\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39me(\u001b[32m\".mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar:first\"\u001b[39m)\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39moe(n\u001b[33m.\u001b[39mscrollButtons\u001b[33m.\u001b[39mtabindex)\u001b[33m?\u001b[39m\u001b[32m\"tabindex='\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39mscrollButtons\u001b[33m.\u001b[39mtabindex\u001b[33m+\u001b[39m\u001b[32m\"'\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"\"\u001b[39m\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m[\u001b[32m\"<a href='#' class='\"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m13\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\"' \"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" />\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"<a href='#' class='\"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m14\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\"' \"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" />\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"<a href='#' class='\"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m15\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\"' \"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" />\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"<a href='#' class='\"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m16\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\"' \"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" />\"\u001b[39m]\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39m[\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39ml[\u001b[35m2\u001b[39m]\u001b[33m:\u001b[39ml[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39ml[\u001b[35m3\u001b[39m]\u001b[33m:\u001b[39ml[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39ml[\u001b[35m2\u001b[39m]\u001b[33m,\u001b[39ml[\u001b[35m3\u001b[39m]]\u001b[33m;\u001b[39mn\u001b[33m.\u001b[39mscrollButtons\u001b[33m.\u001b[39menable\u001b[33m&&\u001b[39mi\u001b[33m.\u001b[39mprepend(s[\u001b[35m0\u001b[39m])\u001b[33m.\u001b[39mappend(s[\u001b[35m1\u001b[39m])\u001b[33m.\u001b[39mnext(\u001b[32m\".mCSB_scrollTools\"\u001b[39m)\u001b[33m.\u001b[39mprepend(s[\u001b[35m2\u001b[39m])\u001b[33m.\u001b[39mappend(s[\u001b[35m3\u001b[39m])}\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m[e(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical\"\u001b[39m)\u001b[33m,\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal\"\u001b[39m)]\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m[n\u001b[33m.\u001b[39mheight()\u001b[35m/i.outerHeight(!1),n.width()/i\u001b[39m\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)]\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m[parseInt(r[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mcss(\u001b[32m\"min-height\"\u001b[39m))\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mround(l[\u001b[35m0\u001b[39m]\u001b[33m*\u001b[39mr[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mheight())\u001b[33m,\u001b[39mparseInt(r[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mcss(\u001b[32m\"min-width\"\u001b[39m))\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mround(l[\u001b[35m1\u001b[39m]\u001b[33m*\u001b[39mr[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mwidth())]\u001b[33m,\u001b[39md\u001b[33m=\u001b[39ms\u001b[33m&&\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m<\u001b[39m\u001b[33mc\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m?\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m:\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39mu\u001b[33m=\u001b[39ms\u001b[33m&&\u001b[39mc[\u001b[35m3\u001b[39m]\u001b[33m<\u001b[39m\u001b[33mc\u001b[39m[\u001b[35m2\u001b[39m]\u001b[33m?\u001b[39mc[\u001b[35m2\u001b[39m]\u001b[33m:\u001b[39mc[\u001b[35m3\u001b[39m]\u001b[33m;\u001b[39mr[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mcss({height\u001b[33m:\u001b[39md\u001b[33m,\u001b[39m\u001b[32m\"max-height\"\u001b[39m\u001b[33m:\u001b[39mr[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mheight()\u001b[33m-\u001b[39m\u001b[35m10\u001b[39m})\u001b[33m.\u001b[39mfind(\u001b[32m\".mCSB_dragger_bar\"\u001b[39m)\u001b[33m.\u001b[39mcss({\u001b[32m\"line-height\"\u001b[39m\u001b[33m:\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\"px\"\u001b[39m})\u001b[33m,\u001b[39mr[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mcss({width\u001b[33m:\u001b[39mu\u001b[33m,\u001b[39m\u001b[32m\"max-width\"\u001b[39m\u001b[33m:\u001b[39mr[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mwidth()\u001b[33m-\u001b[39m\u001b[35m10\u001b[39m})}\u001b[33m,\u001b[39mb\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m[e(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical\"\u001b[39m)\u001b[33m,\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal\"\u001b[39m)]\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m[i\u001b[33m.\u001b[39mouterHeight(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m-\u001b[39mn\u001b[33m.\u001b[39mheight()\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m-\u001b[39mn\u001b[33m.\u001b[39mwidth()]\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39m[l[\u001b[35m0\u001b[39m]\u001b[33m/\u001b[39m(r[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mheight()\u001b[33m-\u001b[39mr[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mheight())\u001b[33m,\u001b[39ml[\u001b[35m1\u001b[39m]\u001b[33m/\u001b[39m(r[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mwidth()\u001b[33m-\u001b[39mr[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mwidth())]\u001b[33m;\u001b[39mo\u001b[33m.\u001b[39mscrollRatio\u001b[33m=\u001b[39m{y\u001b[33m:\u001b[39ms[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mx\u001b[33m:\u001b[39ms[\u001b[35m1\u001b[39m]}}\u001b[33m,\u001b[39m\u001b[33mC\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e\u001b[33m,\u001b[39mt\u001b[33m,\u001b[39mo){\u001b[36mvar\u001b[39m a\u001b[33m=\u001b[39mo\u001b[33m?\u001b[39md[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\"_expanded\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"\"\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39me\u001b[33m.\u001b[39mclosest(\u001b[32m\".mCSB_scrollTools\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[32m\"active\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m?\u001b[39m(e\u001b[33m.\u001b[39mtoggleClass(d[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39ma)\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39mtoggleClass(d[\u001b[35m1\u001b[39m])\u001b[33m,\u001b[39me[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39m_draggable\u001b[33m=\u001b[39me[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39m_draggable\u001b[33m?\u001b[39m\u001b[35m0\u001b[39m\u001b[33m:\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m:\u001b[39me[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39m_draggable\u001b[33m||\u001b[39m(\u001b[32m\"hide\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m?\u001b[39m(e\u001b[33m.\u001b[39mremoveClass(d[\u001b[35m0\u001b[39m])\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39mremoveClass(d[\u001b[35m1\u001b[39m]))\u001b[33m:\u001b[39m(e\u001b[33m.\u001b[39maddClass(d[\u001b[35m0\u001b[39m])\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39maddClass(d[\u001b[35m1\u001b[39m])))}\u001b[33m,\u001b[39my\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m==\u001b[39mo\u001b[33m.\u001b[39moverflowed\u001b[33m?\u001b[39mi\u001b[33m.\u001b[39mheight()\u001b[33m:\u001b[39mi\u001b[33m.\u001b[39mouterHeight(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m==\u001b[39mo\u001b[33m.\u001b[39moverflowed\u001b[33m?\u001b[39mi\u001b[33m.\u001b[39mwidth()\u001b[33m:\u001b[39mi\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39mi[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mscrollHeight\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39mi[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mscrollWidth\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m s\u001b[33m>\u001b[39mr\u001b[33m&&\u001b[39m(r\u001b[33m=\u001b[39ms)\u001b[33m,\u001b[39mc\u001b[33m>\u001b[39ml\u001b[33m&&\u001b[39m(l\u001b[33m=\u001b[39mc)\u001b[33m,\u001b[39m[r\u001b[33m>\u001b[39mn\u001b[33m.\u001b[39mheight()\u001b[33m,\u001b[39ml\u001b[33m>\u001b[39mn\u001b[33m.\u001b[39mwidth()]}\u001b[33m,\u001b[39m\u001b[33mB\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx)\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m[e(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical\"\u001b[39m)\u001b[33m,\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal\"\u001b[39m)]\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33mQ\u001b[39m(t)\u001b[33m,\u001b[39m(\u001b[32m\"x\"\u001b[39m\u001b[33m!==\u001b[39mn\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mo\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m||\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39mo\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m])\u001b[33m&&\u001b[39m(l[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39madd(r)\u001b[33m.\u001b[39mcss(\u001b[32m\"top\"\u001b[39m\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mG\u001b[39m(t\u001b[33m,\u001b[39m\u001b[32m\"_resetY\"\u001b[39m))\u001b[33m,\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m!==\u001b[39mn\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mo\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m||\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39mo\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]){\u001b[36mvar\u001b[39m s\u001b[33m=\u001b[39mdx\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m;\u001b[39m\u001b[32m\"rtl\"\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m.\u001b[39mlangDir\u001b[33m&&\u001b[39m(s\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mwidth()\u001b[33m-\u001b[39mr\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39mdx\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(s\u001b[33m/\u001b[39mo\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39mx))\u001b[33m,\u001b[39mr\u001b[33m.\u001b[39mcss(\u001b[32m\"left\"\u001b[39m\u001b[33m,\u001b[39ms)\u001b[33m,\u001b[39ml[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mcss(\u001b[32m\"left\"\u001b[39m\u001b[33m,\u001b[39mdx)\u001b[33m,\u001b[39m\u001b[33mG\u001b[39m(t\u001b[33m,\u001b[39m\u001b[32m\"_resetX\"\u001b[39m)}}\u001b[33m,\u001b[39m\u001b[33mT\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mfunction\u001b[39m t(){r\u001b[33m=\u001b[39msetTimeout(\u001b[36mfunction\u001b[39m(){e\u001b[33m.\u001b[39mevent\u001b[33m.\u001b[39mspecial\u001b[33m.\u001b[39mmousewheel\u001b[33m?\u001b[39m(clearTimeout(r)\u001b[33m,\u001b[39m\u001b[33mW\u001b[39m\u001b[33m.\u001b[39mcall(o[\u001b[35m0\u001b[39m]))\u001b[33m:\u001b[39mt()}\u001b[33m,\u001b[39m\u001b[35m100\u001b[39m)}\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mopt\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33m!\u001b[39mn\u001b[33m.\u001b[39mbindEvents){\u001b[36mif\u001b[39m(\u001b[33mI\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39mcontentTouchScroll\u001b[33m&&\u001b[39m\u001b[33mD\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mE\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39menable){\u001b[36mvar\u001b[39m r\u001b[33m;\u001b[39mt()}\u001b[33mP\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mU\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mautoScrollOnFocus\u001b[33m&&\u001b[39m\u001b[33mH\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39mscrollButtons\u001b[33m.\u001b[39menable\u001b[33m&&\u001b[39m\u001b[33mF\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39mkeyboard\u001b[33m.\u001b[39menable\u001b[33m&&\u001b[39mq\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39mbindEvents\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m}}\u001b[33m,\u001b[39mk\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m\u001b[32m\".mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar\"\u001b[39m\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\",#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container,#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container_wrapper,\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" .\"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m12\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\",#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical,#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal,\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\">a\"\u001b[39m)\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m;\u001b[39mn\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mreleaseDraggableSelectors\u001b[33m&&\u001b[39ml\u001b[33m.\u001b[39madd(e(n\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mreleaseDraggableSelectors))\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mextraDraggableSelectors\u001b[33m&&\u001b[39ml\u001b[33m.\u001b[39madd(e(n\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mextraDraggableSelectors))\u001b[33m,\u001b[39mo\u001b[33m.\u001b[39mbindEvents\u001b[33m&&\u001b[39m(e(document)\u001b[33m.\u001b[39madd(e(\u001b[33m!\u001b[39m\u001b[33mA\u001b[39m()\u001b[33m||\u001b[39mtop\u001b[33m.\u001b[39mdocument))\u001b[33m.\u001b[39munbind(\u001b[32m\".\"\u001b[39m\u001b[33m+\u001b[39mi)\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){e(\u001b[36mthis\u001b[39m)\u001b[33m.\u001b[39munbind(\u001b[32m\".\"\u001b[39m\u001b[33m+\u001b[39mi)})\u001b[33m,\u001b[39mclearTimeout(t[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39m_focusTimeout)\u001b[33m,\u001b[39m$(t[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[32m\"_focusTimeout\"\u001b[39m)\u001b[33m,\u001b[39mclearTimeout(o\u001b[33m.\u001b[39msequential\u001b[33m.\u001b[39mstep)\u001b[33m,\u001b[39m$(o\u001b[33m.\u001b[39msequential\u001b[33m,\u001b[39m\u001b[32m\"step\"\u001b[39m)\u001b[33m,\u001b[39mclearTimeout(s[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39monCompleteTimeout)\u001b[33m,\u001b[39m$(s[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[32m\"onCompleteTimeout\"\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m.\u001b[39mbindEvents\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)}\u001b[33m,\u001b[39m\u001b[33mM\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container_wrapper\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39mr\u001b[33m.\u001b[39mlength\u001b[33m?\u001b[39mr\u001b[33m:\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39m[e(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar_vertical\"\u001b[39m)\u001b[33m,\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar_horizontal\"\u001b[39m)]\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m[s[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mfind(\u001b[32m\".mCSB_dragger\"\u001b[39m)\u001b[33m,\u001b[39ms[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mfind(\u001b[32m\".mCSB_dragger\"\u001b[39m)]\u001b[33m;\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39m(n\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mt\u001b[33m?\u001b[39m(s[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39madd(c[\u001b[35m0\u001b[39m])\u001b[33m.\u001b[39madd(s[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mchildren(\u001b[32m\"a\"\u001b[39m))\u001b[33m.\u001b[39mcss(\u001b[32m\"display\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"block\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39mremoveClass(d[\u001b[35m8\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m10\u001b[39m]))\u001b[33m:\u001b[39m(i\u001b[33m.\u001b[39malwaysShowScrollbar\u001b[33m?\u001b[39m(\u001b[35m2\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39malwaysShowScrollbar\u001b[33m&&\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mcss(\u001b[32m\"display\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"none\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39mremoveClass(d[\u001b[35m10\u001b[39m]))\u001b[33m:\u001b[39m(s[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mcss(\u001b[32m\"display\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"none\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39maddClass(d[\u001b[35m10\u001b[39m]))\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39maddClass(d[\u001b[35m8\u001b[39m])))\u001b[33m,\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39m(n\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mt\u001b[33m?\u001b[39m(s[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39madd(c[\u001b[35m1\u001b[39m])\u001b[33m.\u001b[39madd(s[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mchildren(\u001b[32m\"a\"\u001b[39m))\u001b[33m.\u001b[39mcss(\u001b[32m\"display\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"block\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39mremoveClass(d[\u001b[35m9\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\" \"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m11\u001b[39m]))\u001b[33m:\u001b[39m(i\u001b[33m.\u001b[39malwaysShowScrollbar\u001b[33m?\u001b[39m(\u001b[35m2\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39malwaysShowScrollbar\u001b[33m&&\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mcss(\u001b[32m\"display\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"none\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39mremoveClass(d[\u001b[35m11\u001b[39m]))\u001b[33m:\u001b[39m(s[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mcss(\u001b[32m\"display\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"none\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39maddClass(d[\u001b[35m11\u001b[39m]))\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39maddClass(d[\u001b[35m9\u001b[39m])))\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m||\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m?\u001b[39mo\u001b[33m.\u001b[39mremoveClass(d[\u001b[35m5\u001b[39m])\u001b[33m:\u001b[39mo\u001b[33m.\u001b[39maddClass(d[\u001b[35m5\u001b[39m])}\u001b[33m,\u001b[39m\u001b[33mO\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mtype\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mtarget\u001b[33m.\u001b[39mownerDocument\u001b[33m!==\u001b[39mdocument\u001b[33m&&\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m!==\u001b[39mframeElement\u001b[33m?\u001b[39m[e(frameElement)\u001b[33m.\u001b[39moffset()\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39me(frameElement)\u001b[33m.\u001b[39moffset()\u001b[33m.\u001b[39mleft]\u001b[33m:\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39m\u001b[33mA\u001b[39m()\u001b[33m&&\u001b[39mt\u001b[33m.\u001b[39mtarget\u001b[33m.\u001b[39mownerDocument\u001b[33m!==\u001b[39mtop\u001b[33m.\u001b[39mdocument\u001b[33m&&\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m!==\u001b[39mframeElement\u001b[33m?\u001b[39m[e(t\u001b[33m.\u001b[39mview\u001b[33m.\u001b[39mframeElement)\u001b[33m.\u001b[39moffset()\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39me(t\u001b[33m.\u001b[39mview\u001b[33m.\u001b[39mframeElement)\u001b[33m.\u001b[39moffset()\u001b[33m.\u001b[39mleft]\u001b[33m:\u001b[39m[\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m]\u001b[33m;\u001b[39m\u001b[36mswitch\u001b[39m(o){\u001b[36mcase\u001b[39m\u001b[32m\"pointerdown\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"MSPointerDown\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"pointermove\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"MSPointerMove\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"pointerup\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"MSPointerUp\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mreturn\u001b[39m a\u001b[33m?\u001b[39m[t\u001b[33m.\u001b[39moriginalEvent\u001b[33m.\u001b[39mpageY\u001b[33m-\u001b[39ma[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39mn[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39moriginalEvent\u001b[33m.\u001b[39mpageX\u001b[33m-\u001b[39ma[\u001b[35m1\u001b[39m]\u001b[33m+\u001b[39mn[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39m[t\u001b[33m.\u001b[39moriginalEvent\u001b[33m.\u001b[39mpageY\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39moriginalEvent\u001b[33m.\u001b[39mpageX\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m]\u001b[33m;\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"touchstart\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"touchmove\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"touchend\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mvar\u001b[39m i\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39moriginalEvent\u001b[33m.\u001b[39mtouches[\u001b[35m0\u001b[39m]\u001b[33m||\u001b[39mt\u001b[33m.\u001b[39moriginalEvent\u001b[33m.\u001b[39mchangedTouches[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39moriginalEvent\u001b[33m.\u001b[39mtouches\u001b[33m.\u001b[39mlength\u001b[33m||\u001b[39mt\u001b[33m.\u001b[39moriginalEvent\u001b[33m.\u001b[39mchangedTouches\u001b[33m.\u001b[39mlength\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m t\u001b[33m.\u001b[39mtarget\u001b[33m.\u001b[39mownerDocument\u001b[33m!==\u001b[39mdocument\u001b[33m?\u001b[39m[i\u001b[33m.\u001b[39mscreenY\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39mscreenX\u001b[33m,\u001b[39mr\u001b[33m>\u001b[39m\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39m[i\u001b[33m.\u001b[39mpageY\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39mpageX\u001b[33m,\u001b[39mr\u001b[33m>\u001b[39m\u001b[35m1\u001b[39m]\u001b[33m;\u001b[39m\u001b[36mdefault\u001b[39m\u001b[33m:\u001b[39m\u001b[36mreturn\u001b[39m a\u001b[33m?\u001b[39m[t\u001b[33m.\u001b[39mpageY\u001b[33m-\u001b[39ma[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39mn[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mpageX\u001b[33m-\u001b[39ma[\u001b[35m1\u001b[39m]\u001b[33m+\u001b[39mn[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39m[t\u001b[33m.\u001b[39mpageY\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mpageX\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m]}}\u001b[33m,\u001b[39m\u001b[33mI\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mfunction\u001b[39m t(e\u001b[33m,\u001b[39mt\u001b[33m,\u001b[39ma\u001b[33m,\u001b[39mn){\u001b[36mif\u001b[39m(h[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39midleTimer\u001b[33m=\u001b[39md\u001b[33m.\u001b[39mscrollInertia\u001b[33m<\u001b[39m\u001b[35m233\u001b[39m\u001b[33m?\u001b[39m\u001b[35m250\u001b[39m\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mo\u001b[33m.\u001b[39mattr(\u001b[32m\"id\"\u001b[39m)\u001b[33m===\u001b[39mf[\u001b[35m1\u001b[39m])\u001b[36mvar\u001b[39m i\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39m(o[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft\u001b[33m-\u001b[39mt\u001b[33m+\u001b[39mn)\u001b[33m*\u001b[39ml\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39mx\u001b[33m;\u001b[39m\u001b[36melse\u001b[39m \u001b[36mvar\u001b[39m i\u001b[33m=\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39m(o[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop\u001b[33m-\u001b[39me\u001b[33m+\u001b[39ma)\u001b[33m*\u001b[39ml\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39my\u001b[33m;\u001b[39m\u001b[33mG\u001b[39m(r\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39mi\u001b[33m,\u001b[39mdrag\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m})}\u001b[36mvar\u001b[39m o\u001b[33m,\u001b[39mn\u001b[33m,\u001b[39mi\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39mr\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39md\u001b[33m=\u001b[39ml\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mu\u001b[33m=\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39ml\u001b[33m.\u001b[39midx\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39m[\u001b[32m\"mCSB_\"\u001b[39m\u001b[33m+\u001b[39ml\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"mCSB_\"\u001b[39m\u001b[33m+\u001b[39ml\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal\"\u001b[39m]\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39ml\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39me(\u001b[32m\"#\"\u001b[39m\u001b[33m+\u001b[39mf[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39m\u001b[32m\",#\"\u001b[39m\u001b[33m+\u001b[39mf[\u001b[35m1\u001b[39m])\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39md\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mreleaseDraggableSelectors\u001b[33m?\u001b[39mm\u001b[33m.\u001b[39madd(e(d\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mreleaseDraggableSelectors))\u001b[33m:\u001b[39mm\u001b[33m,\u001b[39mg\u001b[33m=\u001b[39md\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mextraDraggableSelectors\u001b[33m?\u001b[39me(\u001b[33m!\u001b[39m\u001b[33mA\u001b[39m()\u001b[33m||\u001b[39mtop\u001b[33m.\u001b[39mdocument)\u001b[33m.\u001b[39madd(e(d\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mextraDraggableSelectors))\u001b[33m:\u001b[39me(\u001b[33m!\u001b[39m\u001b[33mA\u001b[39m()\u001b[33m||\u001b[39mtop\u001b[33m.\u001b[39mdocument)\u001b[33m;\u001b[39mm\u001b[33m.\u001b[39mbind(\u001b[32m\"contextmenu.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){e\u001b[33m.\u001b[39mpreventDefault()})\u001b[33m.\u001b[39mbind(\u001b[32m\"mousedown.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m+\u001b[39m\u001b[32m\" touchstart.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m+\u001b[39m\u001b[32m\" pointerdown.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m+\u001b[39m\u001b[32m\" MSPointerDown.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mif\u001b[39m(t\u001b[33m.\u001b[39mstopImmediatePropagation()\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mpreventDefault()\u001b[33m,\u001b[39mee(t)){c\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39ms\u001b[33m&&\u001b[39m(document\u001b[33m.\u001b[39monselectstart\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mreturn\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m})\u001b[33m,\u001b[39m\u001b[33mL\u001b[39m\u001b[33m.\u001b[39mcall(h\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mQ\u001b[39m(r)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m a\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39moffset()\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(t)[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39ma\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39mu\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(t)[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39ma\u001b[33m.\u001b[39mleft\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mheight()\u001b[33m+\u001b[39ma\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mwidth()\u001b[33m+\u001b[39ma\u001b[33m.\u001b[39mleft\u001b[33m;\u001b[39mf\u001b[33m>\u001b[39ml\u001b[33m&&\u001b[39ml\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39mm\u001b[33m>\u001b[39mu\u001b[33m&&\u001b[39mu\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39m(n\u001b[33m=\u001b[39ml\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mu)\u001b[33m,\u001b[39m\u001b[33mC\u001b[39m(o\u001b[33m,\u001b[39m\u001b[32m\"active\"\u001b[39m\u001b[33m,\u001b[39md\u001b[33m.\u001b[39mautoExpandScrollbar)}})\u001b[33m.\u001b[39mbind(\u001b[32m\"touchmove.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){e\u001b[33m.\u001b[39mstopImmediatePropagation()\u001b[33m,\u001b[39me\u001b[33m.\u001b[39mpreventDefault()\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m a\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39moffset()\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39ma\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39ma\u001b[33m.\u001b[39mleft\u001b[33m;\u001b[39mt(n\u001b[33m,\u001b[39mi\u001b[33m,\u001b[39mr\u001b[33m,\u001b[39ml)})\u001b[33m,\u001b[39me(document)\u001b[33m.\u001b[39madd(g)\u001b[33m.\u001b[39mbind(\u001b[32m\"mousemove.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m+\u001b[39m\u001b[32m\" pointermove.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m+\u001b[39m\u001b[32m\" MSPointerMove.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[36mif\u001b[39m(o){\u001b[36mvar\u001b[39m a\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39moffset()\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39ma\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39ma\u001b[33m.\u001b[39mleft\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(n\u001b[33m===\u001b[39mr\u001b[33m&&\u001b[39mi\u001b[33m===\u001b[39ml)\u001b[36mreturn\u001b[39m\u001b[33m;\u001b[39mt(n\u001b[33m,\u001b[39mi\u001b[33m,\u001b[39mr\u001b[33m,\u001b[39ml)}})\u001b[33m.\u001b[39madd(p)\u001b[33m.\u001b[39mbind(\u001b[32m\"mouseup.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m+\u001b[39m\u001b[32m\" touchend.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m+\u001b[39m\u001b[32m\" pointerup.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m+\u001b[39m\u001b[32m\" MSPointerUp.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(){o\u001b[33m&&\u001b[39m(\u001b[33mC\u001b[39m(o\u001b[33m,\u001b[39m\u001b[32m\"active\"\u001b[39m\u001b[33m,\u001b[39md\u001b[33m.\u001b[39mautoExpandScrollbar)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m)\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39ms\u001b[33m&&\u001b[39m(document\u001b[33m.\u001b[39monselectstart\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mL\u001b[39m\u001b[33m.\u001b[39mcall(h\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m)})}\u001b[33m,\u001b[39m\u001b[33mD\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mfunction\u001b[39m o(e){\u001b[36mif\u001b[39m(\u001b[33m!\u001b[39mte(e)\u001b[33m||\u001b[39mc\u001b[33m||\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m2\u001b[39m])\u001b[36mreturn\u001b[39m \u001b[36mvoid\u001b[39m(t\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m)\u001b[33m;\u001b[39mt\u001b[33m=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mb\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[33mC\u001b[39m\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39md\u001b[33m=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39my\u001b[33m.\u001b[39mremoveClass(\u001b[32m\"mCS_touch_action\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39m\u001b[33mI\u001b[39m\u001b[33m.\u001b[39moffset()\u001b[33m;\u001b[39mu\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39mo\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39mo\u001b[33m.\u001b[39mleft\u001b[33m,\u001b[39mz\u001b[33m=\u001b[39m[\u001b[33mO\u001b[39m(e)[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m1\u001b[39m]]}\u001b[36mfunction\u001b[39m n(e){\u001b[36mif\u001b[39m(te(e)\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mc\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m2\u001b[39m]\u001b[33m&&\u001b[39m(\u001b[33mT\u001b[39m\u001b[33m.\u001b[39mdocumentTouchScroll\u001b[33m||\u001b[39me\u001b[33m.\u001b[39mpreventDefault()\u001b[33m,\u001b[39me\u001b[33m.\u001b[39mstopImmediatePropagation()\u001b[33m,\u001b[39m(\u001b[33m!\u001b[39m\u001b[33mC\u001b[39m\u001b[33m||\u001b[39mb)\u001b[33m&&\u001b[39md)){g\u001b[33m=\u001b[39m\u001b[33mK\u001b[39m()\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39m\u001b[33mM\u001b[39m\u001b[33m.\u001b[39moffset()\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39mt\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39mt\u001b[33m.\u001b[39mleft\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39m\u001b[32m\"mcsLinearOut\"\u001b[39m\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33mE\u001b[39m\u001b[33m.\u001b[39mpush(o)\u001b[33m,\u001b[39m\u001b[33mW\u001b[39m\u001b[33m.\u001b[39mpush(a)\u001b[33m,\u001b[39mz[\u001b[35m2\u001b[39m]\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(\u001b[33mO\u001b[39m(e)[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39mz[\u001b[35m0\u001b[39m])\u001b[33m,\u001b[39mz[\u001b[35m3\u001b[39m]\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(\u001b[33mO\u001b[39m(e)[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39mz[\u001b[35m1\u001b[39m])\u001b[33m,\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m])\u001b[36mvar\u001b[39m i\u001b[33m=\u001b[39m\u001b[33mD\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mheight()\u001b[33m-\u001b[39m\u001b[33mD\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mheight()\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39mu\u001b[33m-\u001b[39mo\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39mo\u001b[33m-\u001b[39mu\u001b[33m>\u001b[39m\u001b[33m-\u001b[39m(i\u001b[33m*\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39my)\u001b[33m&&\u001b[39m(\u001b[35m2\u001b[39m\u001b[33m*\u001b[39mz[\u001b[35m3\u001b[39m]\u001b[33m<\u001b[39m\u001b[33mz\u001b[39m[\u001b[35m2\u001b[39m]\u001b[33m||\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39m\u001b[33mT\u001b[39m\u001b[33m.\u001b[39maxis)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33mB\u001b[39m\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m])\u001b[36mvar\u001b[39m l\u001b[33m=\u001b[39m\u001b[33mD\u001b[39m[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mwidth()\u001b[33m-\u001b[39m\u001b[33mD\u001b[39m[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mwidth()\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39mf\u001b[33m-\u001b[39ma\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39ma\u001b[33m-\u001b[39mf\u001b[33m>\u001b[39m\u001b[33m-\u001b[39m(l\u001b[33m*\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39mx)\u001b[33m&&\u001b[39m(\u001b[35m2\u001b[39m\u001b[33m*\u001b[39mz[\u001b[35m2\u001b[39m]\u001b[33m<\u001b[39m\u001b[33mz\u001b[39m[\u001b[35m3\u001b[39m]\u001b[33m||\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39m\u001b[33mT\u001b[39m\u001b[33m.\u001b[39maxis)\u001b[33m;\u001b[39mr\u001b[33m||\u001b[39mh\u001b[33m?\u001b[39m(\u001b[33mU\u001b[39m\u001b[33m||\u001b[39me\u001b[33m.\u001b[39mpreventDefault()\u001b[33m,\u001b[39mb\u001b[33m=\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m:\u001b[39m(\u001b[33mC\u001b[39m\u001b[33m=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39my\u001b[33m.\u001b[39maddClass(\u001b[32m\"mCS_touch_action\"\u001b[39m))\u001b[33m,\u001b[39m\u001b[33mU\u001b[39m\u001b[33m&&\u001b[39me\u001b[33m.\u001b[39mpreventDefault()\u001b[33m,\u001b[39mw\u001b[33m=\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39m\u001b[33mT\u001b[39m\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m[u\u001b[33m-\u001b[39mo\u001b[33m,\u001b[39mf\u001b[33m-\u001b[39ma]\u001b[33m:\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39m\u001b[33mT\u001b[39m\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m[\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mf\u001b[33m-\u001b[39ma]\u001b[33m:\u001b[39m[u\u001b[33m-\u001b[39mo\u001b[33m,\u001b[39m\u001b[36mnull\u001b[39m]\u001b[33m,\u001b[39m\u001b[33mI\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39midleTimer\u001b[33m=\u001b[39m\u001b[35m250\u001b[39m\u001b[33m,\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39ms(w[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[33mR\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m,\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"all\"\u001b[39m\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39ms(w[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39m\u001b[33mR\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m,\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39m\u001b[33mL\u001b[39m\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m)}}\u001b[36mfunction\u001b[39m i(e){\u001b[36mif\u001b[39m(\u001b[33m!\u001b[39mte(e)\u001b[33m||\u001b[39mc\u001b[33m||\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m2\u001b[39m])\u001b[36mreturn\u001b[39m \u001b[36mvoid\u001b[39m(t\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m)\u001b[33m;\u001b[39mt\u001b[33m=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39me\u001b[33m.\u001b[39mstopImmediatePropagation()\u001b[33m,\u001b[39m\u001b[33mQ\u001b[39m(y)\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39m\u001b[33mK\u001b[39m()\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39m\u001b[33mM\u001b[39m\u001b[33m.\u001b[39moffset()\u001b[33m;\u001b[39mh\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39mo\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39mo\u001b[33m.\u001b[39mleft\u001b[33m,\u001b[39m\u001b[33mE\u001b[39m\u001b[33m=\u001b[39m[]\u001b[33m,\u001b[39m\u001b[33mW\u001b[39m\u001b[33m=\u001b[39m[]}\u001b[36mfunction\u001b[39m r(e){\u001b[36mif\u001b[39m(te(e)\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mc\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m2\u001b[39m]){d\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39me\u001b[33m.\u001b[39mstopImmediatePropagation()\u001b[33m,\u001b[39mb\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[33mC\u001b[39m\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mv\u001b[33m=\u001b[39m\u001b[33mK\u001b[39m()\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39m\u001b[33mM\u001b[39m\u001b[33m.\u001b[39moffset()\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39mt\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39mt\u001b[33m.\u001b[39mleft\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33m!\u001b[39m(v\u001b[33m-\u001b[39mg\u001b[33m>\u001b[39m\u001b[35m30\u001b[39m)){_\u001b[33m=\u001b[39m\u001b[35m1e3\u001b[39m\u001b[33m/\u001b[39m(v\u001b[33m-\u001b[39mp)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m n\u001b[33m=\u001b[39m\u001b[32m\"mcsEaseOut\"\u001b[39m\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39m\u001b[35m2.5\u001b[39m\u001b[33m>\u001b[39m_\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39mi\u001b[33m?\u001b[39m[\u001b[33mE\u001b[39m[\u001b[33mE\u001b[39m\u001b[33m.\u001b[39mlength\u001b[33m-\u001b[39m\u001b[35m2\u001b[39m]\u001b[33m,\u001b[39m\u001b[33mW\u001b[39m[\u001b[33mW\u001b[39m\u001b[33m.\u001b[39mlength\u001b[33m-\u001b[39m\u001b[35m2\u001b[39m]]\u001b[33m:\u001b[39m[\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m]\u001b[33m;\u001b[39mx\u001b[33m=\u001b[39mi\u001b[33m?\u001b[39m[o\u001b[33m-\u001b[39mr[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39ma\u001b[33m-\u001b[39mr[\u001b[35m1\u001b[39m]]\u001b[33m:\u001b[39m[o\u001b[33m-\u001b[39mh\u001b[33m,\u001b[39ma\u001b[33m-\u001b[39mm]\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m u\u001b[33m=\u001b[39m[\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(x[\u001b[35m0\u001b[39m])\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(x[\u001b[35m1\u001b[39m])]\u001b[33m;\u001b[39m_\u001b[33m=\u001b[39mi\u001b[33m?\u001b[39m[\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(x[\u001b[35m0\u001b[39m]\u001b[33m/\u001b[39m\u001b[35m4\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(x[\u001b[35m1\u001b[39m]\u001b[33m/\u001b[39m\u001b[35m4\u001b[39m)]\u001b[33m:\u001b[39m[_\u001b[33m,\u001b[39m_]\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m f\u001b[33m=\u001b[39m[\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(\u001b[33mI\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop)\u001b[33m-\u001b[39mx[\u001b[35m0\u001b[39m]\u001b[33m*\u001b[39ml(u[\u001b[35m0\u001b[39m]\u001b[33m/\u001b[39m_[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m_[\u001b[35m0\u001b[39m])\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(\u001b[33mI\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft)\u001b[33m-\u001b[39mx[\u001b[35m1\u001b[39m]\u001b[33m*\u001b[39ml(u[\u001b[35m1\u001b[39m]\u001b[33m/\u001b[39m_[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39m_[\u001b[35m1\u001b[39m])]\u001b[33m;\u001b[39mw\u001b[33m=\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39m\u001b[33mT\u001b[39m\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m[f[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mf[\u001b[35m1\u001b[39m]]\u001b[33m:\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39m\u001b[33mT\u001b[39m\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m[\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mf[\u001b[35m1\u001b[39m]]\u001b[33m:\u001b[39m[f[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mnull\u001b[39m]\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m\u001b[33m=\u001b[39m[\u001b[35m4\u001b[39m\u001b[33m*\u001b[39mu[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39m\u001b[33mT\u001b[39m\u001b[33m.\u001b[39mscrollInertia\u001b[33m,\u001b[39m\u001b[35m4\u001b[39m\u001b[33m*\u001b[39mu[\u001b[35m1\u001b[39m]\u001b[33m+\u001b[39m\u001b[33mT\u001b[39m\u001b[33m.\u001b[39mscrollInertia]\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m y\u001b[33m=\u001b[39mparseInt(\u001b[33mT\u001b[39m\u001b[33m.\u001b[39mcontentTouchScroll)\u001b[33m||\u001b[39m\u001b[35m0\u001b[39m\u001b[33m;\u001b[39mw[\u001b[35m0\u001b[39m]\u001b[33m=\u001b[39mu[\u001b[35m0\u001b[39m]\u001b[33m>\u001b[39my\u001b[33m?\u001b[39mw[\u001b[35m0\u001b[39m]\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mw[\u001b[35m1\u001b[39m]\u001b[33m=\u001b[39mu[\u001b[35m1\u001b[39m]\u001b[33m>\u001b[39my\u001b[33m?\u001b[39mw[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39ms(w[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mn\u001b[33m,\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39m\u001b[33mL\u001b[39m\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39ms(w[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39mn\u001b[33m,\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39m\u001b[33mL\u001b[39m\u001b[33m,\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)}}}\u001b[36mfunction\u001b[39m l(e\u001b[33m,\u001b[39mt){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39m[\u001b[35m1.5\u001b[39m\u001b[33m*\u001b[39mt\u001b[33m,\u001b[39m\u001b[35m2\u001b[39m\u001b[33m*\u001b[39mt\u001b[33m,\u001b[39mt\u001b[33m/\u001b[39m\u001b[35m1.5\u001b[39m\u001b[33m,\u001b[39mt\u001b[33m/\u001b[39m\u001b[35m2\u001b[39m]\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m e\u001b[33m>\u001b[39m\u001b[35m90\u001b[39m\u001b[33m?\u001b[39mt\u001b[33m>\u001b[39m\u001b[35m4\u001b[39m\u001b[33m?\u001b[39mo[\u001b[35m0\u001b[39m]\u001b[33m:\u001b[39mo[\u001b[35m3\u001b[39m]\u001b[33m:\u001b[39me\u001b[33m>\u001b[39m\u001b[35m60\u001b[39m\u001b[33m?\u001b[39mt\u001b[33m>\u001b[39m\u001b[35m3\u001b[39m\u001b[33m?\u001b[39mo[\u001b[35m3\u001b[39m]\u001b[33m:\u001b[39mo[\u001b[35m2\u001b[39m]\u001b[33m:\u001b[39me\u001b[33m>\u001b[39m\u001b[35m30\u001b[39m\u001b[33m?\u001b[39mt\u001b[33m>\u001b[39m\u001b[35m8\u001b[39m\u001b[33m?\u001b[39mo[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39mt\u001b[33m>\u001b[39m\u001b[35m6\u001b[39m\u001b[33m?\u001b[39mo[\u001b[35m0\u001b[39m]\u001b[33m:\u001b[39mt\u001b[33m>\u001b[39m\u001b[35m4\u001b[39m\u001b[33m?\u001b[39mt\u001b[33m:\u001b[39mo[\u001b[35m2\u001b[39m]\u001b[33m:\u001b[39mt\u001b[33m>\u001b[39m\u001b[35m8\u001b[39m\u001b[33m?\u001b[39mt\u001b[33m:\u001b[39mo[\u001b[35m3\u001b[39m]}\u001b[36mfunction\u001b[39m s(e\u001b[33m,\u001b[39mt\u001b[33m,\u001b[39mo\u001b[33m,\u001b[39ma\u001b[33m,\u001b[39mn\u001b[33m,\u001b[39mi){e\u001b[33m&&\u001b[39m\u001b[33mG\u001b[39m(y\u001b[33m,\u001b[39me\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dur\u001b[33m:\u001b[39mt\u001b[33m,\u001b[39mscrollEasing\u001b[33m:\u001b[39mo\u001b[33m,\u001b[39mdir\u001b[33m:\u001b[39ma\u001b[33m,\u001b[39moverwrite\u001b[33m:\u001b[39mn\u001b[33m,\u001b[39mdrag\u001b[33m:\u001b[39mi})}\u001b[36mvar\u001b[39m d\u001b[33m,\u001b[39mu\u001b[33m,\u001b[39mf\u001b[33m,\u001b[39mh\u001b[33m,\u001b[39mm\u001b[33m,\u001b[39mp\u001b[33m,\u001b[39mg\u001b[33m,\u001b[39mv\u001b[33m,\u001b[39mx\u001b[33m,\u001b[39m_\u001b[33m,\u001b[39mw\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m\u001b[33m,\u001b[39mb\u001b[33m,\u001b[39m\u001b[33mC\u001b[39m\u001b[33m,\u001b[39my\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mB\u001b[39m\u001b[33m=\u001b[39my\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39m\u001b[33mT\u001b[39m\u001b[33m=\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mk\u001b[33m=\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39midx\u001b[33m,\u001b[39m\u001b[33mM\u001b[39m\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39midx)\u001b[33m,\u001b[39m\u001b[33mI\u001b[39m\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mD\u001b[39m\u001b[33m=\u001b[39m[e(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical\"\u001b[39m)\u001b[33m,\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39m\u001b[33mB\u001b[39m\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal\"\u001b[39m)]\u001b[33m,\u001b[39m\u001b[33mE\u001b[39m\u001b[33m=\u001b[39m[]\u001b[33m,\u001b[39m\u001b[33mW\u001b[39m\u001b[33m=\u001b[39m[]\u001b[33m,\u001b[39m\u001b[33mR\u001b[39m\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[33mL\u001b[39m\u001b[33m=\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39m\u001b[33mT\u001b[39m\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m\u001b[32m\"none\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"all\"\u001b[39m\u001b[33m,\u001b[39mz\u001b[33m=\u001b[39m[]\u001b[33m,\u001b[39m\u001b[33mP\u001b[39m\u001b[33m=\u001b[39m\u001b[33mI\u001b[39m\u001b[33m.\u001b[39mfind(\u001b[32m\"iframe\"\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mH\u001b[39m\u001b[33m=\u001b[39m[\u001b[32m\"touchstart.\"\u001b[39m\u001b[33m+\u001b[39mk\u001b[33m+\u001b[39m\u001b[32m\" pointerdown.\"\u001b[39m\u001b[33m+\u001b[39mk\u001b[33m+\u001b[39m\u001b[32m\" MSPointerDown.\"\u001b[39m\u001b[33m+\u001b[39mk\u001b[33m,\u001b[39m\u001b[32m\"touchmove.\"\u001b[39m\u001b[33m+\u001b[39mk\u001b[33m+\u001b[39m\u001b[32m\" pointermove.\"\u001b[39m\u001b[33m+\u001b[39mk\u001b[33m+\u001b[39m\u001b[32m\" MSPointerMove.\"\u001b[39m\u001b[33m+\u001b[39mk\u001b[33m,\u001b[39m\u001b[32m\"touchend.\"\u001b[39m\u001b[33m+\u001b[39mk\u001b[33m+\u001b[39m\u001b[32m\" pointerup.\"\u001b[39m\u001b[33m+\u001b[39mk\u001b[33m+\u001b[39m\u001b[32m\" MSPointerUp.\"\u001b[39m\u001b[33m+\u001b[39mk]\u001b[33m,\u001b[39m\u001b[33mU\u001b[39m\u001b[33m=\u001b[39m\u001b[36mvoid\u001b[39m \u001b[35m0\u001b[39m\u001b[33m!==\u001b[39mdocument\u001b[33m.\u001b[39mbody\u001b[33m.\u001b[39mstyle\u001b[33m.\u001b[39mtouchAction\u001b[33m&&\u001b[39m\u001b[32m\"\"\u001b[39m\u001b[33m!==\u001b[39mdocument\u001b[33m.\u001b[39mbody\u001b[33m.\u001b[39mstyle\u001b[33m.\u001b[39mtouchAction\u001b[33m;\u001b[39m\u001b[33mI\u001b[39m\u001b[33m.\u001b[39mbind(\u001b[33mH\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){o(e)})\u001b[33m.\u001b[39mbind(\u001b[33mH\u001b[39m[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){n(e)})\u001b[33m,\u001b[39m\u001b[33mM\u001b[39m\u001b[33m.\u001b[39mbind(\u001b[33mH\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){i(e)})\u001b[33m.\u001b[39mbind(\u001b[33mH\u001b[39m[\u001b[35m2\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){r(e)})\u001b[33m,\u001b[39m\u001b[33mP\u001b[39m\u001b[33m.\u001b[39mlength\u001b[33m&&\u001b[39m\u001b[33mP\u001b[39m\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){e(\u001b[36mthis\u001b[39m)\u001b[33m.\u001b[39mbind(\u001b[32m\"load\"\u001b[39m\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[33mA\u001b[39m(\u001b[36mthis\u001b[39m)\u001b[33m&&\u001b[39me(\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mcontentDocument\u001b[33m||\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mcontentWindow\u001b[33m.\u001b[39mdocument)\u001b[33m.\u001b[39mbind(\u001b[33mH\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){o(e)\u001b[33m,\u001b[39mi(e)})\u001b[33m.\u001b[39mbind(\u001b[33mH\u001b[39m[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){n(e)})\u001b[33m.\u001b[39mbind(\u001b[33mH\u001b[39m[\u001b[35m2\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){r(e)})})})}\u001b[33m,\u001b[39m\u001b[33mE\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mfunction\u001b[39m o(){\u001b[36mreturn\u001b[39m window\u001b[33m.\u001b[39mgetSelection\u001b[33m?\u001b[39mwindow\u001b[33m.\u001b[39mgetSelection()\u001b[33m.\u001b[39mtoString()\u001b[33m:\u001b[39mdocument\u001b[33m.\u001b[39mselection\u001b[33m&&\u001b[39m\u001b[32m\"Control\"\u001b[39m\u001b[33m!=\u001b[39mdocument\u001b[33m.\u001b[39mselection\u001b[33m.\u001b[39mtype\u001b[33m?\u001b[39mdocument\u001b[33m.\u001b[39mselection\u001b[33m.\u001b[39mcreateRange()\u001b[33m.\u001b[39mtext\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m}\u001b[36mfunction\u001b[39m n(e\u001b[33m,\u001b[39mt\u001b[33m,\u001b[39mo){d\u001b[33m.\u001b[39mtype\u001b[33m=\u001b[39mo\u001b[33m&&\u001b[39mi\u001b[33m?\u001b[39m\u001b[32m\"stepped\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"stepless\"\u001b[39m\u001b[33m,\u001b[39md\u001b[33m.\u001b[39mscrollAmount\u001b[33m=\u001b[39m\u001b[35m10\u001b[39m\u001b[33m,\u001b[39mj(r\u001b[33m,\u001b[39me\u001b[33m,\u001b[39mt\u001b[33m,\u001b[39m\u001b[32m\"mcsLinearOut\"\u001b[39m\u001b[33m,\u001b[39mo\u001b[33m?\u001b[39m\u001b[35m60\u001b[39m\u001b[33m:\u001b[39m\u001b[36mnull\u001b[39m)}\u001b[36mvar\u001b[39m i\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39mr\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39ml\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39md\u001b[33m=\u001b[39ml\u001b[33m.\u001b[39msequential\u001b[33m,\u001b[39mu\u001b[33m=\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39ml\u001b[33m.\u001b[39midx\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39ml\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39mf\u001b[33m.\u001b[39mparent()\u001b[33m;\u001b[39mf\u001b[33m.\u001b[39mbind(\u001b[32m\"mousedown.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(){t\u001b[33m||\u001b[39mi\u001b[33m||\u001b[39m(i\u001b[33m=\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m)})\u001b[33m.\u001b[39madd(document)\u001b[33m.\u001b[39mbind(\u001b[32m\"mousemove.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[36mif\u001b[39m(\u001b[33m!\u001b[39mt\u001b[33m&&\u001b[39mi\u001b[33m&&\u001b[39mo()){\u001b[36mvar\u001b[39m a\u001b[33m=\u001b[39mf\u001b[33m.\u001b[39moffset()\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39ma\u001b[33m.\u001b[39mtop\u001b[33m+\u001b[39mf[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m\u001b[33mO\u001b[39m(e)[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39ma\u001b[33m.\u001b[39mleft\u001b[33m+\u001b[39mf[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft\u001b[33m;\u001b[39mr\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39mr\u001b[33m<\u001b[39m\u001b[33mh\u001b[39m\u001b[33m.\u001b[39mheight()\u001b[33m&&\u001b[39mc\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39mc\u001b[33m<\u001b[39m\u001b[33mh\u001b[39m\u001b[33m.\u001b[39mwidth()\u001b[33m?\u001b[39md\u001b[33m.\u001b[39mstep\u001b[33m&&\u001b[39mn(\u001b[32m\"off\"\u001b[39m\u001b[33m,\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"stepped\"\u001b[39m)\u001b[33m:\u001b[39m(\u001b[32m\"x\"\u001b[39m\u001b[33m!==\u001b[39ms\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39ml\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39m(\u001b[35m0\u001b[39m\u001b[33m>\u001b[39mr\u001b[33m?\u001b[39mn(\u001b[32m\"on\"\u001b[39m\u001b[33m,\u001b[39m\u001b[35m38\u001b[39m)\u001b[33m:\u001b[39mr\u001b[33m>\u001b[39mh\u001b[33m.\u001b[39mheight()\u001b[33m&&\u001b[39mn(\u001b[32m\"on\"\u001b[39m\u001b[33m,\u001b[39m\u001b[35m40\u001b[39m))\u001b[33m,\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m!==\u001b[39ms\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39ml\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39m(\u001b[35m0\u001b[39m\u001b[33m>\u001b[39mc\u001b[33m?\u001b[39mn(\u001b[32m\"on\"\u001b[39m\u001b[33m,\u001b[39m\u001b[35m37\u001b[39m)\u001b[33m:\u001b[39mc\u001b[33m>\u001b[39mh\u001b[33m.\u001b[39mwidth()\u001b[33m&&\u001b[39mn(\u001b[32m\"on\"\u001b[39m\u001b[33m,\u001b[39m\u001b[35m39\u001b[39m)))}})\u001b[33m.\u001b[39mbind(\u001b[32m\"mouseup.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m+\u001b[39m\u001b[32m\" dragend.\"\u001b[39m\u001b[33m+\u001b[39mu\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(){t\u001b[33m||\u001b[39m(i\u001b[33m&&\u001b[39m(i\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mn(\u001b[32m\"off\"\u001b[39m\u001b[33m,\u001b[39m\u001b[36mnull\u001b[39m))\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)})}\u001b[33m,\u001b[39m\u001b[33mW\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mfunction\u001b[39m t(t\u001b[33m,\u001b[39ma){\u001b[36mif\u001b[39m(\u001b[33mQ\u001b[39m(o)\u001b[33m,\u001b[39m\u001b[33m!\u001b[39mz(o\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mtarget)){\u001b[36mvar\u001b[39m r\u001b[33m=\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mdeltaFactor\u001b[33m?\u001b[39mparseInt(i\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mdeltaFactor)\u001b[33m:\u001b[39ms\u001b[33m&&\u001b[39mt\u001b[33m.\u001b[39mdeltaFactor\u001b[33m<\u001b[39m\u001b[35m100\u001b[39m\u001b[33m?\u001b[39m\u001b[35m100\u001b[39m\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mdeltaFactor\u001b[33m||\u001b[39m\u001b[35m100\u001b[39m\u001b[33m,\u001b[39md\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mscrollInertia\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m||\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39maxis)\u001b[36mvar\u001b[39m u\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39m[\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mround(r\u001b[33m*\u001b[39mn\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39mx)\u001b[33m,\u001b[39mparseInt(i\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mscrollAmount)]\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mscrollAmount\u001b[33m?\u001b[39mf[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39mf[\u001b[35m0\u001b[39m]\u001b[33m>=\u001b[39ml\u001b[33m.\u001b[39mwidth()\u001b[33m?\u001b[39m\u001b[35m.9\u001b[39m\u001b[33m*\u001b[39ml\u001b[33m.\u001b[39mwidth()\u001b[33m:\u001b[39mf[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(e(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft)\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39mc[\u001b[35m1\u001b[39m][\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft\u001b[33m,\u001b[39mg\u001b[33m=\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mwidth()\u001b[33m-\u001b[39mc[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mwidth()\u001b[33m,\u001b[39mv\u001b[33m=\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39mt\u001b[33m.\u001b[39mdeltaY\u001b[33m||\u001b[39ma\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mdeltaX\u001b[33m;\u001b[39m\u001b[36melse\u001b[39m \u001b[36mvar\u001b[39m u\u001b[33m=\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39m[\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mround(r\u001b[33m*\u001b[39mn\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39my)\u001b[33m,\u001b[39mparseInt(i\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mscrollAmount)]\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mscrollAmount\u001b[33m?\u001b[39mf[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39mf[\u001b[35m0\u001b[39m]\u001b[33m>=\u001b[39ml\u001b[33m.\u001b[39mheight()\u001b[33m?\u001b[39m\u001b[35m.9\u001b[39m\u001b[33m*\u001b[39ml\u001b[33m.\u001b[39mheight()\u001b[33m:\u001b[39mf[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(e(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop)\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39mc[\u001b[35m0\u001b[39m][\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop\u001b[33m,\u001b[39mg\u001b[33m=\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mparent()\u001b[33m.\u001b[39mheight()\u001b[33m-\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mheight()\u001b[33m,\u001b[39mv\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdeltaY\u001b[33m||\u001b[39ma\u001b[33m;\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m===\u001b[39mu\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m||\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mu\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m||\u001b[39m((i\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39minvert\u001b[33m||\u001b[39mt\u001b[33m.\u001b[39mwebkitDirectionInvertedFromDevice)\u001b[33m&&\u001b[39m(v\u001b[33m=\u001b[39m\u001b[33m-\u001b[39mv)\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mnormalizeDelta\u001b[33m&&\u001b[39m(v\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m>\u001b[39mv\u001b[33m?\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m:\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39m(v\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39m\u001b[35m0\u001b[39m\u001b[33m!==\u001b[39mp\u001b[33m||\u001b[39m\u001b[35m0\u001b[39m\u001b[33m>\u001b[39mv\u001b[33m&&\u001b[39mp\u001b[33m!==\u001b[39mg\u001b[33m||\u001b[39mi\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mpreventDefault)\u001b[33m&&\u001b[39m(t\u001b[33m.\u001b[39mstopImmediatePropagation()\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mpreventDefault())\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mdeltaFactor\u001b[33m<\u001b[39m\u001b[35m5\u001b[39m\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mi\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mnormalizeDelta\u001b[33m&&\u001b[39m(h\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdeltaFactor\u001b[33m,\u001b[39md\u001b[33m=\u001b[39m\u001b[35m17\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mG\u001b[39m(o\u001b[33m,\u001b[39m(m\u001b[33m-\u001b[39mv\u001b[33m*\u001b[39mh)\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39mu\u001b[33m,\u001b[39mdur\u001b[33m:\u001b[39md}))}}\u001b[36mif\u001b[39m(e(\u001b[36mthis\u001b[39m)\u001b[33m.\u001b[39mdata(a)){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx)\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m[e(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical\"\u001b[39m)\u001b[33m,\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal\"\u001b[39m)]\u001b[33m,\u001b[39md\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m.\u001b[39mfind(\u001b[32m\"iframe\"\u001b[39m)\u001b[33m;\u001b[39md\u001b[33m.\u001b[39mlength\u001b[33m&&\u001b[39md\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){e(\u001b[36mthis\u001b[39m)\u001b[33m.\u001b[39mbind(\u001b[32m\"load\"\u001b[39m\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[33mA\u001b[39m(\u001b[36mthis\u001b[39m)\u001b[33m&&\u001b[39me(\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mcontentDocument\u001b[33m||\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mcontentWindow\u001b[33m.\u001b[39mdocument)\u001b[33m.\u001b[39mbind(\u001b[32m\"mousewheel.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e\u001b[33m,\u001b[39mo){t(e\u001b[33m,\u001b[39mo)})})})\u001b[33m,\u001b[39ml\u001b[33m.\u001b[39mbind(\u001b[32m\"mousewheel.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e\u001b[33m,\u001b[39mo){t(e\u001b[33m,\u001b[39mo)})}}\u001b[33m,\u001b[39m\u001b[33mR\u001b[39m\u001b[33m=\u001b[39m\u001b[36mnew\u001b[39m \u001b[33mObject\u001b[39m\u001b[33m,\u001b[39m\u001b[33mA\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[36mvoid\u001b[39m \u001b[35m0\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m?\u001b[39ma\u001b[33m=\u001b[39m\u001b[32m\"#empty\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mvoid\u001b[39m \u001b[35m0\u001b[39m\u001b[33m!==\u001b[39me(t)\u001b[33m.\u001b[39mattr(\u001b[32m\"id\"\u001b[39m)\u001b[33m&&\u001b[39m(a\u001b[33m=\u001b[39me(t)\u001b[33m.\u001b[39mattr(\u001b[32m\"id\"\u001b[39m))\u001b[33m,\u001b[39ma\u001b[33m!==\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m&&\u001b[39m\u001b[36mvoid\u001b[39m \u001b[35m0\u001b[39m\u001b[33m!==\u001b[39m\u001b[33mR\u001b[39m[a])\u001b[36mreturn\u001b[39m \u001b[33mR\u001b[39m[a]\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(t){\u001b[36mtry\u001b[39m{\u001b[36mvar\u001b[39m i\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mcontentDocument\u001b[33m||\u001b[39mt\u001b[33m.\u001b[39mcontentWindow\u001b[33m.\u001b[39mdocument\u001b[33m;\u001b[39mn\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mbody\u001b[33m.\u001b[39minnerHTML}\u001b[36mcatch\u001b[39m(r){}o\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m!==\u001b[39mn}\u001b[36melse\u001b[39m{\u001b[36mtry\u001b[39m{\u001b[36mvar\u001b[39m i\u001b[33m=\u001b[39mtop\u001b[33m.\u001b[39mdocument\u001b[33m;\u001b[39mn\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mbody\u001b[33m.\u001b[39minnerHTML}\u001b[36mcatch\u001b[39m(r){}o\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m!==\u001b[39mn}\u001b[36mreturn\u001b[39m a\u001b[33m!==\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m&&\u001b[39m(\u001b[33mR\u001b[39m[a]\u001b[33m=\u001b[39mo)\u001b[33m,\u001b[39mo}\u001b[33m,\u001b[39m\u001b[33mL\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mfind(\u001b[32m\"iframe\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(t\u001b[33m.\u001b[39mlength){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39me\u001b[33m?\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"none\"\u001b[39m\u001b[33m;\u001b[39mt\u001b[33m.\u001b[39mcss(\u001b[32m\"pointer-events\"\u001b[39m\u001b[33m,\u001b[39mo)}}\u001b[33m,\u001b[39mz\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t\u001b[33m,\u001b[39mo){\u001b[36mvar\u001b[39m n\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mnodeName\u001b[33m.\u001b[39mtoLowerCase()\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m.\u001b[39mopt\u001b[33m.\u001b[39mmouseWheel\u001b[33m.\u001b[39mdisableOver\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m[\u001b[32m\"select\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"textarea\"\u001b[39m]\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m e\u001b[33m.\u001b[39minArray(n\u001b[33m,\u001b[39mi)\u001b[33m>\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39m(e\u001b[33m.\u001b[39minArray(n\u001b[33m,\u001b[39mr)\u001b[33m>\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39me(o)\u001b[33m.\u001b[39mis(\u001b[32m\":focus\"\u001b[39m))}\u001b[33m,\u001b[39m\u001b[33mP\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39mr\u001b[33m.\u001b[39mparent()\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39me(\u001b[32m\".mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar .\"\u001b[39m\u001b[33m+\u001b[39md[\u001b[35m12\u001b[39m])\u001b[33m;\u001b[39ms\u001b[33m.\u001b[39mbind(\u001b[32m\"mousedown.\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m+\u001b[39m\u001b[32m\" touchstart.\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m+\u001b[39m\u001b[32m\" pointerdown.\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m+\u001b[39m\u001b[32m\" MSPointerDown.\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(o){c\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39me(o\u001b[33m.\u001b[39mtarget)\u001b[33m.\u001b[39mhasClass(\u001b[32m\"mCSB_dragger\"\u001b[39m)\u001b[33m||\u001b[39m(t\u001b[33m=\u001b[39m\u001b[35m1\u001b[39m)})\u001b[33m.\u001b[39mbind(\u001b[32m\"touchend.\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m+\u001b[39m\u001b[32m\" pointerup.\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m+\u001b[39m\u001b[32m\" MSPointerUp.\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(){c\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m})\u001b[33m.\u001b[39mbind(\u001b[32m\"click.\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(a){\u001b[36mif\u001b[39m(t\u001b[33m&&\u001b[39m(t\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39me(a\u001b[33m.\u001b[39mtarget)\u001b[33m.\u001b[39mhasClass(d[\u001b[35m12\u001b[39m])\u001b[33m||\u001b[39me(a\u001b[33m.\u001b[39mtarget)\u001b[33m.\u001b[39mhasClass(\u001b[32m\"mCSB_draggerRail\"\u001b[39m))){\u001b[33mQ\u001b[39m(o)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m i\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mfind(\u001b[32m\".mCSB_dragger\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(i\u001b[33m.\u001b[39mparent(\u001b[32m\".mCSB_scrollTools_horizontal\"\u001b[39m)\u001b[33m.\u001b[39mlength\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m){\u001b[36mif\u001b[39m(\u001b[33m!\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m])\u001b[36mreturn\u001b[39m\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m c\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39mu\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mpageX\u001b[33m>\u001b[39ms\u001b[33m.\u001b[39moffset()\u001b[33m.\u001b[39mleft\u001b[33m?\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m:\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(r[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft)\u001b[33m-\u001b[39mu\u001b[33m*\u001b[39m(\u001b[35m.9\u001b[39m\u001b[33m*\u001b[39ml\u001b[33m.\u001b[39mwidth())}\u001b[36melse\u001b[39m{\u001b[36mif\u001b[39m(\u001b[33m!\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m])\u001b[36mreturn\u001b[39m\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m c\u001b[33m=\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mu\u001b[33m=\u001b[39ma\u001b[33m.\u001b[39mpageY\u001b[33m>\u001b[39ms\u001b[33m.\u001b[39moffset()\u001b[33m.\u001b[39mtop\u001b[33m?\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m:\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(r[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop)\u001b[33m-\u001b[39mu\u001b[33m*\u001b[39m(\u001b[35m.9\u001b[39m\u001b[33m*\u001b[39ml\u001b[33m.\u001b[39mheight())}\u001b[33mG\u001b[39m(o\u001b[33m,\u001b[39mf\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39mc\u001b[33m,\u001b[39mscrollEasing\u001b[33m:\u001b[39m\u001b[32m\"mcsEaseInOut\"\u001b[39m})}})}\u001b[33m,\u001b[39m\u001b[33mH\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39mr\u001b[33m.\u001b[39mparent()\u001b[33m;\u001b[39mr\u001b[33m.\u001b[39mbind(\u001b[32m\"focusin.\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39me(document\u001b[33m.\u001b[39mactiveElement)\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39mr\u001b[33m.\u001b[39mfind(\u001b[32m\".mCustomScrollBox\"\u001b[39m)\u001b[33m.\u001b[39mlength\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m;\u001b[39mo\u001b[33m.\u001b[39mis(n\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mautoScrollOnFocus)\u001b[33m&&\u001b[39m(\u001b[33mQ\u001b[39m(t)\u001b[33m,\u001b[39mclearTimeout(t[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39m_focusTimeout)\u001b[33m,\u001b[39mt[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39m_focusTimer\u001b[33m=\u001b[39ma\u001b[33m?\u001b[39m(i\u001b[33m+\u001b[39m\u001b[35m17\u001b[39m)\u001b[33m*\u001b[39ma\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mt[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39m_focusTimeout\u001b[33m=\u001b[39msetTimeout(\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m e\u001b[33m=\u001b[39m[ae(o)[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mae(o)[\u001b[35m1\u001b[39m]]\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39m[r[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop\u001b[33m,\u001b[39mr[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft]\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39m[a[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39me[\u001b[35m0\u001b[39m]\u001b[33m>=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39ma[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39me[\u001b[35m0\u001b[39m]\u001b[33m<\u001b[39m\u001b[33ml\u001b[39m\u001b[33m.\u001b[39mheight()\u001b[33m-\u001b[39mo\u001b[33m.\u001b[39mouterHeight(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39ma[\u001b[35m1\u001b[39m]\u001b[33m+\u001b[39me[\u001b[35m1\u001b[39m]\u001b[33m>=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39ma[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39me[\u001b[35m1\u001b[39m]\u001b[33m<\u001b[39m\u001b[33ml\u001b[39m\u001b[33m.\u001b[39mwidth()\u001b[33m-\u001b[39mo\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)]\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m!==\u001b[39mn\u001b[33m.\u001b[39maxis\u001b[33m||\u001b[39ms[\u001b[35m0\u001b[39m]\u001b[33m||\u001b[39ms[\u001b[35m1\u001b[39m]\u001b[33m?\u001b[39m\u001b[32m\"all\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"none\"\u001b[39m\u001b[33m;\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m.\u001b[39maxis\u001b[33m||\u001b[39ms[\u001b[35m0\u001b[39m]\u001b[33m||\u001b[39m\u001b[33mG\u001b[39m(t\u001b[33m,\u001b[39me[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mscrollEasing\u001b[33m:\u001b[39m\u001b[32m\"mcsEaseInOut\"\u001b[39m\u001b[33m,\u001b[39moverwrite\u001b[33m:\u001b[39mc\u001b[33m,\u001b[39mdur\u001b[33m:\u001b[39mi})\u001b[33m,\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m.\u001b[39maxis\u001b[33m||\u001b[39ms[\u001b[35m1\u001b[39m]\u001b[33m||\u001b[39m\u001b[33mG\u001b[39m(t\u001b[33m,\u001b[39me[\u001b[35m1\u001b[39m]\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39mscrollEasing\u001b[33m:\u001b[39m\u001b[32m\"mcsEaseInOut\"\u001b[39m\u001b[33m,\u001b[39moverwrite\u001b[33m:\u001b[39mc\u001b[33m,\u001b[39mdur\u001b[33m:\u001b[39mi})}\u001b[33m,\u001b[39mt[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39m_focusTimer))})}\u001b[33m,\u001b[39m\u001b[33mU\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m.\u001b[39mparent()\u001b[33m;\u001b[39mi\u001b[33m.\u001b[39mbind(\u001b[32m\"scroll.\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[35m0\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39mscrollTop()\u001b[33m&&\u001b[39m\u001b[35m0\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39mscrollLeft()\u001b[33m||\u001b[39me(\u001b[32m\".mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar\"\u001b[39m)\u001b[33m.\u001b[39mcss(\u001b[32m\"visibility\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"hidden\"\u001b[39m)})}\u001b[33m,\u001b[39m\u001b[33mF\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39msequential\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m\u001b[32m\".mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_scrollbar\"\u001b[39m\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39me(l\u001b[33m+\u001b[39m\u001b[32m\">a\"\u001b[39m)\u001b[33m;\u001b[39ms\u001b[33m.\u001b[39mbind(\u001b[32m\"contextmenu.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){e\u001b[33m.\u001b[39mpreventDefault()})\u001b[33m.\u001b[39mbind(\u001b[32m\"mousedown.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" touchstart.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" pointerdown.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" MSPointerDown.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" mouseup.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" touchend.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" pointerup.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" MSPointerUp.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" mouseout.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" pointerout.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" MSPointerOut.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m+\u001b[39m\u001b[32m\" click.\"\u001b[39m\u001b[33m+\u001b[39mr\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(a){\u001b[36mfunction\u001b[39m r(e\u001b[33m,\u001b[39mo){i\u001b[33m.\u001b[39mscrollAmount\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mscrollButtons\u001b[33m.\u001b[39mscrollAmount\u001b[33m,\u001b[39mj(t\u001b[33m,\u001b[39me\u001b[33m,\u001b[39mo)}\u001b[36mif\u001b[39m(a\u001b[33m.\u001b[39mpreventDefault()\u001b[33m,\u001b[39mee(a)){\u001b[36mvar\u001b[39m l\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m.\u001b[39mattr(\u001b[32m\"class\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mswitch\u001b[39m(i\u001b[33m.\u001b[39mtype\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mscrollButtons\u001b[33m.\u001b[39mscrollType\u001b[33m,\u001b[39ma\u001b[33m.\u001b[39mtype){\u001b[36mcase\u001b[39m\u001b[32m\"mousedown\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"touchstart\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"pointerdown\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"MSPointerDown\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"stepped\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39mtype)\u001b[36mreturn\u001b[39m\u001b[33m;\u001b[39mc\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mo\u001b[33m.\u001b[39mtweenRunning\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mr(\u001b[32m\"on\"\u001b[39m\u001b[33m,\u001b[39ml)\u001b[33m;\u001b[39m\u001b[36mbreak\u001b[39m\u001b[33m;\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"mouseup\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"touchend\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"pointerup\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"MSPointerUp\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"mouseout\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"pointerout\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"MSPointerOut\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"stepped\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39mtype)\u001b[36mreturn\u001b[39m\u001b[33m;\u001b[39mc\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mi\u001b[33m.\u001b[39mdir\u001b[33m&&\u001b[39mr(\u001b[32m\"off\"\u001b[39m\u001b[33m,\u001b[39ml)\u001b[33m;\u001b[39m\u001b[36mbreak\u001b[39m\u001b[33m;\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"click\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"stepped\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39mtype\u001b[33m||\u001b[39mo\u001b[33m.\u001b[39mtweenRunning)\u001b[36mreturn\u001b[39m\u001b[33m;\u001b[39mr(\u001b[32m\"on\"\u001b[39m\u001b[33m,\u001b[39ml)}}})}\u001b[33m,\u001b[39mq\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mfunction\u001b[39m t(t){\u001b[36mfunction\u001b[39m a(e\u001b[33m,\u001b[39mt){r\u001b[33m.\u001b[39mtype\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mkeyboard\u001b[33m.\u001b[39mscrollType\u001b[33m,\u001b[39mr\u001b[33m.\u001b[39mscrollAmount\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mkeyboard\u001b[33m.\u001b[39mscrollAmount\u001b[33m,\u001b[39m\u001b[32m\"stepped\"\u001b[39m\u001b[33m===\u001b[39mr\u001b[33m.\u001b[39mtype\u001b[33m&&\u001b[39mn\u001b[33m.\u001b[39mtweenRunning\u001b[33m||\u001b[39mj(o\u001b[33m,\u001b[39me\u001b[33m,\u001b[39mt)}\u001b[36mswitch\u001b[39m(t\u001b[33m.\u001b[39mtype){\u001b[36mcase\u001b[39m\u001b[32m\"blur\"\u001b[39m\u001b[33m:\u001b[39mn\u001b[33m.\u001b[39mtweenRunning\u001b[33m&&\u001b[39mr\u001b[33m.\u001b[39mdir\u001b[33m&&\u001b[39ma(\u001b[32m\"off\"\u001b[39m\u001b[33m,\u001b[39m\u001b[36mnull\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mbreak\u001b[39m\u001b[33m;\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"keydown\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"keyup\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mvar\u001b[39m l\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mkeyCode\u001b[33m?\u001b[39mt\u001b[33m.\u001b[39mkeyCode\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mwhich\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39m\u001b[32m\"on\"\u001b[39m\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"x\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39m(\u001b[35m38\u001b[39m\u001b[33m===\u001b[39ml\u001b[33m||\u001b[39m\u001b[35m40\u001b[39m\u001b[33m===\u001b[39ml)\u001b[33m||\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39m(\u001b[35m37\u001b[39m\u001b[33m===\u001b[39ml\u001b[33m||\u001b[39m\u001b[35m39\u001b[39m\u001b[33m===\u001b[39ml)){\u001b[36mif\u001b[39m((\u001b[35m38\u001b[39m\u001b[33m===\u001b[39ml\u001b[33m||\u001b[39m\u001b[35m40\u001b[39m\u001b[33m===\u001b[39ml)\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m||\u001b[39m(\u001b[35m37\u001b[39m\u001b[33m===\u001b[39ml\u001b[33m||\u001b[39m\u001b[35m39\u001b[39m\u001b[33m===\u001b[39ml)\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m])\u001b[36mreturn\u001b[39m\u001b[33m;\u001b[39m\u001b[32m\"keyup\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m.\u001b[39mtype\u001b[33m&&\u001b[39m(s\u001b[33m=\u001b[39m\u001b[32m\"off\"\u001b[39m)\u001b[33m,\u001b[39me(document\u001b[33m.\u001b[39mactiveElement)\u001b[33m.\u001b[39mis(u)\u001b[33m||\u001b[39m(t\u001b[33m.\u001b[39mpreventDefault()\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mstopImmediatePropagation()\u001b[33m,\u001b[39ma(s\u001b[33m,\u001b[39ml))}\u001b[36melse\u001b[39m \u001b[36mif\u001b[39m(\u001b[35m33\u001b[39m\u001b[33m===\u001b[39ml\u001b[33m||\u001b[39m\u001b[35m34\u001b[39m\u001b[33m===\u001b[39ml){\u001b[36mif\u001b[39m((n\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m||\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m])\u001b[33m&&\u001b[39m(t\u001b[33m.\u001b[39mpreventDefault()\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mstopImmediatePropagation())\u001b[33m,\u001b[39m\u001b[32m\"keyup\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m.\u001b[39mtype){\u001b[33mQ\u001b[39m(o)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m f\u001b[33m=\u001b[39m\u001b[35m34\u001b[39m\u001b[33m===\u001b[39ml\u001b[33m?\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m:\u001b[39m\u001b[35m1\u001b[39m\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m||\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m])\u001b[36mvar\u001b[39m h\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(c[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft)\u001b[33m-\u001b[39mf\u001b[33m*\u001b[39m(\u001b[35m.9\u001b[39m\u001b[33m*\u001b[39md\u001b[33m.\u001b[39mwidth())\u001b[33m;\u001b[39m\u001b[36melse\u001b[39m \u001b[36mvar\u001b[39m h\u001b[33m=\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(c[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop)\u001b[33m-\u001b[39mf\u001b[33m*\u001b[39m(\u001b[35m.9\u001b[39m\u001b[33m*\u001b[39md\u001b[33m.\u001b[39mheight())\u001b[33m;\u001b[39m\u001b[33mG\u001b[39m(o\u001b[33m,\u001b[39mm\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39mh\u001b[33m,\u001b[39mscrollEasing\u001b[33m:\u001b[39m\u001b[32m\"mcsEaseInOut\"\u001b[39m})}}\u001b[36melse\u001b[39m \u001b[36mif\u001b[39m((\u001b[35m35\u001b[39m\u001b[33m===\u001b[39ml\u001b[33m||\u001b[39m\u001b[35m36\u001b[39m\u001b[33m===\u001b[39ml)\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39me(document\u001b[33m.\u001b[39mactiveElement)\u001b[33m.\u001b[39mis(u)\u001b[33m&&\u001b[39m((n\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m||\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m])\u001b[33m&&\u001b[39m(t\u001b[33m.\u001b[39mpreventDefault()\u001b[33m,\u001b[39mt\u001b[33m.\u001b[39mstopImmediatePropagation())\u001b[33m,\u001b[39m\u001b[32m\"keyup\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m.\u001b[39mtype)){\u001b[36mif\u001b[39m(\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m||\u001b[39m\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39mi\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39mn\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m])\u001b[36mvar\u001b[39m h\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[35m35\u001b[39m\u001b[33m===\u001b[39ml\u001b[33m?\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(d\u001b[33m.\u001b[39mwidth()\u001b[33m-\u001b[39mc\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m))\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m;\u001b[39m\u001b[36melse\u001b[39m \u001b[36mvar\u001b[39m h\u001b[33m=\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[35m35\u001b[39m\u001b[33m===\u001b[39ml\u001b[33m?\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(d\u001b[33m.\u001b[39mheight()\u001b[33m-\u001b[39mc\u001b[33m.\u001b[39mouterHeight(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m))\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m;\u001b[39m\u001b[33mG\u001b[39m(o\u001b[33m,\u001b[39mm\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39mh\u001b[33m,\u001b[39mscrollEasing\u001b[33m:\u001b[39m\u001b[32m\"mcsEaseInOut\"\u001b[39m})}}}\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39mo\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39msequential\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39ma\u001b[33m+\u001b[39m\u001b[32m\"_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx)\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mn\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39md\u001b[33m=\u001b[39mc\u001b[33m.\u001b[39mparent()\u001b[33m,\u001b[39mu\u001b[33m=\u001b[39m\u001b[32m\"input,textarea,select,datalist,keygen,[contenteditable='true']\"\u001b[39m\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39mc\u001b[33m.\u001b[39mfind(\u001b[32m\"iframe\"\u001b[39m)\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39m[\u001b[32m\"blur.\"\u001b[39m\u001b[33m+\u001b[39ml\u001b[33m+\u001b[39m\u001b[32m\" keydown.\"\u001b[39m\u001b[33m+\u001b[39ml\u001b[33m+\u001b[39m\u001b[32m\" keyup.\"\u001b[39m\u001b[33m+\u001b[39ml]\u001b[33m;\u001b[39mf\u001b[33m.\u001b[39mlength\u001b[33m&&\u001b[39mf\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){e(\u001b[36mthis\u001b[39m)\u001b[33m.\u001b[39mbind(\u001b[32m\"load\"\u001b[39m\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[33mA\u001b[39m(\u001b[36mthis\u001b[39m)\u001b[33m&&\u001b[39me(\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mcontentDocument\u001b[33m||\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39mcontentWindow\u001b[33m.\u001b[39mdocument)\u001b[33m.\u001b[39mbind(h[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){t(e)})})})\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mattr(\u001b[32m\"tabindex\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"0\"\u001b[39m)\u001b[33m.\u001b[39mbind(h[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(e){t(e)})}\u001b[33m,\u001b[39mj\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t\u001b[33m,\u001b[39mo\u001b[33m,\u001b[39mn\u001b[33m,\u001b[39mi\u001b[33m,\u001b[39mr){\u001b[36mfunction\u001b[39m l(e){u\u001b[33m.\u001b[39msnapAmount\u001b[33m&&\u001b[39m(f\u001b[33m.\u001b[39mscrollAmount\u001b[33m=\u001b[39mu\u001b[33m.\u001b[39msnapAmount \u001b[36minstanceof\u001b[39m \u001b[33mArray\u001b[39m\u001b[33m?\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mf\u001b[33m.\u001b[39mdir[\u001b[35m0\u001b[39m]\u001b[33m?\u001b[39mu\u001b[33m.\u001b[39msnapAmount[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39mu\u001b[33m.\u001b[39msnapAmount[\u001b[35m0\u001b[39m]\u001b[33m:\u001b[39mu\u001b[33m.\u001b[39msnapAmount)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39m\u001b[32m\"stepped\"\u001b[39m\u001b[33m!==\u001b[39mf\u001b[33m.\u001b[39mtype\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39mr\u001b[33m?\u001b[39mr\u001b[33m:\u001b[39me\u001b[33m?\u001b[39mo\u001b[33m?\u001b[39mp\u001b[33m/\u001b[39m\u001b[35m1.5\u001b[39m\u001b[33m:\u001b[39mg\u001b[33m:\u001b[39m\u001b[35m1e3\u001b[39m\u001b[33m/\u001b[39m\u001b[35m60\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39me\u001b[33m?\u001b[39mo\u001b[33m?\u001b[39m\u001b[35m7.5\u001b[39m\u001b[33m:\u001b[39m\u001b[35m40\u001b[39m\u001b[33m:\u001b[39m\u001b[35m2.5\u001b[39m\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39m[\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(h[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop)\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(h[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft)]\u001b[33m,\u001b[39md\u001b[33m=\u001b[39m[c\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39my\u001b[33m>\u001b[39m\u001b[35m10\u001b[39m\u001b[33m?\u001b[39m\u001b[35m10\u001b[39m\u001b[33m:\u001b[39mc\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39my\u001b[33m,\u001b[39mc\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39mx\u001b[33m>\u001b[39m\u001b[35m10\u001b[39m\u001b[33m?\u001b[39m\u001b[35m10\u001b[39m\u001b[33m:\u001b[39mc\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39mx]\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mf\u001b[33m.\u001b[39mdir[\u001b[35m0\u001b[39m]\u001b[33m?\u001b[39ms[\u001b[35m1\u001b[39m]\u001b[33m+\u001b[39mf\u001b[33m.\u001b[39mdir[\u001b[35m1\u001b[39m]\u001b[33m*\u001b[39m(d[\u001b[35m1\u001b[39m]\u001b[33m*\u001b[39mn)\u001b[33m:\u001b[39ms[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39mf\u001b[33m.\u001b[39mdir[\u001b[35m1\u001b[39m]\u001b[33m*\u001b[39m(d[\u001b[35m0\u001b[39m]\u001b[33m*\u001b[39mn)\u001b[33m,\u001b[39mv\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mf\u001b[33m.\u001b[39mdir[\u001b[35m0\u001b[39m]\u001b[33m?\u001b[39ms[\u001b[35m1\u001b[39m]\u001b[33m+\u001b[39mf\u001b[33m.\u001b[39mdir[\u001b[35m1\u001b[39m]\u001b[33m*\u001b[39mparseInt(f\u001b[33m.\u001b[39mscrollAmount)\u001b[33m:\u001b[39ms[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39mf\u001b[33m.\u001b[39mdir[\u001b[35m1\u001b[39m]\u001b[33m*\u001b[39mparseInt(f\u001b[33m.\u001b[39mscrollAmount)\u001b[33m,\u001b[39mx\u001b[33m=\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m!==\u001b[39mf\u001b[33m.\u001b[39mscrollAmount\u001b[33m?\u001b[39mv\u001b[33m:\u001b[39mm\u001b[33m,\u001b[39m_\u001b[33m=\u001b[39mi\u001b[33m?\u001b[39mi\u001b[33m:\u001b[39me\u001b[33m?\u001b[39mo\u001b[33m?\u001b[39m\u001b[32m\"mcsLinearOut\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"mcsEaseInOut\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"mcsLinear\"\u001b[39m\u001b[33m,\u001b[39mw\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[33m!\u001b[39me\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m e\u001b[33m&&\u001b[39m\u001b[35m17\u001b[39m\u001b[33m>\u001b[39ma\u001b[33m&&\u001b[39m(x\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mf\u001b[33m.\u001b[39mdir[\u001b[35m0\u001b[39m]\u001b[33m?\u001b[39ms[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39ms[\u001b[35m0\u001b[39m])\u001b[33m,\u001b[39m\u001b[33mG\u001b[39m(t\u001b[33m,\u001b[39mx\u001b[33m.\u001b[39mtoString()\u001b[33m,\u001b[39m{dir\u001b[33m:\u001b[39mf\u001b[33m.\u001b[39mdir[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mscrollEasing\u001b[33m:\u001b[39m_\u001b[33m,\u001b[39mdur\u001b[33m:\u001b[39ma\u001b[33m,\u001b[39monComplete\u001b[33m:\u001b[39mw})\u001b[33m,\u001b[39me\u001b[33m?\u001b[39m\u001b[36mvoid\u001b[39m(f\u001b[33m.\u001b[39mdir\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m:\u001b[39m(clearTimeout(f\u001b[33m.\u001b[39mstep)\u001b[33m,\u001b[39m\u001b[36mvoid\u001b[39m(f\u001b[33m.\u001b[39mstep\u001b[33m=\u001b[39msetTimeout(\u001b[36mfunction\u001b[39m(){l()}\u001b[33m,\u001b[39ma)))}\u001b[36mfunction\u001b[39m s(){clearTimeout(f\u001b[33m.\u001b[39mstep)\u001b[33m,\u001b[39m$(f\u001b[33m,\u001b[39m\u001b[32m\"step\"\u001b[39m)\u001b[33m,\u001b[39m\u001b[33mQ\u001b[39m(t)}\u001b[36mvar\u001b[39m c\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mu\u001b[33m=\u001b[39mc\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39mc\u001b[33m.\u001b[39msequential\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mc\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39m\u001b[32m\"stepped\"\u001b[39m\u001b[33m===\u001b[39mf\u001b[33m.\u001b[39mtype\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39mu\u001b[33m.\u001b[39mscrollInertia\u001b[33m<\u001b[39m\u001b[35m26\u001b[39m\u001b[33m?\u001b[39m\u001b[35m26\u001b[39m\u001b[33m:\u001b[39mu\u001b[33m.\u001b[39mscrollInertia\u001b[33m,\u001b[39mg\u001b[33m=\u001b[39mu\u001b[33m.\u001b[39mscrollInertia\u001b[33m<\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m\u001b[35m17\u001b[39m\u001b[33m:\u001b[39mu\u001b[33m.\u001b[39mscrollInertia\u001b[33m;\u001b[39m\u001b[36mswitch\u001b[39m(o){\u001b[36mcase\u001b[39m\u001b[32m\"on\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mif\u001b[39m(f\u001b[33m.\u001b[39mdir\u001b[33m=\u001b[39m[n\u001b[33m===\u001b[39md[\u001b[35m16\u001b[39m]\u001b[33m||\u001b[39mn\u001b[33m===\u001b[39md[\u001b[35m15\u001b[39m]\u001b[33m||\u001b[39m\u001b[35m39\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m||\u001b[39m\u001b[35m37\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m?\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m,\u001b[39mn\u001b[33m===\u001b[39md[\u001b[35m13\u001b[39m]\u001b[33m||\u001b[39mn\u001b[33m===\u001b[39md[\u001b[35m15\u001b[39m]\u001b[33m||\u001b[39m\u001b[35m38\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m||\u001b[39m\u001b[35m37\u001b[39m\u001b[33m===\u001b[39mn\u001b[33m?\u001b[39m\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m:\u001b[39m\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39m\u001b[33mQ\u001b[39m(t)\u001b[33m,\u001b[39moe(n)\u001b[33m&&\u001b[39m\u001b[32m\"stepped\"\u001b[39m\u001b[33m===\u001b[39mf\u001b[33m.\u001b[39mtype)\u001b[36mreturn\u001b[39m\u001b[33m;\u001b[39ml(m)\u001b[33m;\u001b[39m\u001b[36mbreak\u001b[39m\u001b[33m;\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"off\"\u001b[39m\u001b[33m:\u001b[39ms()\u001b[33m,\u001b[39m(m\u001b[33m||\u001b[39mc\u001b[33m.\u001b[39mtweenRunning\u001b[33m&&\u001b[39mf\u001b[33m.\u001b[39mdir)\u001b[33m&&\u001b[39ml(\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m)}}\u001b[33m,\u001b[39m\u001b[33mY\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m.\u001b[39mdata(a)\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39m[]\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m t\u001b[33m&&\u001b[39m(t\u001b[33m=\u001b[39mt())\u001b[33m,\u001b[39mt \u001b[36minstanceof\u001b[39m \u001b[33mArray\u001b[39m\u001b[33m?\u001b[39mn\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mlength\u001b[33m>\u001b[39m\u001b[35m1\u001b[39m\u001b[33m?\u001b[39m[t[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mt[\u001b[35m1\u001b[39m]]\u001b[33m:\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m[\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mt[\u001b[35m0\u001b[39m]]\u001b[33m:\u001b[39m[t[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[36mnull\u001b[39m]\u001b[33m:\u001b[39m(n[\u001b[35m0\u001b[39m]\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39my\u001b[33m?\u001b[39mt\u001b[33m.\u001b[39my\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39mx\u001b[33m||\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m:\u001b[39mt\u001b[33m,\u001b[39mn[\u001b[35m1\u001b[39m]\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mx\u001b[33m?\u001b[39mt\u001b[33m.\u001b[39mx\u001b[33m:\u001b[39mt\u001b[33m.\u001b[39my\u001b[33m||\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m:\u001b[39mt)\u001b[33m,\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m n[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39m(n[\u001b[35m0\u001b[39m]\u001b[33m=\u001b[39mn[\u001b[35m0\u001b[39m]())\u001b[33m,\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m n[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39m(n[\u001b[35m1\u001b[39m]\u001b[33m=\u001b[39mn[\u001b[35m1\u001b[39m]())\u001b[33m,\u001b[39mn}\u001b[33m,\u001b[39m\u001b[33mX\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t\u001b[33m,\u001b[39mo){\u001b[36mif\u001b[39m(\u001b[36mnull\u001b[39m\u001b[33m!=\u001b[39mt\u001b[33m&&\u001b[39m\u001b[32m\"undefined\"\u001b[39m\u001b[33m!=\u001b[39m\u001b[36mtypeof\u001b[39m t){\u001b[36mvar\u001b[39m n\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mi\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39ml\u001b[33m.\u001b[39mparent()\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39m\u001b[36mtypeof\u001b[39m t\u001b[33m;\u001b[39mo\u001b[33m||\u001b[39m(o\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mr\u001b[33m.\u001b[39maxis\u001b[33m?\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"y\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m d\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m?\u001b[39ml\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m-\u001b[39ms\u001b[33m.\u001b[39mwidth()\u001b[33m:\u001b[39ml\u001b[33m.\u001b[39mouterHeight(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m-\u001b[39ms\u001b[33m.\u001b[39mheight()\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m?\u001b[39ml[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft\u001b[33m:\u001b[39ml[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m?\u001b[39m\u001b[32m\"left\"\u001b[39m\u001b[33m:\u001b[39m\u001b[32m\"top\"\u001b[39m\u001b[33m;\u001b[39m\u001b[36mswitch\u001b[39m(c){\u001b[36mcase\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mreturn\u001b[39m t()\u001b[33m;\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"object\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mvar\u001b[39m m\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mjquery\u001b[33m?\u001b[39mt\u001b[33m:\u001b[39me(t)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33m!\u001b[39mm\u001b[33m.\u001b[39mlength)\u001b[36mreturn\u001b[39m\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m?\u001b[39mae(m)[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39mae(m)[\u001b[35m0\u001b[39m]\u001b[33m;\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"string\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"number\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mif\u001b[39m(oe(t))\u001b[36mreturn\u001b[39m \u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(t)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m!==\u001b[39mt\u001b[33m.\u001b[39mindexOf(\u001b[32m\"%\"\u001b[39m))\u001b[36mreturn\u001b[39m \u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(d\u001b[33m*\u001b[39mparseInt(t)\u001b[33m/\u001b[39m\u001b[35m100\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m!==\u001b[39mt\u001b[33m.\u001b[39mindexOf(\u001b[32m\"-=\"\u001b[39m))\u001b[36mreturn\u001b[39m \u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(f\u001b[33m-\u001b[39mparseInt(t\u001b[33m.\u001b[39msplit(\u001b[32m\"-=\"\u001b[39m)[\u001b[35m1\u001b[39m]))\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m!==\u001b[39mt\u001b[33m.\u001b[39mindexOf(\u001b[32m\"+=\"\u001b[39m)){\u001b[36mvar\u001b[39m p\u001b[33m=\u001b[39mf\u001b[33m+\u001b[39mparseInt(t\u001b[33m.\u001b[39msplit(\u001b[32m\"+=\"\u001b[39m)[\u001b[35m1\u001b[39m])\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m p\u001b[33m>=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m?\u001b[39m\u001b[35m0\u001b[39m\u001b[33m:\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(p)}\u001b[36mif\u001b[39m(\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m!==\u001b[39mt\u001b[33m.\u001b[39mindexOf(\u001b[32m\"px\"\u001b[39m)\u001b[33m&&\u001b[39moe(t\u001b[33m.\u001b[39msplit(\u001b[32m\"px\"\u001b[39m)[\u001b[35m0\u001b[39m]))\u001b[36mreturn\u001b[39m \u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(t\u001b[33m.\u001b[39msplit(\u001b[32m\"px\"\u001b[39m)[\u001b[35m0\u001b[39m])\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"top\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m||\u001b[39m\u001b[32m\"left\"\u001b[39m\u001b[33m===\u001b[39mt)\u001b[36mreturn\u001b[39m \u001b[35m0\u001b[39m\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"bottom\"\u001b[39m\u001b[33m===\u001b[39mt)\u001b[36mreturn\u001b[39m \u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(s\u001b[33m.\u001b[39mheight()\u001b[33m-\u001b[39ml\u001b[33m.\u001b[39mouterHeight(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m))\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"right\"\u001b[39m\u001b[33m===\u001b[39mt)\u001b[36mreturn\u001b[39m \u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(s\u001b[33m.\u001b[39mwidth()\u001b[33m-\u001b[39ml\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m))\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"first\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m||\u001b[39m\u001b[32m\"last\"\u001b[39m\u001b[33m===\u001b[39mt){\u001b[36mvar\u001b[39m m\u001b[33m=\u001b[39ml\u001b[33m.\u001b[39mfind(\u001b[32m\":\"\u001b[39m\u001b[33m+\u001b[39mt)\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m?\u001b[39mae(m)[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39mae(m)[\u001b[35m0\u001b[39m]}\u001b[36mreturn\u001b[39m e(t)\u001b[33m.\u001b[39mlength\u001b[33m?\u001b[39m\u001b[32m\"x\"\u001b[39m\u001b[33m===\u001b[39mo\u001b[33m?\u001b[39mae(e(t))[\u001b[35m1\u001b[39m]\u001b[33m:\u001b[39mae(e(t))[\u001b[35m0\u001b[39m]\u001b[33m:\u001b[39m(l\u001b[33m.\u001b[39mcss(h\u001b[33m,\u001b[39mt)\u001b[33m,\u001b[39m\u001b[36mvoid\u001b[39m u\u001b[33m.\u001b[39mupdate\u001b[33m.\u001b[39mcall(\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39mn[\u001b[35m0\u001b[39m]))}}}\u001b[33m,\u001b[39m\u001b[33mN\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mfunction\u001b[39m o(){\u001b[36mreturn\u001b[39m clearTimeout(f[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mautoUpdate)\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m\u001b[33m===\u001b[39ml\u001b[33m.\u001b[39mparents(\u001b[32m\"html\"\u001b[39m)\u001b[33m.\u001b[39mlength\u001b[33m?\u001b[39m\u001b[36mvoid\u001b[39m(l\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m)\u001b[33m:\u001b[39m\u001b[36mvoid\u001b[39m(f[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mautoUpdate\u001b[33m=\u001b[39msetTimeout(\u001b[36mfunction\u001b[39m(){\u001b[36mreturn\u001b[39m c\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnSelectorChange\u001b[33m&&\u001b[39m(s\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39mchange\u001b[33m.\u001b[39mn\u001b[33m=\u001b[39mi()\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39mchange\u001b[33m.\u001b[39mn\u001b[33m!==\u001b[39ms\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39mchange\u001b[33m.\u001b[39mo)\u001b[33m?\u001b[39m(s\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39mchange\u001b[33m.\u001b[39mo\u001b[33m=\u001b[39ms\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39mchange\u001b[33m.\u001b[39mn\u001b[33m,\u001b[39m\u001b[36mvoid\u001b[39m r(\u001b[35m3\u001b[39m))\u001b[33m:\u001b[39mc\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnContentResize\u001b[33m&&\u001b[39m(s\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39msize\u001b[33m.\u001b[39mn\u001b[33m=\u001b[39ml[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mscrollHeight\u001b[33m+\u001b[39ml[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mscrollWidth\u001b[33m+\u001b[39mf[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetHeight\u001b[33m+\u001b[39ml[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetHeight\u001b[33m+\u001b[39ml[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetWidth\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39msize\u001b[33m.\u001b[39mn\u001b[33m!==\u001b[39ms\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39msize\u001b[33m.\u001b[39mo)\u001b[33m?\u001b[39m(s\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39msize\u001b[33m.\u001b[39mo\u001b[33m=\u001b[39ms\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39msize\u001b[33m.\u001b[39mn\u001b[33m,\u001b[39m\u001b[36mvoid\u001b[39m r(\u001b[35m1\u001b[39m))\u001b[33m:\u001b[39m\u001b[33m!\u001b[39mc\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnImageLoad\u001b[33m||\u001b[39m\u001b[32m\"auto\"\u001b[39m\u001b[33m===\u001b[39mc\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnImageLoad\u001b[33m&&\u001b[39m\u001b[32m\"y\"\u001b[39m\u001b[33m===\u001b[39mc\u001b[33m.\u001b[39maxis\u001b[33m||\u001b[39m(s\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39mimg\u001b[33m.\u001b[39mn\u001b[33m=\u001b[39mf\u001b[33m.\u001b[39mfind(\u001b[32m\"img\"\u001b[39m)\u001b[33m.\u001b[39mlength\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39mimg\u001b[33m.\u001b[39mn\u001b[33m===\u001b[39ms\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39mimg\u001b[33m.\u001b[39mo)\u001b[33m?\u001b[39m\u001b[36mvoid\u001b[39m((c\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnSelectorChange\u001b[33m||\u001b[39mc\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnContentResize\u001b[33m||\u001b[39mc\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnImageLoad)\u001b[33m&&\u001b[39mo())\u001b[33m:\u001b[39m(s\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39mimg\u001b[33m.\u001b[39mo\u001b[33m=\u001b[39ms\u001b[33m.\u001b[39mpoll\u001b[33m.\u001b[39mimg\u001b[33m.\u001b[39mn\u001b[33m,\u001b[39m\u001b[36mvoid\u001b[39m f\u001b[33m.\u001b[39mfind(\u001b[32m\"img\"\u001b[39m)\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){n(\u001b[36mthis\u001b[39m)}))}\u001b[33m,\u001b[39mc\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mautoUpdateTimeout))}\u001b[36mfunction\u001b[39m n(t){\u001b[36mfunction\u001b[39m o(e\u001b[33m,\u001b[39mt){\u001b[36mreturn\u001b[39m \u001b[36mfunction\u001b[39m(){\u001b[0m\n\u001b[0m \u001b[90m   | \u001b[39m                                                                                                                                                                                                                                                                                                                                              \u001b[31m\u001b[1m^\u001b[22m\u001b[39m\u001b[0m\n\u001b[0m \u001b[90m 5 | \u001b[39m\u001b[36mreturn\u001b[39m t\u001b[33m.\u001b[39mapply(e\u001b[33m,\u001b[39marguments)}}\u001b[36mfunction\u001b[39m a(){\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39monload\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39me(t)\u001b[33m.\u001b[39maddClass(d[\u001b[35m2\u001b[39m])\u001b[33m,\u001b[39mr(\u001b[35m2\u001b[39m)}\u001b[36mif\u001b[39m(e(t)\u001b[33m.\u001b[39mhasClass(d[\u001b[35m2\u001b[39m]))\u001b[36mreturn\u001b[39m \u001b[36mvoid\u001b[39m r()\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m n\u001b[33m=\u001b[39m\u001b[36mnew\u001b[39m \u001b[33mImage\u001b[39m\u001b[33m;\u001b[39mn\u001b[33m.\u001b[39monload\u001b[33m=\u001b[39mo(n\u001b[33m,\u001b[39ma)\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39msrc\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39msrc}\u001b[36mfunction\u001b[39m i(){c\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnSelectorChange\u001b[33m===\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39m(c\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnSelectorChange\u001b[33m=\u001b[39m\u001b[32m\"*\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m e\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mt\u001b[33m=\u001b[39mf\u001b[33m.\u001b[39mfind(c\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnSelectorChange)\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m c\u001b[33m.\u001b[39madvanced\u001b[33m.\u001b[39mupdateOnSelectorChange\u001b[33m&&\u001b[39mt\u001b[33m.\u001b[39mlength\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39mt\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){e\u001b[33m+=\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39moffsetHeight\u001b[33m+\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m.\u001b[39moffsetWidth})\u001b[33m,\u001b[39me}\u001b[36mfunction\u001b[39m r(e){clearTimeout(f[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mautoUpdate)\u001b[33m,\u001b[39mu\u001b[33m.\u001b[39mupdate\u001b[33m.\u001b[39mcall(\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39ml[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39me)}\u001b[36mvar\u001b[39m l\u001b[33m=\u001b[39me(\u001b[36mthis\u001b[39m)\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39ml\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39ms\u001b[33m.\u001b[39mopt\u001b[33m,\u001b[39mf\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39ms\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m t\u001b[33m?\u001b[39m(clearTimeout(f[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mautoUpdate)\u001b[33m,\u001b[39m\u001b[36mvoid\u001b[39m $(f[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[32m\"autoUpdate\"\u001b[39m))\u001b[33m:\u001b[39m\u001b[36mvoid\u001b[39m o()}\u001b[33m,\u001b[39m\u001b[33mV\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e\u001b[33m,\u001b[39mt\u001b[33m,\u001b[39mo){\u001b[36mreturn\u001b[39m \u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mround(e\u001b[33m/\u001b[39mt)\u001b[33m*\u001b[39mt\u001b[33m-\u001b[39mo}\u001b[33m,\u001b[39m\u001b[33mQ\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39mt\u001b[33m.\u001b[39mdata(a)\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39me(\u001b[32m\"#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container,#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_container_wrapper,#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_vertical,#mCSB_\"\u001b[39m\u001b[33m+\u001b[39mo\u001b[33m.\u001b[39midx\u001b[33m+\u001b[39m\u001b[32m\"_dragger_horizontal\"\u001b[39m)\u001b[33m;\u001b[39mn\u001b[33m.\u001b[39meach(\u001b[36mfunction\u001b[39m(){\u001b[33mZ\u001b[39m\u001b[33m.\u001b[39mcall(\u001b[36mthis\u001b[39m)})}\u001b[33m,\u001b[39m\u001b[33mG\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t\u001b[33m,\u001b[39mo\u001b[33m,\u001b[39mn){\u001b[36mfunction\u001b[39m i(e){\u001b[36mreturn\u001b[39m s\u001b[33m&&\u001b[39mc\u001b[33m.\u001b[39mcallbacks[e]\u001b[33m&&\u001b[39m\u001b[32m\"function\"\u001b[39m\u001b[33m==\u001b[39m\u001b[36mtypeof\u001b[39m c\u001b[33m.\u001b[39mcallbacks[e]}\u001b[36mfunction\u001b[39m r(){\u001b[36mreturn\u001b[39m[c\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39malwaysTriggerOffsets\u001b[33m||\u001b[39mw\u001b[33m>=\u001b[39m\u001b[33mS\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39my\u001b[33m,\u001b[39mc\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39malwaysTriggerOffsets\u001b[33m||\u001b[39m\u001b[33m-\u001b[39m\u001b[33mB\u001b[39m\u001b[33m>=\u001b[39mw]}\u001b[36mfunction\u001b[39m l(){\u001b[36mvar\u001b[39m e\u001b[33m=\u001b[39m[h[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop\u001b[33m,\u001b[39mh[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft]\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39m[x[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop\u001b[33m,\u001b[39mx[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft]\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39m[h\u001b[33m.\u001b[39mouterHeight(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39mh\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)]\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39m[f\u001b[33m.\u001b[39mheight()\u001b[33m,\u001b[39mf\u001b[33m.\u001b[39mwidth()]\u001b[33m;\u001b[39mt[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mmcs\u001b[33m=\u001b[39m{content\u001b[33m:\u001b[39mh\u001b[33m,\u001b[39mtop\u001b[33m:\u001b[39me[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mleft\u001b[33m:\u001b[39me[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39mdraggerTop\u001b[33m:\u001b[39mo[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mdraggerLeft\u001b[33m:\u001b[39mo[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39mtopPct\u001b[33m:\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mround(\u001b[35m100\u001b[39m\u001b[33m*\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(e[\u001b[35m0\u001b[39m])\u001b[33m/\u001b[39m(\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(a[\u001b[35m0\u001b[39m])\u001b[33m-\u001b[39mi[\u001b[35m0\u001b[39m]))\u001b[33m,\u001b[39mleftPct\u001b[33m:\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mround(\u001b[35m100\u001b[39m\u001b[33m*\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mabs(e[\u001b[35m1\u001b[39m])\u001b[35m/(Math.abs(a[1])-i[1])),direction:n.dir}}var s=t.data(a),c=s.opt,d={trigger:\"internal\",dir:\"y\",scrollEasing:\"mcsEaseOut\",drag:!1,dur:c.scrollInertia,overwrite:\"all\",callbacks:!0,onStart:!0,onUpdate:!0,onComplete:!0},n=e.extend(d,n),u=[n.dur,n.drag?0:n.dur],f=e(\"#mCSB_\"+s.idx),h=e(\"#mCSB_\"+s.idx+\"_container\"),m=h.parent(),p=c.callbacks.onTotalScrollOffset?Y.call(t,c.callbacks.onTotalScrollOffset):[0,0],g=c.callbacks.onTotalScrollBackOffset?Y.call(t,c.callbacks.onTotalScrollBackOffset):[0,0];if(s.trigger=n.trigger,0===m.scrollTop()&&0===m.scrollLeft()||(e(\".mCSB_\"+s.idx+\"_scrollbar\").css(\"visibility\",\"visible\"),m.scrollTop(0).scrollLeft(0)),\"_resetY\"!==o||s.contentReset.y||(i(\"onOverflowYNone\")&&c.callbacks.onOverflowYNone.call(t[0]),s.contentReset.y=1),\"_resetX\"!==o||s.contentReset.x||(i(\"onOverflowXNone\")&&c.callbacks.onOverflowXNone.call(t[0]),s.contentReset.x=1),\"_resetY\"!==o&&\"_resetX\"!==o){if(!s.contentReset.y&&t[0].mcs||!s.overflowed[0]||(i(\"onOverflowY\")&&c.callbacks.onOverflowY.call(t[0]),s.contentReset.x=null),!s.contentReset.x&&t[0].mcs||!s.overflowed[1]||(i(\"onOverflowX\")&&c.callbacks.onOverflowX.call(t[0]),s.contentReset.x=null),c.snapAmount){var v=c.snapAmount instanceof Array?\"x\"===n.dir?c.snapAmount[1]:c.snapAmount[0]:c.snapAmount;o=V(o,v,c.snapOffset)}switch(n.dir){case\"x\":var x=e(\"#mCSB_\"+s.idx+\"_dragger_horizontal\"),_=\"left\",w=h[0].offsetLeft,S=[f.width()-h.outerWidth(!1),x.parent().width()-x.width()],b=[o,0===o?0:o/s.scrollRatio.x],y=p[1],B=g[1],T=y>0?y/s\u001b[39m\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39mx\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mk\u001b[33m=\u001b[39m\u001b[33mB\u001b[39m\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m?\u001b[39m\u001b[33mB\u001b[39m\u001b[35m/s.scrollRatio.x:0;break;case\"y\":var x=e(\"#mCSB_\"+s.idx+\"_dragger_vertical\"),_=\"top\",w=h[0].offsetTop,S=[f.height()-h.outerHeight(!1),x.parent().height()-x.height()],b=[o,0===o?0:o/s.scrollRatio.y],y=p[0],B=g[0],T=y>0?y/s\u001b[39m\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39my\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39mk\u001b[33m=\u001b[39m\u001b[33mB\u001b[39m\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m?\u001b[39m\u001b[33mB\u001b[39m\u001b[33m/\u001b[39ms\u001b[33m.\u001b[39mscrollRatio\u001b[33m.\u001b[39my\u001b[33m:\u001b[39m\u001b[35m0\u001b[39m}b[\u001b[35m1\u001b[39m]\u001b[33m<\u001b[39m\u001b[35m0\u001b[39m\u001b[33m||\u001b[39m\u001b[35m0\u001b[39m\u001b[33m===\u001b[39mb[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39m\u001b[35m0\u001b[39m\u001b[33m===\u001b[39mb[\u001b[35m1\u001b[39m]\u001b[33m?\u001b[39mb\u001b[33m=\u001b[39m[\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m]\u001b[33m:\u001b[39mb[\u001b[35m1\u001b[39m]\u001b[33m>=\u001b[39m\u001b[33mS\u001b[39m[\u001b[35m1\u001b[39m]\u001b[33m?\u001b[39mb\u001b[33m=\u001b[39m[\u001b[33mS\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m[\u001b[35m1\u001b[39m]]\u001b[33m:\u001b[39mb[\u001b[35m0\u001b[39m]\u001b[33m=\u001b[39m\u001b[33m-\u001b[39mb[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mt[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39mmcs\u001b[33m||\u001b[39m(l()\u001b[33m,\u001b[39mi(\u001b[32m\"onInit\"\u001b[39m)\u001b[33m&&\u001b[39mc\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monInit\u001b[33m.\u001b[39mcall(t[\u001b[35m0\u001b[39m]))\u001b[33m,\u001b[39mclearTimeout(h[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39monCompleteTimeout)\u001b[33m,\u001b[39m\u001b[33mJ\u001b[39m(x[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m_\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mround(b[\u001b[35m1\u001b[39m])\u001b[33m,\u001b[39mu[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39mscrollEasing)\u001b[33m,\u001b[39m\u001b[33m!\u001b[39ms\u001b[33m.\u001b[39mtweenRunning\u001b[33m&&\u001b[39m(\u001b[35m0\u001b[39m\u001b[33m===\u001b[39mw\u001b[33m&&\u001b[39mb[\u001b[35m0\u001b[39m]\u001b[33m>=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m||\u001b[39mw\u001b[33m===\u001b[39m\u001b[33mS\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39mb[\u001b[35m0\u001b[39m]\u001b[33m<=\u001b[39m\u001b[33mS\u001b[39m[\u001b[35m0\u001b[39m])\u001b[33m||\u001b[39m\u001b[33mJ\u001b[39m(h[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m_\u001b[33m,\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mround(b[\u001b[35m0\u001b[39m])\u001b[33m,\u001b[39mu[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39mscrollEasing\u001b[33m,\u001b[39mn\u001b[33m.\u001b[39moverwrite\u001b[33m,\u001b[39m{onStart\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(){n\u001b[33m.\u001b[39mcallbacks\u001b[33m&&\u001b[39mn\u001b[33m.\u001b[39monStart\u001b[33m&&\u001b[39m\u001b[33m!\u001b[39ms\u001b[33m.\u001b[39mtweenRunning\u001b[33m&&\u001b[39m(i(\u001b[32m\"onScrollStart\"\u001b[39m)\u001b[33m&&\u001b[39m(l()\u001b[33m,\u001b[39mc\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monScrollStart\u001b[33m.\u001b[39mcall(t[\u001b[35m0\u001b[39m]))\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mtweenRunning\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[33mC\u001b[39m(x)\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mcbOffsets\u001b[33m=\u001b[39mr())}\u001b[33m,\u001b[39monUpdate\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(){n\u001b[33m.\u001b[39mcallbacks\u001b[33m&&\u001b[39mn\u001b[33m.\u001b[39monUpdate\u001b[33m&&\u001b[39mi(\u001b[32m\"whileScrolling\"\u001b[39m)\u001b[33m&&\u001b[39m(l()\u001b[33m,\u001b[39mc\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39mwhileScrolling\u001b[33m.\u001b[39mcall(t[\u001b[35m0\u001b[39m]))}\u001b[33m,\u001b[39monComplete\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mif\u001b[39m(n\u001b[33m.\u001b[39mcallbacks\u001b[33m&&\u001b[39mn\u001b[33m.\u001b[39monComplete){\u001b[32m\"yx\"\u001b[39m\u001b[33m===\u001b[39mc\u001b[33m.\u001b[39maxis\u001b[33m&&\u001b[39mclearTimeout(h[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39monCompleteTimeout)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m e\u001b[33m=\u001b[39mh[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39midleTimer\u001b[33m||\u001b[39m\u001b[35m0\u001b[39m\u001b[33m;\u001b[39mh[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39monCompleteTimeout\u001b[33m=\u001b[39msetTimeout(\u001b[36mfunction\u001b[39m(){i(\u001b[32m\"onScroll\"\u001b[39m)\u001b[33m&&\u001b[39m(l()\u001b[33m,\u001b[39mc\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monScroll\u001b[33m.\u001b[39mcall(t[\u001b[35m0\u001b[39m]))\u001b[33m,\u001b[39mi(\u001b[32m\"onTotalScroll\"\u001b[39m)\u001b[33m&&\u001b[39mb[\u001b[35m1\u001b[39m]\u001b[33m>=\u001b[39m\u001b[33mS\u001b[39m[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39m\u001b[33mT\u001b[39m\u001b[33m&&\u001b[39ms\u001b[33m.\u001b[39mcbOffsets[\u001b[35m0\u001b[39m]\u001b[33m&&\u001b[39m(l()\u001b[33m,\u001b[39mc\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monTotalScroll\u001b[33m.\u001b[39mcall(t[\u001b[35m0\u001b[39m]))\u001b[33m,\u001b[39mi(\u001b[32m\"onTotalScrollBack\"\u001b[39m)\u001b[33m&&\u001b[39mb[\u001b[35m1\u001b[39m]\u001b[33m<=\u001b[39mk\u001b[33m&&\u001b[39ms\u001b[33m.\u001b[39mcbOffsets[\u001b[35m1\u001b[39m]\u001b[33m&&\u001b[39m(l()\u001b[33m,\u001b[39mc\u001b[33m.\u001b[39mcallbacks\u001b[33m.\u001b[39monTotalScrollBack\u001b[33m.\u001b[39mcall(t[\u001b[35m0\u001b[39m]))\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mtweenRunning\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39mh[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39midleTimer\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[33mC\u001b[39m(x\u001b[33m,\u001b[39m\u001b[32m\"hide\"\u001b[39m)}\u001b[33m,\u001b[39me)}}})}}\u001b[33m,\u001b[39m\u001b[33mJ\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e\u001b[33m,\u001b[39mt\u001b[33m,\u001b[39mo\u001b[33m,\u001b[39ma\u001b[33m,\u001b[39mn\u001b[33m,\u001b[39mi\u001b[33m,\u001b[39mr){\u001b[36mfunction\u001b[39m l(){\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mstop\u001b[33m||\u001b[39m(x\u001b[33m||\u001b[39mm\u001b[33m.\u001b[39mcall()\u001b[33m,\u001b[39mx\u001b[33m=\u001b[39m\u001b[33mK\u001b[39m()\u001b[33m-\u001b[39mv\u001b[33m,\u001b[39ms()\u001b[33m,\u001b[39mx\u001b[33m>=\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mtime\u001b[33m&&\u001b[39m(\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mtime\u001b[33m=\u001b[39mx\u001b[33m>\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mtime\u001b[33m?\u001b[39mx\u001b[33m+\u001b[39mf\u001b[33m-\u001b[39m(x\u001b[33m-\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mtime)\u001b[33m:\u001b[39mx\u001b[33m+\u001b[39mf\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mtime\u001b[33m<\u001b[39m\u001b[33mx\u001b[39m\u001b[33m+\u001b[39m\u001b[35m1\u001b[39m\u001b[33m&&\u001b[39m(\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mtime\u001b[33m=\u001b[39mx\u001b[33m+\u001b[39m\u001b[35m1\u001b[39m))\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mtime\u001b[33m<\u001b[39m\u001b[33ma\u001b[39m\u001b[33m?\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mid\u001b[33m=\u001b[39mh(l)\u001b[33m:\u001b[39mg\u001b[33m.\u001b[39mcall())}\u001b[36mfunction\u001b[39m s(){a\u001b[33m>\u001b[39m\u001b[35m0\u001b[39m\u001b[33m?\u001b[39m(\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mcurrVal\u001b[33m=\u001b[39mu(\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mtime\u001b[33m,\u001b[39m_\u001b[33m,\u001b[39mb\u001b[33m,\u001b[39ma\u001b[33m,\u001b[39mn)\u001b[33m,\u001b[39mw[t]\u001b[33m=\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mround(\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mcurrVal)\u001b[33m+\u001b[39m\u001b[32m\"px\"\u001b[39m)\u001b[33m:\u001b[39mw[t]\u001b[33m=\u001b[39mo\u001b[33m+\u001b[39m\u001b[32m\"px\"\u001b[39m\u001b[33m,\u001b[39mp\u001b[33m.\u001b[39mcall()}\u001b[36mfunction\u001b[39m c(){f\u001b[33m=\u001b[39m\u001b[35m1e3\u001b[39m\u001b[33m/\u001b[39m\u001b[35m60\u001b[39m\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mtime\u001b[33m=\u001b[39mx\u001b[33m+\u001b[39mf\u001b[33m,\u001b[39mh\u001b[33m=\u001b[39mwindow\u001b[33m.\u001b[39mrequestAnimationFrame\u001b[33m?\u001b[39mwindow\u001b[33m.\u001b[39mrequestAnimationFrame\u001b[33m:\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[36mreturn\u001b[39m s()\u001b[33m,\u001b[39msetTimeout(e\u001b[33m,\u001b[39m\u001b[35m.01\u001b[39m)}\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mid\u001b[33m=\u001b[39mh(l)}\u001b[36mfunction\u001b[39m d(){\u001b[36mnull\u001b[39m\u001b[33m!=\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mid\u001b[33m&&\u001b[39m(window\u001b[33m.\u001b[39mrequestAnimationFrame\u001b[33m?\u001b[39mwindow\u001b[33m.\u001b[39mcancelAnimationFrame(\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mid)\u001b[33m:\u001b[39mclearTimeout(\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mid)\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mid\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m)}\u001b[36mfunction\u001b[39m u(e\u001b[33m,\u001b[39mt\u001b[33m,\u001b[39mo\u001b[33m,\u001b[39ma\u001b[33m,\u001b[39mn){\u001b[36mswitch\u001b[39m(n){\u001b[36mcase\u001b[39m\u001b[32m\"linear\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"mcsLinear\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mreturn\u001b[39m o\u001b[33m*\u001b[39me\u001b[35m/a+t;case\"mcsLinearOut\":return e/\u001b[39m\u001b[33m=\u001b[39ma\u001b[33m,\u001b[39me\u001b[33m--\u001b[39m\u001b[33m,\u001b[39mo\u001b[33m*\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39msqrt(\u001b[35m1\u001b[39m\u001b[33m-\u001b[39me\u001b[33m*\u001b[39me)\u001b[33m+\u001b[39mt\u001b[33m;\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"easeInOutSmooth\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mreturn\u001b[39m e\u001b[33m/=\u001b[39ma\u001b[33m/\u001b[39m\u001b[35m2\u001b[39m\u001b[33m,\u001b[39m\u001b[35m1\u001b[39m\u001b[33m>\u001b[39me\u001b[33m?\u001b[39mo\u001b[33m/\u001b[39m\u001b[35m2\u001b[39m\u001b[33m*\u001b[39me\u001b[33m*\u001b[39me\u001b[33m+\u001b[39mt\u001b[33m:\u001b[39m(e\u001b[33m--\u001b[39m\u001b[33m,\u001b[39m\u001b[33m-\u001b[39mo\u001b[35m/2*(e*(e-2)-1)+t);case\"easeInOutStrong\":return e/\u001b[39m\u001b[33m=\u001b[39ma\u001b[33m/\u001b[39m\u001b[35m2\u001b[39m\u001b[33m,\u001b[39m\u001b[35m1\u001b[39m\u001b[33m>\u001b[39me\u001b[33m?\u001b[39mo\u001b[33m/\u001b[39m\u001b[35m2\u001b[39m\u001b[33m*\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mpow(\u001b[35m2\u001b[39m\u001b[33m,\u001b[39m\u001b[35m10\u001b[39m\u001b[33m*\u001b[39m(e\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m))\u001b[33m+\u001b[39mt\u001b[33m:\u001b[39m(e\u001b[33m--\u001b[39m\u001b[33m,\u001b[39mo\u001b[35m/2*(-Math.pow(2,-10*e)+2)+t);case\"easeInOut\":case\"mcsEaseInOut\":return e/\u001b[39m\u001b[33m=\u001b[39ma\u001b[33m/\u001b[39m\u001b[35m2\u001b[39m\u001b[33m,\u001b[39m\u001b[35m1\u001b[39m\u001b[33m>\u001b[39me\u001b[33m?\u001b[39mo\u001b[33m/\u001b[39m\u001b[35m2\u001b[39m\u001b[33m*\u001b[39me\u001b[33m*\u001b[39me\u001b[33m*\u001b[39me\u001b[33m+\u001b[39mt\u001b[33m:\u001b[39m(e\u001b[33m-=\u001b[39m\u001b[35m2\u001b[39m\u001b[33m,\u001b[39mo\u001b[35m/2*(e*e*e+2)+t);case\"easeOutSmooth\":return e/\u001b[39m\u001b[33m=\u001b[39ma\u001b[33m,\u001b[39me\u001b[33m--\u001b[39m\u001b[33m,\u001b[39m\u001b[33m-\u001b[39mo\u001b[33m*\u001b[39m(e\u001b[33m*\u001b[39me\u001b[33m*\u001b[39me\u001b[33m*\u001b[39me\u001b[33m-\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m+\u001b[39mt\u001b[33m;\u001b[39m\u001b[36mcase\u001b[39m\u001b[32m\"easeOutStrong\"\u001b[39m\u001b[33m:\u001b[39m\u001b[36mreturn\u001b[39m o\u001b[33m*\u001b[39m(\u001b[33m-\u001b[39m\u001b[33mMath\u001b[39m\u001b[33m.\u001b[39mpow(\u001b[35m2\u001b[39m\u001b[33m,\u001b[39m\u001b[33m-\u001b[39m\u001b[35m10\u001b[39m\u001b[33m*\u001b[39me\u001b[35m/a)+1)+t;case\"easeOut\":case\"mcsEaseOut\":default:var i=(e/\u001b[39m\u001b[33m=\u001b[39ma)\u001b[33m*\u001b[39me\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39mi\u001b[33m*\u001b[39me\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m t\u001b[33m+\u001b[39mo\u001b[33m*\u001b[39m(\u001b[35m.499999999999997\u001b[39m\u001b[33m*\u001b[39mr\u001b[33m*\u001b[39mi\u001b[33m+\u001b[39m\u001b[33m-\u001b[39m\u001b[35m2.5\u001b[39m\u001b[33m*\u001b[39mi\u001b[33m*\u001b[39mi\u001b[33m+\u001b[39m\u001b[35m5.5\u001b[39m\u001b[33m*\u001b[39mr\u001b[33m+\u001b[39m\u001b[33m-\u001b[39m\u001b[35m6.5\u001b[39m\u001b[33m*\u001b[39mi\u001b[33m+\u001b[39m\u001b[35m4\u001b[39m\u001b[33m*\u001b[39me)}}e\u001b[33m.\u001b[39m_mTween\u001b[33m||\u001b[39m(e\u001b[33m.\u001b[39m_mTween\u001b[33m=\u001b[39m{top\u001b[33m:\u001b[39m{}\u001b[33m,\u001b[39mleft\u001b[33m:\u001b[39m{}})\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m f\u001b[33m,\u001b[39mh\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39mr\u001b[33m||\u001b[39m{}\u001b[33m,\u001b[39mm\u001b[33m=\u001b[39mr\u001b[33m.\u001b[39monStart\u001b[33m||\u001b[39m\u001b[36mfunction\u001b[39m(){}\u001b[33m,\u001b[39mp\u001b[33m=\u001b[39mr\u001b[33m.\u001b[39monUpdate\u001b[33m||\u001b[39m\u001b[36mfunction\u001b[39m(){}\u001b[33m,\u001b[39mg\u001b[33m=\u001b[39mr\u001b[33m.\u001b[39monComplete\u001b[33m||\u001b[39m\u001b[36mfunction\u001b[39m(){}\u001b[33m,\u001b[39mv\u001b[33m=\u001b[39m\u001b[33mK\u001b[39m()\u001b[33m,\u001b[39mx\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m_\u001b[33m=\u001b[39me\u001b[33m.\u001b[39moffsetTop\u001b[33m,\u001b[39mw\u001b[33m=\u001b[39me\u001b[33m.\u001b[39mstyle\u001b[33m,\u001b[39m\u001b[33mS\u001b[39m\u001b[33m=\u001b[39me\u001b[33m.\u001b[39m_mTween[t]\u001b[33m;\u001b[39m\u001b[32m\"left\"\u001b[39m\u001b[33m===\u001b[39mt\u001b[33m&&\u001b[39m(_\u001b[33m=\u001b[39me\u001b[33m.\u001b[39moffsetLeft)\u001b[33m;\u001b[39m\u001b[36mvar\u001b[39m b\u001b[33m=\u001b[39mo\u001b[33m-\u001b[39m_\u001b[33m;\u001b[39m\u001b[33mS\u001b[39m\u001b[33m.\u001b[39mstop\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"none\"\u001b[39m\u001b[33m!==\u001b[39mi\u001b[33m&&\u001b[39md()\u001b[33m,\u001b[39mc()}\u001b[33m,\u001b[39m\u001b[33mK\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mreturn\u001b[39m window\u001b[33m.\u001b[39mperformance\u001b[33m&&\u001b[39mwindow\u001b[33m.\u001b[39mperformance\u001b[33m.\u001b[39mnow\u001b[33m?\u001b[39mwindow\u001b[33m.\u001b[39mperformance\u001b[33m.\u001b[39mnow()\u001b[33m:\u001b[39mwindow\u001b[33m.\u001b[39mperformance\u001b[33m&&\u001b[39mwindow\u001b[33m.\u001b[39mperformance\u001b[33m.\u001b[39mwebkitNow\u001b[33m?\u001b[39mwindow\u001b[33m.\u001b[39mperformance\u001b[33m.\u001b[39mwebkitNow()\u001b[33m:\u001b[39m\u001b[33mDate\u001b[39m\u001b[33m.\u001b[39mnow\u001b[33m?\u001b[39m\u001b[33mDate\u001b[39m\u001b[33m.\u001b[39mnow()\u001b[33m:\u001b[39m(\u001b[36mnew\u001b[39m \u001b[33mDate\u001b[39m)\u001b[33m.\u001b[39mgetTime()}\u001b[33m,\u001b[39m\u001b[33mZ\u001b[39m\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mvar\u001b[39m e\u001b[33m=\u001b[39m\u001b[36mthis\u001b[39m\u001b[33m;\u001b[39me\u001b[33m.\u001b[39m_mTween\u001b[33m||\u001b[39m(e\u001b[33m.\u001b[39m_mTween\u001b[33m=\u001b[39m{top\u001b[33m:\u001b[39m{}\u001b[33m,\u001b[39mleft\u001b[33m:\u001b[39m{}})\u001b[33m;\u001b[39m\u001b[36mfor\u001b[39m(\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39m[\u001b[32m\"top\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"left\"\u001b[39m]\u001b[33m,\u001b[39mo\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m;\u001b[39mo\u001b[33m<\u001b[39m\u001b[33mt\u001b[39m\u001b[33m.\u001b[39mlength\u001b[33m;\u001b[39mo\u001b[33m++\u001b[39m){\u001b[36mvar\u001b[39m a\u001b[33m=\u001b[39mt[o]\u001b[33m;\u001b[39me\u001b[33m.\u001b[39m_mTween[a]\u001b[33m.\u001b[39mid\u001b[33m&&\u001b[39m(window\u001b[33m.\u001b[39mrequestAnimationFrame\u001b[33m?\u001b[39mwindow\u001b[33m.\u001b[39mcancelAnimationFrame(e\u001b[33m.\u001b[39m_mTween[a]\u001b[33m.\u001b[39mid)\u001b[33m:\u001b[39mclearTimeout(e\u001b[33m.\u001b[39m_mTween[a]\u001b[33m.\u001b[39mid)\u001b[33m,\u001b[39me\u001b[33m.\u001b[39m_mTween[a]\u001b[33m.\u001b[39mid\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m\u001b[33m,\u001b[39me\u001b[33m.\u001b[39m_mTween[a]\u001b[33m.\u001b[39mstop\u001b[33m=\u001b[39m\u001b[35m1\u001b[39m)}}\u001b[33m,\u001b[39m$\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e\u001b[33m,\u001b[39mt){\u001b[36mtry\u001b[39m{\u001b[36mdelete\u001b[39m e[t]}\u001b[36mcatch\u001b[39m(o){e[t]\u001b[33m=\u001b[39m\u001b[36mnull\u001b[39m}}\u001b[33m,\u001b[39mee\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[36mreturn\u001b[39m\u001b[33m!\u001b[39m(e\u001b[33m.\u001b[39mwhich\u001b[33m&&\u001b[39m\u001b[35m1\u001b[39m\u001b[33m!==\u001b[39me\u001b[33m.\u001b[39mwhich)}\u001b[33m,\u001b[39mte\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me\u001b[33m.\u001b[39moriginalEvent\u001b[33m.\u001b[39mpointerType\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m\u001b[33m!\u001b[39m(t\u001b[33m&&\u001b[39m\u001b[32m\"touch\"\u001b[39m\u001b[33m!==\u001b[39mt\u001b[33m&&\u001b[39m\u001b[35m2\u001b[39m\u001b[33m!==\u001b[39mt)}\u001b[33m,\u001b[39moe\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[36mreturn\u001b[39m\u001b[33m!\u001b[39misNaN(parseFloat(e))\u001b[33m&&\u001b[39misFinite(e)}\u001b[33m,\u001b[39mae\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(e){\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me\u001b[33m.\u001b[39mparents(\u001b[32m\".mCSB_container\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m[e\u001b[33m.\u001b[39moffset()\u001b[33m.\u001b[39mtop\u001b[33m-\u001b[39mt\u001b[33m.\u001b[39moffset()\u001b[33m.\u001b[39mtop\u001b[33m,\u001b[39me\u001b[33m.\u001b[39moffset()\u001b[33m.\u001b[39mleft\u001b[33m-\u001b[39mt\u001b[33m.\u001b[39moffset()\u001b[33m.\u001b[39mleft]}\u001b[33m,\u001b[39mne\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(){\u001b[36mfunction\u001b[39m e(){\u001b[36mvar\u001b[39m e\u001b[33m=\u001b[39m[\u001b[32m\"webkit\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"moz\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"ms\"\u001b[39m\u001b[33m,\u001b[39m\u001b[32m\"o\"\u001b[39m]\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(\u001b[32m\"hidden\"\u001b[39m\u001b[36min\u001b[39m document)\u001b[36mreturn\u001b[39m\u001b[32m\"hidden\"\u001b[39m\u001b[33m;\u001b[39m\u001b[36mfor\u001b[39m(\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m;\u001b[39mt\u001b[33m<\u001b[39m\u001b[33me\u001b[39m\u001b[33m.\u001b[39mlength\u001b[33m;\u001b[39mt\u001b[33m++\u001b[39m)\u001b[36mif\u001b[39m(e[t]\u001b[33m+\u001b[39m\u001b[32m\"Hidden\"\u001b[39m\u001b[36min\u001b[39m document)\u001b[36mreturn\u001b[39m e[t]\u001b[33m+\u001b[39m\u001b[32m\"Hidden\"\u001b[39m\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m \u001b[36mnull\u001b[39m}\u001b[36mvar\u001b[39m t\u001b[33m=\u001b[39me()\u001b[33m;\u001b[39m\u001b[36mreturn\u001b[39m t\u001b[33m?\u001b[39mdocument[t]\u001b[33m:\u001b[39m\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m}\u001b[33m;\u001b[39me\u001b[33m.\u001b[39mfn[o]\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mreturn\u001b[39m u[t]\u001b[33m?\u001b[39mu[t]\u001b[33m.\u001b[39mapply(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[33mArray\u001b[39m\u001b[33m.\u001b[39mprototype\u001b[33m.\u001b[39mslice\u001b[33m.\u001b[39mcall(arguments\u001b[33m,\u001b[39m\u001b[35m1\u001b[39m))\u001b[33m:\u001b[39m\u001b[32m\"object\"\u001b[39m\u001b[33m!=\u001b[39m\u001b[36mtypeof\u001b[39m t\u001b[33m&&\u001b[39mt\u001b[33m?\u001b[39m\u001b[36mvoid\u001b[39m e\u001b[33m.\u001b[39merror(\u001b[32m\"Method \"\u001b[39m\u001b[33m+\u001b[39mt\u001b[33m+\u001b[39m\u001b[32m\" does not exist\"\u001b[39m)\u001b[33m:\u001b[39mu\u001b[33m.\u001b[39minit\u001b[33m.\u001b[39mapply(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39marguments)}\u001b[33m,\u001b[39me[o]\u001b[33m=\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mreturn\u001b[39m u[t]\u001b[33m?\u001b[39mu[t]\u001b[33m.\u001b[39mapply(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39m\u001b[33mArray\u001b[39m\u001b[33m.\u001b[39mprototype\u001b[33m.\u001b[39mslice\u001b[33m.\u001b[39mcall(arguments\u001b[33m,\u001b[39m\u001b[35m1\u001b[39m))\u001b[33m:\u001b[39m\u001b[32m\"object\"\u001b[39m\u001b[33m!=\u001b[39m\u001b[36mtypeof\u001b[39m t\u001b[33m&&\u001b[39mt\u001b[33m?\u001b[39m\u001b[36mvoid\u001b[39m e\u001b[33m.\u001b[39merror(\u001b[32m\"Method \"\u001b[39m\u001b[33m+\u001b[39mt\u001b[33m+\u001b[39m\u001b[32m\" does not exist\"\u001b[39m)\u001b[33m:\u001b[39mu\u001b[33m.\u001b[39minit\u001b[33m.\u001b[39mapply(\u001b[36mthis\u001b[39m\u001b[33m,\u001b[39marguments)}\u001b[33m,\u001b[39me[o]\u001b[33m.\u001b[39mdefaults\u001b[33m=\u001b[39mi\u001b[33m,\u001b[39mwindow[o]\u001b[33m=\u001b[39m\u001b[33m!\u001b[39m\u001b[35m0\u001b[39m\u001b[33m,\u001b[39me(window)\u001b[33m.\u001b[39mbind(\u001b[32m\"load\"\u001b[39m\u001b[33m,\u001b[39m\u001b[36mfunction\u001b[39m(){e(n)[o]()\u001b[33m,\u001b[39me\u001b[33m.\u001b[39mextend(e\u001b[33m.\u001b[39mexpr[\u001b[32m\":\"\u001b[39m]\u001b[33m,\u001b[39m{mcsInView\u001b[33m:\u001b[39me\u001b[33m.\u001b[39mexpr[\u001b[32m\":\"\u001b[39m]\u001b[33m.\u001b[39mmcsInView\u001b[33m||\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m,\u001b[39ma\u001b[33m,\u001b[39mn\u001b[33m=\u001b[39me(t)\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39mn\u001b[33m.\u001b[39mparents(\u001b[32m\".mCSB_container\"\u001b[39m)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(i\u001b[33m.\u001b[39mlength)\u001b[36mreturn\u001b[39m o\u001b[33m=\u001b[39mi\u001b[33m.\u001b[39mparent()\u001b[33m,\u001b[39ma\u001b[33m=\u001b[39m[i[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop\u001b[33m,\u001b[39mi[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft]\u001b[33m,\u001b[39ma[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39mae(n)[\u001b[35m0\u001b[39m]\u001b[33m>=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39ma[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39mae(n)[\u001b[35m0\u001b[39m]\u001b[33m<\u001b[39m\u001b[33mo\u001b[39m\u001b[33m.\u001b[39mheight()\u001b[33m-\u001b[39mn\u001b[33m.\u001b[39mouterHeight(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m&&\u001b[39ma[\u001b[35m1\u001b[39m]\u001b[33m+\u001b[39mae(n)[\u001b[35m1\u001b[39m]\u001b[33m>=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39ma[\u001b[35m1\u001b[39m]\u001b[33m+\u001b[39mae(n)[\u001b[35m1\u001b[39m]\u001b[33m<\u001b[39m\u001b[33mo\u001b[39m\u001b[33m.\u001b[39mwidth()\u001b[33m-\u001b[39mn\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)}\u001b[33m,\u001b[39mmcsInSight\u001b[33m:\u001b[39me\u001b[33m.\u001b[39mexpr[\u001b[32m\":\"\u001b[39m]\u001b[33m.\u001b[39mmcsInSight\u001b[33m||\u001b[39m\u001b[36mfunction\u001b[39m(t\u001b[33m,\u001b[39mo\u001b[33m,\u001b[39ma){\u001b[36mvar\u001b[39m n\u001b[33m,\u001b[39mi\u001b[33m,\u001b[39mr\u001b[33m,\u001b[39ml\u001b[33m,\u001b[39ms\u001b[33m=\u001b[39me(t)\u001b[33m,\u001b[39mc\u001b[33m=\u001b[39ms\u001b[33m.\u001b[39mparents(\u001b[32m\".mCSB_container\"\u001b[39m)\u001b[33m,\u001b[39md\u001b[33m=\u001b[39m\u001b[32m\"exact\"\u001b[39m\u001b[33m===\u001b[39ma[\u001b[35m3\u001b[39m]\u001b[33m?\u001b[39m[[\u001b[35m1\u001b[39m\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39m[\u001b[35m1\u001b[39m\u001b[33m,\u001b[39m\u001b[35m0\u001b[39m]]\u001b[33m:\u001b[39m[[\u001b[35m.9\u001b[39m\u001b[33m,\u001b[39m\u001b[35m.1\u001b[39m]\u001b[33m,\u001b[39m[\u001b[35m.6\u001b[39m\u001b[33m,\u001b[39m\u001b[35m.4\u001b[39m]]\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(c\u001b[33m.\u001b[39mlength)\u001b[36mreturn\u001b[39m n\u001b[33m=\u001b[39m[s\u001b[33m.\u001b[39mouterHeight(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)\u001b[33m,\u001b[39ms\u001b[33m.\u001b[39mouterWidth(\u001b[33m!\u001b[39m\u001b[35m1\u001b[39m)]\u001b[33m,\u001b[39mr\u001b[33m=\u001b[39m[c[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetTop\u001b[33m+\u001b[39mae(s)[\u001b[35m0\u001b[39m]\u001b[33m,\u001b[39mc[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetLeft\u001b[33m+\u001b[39mae(s)[\u001b[35m1\u001b[39m]]\u001b[33m,\u001b[39mi\u001b[33m=\u001b[39m[c\u001b[33m.\u001b[39mparent()[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetHeight\u001b[33m,\u001b[39mc\u001b[33m.\u001b[39mparent()[\u001b[35m0\u001b[39m]\u001b[33m.\u001b[39moffsetWidth]\u001b[33m,\u001b[39ml\u001b[33m=\u001b[39m[n[\u001b[35m0\u001b[39m]\u001b[33m<\u001b[39m\u001b[33mi\u001b[39m[\u001b[35m0\u001b[39m]\u001b[33m?\u001b[39md[\u001b[35m0\u001b[39m]\u001b[33m:\u001b[39md[\u001b[35m1\u001b[39m]\u001b[33m,\u001b[39mn[\u001b[35m1\u001b[39m]\u001b[33m<\u001b[39m\u001b[33mi\u001b[39m[\u001b[35m1\u001b[39m]\u001b[33m?\u001b[39md[\u001b[35m0\u001b[39m]\u001b[33m:\u001b[39md[\u001b[35m1\u001b[39m]]\u001b[33m,\u001b[39mr[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39mi[\u001b[35m0\u001b[39m]\u001b[33m*\u001b[39ml[\u001b[35m0\u001b[39m][\u001b[35m0\u001b[39m]\u001b[33m<\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39mr[\u001b[35m0\u001b[39m]\u001b[33m+\u001b[39mn[\u001b[35m0\u001b[39m]\u001b[33m-\u001b[39mi[\u001b[35m0\u001b[39m]\u001b[33m*\u001b[39ml[\u001b[35m0\u001b[39m][\u001b[35m1\u001b[39m]\u001b[33m>=\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39mr[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39mi[\u001b[35m1\u001b[39m]\u001b[33m*\u001b[39ml[\u001b[35m1\u001b[39m][\u001b[35m0\u001b[39m]\u001b[33m<\u001b[39m\u001b[35m0\u001b[39m\u001b[33m&&\u001b[39mr[\u001b[35m1\u001b[39m]\u001b[33m+\u001b[39mn[\u001b[35m1\u001b[39m]\u001b[33m-\u001b[39mi[\u001b[35m1\u001b[39m]\u001b[33m*\u001b[39ml[\u001b[35m1\u001b[39m][\u001b[35m1\u001b[39m]\u001b[33m>=\u001b[39m\u001b[35m0\u001b[39m}\u001b[33m,\u001b[39mmcsOverflow\u001b[33m:\u001b[39me\u001b[33m.\u001b[39mexpr[\u001b[32m\":\"\u001b[39m]\u001b[33m.\u001b[39mmcsOverflow\u001b[33m||\u001b[39m\u001b[36mfunction\u001b[39m(t){\u001b[36mvar\u001b[39m o\u001b[33m=\u001b[39me(t)\u001b[33m.\u001b[39mdata(a)\u001b[33m;\u001b[39m\u001b[36mif\u001b[39m(o)\u001b[36mreturn\u001b[39m o\u001b[33m.\u001b[39moverflowed[\u001b[35m0\u001b[39m]\u001b[33m||\u001b[39mo\u001b[33m.\u001b[39moverflowed[\u001b[35m1\u001b[39m]}})})})})\u001b[33m;\u001b[39m\u001b[0m\n    at Parser._raise (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:742:17)\n    at Parser.raiseWithData (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:735:17)\n    at Parser.raise (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:729:17)\n    at Parser.unexpected (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:8779:16)\n    at Parser.expect (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:8765:28)\n    at Parser.parseConditional (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:9462:12)\n    at Parser.parseMaybeConditional (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:9454:17)\n    at Parser.parseMaybeAssign (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:9402:21)\n    at Parser.parseVar (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11763:26)\n    at Parser.parseVarStatement (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11572:10)\n    at Parser.parseStatementContent (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11171:21)\n    at Parser.parseStatement (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11104:17)\n    at Parser.parseBlockOrModuleBlockBody (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11679:25)\n    at Parser.parseBlockBody (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11665:10)\n    at Parser.parseBlock (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11649:10)\n    at Parser.parseFunctionBody (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:10656:24)\n    at Parser.parseFunctionBodyAndFinish (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:10639:10)\n    at /home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11819:12\n    at Parser.withTopicForbiddingContext (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:10979:14)\n    at Parser.parseFunction (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11818:10)\n    at Parser.parseFunctionExpression (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:10115:17)\n    at Parser.parseExprAtom (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:10023:21)\n    at Parser.parseExprSubscripts (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:9624:23)\n    at Parser.parseMaybeUnary (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:9604:21)\n    at Parser.parseMaybeUnary (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:9584:28)\n    at Parser.parseExprOps (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:9474:23)\n    at Parser.parseMaybeConditional (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:9447:23)\n    at Parser.parseMaybeAssign (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:9402:21)\n    at Parser.parseExpression (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:9354:23)\n    at Parser.parseStatementContent (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11233:23)\n    at Parser.parseStatement (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11104:17)\n    at Parser.parseBlockOrModuleBlockBody (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11679:25)\n    at Parser.parseBlockBody (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11665:10)\n    at Parser.parseBlock (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:11649:10)\n    at Parser.parseFunctionBody (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:10656:24)\n    at Parser.parseFunctionBodyAndFinish (/home/casudin/www/micsimbawa/node_modules/@babel/parser/lib/index.js:10639:10)");

/***/ }),

/***/ 0:
/*!**************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** multi ./public/assets/js/libs/jquery-3.1.1.min.js ./public/bootstrap/js/popper.min.js ./public/bootstrap/js/bootstrap.min.js ./public/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js ./public/assets/js/app.js ./public/alert/dist/sweetalert2.all.min.js ***!
  \**************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/casudin/www/micsimbawa/public/assets/js/libs/jquery-3.1.1.min.js */"./public/assets/js/libs/jquery-3.1.1.min.js");
__webpack_require__(/*! /home/casudin/www/micsimbawa/public/bootstrap/js/popper.min.js */"./public/bootstrap/js/popper.min.js");
__webpack_require__(/*! /home/casudin/www/micsimbawa/public/bootstrap/js/bootstrap.min.js */"./public/bootstrap/js/bootstrap.min.js");
__webpack_require__(/*! /home/casudin/www/micsimbawa/public/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js */"./public/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js");
__webpack_require__(/*! /home/casudin/www/micsimbawa/public/assets/js/app.js */"./public/assets/js/app.js");
module.exports = __webpack_require__(/*! /home/casudin/www/micsimbawa/public/alert/dist/sweetalert2.all.min.js */"./public/alert/dist/sweetalert2.all.min.js");


/***/ })

/******/ });