<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbJadwalUjianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_jadwal_ujian', function (Blueprint $table) {
            $table->id();
            $table->string('mkKode', 10)->index('mkKode');
            $table->integer('ujianId')->index('ujianId');
            $table->integer('kuota');
            $table->dateTime('start');
            $table->dateTime('finish');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_jadwal_ujian');
    }
}
