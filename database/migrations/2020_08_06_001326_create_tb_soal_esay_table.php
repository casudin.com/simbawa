<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbSoalEsayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_soal_esay', function (Blueprint $table) {
            $table->id();
            $table->integer('jadwal_id')->index('jadwal_id');
            $table->string('keterangan');
            $table->text('pertanyaan');
            $table->integer('poin')->default(0);
            $table->string('user', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_soal_esay');
    }
}
