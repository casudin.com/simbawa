<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbJawabanPilihanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_jawaban_pilihan', function (Blueprint $table) {
            $table->id();
            $table->integer('soal_id')->index('soal_id');
            $table->integer('idp')->index('idp');
            $table->text('pertanyaan');
            $table->string('jawaban')->nullable();
            $table->string('jawaban_benar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_jawaban_pilihan');
    }
}
