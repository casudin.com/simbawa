<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    <div class="modal-body">
        <div class="form-row mb-4">


            <div class="form-group col-md-6">
                <label for="inputPassword4">Nama / Keterangan</label>
                <textarea type="text" class="form-control-rounded form-control input" id="name"
                    placeholder="Nama / Keterangan" name="name"></textarea>

            </div>
            <div class="form-group col-md-3" data-role="datepicker">
                <label for="inputEmail4">Tanggal Mulai</label>
                <input type="text" class="form-control-rounded form-control" name="start" required>
            </div>
            <div class="form-group col-md-3" data-role="datepicker">
                <label for="inputEmail4">Tanggal Berakhir</label>
                <input type="text" class="form-control-rounded form-control" name="finish" required>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Submit</button>
        </div>
    </div>
</form>
<script>
    $('#mhs').select2({
    placeholder: 'Cari...',
    //minimumInputLength:1,
    allowClear: true,
    ajax: {
      url: '{{route('all.mhs')}}',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nama+' '+item.nim,
              id: item.nim
            }
          })
        };
      },
      cache: true
    }
  });
    $("#mhs").change(function(e){
    var nim= $("#mhs").val();
    
    $.ajax({
        type: "GET",
        url: "{{route('mhs.info')}}",
        data: {nim:nim},
        dataType: "JSON",
        success: function (response) {
            $("#nim").val('');
            $("#idp").val('');
            $("#nama").val('');
            $("#nim").val(response.nim);
            $("#idp").val(response.idp);
            $("#nama").val(response.nama);
            //summary();

        }
    });

  });
    $("#form_data").submit(function (e) {
        e.preventDefault();
       $('#myModal').modal('hide')
        var form=$("#form_data").serialize();
       $.ajax({
           type: "POST",
           url: "{{route('data-ujian.store')}}",
           data: form,
           dataType: "JSON",
           success: function (response) {
            if (response.status!="error") {
                Toast.fire({
                icon: 'success',
                title:"Success",
                text: response.msg
                });
                myTable.ajax.reload();
            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title:"Error!",
                text: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: "Error!",
                text: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>