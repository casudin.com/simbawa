@extends('layouts.master')
@section('title', trans('Jawaban Peserta'))
@section('parentPageTitle', 'Ujian')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/custom_dt_customer.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.min.css')}}">
<link href="{{ asset('assets/css/design-css/design.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/date_time_pickers/custom_datetimepicker_style/custom_datetimepicker.css') }}"
    rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/components/custom-page_style_datetime.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-select/bootstrap-select.min.css') }}">
<style>
    .special {font-weight: bold !important;color: #fff !important;background: #e7515a !important;text-transform: uppercase;}
    .bootstrap-select.btn-group .dropdown-menu a.dropdown-item span.dropdown-item-inner { color: #171820; }
    .dropdown-item:active { background-color: #f1f3f9; }
    .dropdown-menu.select-dropdown .dropdown-item:focus, .dropdown-menu.select-dropdown .dropdown-item:hover { background-color: #e6e3fe; }
    .dropdown-item.active { background-color: #f1f3f0; }
    .row [class*="col-"] .widget .widget-header h4 { color: #6156ce; }
    .btn-group.bootstrap-select.dropup:focus { outline: none; }
</style>
@endsection
@section('content')
 

<div class="row layout-spacing">
    <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
   
            <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4 style-1">
                    <form action="" id="cari">
                        <div class="row layout-spacing">
                            <div class="col-lg-6">
                                <div class="form-group mb-4">
                                    <label for="exampleFormControlSelect1">Ujian</label>
                                    <select class="select2 form-control-rounded form-control" id="ujian" name="ujian">
                                        <option value="" ></option>
                                        @forelse ($ujian as $key)
                                            <option value="{{ $key->id }}">{{ $key->name }}</option>
                                        @empty
                                            
                                        @endforelse
                                        
                                    </select>
                                </div>
                                <div class="form-group mb-4">
                                    <label for="exampleFormControlSelect1">Matakuliah</label>
                                    <select class="select2 form-control-rounded form-control" id="mk" name="mk">
                                        <option value="" ></option>
                                        @forelse ($pelajaran as $key)
                                            <option value="{{ $key->kd_ak }}">{{ $key->kd_ak }} - {{ $key->nama }}</option>
                                        @empty
                                            
                                        @endforelse
                                        
                                    </select>
                                </div>
                            </div>
                             <div class="col-lg-6">
                                <div class="form-group mb-4">
                                    <label for="exampleFormControlSelect1">Nama Mahasiswa</label>
                                    <input class="form-control-rounded form-control input" id="exampleFormControlSelect1" name="mhs">
                                        
                                </div>
                            </div>
                        </div>
                        <div class="widget-content widget-content-area text-right">
            
                            <button type="submit" class="btn btn-secondary mb-4 mr-2">Filter <i class="flaticon-search-1  ml-1"></i></button>
            
                        </div>
                        </form>
                   
                    <table id="userss" class="table style-1  table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> No. </th>
                                <th>{{trans('Ujian')}}</th>
                                <th>Matakuliah</th>
                                <th>{{trans('Keterangan')}}</th>
                                <th>Mahasiswa</th>
                                <th>Soal Ujian</th>
                                <th>Jawaban Ujian</th>
                                <th>Nilai</th>
                                {{-- <th>Action</th> --}}


                            </tr>
                        </thead>
                         
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>

<script src="{{ asset('plugins/date_time_pickers/bootstrap_date_range_picker/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/design-js/design.js') }}">
</script>
<script src="{{ asset('plugins/date_time_pickers/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}">
        <script src="{{asset ('plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>

</script>
@endsection
@section('modal')
<div class="modal fade animated zoomInUp custo-zoomInUp" role="dialog" aria-labelledby="myExtraLargeModalLabel"
    aria-hidden="true" id="myModal">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
    $("#myModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $(".select2").select2({
        placeholder: "Pilih",
        allowClear: true,
    });


var myTable=$('#userss').DataTable({
        processing: true,
        serverSide: true,
        pageLength:10,
        searching:false,
        //lengthChange:false,
       // lengthMenu:[[5,10,15,20,50,100],[5,10,15,20,50,100]],
        lengthMenu: [ 10, 10, 20, 50, 100 ],
        language: {
                paginate: {
                  previous: "<i class='flaticon-arrow-left-1'></i>",
                  next: "<i class='flaticon-arrow-right'></i>"
                },
                //info: "Showing page _PAGE_ of _PAGES_"
            },
        ajax: {
            url: '{{route("list.jawaban.mhs")}}',
            data: function (d) {
                d.mhs = $('input[name=mhs]').val();
                d.mk = $('#mk').val();
                d.ujian = $('#ujian').val();
            },
            method: 'POST',
            
        },
        columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'nama_ujian', name: 'name',searchable:false,orderable: false},
            {data: 'pelajaran', name: 'nama',searchable:false,orderable: false},
            {data: 'keterangan', name: 'keterangan',searchable:false,orderable: false},
            {data: 'peserta', name: 'nama',searchable:false,orderable: false},
            {data: 'soal', name: 'soal',searchable:false,orderable: false},
            {data: 'jawaban', name: 'jawaban',searchable:false,orderable: false},
            {data: 'nilai', name: 'nilai',searchable:false,orderable: false},
            //{data: 'action', name: 'action',orderable: false, searchable: false}
        ],
         
    });
    $('#cari').on('submit', function(e) {
        myTable.draw();
        e.preventDefault();
    });
function hapus(id){

    swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    }).then((result) => {
  if (result.value) {
      $.ajax({
          type: "POST",
          method:"DELETE",
          url: "{{url('ujian/peserta/')}}/"+id,
          data: {id:id},
          dataType: "JSON",
          success: function (response) {
              if (response.status!='error') {
                  swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                ''
                );
                Toast.fire({
                icon: 'success',
                title: 'Your User has been deleted.'
                });
                myTable.ajax.reload();
              } else {

              }
          }
      });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary User is safe :)',
      'error'
    );
  }
})

 }
 function destroy(){

    swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    }).then((result) => {
    if (result.value) {
      $.ajax({
          type: "POST",
         
          url: "{{route("destoyed.peserta")}}",
         
          dataType: "JSON",
          success: function (response) {
              if (response.status!='error') {
                  swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                ''
                );
                Toast.fire({
                icon: 'success',
                title: 'Your User has been Destoyed.'
                });
                myTable.ajax.reload();
              } else {

              }
          }
      });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary User is safe :)',
      'error'
    );
  }
})

 }

</script>
@endsection