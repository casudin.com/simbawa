@extends('layouts.master')
@section('title', trans('Peserta'))
@section('parentPageTitle', 'Ujian')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/custom_dt_customer.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.min.css')}}">
<link href="{{ asset('assets/css/design-css/design.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/date_time_pickers/custom_datetimepicker_style/custom_datetimepicker.css') }}"
    rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/components/custom-page_style_datetime.css') }}">
@endsection
@section('content')

<div class="row layout-spacing">
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <br>
                <div class="row">
                    <div class="col-md-5 col-sm-5 mb-4 mb-sm-0">
                        <h6 class="mt-3 mb-0"> </h6>
                    </div>
                    <div class="col-md-12 col-sm-12 text-sm-right">
                        <a href="{{ route("template") }}" class="btn btn-primary mb-4 mr-2"><i
                                class="flaticon-plus-file -m-3mr-1"></i>
                            Template</a>
                        <a href="{{ route('template.upload') }}" data-toggle="modal" data-target="#myModal"
                            class="btn btn-warning mb-4 mr-2"><i class="flaticon-cloud-upload mr-1"></i>
                            Upload</a>
                        <a href="{{ route('peserta.create') }}" data-toggle="modal" data-target="#myModal" type="button"
                            class="btn btn-success mb-4 mr-2"><i class="flaticon-plus-2 mr-1"></i>
                            Tambah</a>
                        <button type="button" class="btn btn-danger mb-4 mr-2" onclick="destroy()"><i
                                class="flaticon-delete  mr-1"></i>
                            Kosongkan Data </button>
                        <a href="{{ route('export.peserta') }}" class="btn btn-warning mb-4 mr-2"><i
                                class="flaticon-download-1 mr-1"></i>
                            Export Peserta</a>
                            <span><p><code>* Perhatian! : Jika Data sebelumnya sudah ada, maka secara otomatis akan memperbarui data tersebut</code></p></span>
                    </div>

                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4 style-1">

                    <table id="userss" class="table style-1  table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> No. </th>
                                <th>{{trans('NIM')}}</th>

                                <th>Nama</th>
                                <th>{{trans('Token')}}</th>
                                <th>Expired Token</th>
                                <th>Dibuat</th>
                                <th>Action</th>


                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{ asset('plugins/date_time_pickers/bootstrap_date_range_picker/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/design-js/design.js') }}">
</script>
<script src="{{ asset('plugins/date_time_pickers/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}">
</script>
@endsection
@section('modal')
<div class="modal fade animated zoomInUp custo-zoomInUp" role="dialog" aria-labelledby="myExtraLargeModalLabel"
    aria-hidden="true" id="myModal">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
    $("#myModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    // $(".select2").select2({
    //     placeholder: 'Cari',
    //     allowClear: true
    // });

var myTable=$('#userss').DataTable({
        processing: true,
        serverSide: true,
        pageLength:5,
       // lengthMenu:[[5,10,15,20,50,100],[5,10,15,20,50,100]],
        lengthMenu: [ 5, 10, 20, 50, 100 ],
        language: {
                paginate: {
                  previous: "<i class='flaticon-arrow-left-1'></i>",
                  next: "<i class='flaticon-arrow-right'></i>"
                },
                //info: "Showing page _PAGE_ of _PAGES_"
            },
        ajax: {
            url: '{{route("api.peserta")}}',
            method: 'POST'
        },
        columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'nim', name: 'nim'},
            {data: 'nama', name: 'nama'},
            {data: 'token', name: 'token',searchable:false},
            {data: 'expired', name: 'expired',searchable:false},
            {data: 'created_at', name: 'created_at',searchable:false},
            {data: 'action', name: 'action',orderable: false, searchable: false}
        ],

    });
function hapus(id){

    swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    }).then((result) => {
  if (result.value) {
      $.ajax({
          type: "POST",
          method:"DELETE",
          url: "{{url('ujian/peserta/')}}/"+id,
          data: {id:id},
          dataType: "JSON",
          success: function (response) {
              if (response.status!='error') {
                  swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                ''
                );
                Toast.fire({
                icon: 'success',
                title: 'Your User has been deleted.'
                });
                myTable.ajax.reload();
              } else {

              }
          }
      });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary User is safe :)',
      'error'
    );
  }
})

 }
 function destroy(){

    swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    }).then((result) => {
    if (result.value) {
      $.ajax({
          type: "POST",
         
          url: "{{route("destoyed.peserta")}}",
         
          dataType: "JSON",
          success: function (response) {
              if (response.status!='error') {
                  swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                ''
                );
                Toast.fire({
                icon: 'success',
                title: 'Your User has been Destoyed.'
                });
                myTable.ajax.reload();
              } else {

              }
          }
      });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary User is safe :)',
      'error'
    );
  }
})

 }

</script>
@endsection