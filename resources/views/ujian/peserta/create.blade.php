<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    <div class="modal-body">
        <div class="form-row mb-4">
            <div class="form-group col-md-6">
                <label for="inputPassword4">Pilih Mahasiswa</label>
                <select name="mhs" class="select2 form-control-rounded form-control" id="mhs" style="width: 100%" required>
                    <option value=""></option>

                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4">{{trans('Token')??"Token"}}</label>
                <input type="email" class="form-control-rounded form-control" name="token" placeholder="AUTO" disabled>
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">NIM</label>
                <input type="text" class="form-control-rounded form-control" id="nim" placeholder="NIM" name="nim"
                    onkeypress="return angka(event)" readonly>
                <input type="hidden" class="form-control-rounded form-control" id="idp" placeholder="IDP" name="idp"
                    onkeypress="return angka(event)" readonly>
                <input type="hidden" name="nama" id="nama">
            </div>
            <div class="form-group col-md-6" data-role="datepicker">
                <label for="inputEmail4">Expired</label>
                <input type="text" class="form-control-rounded form-control" name="expired" required>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Submit</button>
        </div>
    </div>
</form>
<script>
    $('#mhs').select2({
    placeholder: 'Cari...',
    //minimumInputLength:1,
    allowClear: true,
    ajax: {
      url: '{{route('all.mhs')}}',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nama+' '+item.nim,
              id: item.nim
            }
          })
        };
      },
      cache: true
    }
  });
    $("#mhs").change(function(e){
    var nim= $("#mhs").val();
    
    $.ajax({
        type: "GET",
        url: "{{route('mhs.info')}}",
        data: {nim:nim},
        dataType: "JSON",
        success: function (response) {
            $("#nim").val('');
            $("#idp").val('');
            $("#nama").val('');
            $("#nim").val(response.nim);
            $("#idp").val(response.idp);
            $("#nama").val(response.nama);
            //summary();

        }
    });

  });
    $("#form_data").submit(function (e) {
        e.preventDefault();
       $('#myModal').modal('hide')
        var form=$("#form_data").serialize();
       $.ajax({
           type: "POST",
           url: "{{route('peserta.store')}}",
           data: form,
           dataType: "JSON",
           success: function (response) {
            if (response.status!="error") {
                Toast.fire({
                icon: 'success',
                title: response.msg
                });
                myTable.ajax.reload();
            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>