<table>
    <thead>
        <tr>
            <th>No</th>
            <th>IDP</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Token</th>
            <th>Token Expired</th>
        </tr>

    </thead>
    <tbody>
        @php
        $no=1;
        @endphp
        @forelse ($data as $key)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $key->idp }}</td>
            <td>{{ $key->nim }}</td>
            <td>{{ $key->nama }}</td>
            <td>{{ $key->token }}</td>
            <td>{{ $key->expired }}</td>
            @empty
            <p>Tidak Ada Data</p>
        </tr>
        @endforelse
    </tbody>
</table>