<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form Upload Soal</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    <div class="modal-body">
        <div class="form-row mb-4">


            <div class="form-group col-md-6">
                <label for="inputPassword4">Lampiran</label>
                <input type="file" class="form-control-rounded form-control" name="file" required placeholder="Kuota">
                <span style="color: red; font-size: 10pt">File Yang didukung adalah PDF max 5MB</span>
            </div>
            <div class="form-group col-md-6">
                <label for="xx">Mata Kuliah</label>
                <select name="mk" class="select2 form-control-rounded form-control" required>


                    <option value="{{$pelajaran->kd_ak}}" selected>{{ $pelajaran->kd_ak }} - {{$pelajaran->nama}}
                    </option>


                </select>
                <input type="hidden" name="jadwal" value="{{ $jadwal->id }}">

            </div>
            <div class="form-group col-md-6">
                <label for="xx">Data Ujian</label>
                <select name="ujian" class="select2 form-control-rounded form-control" required>

                    <option value="{{$ujian->id}}" selected> {{$ujian->name}}</option>

                </select>

            </div>
            <div class="form-group col-md-6">
                <label for="xx">Link</label>

                <input type="url" class="form-control-rounded form-control" name="link" placeholder="Link External">
                <span style="color: red; font-size: 10pt">Silahkan di isi jika berupa file yang berada di luar Server
                    MIC</span>
            </div>



        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Submit</button>
        </div>
    </div>
</form>
<script>
    $('.select2').select2({
    placeholder: 'Cari...',
    });
    $('.timepicker').datetimepicker({
//          format: 'H:mm',    // use this format if you want the 24hours timepicker
    format: 'H:mm', 
       //use this format if you want the 12hours timpiecker with AM/PM toggle
    icons: {
        time: "now-ui-icons tech_watch-time",
        date: "now-ui-icons ui-1_calendar-60",
        up: "flaticon-arrows-1",
        down: "flaticon-down-arrow",
        previous: 'now-ui-icons arrows-1_minimal-left',
        next: 'now-ui-icons arrows-1_minimal-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
    },

    // inline: true,
    // sideBySide: true
 });

  

    $("#form_data").submit(function (e) {
        e.preventDefault();
       $('#myModal').modal('hide')
       var form= new FormData(this);
       $.ajax({
           type: "POST",
           url: "{{route('soal.store')}}",
           data: form,
         cache: false,
        contentType: false,
        processData: false,
           success: function (response) {
            if (response.status!="error") {
                Toast.fire({
                icon: 'success',
                title:"Success",
                text: response.msg
                });
                myTable.ajax.reload();
            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title:"Error!",
                text: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: "Error!",
                text: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>