<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form Edit</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    @csrf
    <div class="modal-body">
        <div class="form-row mb-4">


            <div class="form-group col-md-6">
                <label for="inputPassword4">Lampiran</label>
                <input type="file" class="form-control-rounded form-control" name="file" required>
                <span style="color: red; font-size: 10pt">File Yang didukung adalah PDF max 5MB</span>
            </div>

            <div class="form-group col-md-6">
                <label for="xx">Link</label>

                <input type="url" class="form-control-rounded form-control" name="link" placeholder="Link External">
                <span style="color: red; font-size: 10pt">Silahkan di isi jika berupa file yang berada di luar Server
                    MIC</span>
            </div>
            @method('PUT')


        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Submit</button>
        </div>
    </div>
</form>
<script>
    $("#form_data").submit(function (e) {
        e.preventDefault();
       $('#myModal').modal('hide')
       var form= new FormData(this);
       $.ajax({
           type: "POST",          
           url: "{{route('soal.update',[$data->id])}}",
           data: form,
          cache: false,
        contentType: false,
        processData: false,
           success: function (response) {
            if (response.status!="error") {
               Toast.fire({
                icon: 'success',
                title: 'Success!',
                text:response.msg
                });
                myTable.ajax.reload();

            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title:"Error!",
                text: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
                myTable.ajax.reload();
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title:"Error!",
                text: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
                myTable.ajax.reload();
           }
       });
    });
</script>