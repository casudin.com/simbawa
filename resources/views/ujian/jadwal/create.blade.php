<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    <div class="modal-body">
        <div class="form-row mb-4">


            <div class="form-group col-md-6">
                <label for="inputPassword4">Nama / Keterangan</label>
                <textarea type="text" class="form-control-rounded form-control input" id="name"
                    placeholder="Nama / Keterangan / Nama Dosen" name="keterangan"></textarea>

            </div>
            <div class="form-group col-md-6">
                <label for="xx">Mata Kuliah</label>
                <select name="mk" class="select2 form-control-rounded form-control" required>
                    <option value=""></option>
                    @foreach ($pelajaran as $key)
                    <option value="{{$key->kd_ak}}">{{ $key->kd_ak }} - {{$key->nama}}</option>

                    @endforeach
                </select>

            </div>
            <div class="form-group col-md-6">
                <label for="xx">Data Ujian</label>
                <select name="ujian" class="select2 form-control-rounded form-control" required>
                    <option value=""></option>
                    @foreach ($ujian as $key)
                    <option value="{{$key->id}}"> {{$key->name}}</option>

                    @endforeach
                </select>

            </div>
            <div class="form-group col-md-3" data-role="datepicker">
                <label for="inputEmail4">Tanggal Mulai</label>
                <input type="text" class="form-control-rounded form-control" name="start" required>
            </div>
            <div class="form-group col-md-3" data-role="datepicker">
                <label for="inputEmail4">Tanggal Berakhir</label>
                <input type="text" class="form-control-rounded form-control" name="finish" required>
            </div>
            <div class="form-group col-md-3">
                <label for="inputEmail4">Jam Mulai</label>
                <input type="text" class="form-control-rounded form-control timepicker" name="time_start" required>
            </div>
            <div class="form-group col-md-3">
                <label for="inputEmail4">Jam Berakhir</label>
                <input type="text" class="form-control-rounded form-control timepicker" name="time_finish" required>
            </div>
            <div class="form-group col-md-3">
                <label for="inputEmail4">Kuota</label>
                <input type="text" class="form-control-rounded form-control" name="kuota" required placeholder="Kuota"
                    onkeypress="return angka(event)">
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Submit</button>
        </div>
    </div>
</form>
<script>
    $('.select2').select2({
    placeholder: 'Cari...',
    });
    $('.timepicker').datetimepicker({
//          format: 'H:mm',    // use this format if you want the 24hours timepicker
    format: 'H:mm', 
       //use this format if you want the 12hours timpiecker with AM/PM toggle
    icons: {
        time: "now-ui-icons tech_watch-time",
        date: "now-ui-icons ui-1_calendar-60",
        up: "flaticon-arrows-1",
        down: "flaticon-down-arrow",
        previous: 'now-ui-icons arrows-1_minimal-left',
        next: 'now-ui-icons arrows-1_minimal-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
    },

    // inline: true,
    // sideBySide: true
 });

  

    $("#form_data").submit(function (e) {
        e.preventDefault();
       $('#myModal').modal('hide')
        var form=$("#form_data").serialize();
       $.ajax({
           type: "POST",
           url: "{{route('jadwal.store')}}",
           data: form,
           dataType: "JSON",
           success: function (response) {
            if (response.status!="error") {
                Toast.fire({
                icon: 'success',
                title:"Success",
                text: response.msg
                });
                myTable.ajax.reload();
            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title:"Error!",
                text: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: "Error!",
                text: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>