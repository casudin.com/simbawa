@extends('layouts.master')
@section('title', trans('Jadwal Ujian'))
@section('parentPageTitle', 'Ujian')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/custom_dt_customer.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.min.css')}}">
<link href="{{ asset('assets/css/design-css/design.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/date_time_pickers/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}"
    rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/date_time_pickers/custom_datetimepicker_style/custom_datetimepicker.css') }}"
    rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/components/custom-page_style_datetime.css') }}">

@endsection
@section('content')

<div class="row layout-spacing">
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <br>
                <div class="row">
                    <div class="col-md-5 col-sm-5 mb-4 mb-sm-0">
                        <h6 class="mt-3 mb-0"> </h6>
                    </div>
                    <div class="col-md-12 col-sm-12 text-sm-center">

                        <a href="{{ route('jadwal.create') }}" data-toggle="modal" data-target="#myModal" type="button"
                            class="btn btn-success mb-4 mr-2"><i class="flaticon-plus-2 mr-1"></i>
                            Tambah</a>
                        <a href="{{ route('template.jadwal') }}" type="button" class="btn btn-dark mb-4 mr-2"><i
                                class="flaticon-file mr-1"></i>
                            Template</a>
                        <a href="{{ route('template.ujian.upload') }}" data-toggle="modal" data-target="#myModal"
                            type="button" class="btn btn-warning mb-4 mr-2"><i class="flaticon-upload-file mr-1"></i>
                            Upload Jadwal</a>
                        <a href="#" onclick="destroy()" class="btn btn-danger mb-4 mr-2"><i
                                class="flaticon-delete-6 mr-1"></i>
                            Hapus Semua</a>

                    </div>

                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4 style-1">

                    <table id="userss" class="table style-1  table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> No. </th>
                                <th>Ujian</th>
                                <th>Matakuliah</th>
                                <th>Kuota</th>
                                <th>{{trans('Mulai')}}</th>
                                <th>Berakhir</th>
                                <th>Keterangan</th>
                                <th>Action</th>


                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{ asset('plugins/date_time_pickers/bootstrap_date_range_picker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/date_time_pickers/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}">
</script>
<script src="{{ asset('assets/js/design-js/design.js') }}">
</script>
<script src="{{ asset('plugins/date_time_pickers/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}">
</script>

@endsection
@section('modal')
<div class="modal fade animated zoomInUp custo-zoomInUp" role="dialog" aria-labelledby="myExtraLargeModalLabel"
    aria-hidden="true" id="myModal">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
    $("#myModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    // $(".select2").select2({
    //     placeholder: 'Cari',
    //     allowClear: true
    // });

var myTable=$('#userss').DataTable({
        processing: true,
        serverSide: true,
        pageLength:5,
       // lengthMenu:[[5,10,15,20,50,100],[5,10,15,20,50,100]],
        lengthMenu: [ 5, 10, 20, 50, 100 ],
        language: {
                paginate: {
                  previous: "<i class='flaticon-arrow-left-1'></i>",
                  next: "<i class='flaticon-arrow-right'></i>"
                },
                //info: "Showing page _PAGE_ of _PAGES_"
            },
        ajax: {
            url: '{{route("jadwal.list.ujian")}}',
            method: 'POST'
        },
        columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'nama', name: 'nama'},
            {data: 'kuota', name: 'kuota'},
            {data: 'start', name: 'start'},
            {data: 'finish', name: 'finish',searchable:false},
            {data: 'keterangan', name: 'keterangan',searchable:false},           
            {data: 'action', name: 'action',orderable: false, searchable: false}
        ],

    });
function hapus(id){

    swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    }).then((result) => {
  if (result.value) {
      $.ajax({
          type: "POST",
          method:"DELETE",
          url: "{{url('ujian/jadwal/')}}/"+id,
          data: {id:id},
          dataType: "JSON",
          success: function (response) {
              if (response.status!='error') {
                  swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                ''
                );
                Toast.fire({
                icon: 'success',
                title: 'Your User has been deleted.'
                });
                myTable.ajax.reload();
              } else {

              }
          }
      });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary User is safe :)',
      'error'
    );
  }
})

 }
 function destroy(){

    swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    }).then((result) => {
    if (result.value) {
      $.ajax({
          type: "POST",
         
          url: "{{route("destoyed.jadwal")}}",
         
          dataType: "JSON",
          success: function (response) {
              if (response.status!='error') {
                  swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                ''
                );
                Toast.fire({
                icon: 'success',
                title: 'Success.',
                text:response.msg,
                });
                myTable.ajax.reload();
              } else {

              }
          }
      });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary User is safe :)',
      'error'
    );
  }
})

 }

</script>
@endsection