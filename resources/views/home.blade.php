@extends('layouts.master')
@section('title', 'Home')
@section('parentPageTitle', 'App')
@section('css')

<link href="{{asset('plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.cs')}}s" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/charts/chartist/chartist.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/accounting-dashboard/style.css') }}" rel="stylesheet" type="text/css" />
<style>
    #simplecolumnchartdiv, #columnlinemixchartdiv, #clusteredbarchartdiv, #cylinderchartdiv { width: 100%; height: 500px; font-size: 11px; }
    #clusteredbarchartdiv .amcharts-chart-div a {
        color: #fff !important;
        font-size: 11px !important;
        opacity: 1 !important;
        top: 40px !important; 
    }
    .tablesaw-bar .new-control { line-height: 1.7 }
</style>
<link href="{{ asset('plugins/table/tablesaw/css/tablesaw.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/table/tablesaw/css/style.css') }}" rel="stylesheet" type="text/css">
<script src="{{ asset('plugins/table/tablesaw/js/tablesaw.js') }}"></script>
@endsection
@section('content')
@if ($mhs)
<div class="row layout-spacing accounts-widgets">
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mb-xl-0 mb-4">
        <div class="widget-content widget-content-area br-4 accounts-income">
            <div class="row">
                <div class="col-md-6 col-6">
                    <h6 class="value">{{ number_format($biaya->biaya) ??"-"}}</h6>
                    <p class="mt-2">Total Biaya</p>
                </div>
                <div class="col-md-6 col-6 text-right">
                    <i class="flaticon-currency"></i>
                </div>
            </div>
            <div class="progress br-30 mb-0 mt-5">
                <div class="progress-bar bg-primary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mb-xl-0 mb-4">
        <div class="widget-content widget-content-area br-4 accounts-cogs">
            <div class="row">
                <div class="col-md-6 col-6">
                    <h6 class="value">{{ number_format($bayar->pembayaran)??"-" }}</h6>
                    <p class="mt-2">Dibayarkan</p>
                </div>
                <div class="col-md-6 col-6 text-right">
                    <i class="flaticon-dollar-coin"></i>
                </div>
            </div>
            <div class="progress br-30 mb-0 mt-5">
                <div class="progress-bar bg-success" role="progressbar" style="width: {{  $bayar->pembayaran/$biaya->biaya*100 }}%" aria-valuenow="{{ $bayar->pembayaran/$biaya->biaya*100 }}" aria-valuemin="0" aria-valuemax="100">{{ round( $bayar->pembayaran/$biaya->biaya*100) }}%</div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mb-sm-0 mb-4">
        <div class="widget-content widget-content-area br-4 accounts-profit">
            <div class="row">
                @php
                    $tunggakan=$biaya->biaya-$bayar->pembayaran;
                @endphp
                <div class="col-md-6 col-6">
                    <h6 class="value">{{ number_format($biaya->biaya-$bayar->pembayaran) }}</h6>
                    <p class="mt-2">Tunggakan</p>
                </div>
                <div class="col-md-6 col-6 text-right">
                    <i class="flaticon-money"></i>
                </div>
            </div>
            <div class="progress br-30 mb-0 mt-5">
                <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $tunggakan/$biaya->biaya*100  }}%" aria-valuenow="{{ $tunggakan/$biaya->biaya*100 }}" aria-valuemin="0" aria-valuemax="100">{{ round($tunggakan/$biaya->biaya*100) }}%</div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12">
        <div class="widget-content widget-content-area br-4 accounts-expenses">
            <div class="row">
                <div class="col-md-6 col-6">
                    <h6 class="value">0</h6>
                    <p class="mt-2">Desposit</p>
                </div>
                <div class="col-md-6 col-6 text-right">
            <i class="flaticon-wallet"></i>
                </div>
            </div>
            <div class="progress br-30 mb-0 mt-5">
                <div class="progress-bar bg-danger" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
    </div>
</div>

@endif

@if ($mhs)
<div class="row layout-spacing">
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Analyst IPK By Period</h4>
                    </div>
                                                        
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="row">
                    <div class="col-lg-12 mb-4">
                        <div id="simplecolumnchartdiv"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row layout-spacing">
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Nilai</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="row">
                    <div class="col-lg-12 mb-4 column-mode">
                        <div class="table-responsive">
                            <table class="tablesaw  table-striped tablesaw_columns_titles" data-tablesaw-mode="swipe" data-tablesaw-mode-switch data-tablesaw-minimap>
                                <thead>
                                    <tr>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Pelajaran</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="3">Kehadiran</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Tugas</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1"><abbr title="Rotten Tomato Rating">UTS</abbr></th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">UAS</th>
                                         
                                    </tr>
                                </thead>
                                <tbody>
                                     
                                    @forelse ($nilai as $key=>$val)
                                        <tr>
                                       <td> {{ $val['pelajaran'] }}</td>
                                       <td>
                                          @foreach ($val['poin'] as $item)
                                              @if ($item->jenis=='AB')
                                                  {{ $item->nilai }}
                                              @endif
                                          @endforeach
                                       </td>
                                       <td>
                                        @foreach ($val['poin'] as $item)
                                        @if ($item->jenis=='TG')
                                            {{ $item->nilai }}
                                        @endif
                                    @endforeach
                                       </td>
                                       <td>
                                        @foreach ($val['poin'] as $item)
                                        @if ($item->jenis=='UT')
                                            {{ $item->nilai }}
                                        @endif
                                    @endforeach
                                       </td>
                                       <td>
                                        @foreach ($val['poin'] as $item)
                                        @if ($item->jenis=='UA')
                                            {{ $item->nilai }}
                                        @endif
                                    @endforeach
                                       </td>
                                        @empty
                                        
                                        @endforelse
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@else
<div class="row layout-spacing">
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Jadwal Mengajar Saya</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="row">
                    <div class="col-lg-12 mb-4 column-mode">
                        <div class="table-responsive">
                            <table class="tablesaw  table-striped tablesaw_columns_titles" data-tablesaw-mode="swipe" data-tablesaw-mode-switch data-tablesaw-minimap>
                                <thead>
                                    <tr>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Pelajaran</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="3">Hari</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Ruangan</th>
                                         
                                         
                                    </tr>
                                </thead>
                                <tbody>
                                     @php
                                     $aka=DB::connection('loker')->table('periode_akademik')->where('aktif', 1)->first();
                                         $data=DB::connection('loker')->table('kelas')
                                         ->join('dt_hari','dt_hari.kd_hari','=','kelas.hari')
                                         ->join('pengajar','pengajar.idp','=','kelas.dosen')
                                         ->join('pelajaran','pelajaran.kd_ak','=','kelas.kd_mk')
                                         ->select([
                                             'pelajaran.nama as pelajaran',
                                             'dt_hari.hari',
                                             'kelas.jam_mulai',
                                             'kelas.jam_akhir',
                                             'kelas.kd_ruang'
                                         ])->where('kelas.dosen',useridp())
                                         ->where('kelas.kd_pr_akademik',$aka->pr_akademik)->get();
                                     @endphp
                                        <tr>
                                    @forelse ($data as $key)
                                            <td>{{ $key->pelajaran }}</td>
                                            <td>{{ $key->hari }}  {{ $key->jam_mulai }} - {{ $key->jam_akhir }}</td>
                                            <td>{{ $key->kd_ruang }}</td>
                                        </tr>
                                        @empty
                                        
                                        @endforelse
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
@section('js')
<script src="{{asset('plugins/charts/chartist/chartist.js')}}"></script>
<script src="{{asset('plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.min.js')}}"></script>
<script src="{{asset('plugins/maps/vector/jvector/worldmap_script/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('plugins/calendar/pignose/moment.latest.min.js')}}"></script>
<script src="{{asset('plugins/calendar/pignose/pignose.calendar.js')}}"></script>
<script src="{{asset('plugins/progressbar/progressbar.min.js')}}"></script>

<script src="{{ asset('plugins/charts/amcharts/amcharts.js') }}"></script>
<script src="{{ asset('plugins/charts/amcharts/pie.js') }}"></script>
<script src="{{ asset('plugins/charts/amcharts/serial.js') }}"></script>
<script src="{{ asset('plugins/charts/amcharts/light.js') }}"></script>
<script src="{{ asset('plugins/charts/amcharts/responsive.min.js') }}"></script>
 
@endsection
@section('modal')
@endsection
@section('script')
@if ($mhs)
<script type="text/javascript">
    AmCharts.makeChart( "simplecolumnchartdiv", {
       "type": "serial",
       "theme": "light",
       "dataProvider": @json($khs),
       "gridAboveGraphs": true,
       "startDuration": 1,
       "graphs": [ {
         "balloonText": "[[category]]: <b>[[value]]</b>",
         "fillAlphas": 0.7,
         "lineAlpha": 0.2,
         "type": "column",
         "valueField": "ipk",
         "fillColors":"#5247bd",
         "lineColor":"#5247bd"
       } ],
       "chartCursor": {
         "categoryBalloonEnabled": false,
         "cursorAlpha": 0,
         "zoomable": true
       },
       
       "categoryField": "periode",
       
       "categoryAxis": {
           "autoRotateAngle": 45,
            "autoRotateCount": 5
       },
       "export": {
         "enabled": true
       },
       "responsive": {
     "enabled": true,
     "rules": [
       // at 767px to below
       {
         "maxWidth": 767,
         "overrides": {
           "legend":{
             "position":"bottom",
           }
         }
       }

     ]
   },
   
   });

</script>
@endif

@endsection