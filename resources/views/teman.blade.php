@extends('layouts.master')
@section('title', 'Teman')
@section('parentPageTitle', 'Layanan')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/custom_dt_customer.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.min.css')}}">
@endsection
@section('content')
<div class="row">

    <div class="col-xl-12 col-lg-12 col-12 layout-spacing">
        <div class="col-lg-12">
            <div class="statbox widget box box-shadow">
                <div class="widget-header widget-heading">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Jalin Silaturahim Bersama</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="statbox widget box box-shadow">
            <div class="widget-content widget-content-area rounded-pills">

                <ul class="nav nav-pills mb-4  nav-fill" id="rounded-pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  btn-rounded active" id="rounded-pills-home-tab" data-toggle="pill"
                            href="#rounded-pills-home" role="tab" aria-controls="rounded-pills-home"
                            aria-selected="true"><i class="flaticon-home-fill-1"></i> Sekampung</a>
                    </li>
                    <li class="nav-item ml-1">
                        <a class="nav-link  btn-rounded" id="rounded-pills-profile-tab" data-toggle="pill"
                            href="#rounded-pills-profile" role="tab" aria-controls="rounded-pills-profile"
                            aria-selected="false"><i class="flaticon-user-7"></i> Seangkatan</a>
                    </li>
                    <li class="nav-item ml-1">
                        <a class="nav-link  btn-rounded" id="ronded-pills-contact-tab" data-toggle="pill"
                            href="#rounded-pills-contact" role="tab" aria-controls="rounded-pills-contact"
                            aria-selected="false"><i class="flaticon-telephone"></i> Sejurusan</a>
                    </li>


                </ul>
                <div class="tab-content" id="rounded-pills-tabContent">
                    <div class="tab-pane fade show active" id="rounded-pills-home" role="tabpanel"
                        aria-labelledby="rounded-pills-home-tab">
                        <div class="col-lg-12">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5 mb-4 mb-sm-0">
                                            <h6 class="mt-3 mb-0"> </h6>
                                        </div>
                                        <div class="col-md-7 col-sm-7 text-sm-right">
                                            <span style="color: red; font-size: 10pt"> Sumber Data <a
                                                    href="http://api.mic.ac.id" target="_blank">api.mic.ac.id</a></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="widget-content widget-content-area">
                                    <div class="table-responsive mb-4 style-1">

                                        <table id="userss" class="table style-1  table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th> No. </th>
                                                    <th>{{trans('NIM')}}</th>
                                                    <th>Nama</th>
                                                    <th>Alamat</th>
                                                    <th>{{trans('Angkatan')}}</th>
                                                    <th>Program</th>
                                                    <th>Email</th>
                                                    <th>Kontak</th>
                                                    <th>Status</th>


                                                </tr>
                                            </thead>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="rounded-pills-profile" role="tabpanel"
                        aria-labelledby="rounded-pills-profile-tab">
                        <div class="col-lg-12">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5 mb-4 mb-sm-0">
                                            <h6 class="mt-3 mb-0"> </h6>
                                        </div>
                                        <div class="col-md-7 col-sm-7 text-sm-right">
                                            <span style="color: red; font-size: 10pt"> Sumber Data <a
                                                    href="http://api.mic.ac.id" target="_blank">api.mic.ac.id</a></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="widget-content widget-content-area">
                                    <div class="table-responsive mb-4 style-1">

                                        <table id="angkatan" class="table style-1  table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th> No. </th>
                                                    <th>{{trans('NIM')}}</th>
                                                    <th>Nama</th>
                                                    <th>Alamat</th>
                                                    <th>{{trans('Angkatan')}}</th>
                                                    <th>Program</th>
                                                    <th>Email</th>
                                                    <th>Kontak</th>
                                                    <th>Status</th>


                                                </tr>
                                            </thead>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="rounded-pills-contact" role="tabpanel"
                        aria-labelledby="ronded-pills-contact-tab">
                        <div class="col-lg-12">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5 mb-4 mb-sm-0">
                                            <h6 class="mt-3 mb-0"> </h6>
                                        </div>
                                        <div class="col-md-7 col-sm-7 text-sm-right">
                                            <span style="color: red; font-size: 10pt"> Sumber Data <a
                                                    href="http://api.mic.ac.id" target="_blank">api.mic.ac.id</a></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="widget-content widget-content-area">
                                    <div class="table-responsive mb-4 style-1">

                                        <table id="jurusan" class="table style-1  table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th> No. </th>
                                                    <th>{{trans('NIM')}}</th>
                                                    <th>Nama</th>
                                                    <th>Alamat</th>
                                                    <th>{{trans('Angkatan')}}</th>
                                                    <th>Program</th>
                                                    <th>Email</th>
                                                    <th>Kontak</th>
                                                    <th>Status</th>


                                                </tr>
                                            </thead>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
@endsection
@section('modal')
@endsection
@section('script')
<script>
    var sekampung=$('#userss').DataTable({
        processing: true,
        serverSide: true,
        pageLength:5,
       // lengthMenu:[[5,10,15,20,50,100],[5,10,15,20,50,100]],
        lengthMenu: [ 5, 10, 20, 50, 100 ],
        language: {
                paginate: {
                  previous: "<i class='flaticon-arrow-left-1'></i>",
                  next: "<i class='flaticon-arrow-right'></i>"
                },
                //info: "Showing page _PAGE_ of _PAGES_"
            },
        ajax: {
            url: '{{route("teman.sekampung")}}',
            method: 'POST'
        },
        columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'nim', name: 'nim'},
            {data: 'nama', name: 'nama'},
            {data: 'tmp_lahir', name: 'tmp_lahir'},
            {data: 'angkatan', name: 'angkatan',searchable:false},
            {data: 'program_alias', name: 'program_alias',searchable:false},
            {data: 'email1', name: 'email1',searchable:false},
            {data: 'hp', name: 'hp',searchable:false},
            {data: 'status', name: 'status',searchable:false},
        ],

    });

     var angkatan=$('#angkatan').DataTable({
        processing: true,
        serverSide: true,
        pageLength:5,
       // lengthMenu:[[5,10,15,20,50,100],[5,10,15,20,50,100]],
        lengthMenu: [ 5, 10, 20, 50, 100 ],
        language: {
                paginate: {
                  previous: "<i class='flaticon-arrow-left-1'></i>",
                  next: "<i class='flaticon-arrow-right'></i>"
                },
                //info: "Showing page _PAGE_ of _PAGES_"
            },
        ajax: {
            url: '{{route("teman.seangkatan")}}',
            method: 'POST'
        },
        columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'nim', name: 'nim'},
            {data: 'nama', name: 'nama'},
            {data: 'tmp_lahir', name: 'tmp_lahir'},
            {data: 'angkatan', name: 'angkatan',searchable:false},
            {data: 'program_alias', name: 'program_alias',searchable:false},
            {data: 'email1', name: 'email1',searchable:false},
            {data: 'hp', name: 'hp',searchable:false},
            {data: 'status', name: 'status',searchable:false},
        ],

    });
    var jurusan=$('#jurusan').DataTable({
        processing: true,
        serverSide: true,
        pageLength:5,
       // lengthMenu:[[5,10,15,20,50,100],[5,10,15,20,50,100]],
        lengthMenu: [ 5, 10, 20, 50, 100 ],
        language: {
                paginate: {
                  previous: "<i class='flaticon-arrow-left-1'></i>",
                  next: "<i class='flaticon-arrow-right'></i>"
                },
                //info: "Showing page _PAGE_ of _PAGES_"
            },
        ajax: {
            url: '{{route("teman.sejurusan")}}',
            method: 'POST'
        },
        columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'nim', name: 'nim'},
            {data: 'nama', name: 'nama'},
            {data: 'tmp_lahir', name: 'tmp_lahir'},
            {data: 'angkatan', name: 'angkatan',searchable:false},
            {data: 'program_alias', name: 'program_alias',searchable:false},
            {data: 'email1', name: 'email1',searchable:false},
            {data: 'hp', name: 'hp',searchable:false},
            {data: 'status', name: 'status',searchable:false},
        ],

    });
</script>
@endsection