@component('mail::message')
# Dear, {{$content['name']}}

<p>Terdapat Transaksi Baru Pada aplikasi dengan rincian berikut :</p>
<p>&nbsp;</p>
<p>Nama Mahasiswa : &nbsp;{{$content['mhs']}}</p>
<p>Keterangan : &nbsp; {{ $content['jumlah'] }}</p>
<p>&nbsp;</p>
<p>Silahkan Login ke Aplikasi Umtuk memulai Proses Validasi.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

@component('mail::button', ['url' => route('login')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent