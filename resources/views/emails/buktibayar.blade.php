@component('mail::message')
# Dear, {{$content['name']}}

<table style="margin-left: auto; margin-right: auto;">
    <tbody>
        <tr align="center">
            <td>MEDIA INFORMATIKA CENDEKIA</td>
        </tr>
        <tr align="center">
            <td>BUKTI PEMBAYARAN</td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<table style="margin-left: auto; margin-right: auto; width: 244px; height: 287px;">
    <tbody>
        <tr style="height: 18px;">
            <td style="width: 87px; height: 18px;">{{$content['kwitansi']}}</td>
            <td style="width: 104px; height: 18px;">&nbsp;</td>
        </tr>
        <tr style="height: 18px;">
            <td style="width: 87px; height: 18px;">&nbsp;</td>
            <td style="width: 104px; height: 18px;">&nbsp;</td>
        </tr>
        <tr style="height: 18px;">
            <td style="width: 87px; height: 18px;">&nbsp;</td>
        </tr>
        <tr style="height: 18px;">
            <td style="width: 87px; height: 18px;">NIM &nbsp;&nbsp;</td>
            <td style="width: 104px; height: 18px;">:&nbsp; &nbsp;{{$content['nim']}}</td>
        </tr>
        <tr style="height: 18px;">
            <td style="width: 87px; height: 18px;">Nama &nbsp;&nbsp;</td>
            <td style="width: 104px; height: 18px;">:&nbsp; &nbsp;{{$content['name']}}</td>
        </tr>
        <tr style="height: 18px;">
            <td style="width: 87px; height: 18px;">Prog/Jur &nbsp;&nbsp;</td>
            <td style="width: 104px; height: 18px;">:&nbsp; {{$content['jurusan']}}</td>
        </tr>
        <tr style="height: 16px;">
            <td style="width: 87px; height: 16px;">
                <hr />
            </td>
        </tr>
    </tbody>
    <tbody>
        <tr style="height: 16px;">
            <td style="width: 87px; height: 16px;">
                <hr />
            </td>
        </tr>
        <tr style="height: 18px;">
            <td style="width: 191px; height: 18px;" colspan="2">Total : {{number_format($content['total'])}}</td>
            <td style="width: 10px; height: 18px;">&nbsp;</td>
        </tr>
        <tr style="height: 33px;">
            <td style="width: 191px; height: 33px;" colspan="2">&nbsp;</td>
            <td style="width: 10px; height: 33px;">&nbsp;</td>
        </tr>
    </tbody>
</table>

Thanks,<br>
{{ config('app.name') }}
@endcomponent