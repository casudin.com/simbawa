@component('mail::message')
# Dear, {{$content['name']}}

<p>Update Terbaru mengenai Status Pembayaran Anda Sebagai Berikut :</p>
<p>Status : {{$content['status']}}</p>
<p>Keterangan : {{$content['ket']}}</p>
<p>&nbsp;</p>

@component('mail::button', ['url' => ''])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent