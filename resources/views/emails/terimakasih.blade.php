@component('mail::message')
# Dear, {{$content['name']}}


<p>Terimkasih telah melaukan proses upload pembayaran.</p>
<p>saat ini bukti pembayaran anda sedang kami proses verifikasi.</p>
<p>Anda dapat mengcek status pembayaran anda via Aplikasi.</p>
<p>&nbsp;</p>

@component('mail::button', ['url' => route('login')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent