@component('mail::message')
# Dear, {{$content['name']}}

Ada Pesan Baru Dari : {{$content['mhs']}} .<br/>
Pesan : <p>{{$content['pesan']}}</p>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
