<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    <div class="modal-body">
        <div class="form-row mb-4">
            <div class="form-group col-md-6">
                <label for="inputEmail4">{{trans('Buti Trasnfer')}}</label>
                <input type="file" class="form-control-rounded form-control" name="foto">
                <span style="color: red">Max 600kb. format PNG,JPG,JPEG</span>
            </div>

            <div class="form-group col-md-12">
                <label for="inputPassword4">Jumlah Bayar</label>
                <input type="text" name="jumlah" class="form-control-rounded form-control"
                    onkeypress="return angka(event)">
            </div>
            <div class="form-group col-md-12">
                <label for="inputPassword4">Keterangan</label>
                <textarea type="text" class="form-control-rounded form-control" id="inputPassword4"
                    placeholder="Keterangan" name="keterangan"></textarea>
            </div>

        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" id="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Submit</button>
            <button class="btn btn-primary btn-rounded mt-3 mb-3" id="loading" disabled>
                <span class="spinner-border spinner-border-sm"></span>Loading
            </button>
        </div>
    </div>
</form>
<script>
    $(".select2").select2({
    placeholder: "Pilih",
    allowClear: true
    });
    $("#loading").hide();
    $("#submit").show();
    $("#form_data").submit(function (e) {
        e.preventDefault();
        $("#submit").hide();
        $("#loading").show();
       
        //var form=$("#form_data").serialize();
        var form = new FormData(this);
       $.ajax({
           type: "POST",
           url: "{{route('pembayaran.store')}}",
           data: form,
          cache: false,
        contentType: false,
        processData: false,
           success: function (response) {
            if (response.status!="error") {
                $('#myModal').modal('hide');
                Toast.fire({
                icon: 'success',
                title: response.msg
                });
                myTable.ajax.reload();
            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
                $("#loading").hide();
                $("#submit").show();
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>