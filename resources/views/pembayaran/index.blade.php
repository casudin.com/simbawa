@extends('layouts.master')
@section('title', trans('Pembayaran'))
@section('parentPageTitle', 'Layanan')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/custom_dt_customer.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('box/jquery.fancybox.min.css')}}">
<link href="{{ asset('assets/css/design-css/design.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/date_time_pickers/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}"
    rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/timepicker/jquery.timepicker.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/date_time_pickers/custom_datetimepicker_style/custom_datetimepicker.css') }}"
    rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/components/custom-page_style_datetime.css') }}">
@endsection
@section('content')

<div class="row layout-spacing">
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <br>
                <div class="row">
                    <div class="col-md-5 col-sm-5 mb-4 mb-sm-0">
                        <h6 class="mt-3 mb-0"> </h6>
                    </div>
                    <div class="col-md-7 col-sm-7 text-sm-right">


                        {{-- <a href="{{route('full')}}" data-toggle="modal" data-target="#myModal"
                        class="btn btn-warning mb-4 mr-2 btn-rounded">{{trans('Entri Baru')}} <i
                            class="flaticon-circle-plus ml-1"></i></a> --}}
                        <a href="{{route('pembayaran.create')}}" data-toggle="modal" data-target="#myModal"
                            class="btn btn-warning mb-4 mr-2 btn-rounded">{{trans('Entri Baru')}} <i
                                class="flaticon-circle-plus ml-1"></i></a>

                    </div>

                </div>
                <div class="widget-content widget-content-area">
                    <div class="table-responsive mb-4 style-1">

                        <table id="userss" class="table style-1  table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th> No </th>
                                    <th> Nama </th>
                                    <th>{{trans('Tanggal')}}</th>
                                    <th>{{trans('Bukti')}}</th>
                                    <th>{{trans('Keterangan')}}</th>
                                    <th>Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('box/jquery.fancybox.min.js')}}"></script>
    <script src="{{ asset('assets/js/design-js/design.js') }}">
    </script>
    <script src="{{ asset('plugins/date_time_pickers/bootstrap_date_range_picker/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/date_time_pickers/bootstrap_date_range_picker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/timepicker/jquery.timepicker.js') }}"></script>
    <script src="{{ asset('plugins/date_time_pickers/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}">
    </script>
    @endsection
    @section('modal')


    <div class="modal hide fade animated zoomInUp custo-zoomInUp" role="dialog" aria-labelledby="myExtraLargeModalLabel"
        aria-hidden="true" id="myModal">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>


    @endsection
    @section('script')
    <script type="text/javascript">
        $("#myModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    // $(".select2").select2({
    //     placeholder: 'Cari',
    //     allowClear: true
    // });

var myTable=$('#userss').DataTable({
        processing: true,
        serverSide: true,
        pageLength:5,
        ordering:false,
       // lengthMenu:[[5,10,15,20,50,100],[5,10,15,20,50,100]],
        lengthMenu: [ 5, 10, 20, 50, 100 ],
        language: {
                paginate: {
                  previous: "<i class='flaticon-arrow-left-1'></i>",
                  next: "<i class='flaticon-arrow-right'></i>"
                },
                //info: "Showing page _PAGE_ of _PAGES_"
            },
        ajax: {
            url: '{{route("api.pembayaran")}}',
            method: 'POST',
           
        },
        columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'nama', name: 'tb_pengguna.nama'},
            {data: 'tgl', name: 'tb_buktitrf.tgl',searchable: false},
            {data: 'foto', name: 'tb_buktitrf.foto',searchable: false},
            {data: 'ket', name: 'tb_buktitrf.ket',searchable: false},
            {data: 'status', name: 'tb_buktitrf.status'},          
            {data: 'action', name: 'action',orderable: false, searchable: false}
        ],
   
    });
    
function hapus(id){

    swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true,
    showLoaderOnConfirm: true,
     
    }).then((result) => {
      
  if (result.value) {
      swal.fire({
        title: "Loading...",
        text: "Please wait",
        imageUrl: "{{ asset("loading_bar.gif") }}",
        button: false,
        closeOnClickOutside: false,
        closeOnEsc: false,
        showCloseButton: false,
        showConfirmButton: false
    });
      $.ajax({
          type: "POST",          
          url: "{{ route("hapus.pembayaran") }}",
          data: {id:id},
          dataType: "JSON",
          success: function (response) {
              if (response.status!='error') {
                  swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                ''
                );
                Toast.fire({
                icon: 'success',
                title: response.msg,
                });
                myTable.ajax.reload();
              } else {

              }
          }
      });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary User is safe :)',
      'error'
    );
  }
})

 }
 function unduh(id)
 {
     swal.fire({
        title: "Loading...",
        text: "Please wait Token is"+' '+btoa(id),
        imageUrl: "{{ asset("loading_bar.gif") }}",
        timerProgressBar: true,
        showCloseButton: false,
        showConfirmButton: false
        });
        $.ajax({
            type: "GET",
            url: "{{ route('donlot.bukti') }}",
            data: {token:btoa(id)},
            dataType: "JSON",
            success: function (response) {
                var url="{{ url('layanan/donlot/bukti?token=') }}"+btoa(id);
                 if (response.status=='success') {                    
                    window.location.replace(url);
                   Swal.close()
                 }else{
                     Swal.fire({
                    title: 'Error!',
                    text: response.msg,
                    icon: 'error',
                    confirmButtonText: 'close'
                    })
                 }
                
            }
        });
    
 }
    $('[data-fancybox]').fancybox({
   afterLoad: function(current) {
           if (current.index === current.group.length - 1) {
               current.arrows = false;
           }
      }
    });
    
    </script>
    @endsection