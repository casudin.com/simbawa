<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    <div class="modal-body">
        <div class="form-row mb-4">

            <div class="form-group col-md-6">
                <label for="inputEmail4">Keterangan</label>
                <textarea type="text" class="form-control-rounded form-control" name="ket" required></textarea>
            </div>


            <div class="form-group col-md-6">
                <label for="inputPassword4">Status</label>
                <select name="status" class="select2 form-control-rounded form-control">
                    <option value="Proses" {{$data->status=="Proses"?"selected":""}}>Proses</option>
                    <option value="Selesai" {{$data->status=="Selesai"?"selected":""}}>Selesai</option>
                    <option value="Dikembalikan" {{$data->status=="Dikembalikan"?"selected":""}}>Dikembalikan</option>

                </select>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" id="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Update</button>
            <button class="btn btn-primary btn-rounded mt-3 mb-3" id="loading" disabled>
                <span class="spinner-border spinner-border-sm"></span>Loading
            </button>
        </div>
    </div>
</form>
<script>
    $(".select2").select2({
    placeholder: "Pilih",
    allowClear: true
    });
    $("#loading").hide();
    $("#form_data").submit(function (e) {
        e.preventDefault();
      $("#loading").hide();
    $("#submit").show();
        var form=$("#form_data").serialize();
       $.ajax({
           type: "POST",
           method:"PUT",
           url: "{{route('pembayaran.update',[$data->id])}}",
           data: form,
           dataType: "JSON",
           success: function (response) {
            if (response.status!="error") {
                $('#myModal').modal('hide')
               Toast.fire({
                icon: 'success',
                title: response.msg
                });
                myTable.ajax.reload();

            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
                $("#loading").hide();
                $("#submit").show();
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>