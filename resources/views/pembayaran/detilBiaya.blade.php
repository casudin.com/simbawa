<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Rincian Biaya</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<div>
    <div class="modal-body">

        <div class="table-responsive">
            <h3>Biaya - Biaya</h3>
            <table class="table table-bordered mb-4">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Potongan %</th>
                        <th class="text-center">Potongan Rp</th>
                        <th class="text-center">Total</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        @php
                        $no=1;
                        $totalBiaya=0;
                        @endphp
                        @forelse ($data as $key)
                        <td>{{ $no++ }}</td>
                        <td>{{ $key->rekening }}</td>
                        <td>{{ number_format($key->harga) }}</td>
                        <td>{{ $key->pot_persen }} %</td>
                        <td>{{ number_format($key->pot_total) }}</td>
                        <td>{{ number_format($key->jumlah_biaya) }}</td>
                    </tr>
                    @php
                    $totalBiaya+=$key->jumlah_biaya;
                    @endphp
                    @empty

                    @endforelse
                </tbody>
                <tfoot>
                    <tr>

                        <td colspan="4"> </td>
                        <td colspan="">Total : </td>
                        <td colspan=""><b>{{ number_format($totalBiaya) }} </b></td>
                    </tr>

                </tfoot>
            </table>
            <h3>Transaksi Pembayaran</h3>
            <table class="table table-bordered mb-4">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kwitansi</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Potongan %</th>
                        <th class="text-center">Potongan Rp</th>
                        <th class="text-center">Total Bayar</th>
                        <th class="text-center">Petugas</th>
                        <th>Tanggal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        @php
                        $no=1;
                        $total=0;
                        $harga=0;
                        $pot=0;
                        $pot1=0;

                        @endphp
                        @forelse ($histori as $key)
                        <td>{{ $no++ }}</td>
                        <td>{{ $key->no_kwitansi }}</td>
                        <td>{{ $key->rekening }}</td>
                        <td>{{ number_format($key->harga) }}</td>
                        <td>{{ $key->pot_persen }} %</td>
                        <td>{{ number_format($key->potongan) }}</td>
                        <td>{{ number_format($key->jumlah) }} - {{ $key->bank }}</td>
                        <td>{{ $key->nama }}</td>
                        <td>{{ tgl_indo($key->tgl_trans_bank) }}</td>
                    </tr>
                    @php
                    $harga+=$key->harga;
                    $pot+=$key->pot_persen;
                    $pot1+=$key->potongan;
                    $total+=$key->jumlah;
                    @endphp
                    @empty

                    @endforelse
                </tbody>
                <tfoot>
                    <tr>

                        <td colspan="2"> </td>
                        <td colspan="">Total : </td>
                        <td colspan=""><b>{{ number_format($harga) }} </b></td>
                        <td colspan="">-</td>
                        <td colspan=""><b>{{ number_format($pot1) }} </b></td>
                        <td colspan=""><b>{{ number_format($total) }} </b></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>Sisa Tagihan :</td>
                        <td><b>{{ number_format($totalBiaya-$total) }}</b></td>
                        <td colspan="5"></td>
                    </tr>
                </tfoot>

            </table>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>

        </div>
    </div>
</div>