<tfoot class="pull-right">
    <tr>
        <th colspan="5" style="text-align: right;">Total Biaya :</th>
        <td>{{ number_format($data[0]->totalbiaya) }}</td>
    </tr>
    <tr>
        <th colspan="5" style="text-align: right;">Total Pembayaran :</th>
        <td>{{ number_format($totalbayar) }}</td>
    </tr>
    <tr>
        <th colspan="5" style="text-align: right;">Kekurangan :</th>
        <td>{{ number_format($data[0]->totalbiaya-$totalbayar) }}</td>
    </tr>
    <tr>
        <th colspan="5" style="text-align: right;">Deposit :</th>
        <td>{{ number_format($deposit[0]->deposit) }}</td>
    </tr>
</tfoot>