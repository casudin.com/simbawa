<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form Kirim Bukti Pembayaran</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    <div class="modal-body">
        <div class="form-row mb-4">


            <div class="form-group col-md-12">
                <label for="inputPassword4">Email</label>
                <input type="email" name="email" class="form-control-rounded form-control" autocomplete="off">
            </div>
            <input type="hidden" name="no_kwitansi" value="{{ request()->no_kwitansi }}">

        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Kirim</button>
        </div>
    </div>
</form>
<script>
    $(".select2").select2({
    placeholder: "Pilih",
    allowClear: true
    });

    $("#form_data").submit(function (e) {
        e.preventDefault();
       $('#myModal').modal('hide')
        //var form=$("#form_data").serialize();
        var form=$("#form_data").serialize();
       $.ajax({
           type: "POST",
           url: "{{route('kirim.bukti')}}",
           data: form,
        
        dataType: "JSON",
           success: function (response) {
            if (response.status!="error") {
                Toast.fire({
                icon: 'success',
                title: response.msg
                });
                myTable.ajax.reload();
            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>