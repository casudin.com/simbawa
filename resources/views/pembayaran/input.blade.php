<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form {{ $no }}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    <div class="modal-body">
        <div class="form-row mb-4">
            <div class="form-group col-md-6">
                <label for="inputEmail4">{{trans('Mahasiswa')}}</label>
                <input type="text" class="form-control-rounded form-control" name="mahasiswa"
                    value="{{$mhs->nama}} - {{ $mhs->nim }}" readonly>
                <input type="hidden" name="idp" value="{{ $mhs->idp }}">
                <input type="hidden" name="kwitansi" value="{{ $no }}">
            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4">Jumlah</label>
                <input type="text" class="form-control-rounded form-control" name="jumlah" placeholder="Rp "
                    onkeypress="return angka(event)">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Bank Trasfer</label>
                <select name="bank" class="select2 form-control-rounded form-control" style="width: 100%">
                    <option value=""></option>
                    @foreach ($bank as $key)
                    <option value="{{$key->kd_bank}}">{{ $key->kd_bank }} - {{$key->bank}}</option>

                    @endforeach
                </select>

            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4">Terbilang</label>
                <textarea type="text" class="form-control-rounded form-control" name="terbilang" required></textarea>
            </div>
        </div>
        <div class="form-row mb-4">
            <div class="form-group col-md-6">
                <label for="inputPassword4">Untuk Pembayaran</label>
                <select name="rekening" class="select2 form-control-rounded form-control" style="width: 100%">
                    <option value=""></option>
                    @foreach ($rekening as $key)
                    <option value="{{$key->kd_rekening}}">{{ $key->kd_rekening }} - {{$key->rekening}}</option>

                    @endforeach
                </select>
                <span style="color: red; font-size: 10pt">(Cek dulu transaksi yang sudah diinput di SIMKU untuk memilih
                    Paket Biaya dalam
                    transaksi ini)</span>
            </div>
            <div class="form-group col-md-6" data-role="datepicker">
                <label for="inputEmail4">Tanggal Trasfer</label>
                <input type="text" class="form-control-rounded form-control" name="tgl_trasfer" required>
            </div>
        </div>



        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Submit</button>
        </div>
    </div>
</form>
<script>
    $(".select2").select2({
    placeholder: "Pilih",
    allowClear: true
    });

    $("#form_data").submit(function (e) {
        e.preventDefault();
       $('#myModal').modal('hide')
        var form=$("#form_data").serialize();
       $.ajax({
           type: "POST",
           url: "{{route('proses.bayar',[$data->id])}}",
           data: form,
           dataType: "JSON",
           success: function (response) {
            if (response.status!="error") {
                Toast.fire({
                icon: 'success',
                title: response.msg
                });
                myTable.ajax.reload();
            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>