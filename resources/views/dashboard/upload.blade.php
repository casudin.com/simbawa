<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form id="form_data">
    @csrf
    <div class="modal-body">
        
        <div class="form-row mb-4">
            <div class="form-group col-md-6">
                <label for="inputEmail4">{{trans('File')}}</label>
                <input type="file" class="form-control-rounded form-control" name="file"
                    placeholder="{{trans('nama')}}" required>
                    <span style="color:red">File yang didukung hanya PDF max 1MB</span>
            </div>


 <input type="hidden" name="id" value="{{ request()->key }}">
 <input type="hidden" name="token" value="{{ request()->token }}">
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Submit</button>
        </div>
    </div>
</form>
<script>
  

    $("#form_data").submit(function (e) {
        e.preventDefault();
       $('#myModal').modal('hide')
        var form= new FormData(this);
       $.ajax({
           type: "POST",
           url: "{{route('soal.file.siswa',[request()->token])}}",
           data: form,
          cache: false,
        contentType: false,
        processData: false,
           success: function (response) {
            if (response.status!="error") {
                Toast.fire({
                icon: 'success',
                title:'Success!',              
                text:response.msg
                });
                //myTable.ajax.reload();
            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title:"Error",
                text: response.msg.toString(),
                showConfirmButton: false,
                showCloseButton:true
                })
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: "Error!",
                text: response.message,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>