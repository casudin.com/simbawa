@extends('layouts.master')
@section('title', 'Dashboard Ujian')
@section('parentPageTitle', 'Ujian')
@section('css')
<link href="{{ asset('assets/css/elements/contact/contact.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="row layout-spacing" id="cancel-row">
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Jadwal Keseluruhan</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="row">

                    <div class="col-md-12">
                        <div class="row">
                            @forelse ($data as $key)
                            @php
                                $enrol=DB::table('tb_enroll')->where('idp',userIdp())->where('jadwal_id',$key->id)->get();
                            @endphp
                            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 mb-4">
                                <div class="contact-1 text-center">

                                    <div class="card-mid-section mb-4">
                                        <div class="usr-profile mx-auto">
                                            <img alt="Mata Kuliah" src="{{ asset('logomic.png') }}"
                                                class="rounded-circle">
                                        </div>
                                        <div class="team-content mb-4">
                                            <h6 class="mt-4 mb-0" style="color: blue">{{ $key->nama }}</h6>
                                            
                                            <p class="mt-2 mb-3">{{ $key->keterangan }} <br>

                                            </p>
                                            <span>
                                                @if ($key->start > now())
                                                Ujian belum dimulai
                                                @elseif ($key->finish < now()) ujian telah usai 
                                                @elseif($key->soal_id!=null) 
                                                <button class="btn btn-gradient-warning btn-rounded mb-4" onclick="enroll('{{ $key->id }}')">Unduh Soal</button>
                                                @else
                                                Soal Belum Tersedia
                                                @endif <br>
                                          @forelse ($enrol as $item)
                                         
                                         @if ($key->finish < now())
                                             -
                                         @else
                                         <button type="button" class="btn btn-gradient-success btn-rounded mb-4" onclick="upload('{{ $item->id }}','{{ $item->token }}')">Upload Jawaban </button>
                                         @endif
                                          

                                          @if ($item->file!=null)
                                          <p>Jawaban Terkirim</p>
                                            @else
                                               <p> Belum Ada Jawaban diupload</p>
                                            @endif
                                          @empty
                                              -
                                          @endforelse

                                           
                                            </span>
                                        
                                        </div>
                                                    </div> <p>
                                                    {{ tgl_indo($key->start) }} {!!
                                                    jam($key->start) !!} <br>
                                                    {{ tgl_indo($key->finish) }} {!! jam($key->finish) !!}
                                                    </p>
                                        </div>
                                    </div>

                                    @empty

                                    @endforelse

                                </div>

                            </div>

                            {{ $data->links() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection
        @section('js')

        @endsection
        @section('modal')
        <div class="modal fade animated zoomInUp custo-zoomInUp" role="dialog" aria-labelledby="myExtraLargeModalLabel"
    aria-hidden="true" id="myModal">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

        @endsection
        @section('script')
        <script>
             $("#myModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
        function enroll(id){
        Swal.fire({
        title: 'Permintaan Token',
        text: 'Silahkan masukan Token Ujian Anda untuk melanjutkan',
        icon: 'warning',
        inputPlaceholder:"Masukan Token Anda disini",
        input: 'number',
        inputAttributes: {
        autocapitalize: 'off',
        required:"true"
        },
        showCancelButton: true,
        confirmButtonText: 'Kirim',
        showLoaderOnConfirm: true,
        preConfirm: (login) => {
        return fetch(`{{ route("cekToken.soal.mhs") }}`,{
        headers: new Headers({
        'Content-Type': 'application/json',
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
        }),
        method: "POST",
        credentials: "same-origin",
        body: JSON.stringify({token:`${login}`,id:id})
        })
        .then(response => {
        if (!response.ok) {
        throw new Error(response.statusText)
        }
        return response.json()
        })
        .catch(error => {
        Swal.showValidationMessage(
        `Request failed: ${error}`
        )
        })
        },
        allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            console.log(result);
            if (result.value.status=="error") {
                Swal.fire({
                icon: 'error',
                title: `${result.value.status}`,
                text:`${result.value.msg}`
                })

               
            }else{
                Swal.fire({
                icon: 'success',
                title: `${result.value.status}`,
                text:'Generate Token Berhasil',
                timer: 3000
                })
              
                window.location.href = "{{ url("dashboard/soal/ujian/") }}"+"/"+`${result.value.token}`+'/'+`${result.value.jadwal}`;
                setTimeout(function(){
                window.location.reload(1);
                }, 4000);
            }
       

        })
    }
    function upload(id,token) {
        Swal.fire({
        icon: 'question',
        title: 'Upload Jawaban',
        footer:'Hanya Mendukung Type PDF Max 1MB',
        showCancelButton: true,
        confirmButtonText: 'Upload',
        input: 'file',
        html:'<input type="hidden" id="swal-input1" class="swal2-input" value="'+id+'">' +'<input  type="hidden" id="swal-input2" class="swal2-input" value="'+token+'">'+'<input type="hidden" id="swal-input3" value="{{ csrf_token() }}">',
        inputAttributes: {
        autocapitalize: 'off',
        required:"true"
        },
        onBeforeOpen: () => {
           
            $(".swal2-file").change(function () {
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
            });
        }
    }).then((file) => {
        console.log(file.value);
       
            var formData = new FormData();
            var file = $('.swal2-file')[0].files[0];
            var id = document.getElementById('swal-input1').value;
            var token=  document.getElementById('swal-input2').value;
            var _token=  document.getElementById('swal-input3').value;
            formData.append("file", file);
            formData.append("id", id);
            formData.append("token", token);
            formData.append("_token", _token);
             formData.getAll("file");
            Swal.showLoading();
            $.ajax({
                
                type: 'POST',
                url: '{{ route('soal.file.siswa') }}',
                data: formData,
                processData: false,
                contentType: false,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                 async: true,
                success: function (response) {
                    if (response.status=='error') {
                    Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error!',
                    text:response.msg,
                    showConfirmButton: true,
                    confirmButtonText: 'Tutup'
                    //timer: 3000
                    })
                    }else{
                     Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    text:"Data Berhasil Di Upload",
                    showConfirmButton: false,
                    timer: 3000
                    })
                    setTimeout(function(){
                    window.location.reload(1);
                    }, 4000);
                    console.log(response);

                    }
                    
                },
                error: function(response) {
                    Swal.fire({
                    title: 'Error!',
                    text: response.msg,
                    icon: 'error',
                    confirmButtonText: 'tutup'
                    })
                }
            })
        
    })
      }
        </script>
        @endsection