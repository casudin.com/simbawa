@extends('layouts.master')
@section('title', trans('user'))
@section('parentPageTitle', 'Admin')
@section('content')
<div>
    <button wire:click="checkout">Checkout</button>

    <div wire:loading.remove>
        Hide Me While Loading...
    </div>
</div>
@endsection
