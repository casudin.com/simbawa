<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex, nofollow">
    <meta name="robots" content="noindex, nofollow">
    <title>{{config('app.name')}} :: Login</title>
    <link rel="icon" type="image/x-icon" href="logomic.png" />


    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700"
        rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->

    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" href="{{ asset('alert/dist/sweetalert2.min.css') }}">
    <link href="{{ asset('assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page"
    data-open="click" data-menu="vertical-menu-modern" data-col="1-column" data-layout="semi-dark-layout">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- login page start -->
                <section id="auth-login" class="row flexbox-container">
                    <div class="col-xl-8 col-11">
                        <div class="card bg-authentication mb-0">
                            <div class="row m-0">
                                <!-- left section-login -->
                                <div class="col-md-6 col-12 px-0">
                                    <div
                                        class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                        <div class="card-header pb-1">
                                            <div class="card-title text-center">
                                                <img src="{{ asset('bg.png') }}" alt="bg"
                                                    class="img img-fluid img-responsive" style="width: 60%">
                                            </div>
                                        </div>
                                        <div class="card-content">
                                            <div class="card-body">


                                                <form class="form-login">
                                                    @csrf
                                                    <div class="form-group mb-50">
                                                        <label class="text-bold-600"
                                                            for="exampleInputEmail1">Username</label>
                                                        <input type="text" class="form-control" id="username"
                                                            placeholder="Username" autocomplete="off" name="username"
                                                            required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-bold-600"
                                                            for="exampleInputPassword1">Password</label>
                                                        <input type="password" class="form-control" id="password"
                                                            placeholder="Password" name="password" required>
                                                    </div>
                                                    <div class="input-group mb-3">


                                                        <div class="input-group-prepend captcha">
                                                            <span class="">
                                                                {!! captcha_img() !!}
                                                            </span>
                                                            <button type="button"
                                                                class="btn btn-rounded btn-refresh btn-sm"
                                                                id="btn-refresh"><i
                                                                    class="flaticon-refresh-line-arrow"></i></button>
                                                            <div col-md="6">
                                                                <input type="text" class="form-control" id="captcha"
                                                                    placeholder="captcha" name="captcha" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <button type="submit"
                                                        class="login_btn btn btn-primary glow w-100 position-relative">Login
                                                        <i id="icon-arrow" class="flaticon-send-arrow"></i></button>
                                                    <button
                                                        class="buttonload btn btn-primary glow w-100 position-relative"
                                                        type="button">

                                                        <i
                                                            class="icon-arrow fa fa-circle-o-notch fa-spin spinner-border text-light"></i>
                                                    </button>
                                                </form>
                                                <hr>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- right section image -->
                                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                    <div class="card-content">
                                        <img class="img-fluid" src="{{ asset('app-assets/images/pages/login.png') }}"
                                            alt="branding logo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- login page ends -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <script src="{{ asset('assets/js/libs/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/popper.min.js') }}"></script>
    <script src="{{asset ('bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{asset ('alert/dist/sweetalert2.all.min.js') }}"></script>
    <script>
        cekLogin();
        $(".buttonload").hide();
            $(".form-login").submit(function (e) { 
                e.preventDefault();
                $(".buttonload").show();
                $(".login_btn").hide();
                var form=$(".form-login").serialize();
                $.ajax({
                    type: "POST",
                    url: "{{ route("sso.login") }}",
                    data: form,
                    dataType: "JSON",
                    success: function (response) {
                      
                        if (response.status=='error') {
                        Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.msg,
                         })
                            $(".login_btn").show();
                            $(".buttonload").hide();
                            kode();
                        } else {
                           Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Anda akan di arahkan dalam 3 Detik',
                        showConfirmButton: false,
                        timer: 3000
                        }).then (function() {
                            $(".login_btn").hide();
                            $(".buttonload").hide();
                            window.location.href = "{{ route("home") }}";
                            });
                           
                        }
                        
                    },
                     
                    error:function(response){
                       Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Username / Password Salah!',
                        })
                        $(".login_btn").show();
                        $(".buttonload").hide();
                        kode();
                    }
                });
            });
            function cekLogin()
            {
               $.ajax({ url: "{{ route("cek.login") }}",
                    context: document.body,
                    success: function(response){
                   if (response.status==true) {
                       window.location.href = "{{ route("home") }}";
                   }
            }});
            }
            $("#btn-refresh").click(function(){
            $.ajax({
                type:'GET',
                url:'{{ route('reload.ca') }}',
                success:function(data){
                    $(".captcha span").html(data.captcha);
                }
            });
           
});
            function kode()
            {
                $.ajax({
                type:'GET',
                url:'{{ route('reload.ca') }}',
                success:function(data){
                $(".captcha span").html(data.captcha);
                }
                });
            }
    </script>
</body>
<!-- END: Body-->

</html>