<header class="header navbar fixed-top navbar-expand-sm">
    <a href="javascript:void(0);" class="sidebarCollapse d-none d-lg-block" data-placement="bottom"><i
            class="flaticon-menu-line-2"></i></a>

    <ul class="navbar-nav flex-row">
        <li class="nav-item dropdown language-dropdown ml-1  ml-lg-0">
            <a href="javascript:void(0);" class="nav-link dropdown-toggle" id="flagDropdown" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                @if(str_replace('_', '-', app()->getLocale())=='id')
                <img src="{{asset('assets/img/id.png')}}" alt=""> <span class="d-lg-inline-block d-none"></span>
                @else
                <img src="{{asset('assets/img/en.png')}}" alt=""> <span class="d-lg-inline-block d-none"></span>
                @endif
            </a>
            <div class="dropdown-menu position-absolute" aria-labelledby="flagDropdown">
                @if (str_replace('_', '-', app()->getLocale())=='id')
                <a class="dropdown-item" href="{{ route('localization.switch', 'en') }}">
                    <img src="{{asset('assets/img/en.png')}}" class="flag-width" alt="">
                    &#xA0;English</a>
                @else
                <a class="dropdown-item" href="{{ route('localization.switch', 'id') }}">
                    <img src="{{asset('assets/img/id.png')}}" class="flag-width" alt="">
                    &#xA0;Indonesia</a>
                @endif

            </div>
        </li>
    </ul>


    <ul class="navbar-nav flex-row ml-lg-auto">

        <li class="nav-item  d-lg-block d-none">
            <form class="form-inline" role="search">
                <input type="text" class="form-control search-form-control" placeholder="Search...">
            </form>
        </li>




        <li class="nav-item dropdown user-profile-dropdown ml-lg-0 mr-lg-2 ml-3 order-lg-0 order-1">
            <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="flaticon-user-12"></span>
            </a>
            <div class="dropdown-menu  position-absolute" aria-labelledby="userProfileDropdown">
                <a class="dropdown-item" href="#">
                    <i class="mr-1 flaticon-user-6"></i> <span>{{ session()->get('user')->data->nama}}</span>
                </a>
                <a class="dropdown-item" href="#">
                    <i class="mr-1 flaticon-calendar-bold"></i>
                    <span>{{ session()->get('user')->data->username}}</span>
                </a>


                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout.aja') }}">
                    <i class="mr-1 flaticon-power-button"></i> <span>Log Out</span>
                </a>

            </div>
        </li>


    </ul>
</header>