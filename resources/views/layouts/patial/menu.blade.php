<ul class="list-unstyled menu-categories" id="accordionExample">
    <li class="menu {{ Request::segment(1) === 'home' ? 'active' : '' }} ">
        <a href="{{route('home')}}" aria-expanded="false" class="dropdown-toggle">
            <div class="{{ Request::segment(1) === 'home' ? 'active' : '' }}">
                <i class="flaticon-home-fill ml-3"></i>
                <span>Home</span>
            </div>


        </a>

    </li>
    @can('admin')
    <li class="menu">
        <a href="#ecommerce" data-toggle="collapse"
            aria-expanded="{{ Request::segment(1) === 'admin' ? 'true' : 'false' }}" class="dropdown-toggle">
            <div class="">
                <i class="flaticon-lock-4"></i>
                <span>Admin</span>
            </div>
            <div>
                <i class="flaticon-right-arrow"></i>
            </div>
        </a>
        <ul class="collapse submenu list-unstyled {{ Request::segment(1) === 'admin' ? 'show' : '' }}  " id="ecommerce"
            data-parent="#accordionExample">
            <li class="{{ Request::segment(2) === 'user' ? 'active' : null }}">
                <a href="{{route('user.index')}}"> {{trans('user')}} </a>
            </li>
            <li class="{{ Request::segment(2) === 'acl' ? 'active' : '' }}">
                <a href="{{route('access')}}"> {{trans('akses')}} </a>
            </li>
            <li>
                <a href="ecommerce_product.html"> Products </a>
            </li>
            <li>
                <a href="ecommerce_product_catalog.html"> Product Catalog </a>
            </li>
            <li>
                <a href="#product-details" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"
                    data-parent="#ecommerce"> Product Details <i class="flaticon-right-arrow"></i> </a>
                <ul class="collapse list-unstyled sub-submenu" id="product-details">
                    <li>
                        <a href="ecommerce_product_details_1.html"> Product Details 1 </a>
                    </li>
                    <li>
                        <a href="ecommerce_product_details_2.html"> Product Details 2 </a>
                    </li>
                </ul>
            </li>


        </ul>
    </li>
    @endcan

    @if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role()))
    <li class="menu">
        <a href="#ui-features1" data-toggle="collapse"
            aria-expanded="{{ Request::segment(1) === 'sistem' ? 'true' : 'false' }}" class="dropdown-toggle">
            <div class="">
                <i class="flaticon-settings-2"></i>
                <span>Sistem</span>
            </div>
            <div>
                <i class="flaticon-right-arrow"></i>
            </div>
        </a>
        <ul class="collapse submenu list-unstyled {{ Request::segment(1) === 'sistem' ? 'show' : '' }}"
            id="ui-features1" data-parent="#accordionExample">
            <li class="{{ Request::segment(2) === 'pengguna' ? 'active' : null }}">
                <a href="{{ route('pengguna.index') }}">Pangguna </a>
            </li>
            <li class="{{ Request::segment(2) === 'rekap-pembayaran' ? 'active' : null }}">
                <a href="{{ route('rekap.pembayaran') }}"> Rekap Pembayaran </a>
            </li>
            <li>
                <a href="{{ route('sgs') }}"> Data SGS </a>
            </li>

        </ul>
    </li>
    @endif
    @if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role()))
    <li class="menu">
        <a href="#ui-features2" data-toggle="collapse"
            aria-expanded="{{ Request::segment(1) === 'sistem' ? 'true' : 'false' }}" class="dropdown-toggle">
            <div class="">
                <i class="flaticon-notes"></i>
                <span>Ujian</span>
            </div>
            <div>
                <i class="flaticon-right-arrow"></i>
            </div>
        </a>
        <ul class="collapse submenu list-unstyled {{ Request::segment(1) === 'ujian' ? 'show' : '' }}" id="ui-features2"
            data-parent="#accordionExample">
            <li class="{{ Request::segment(2) === 'peserta' ? 'active' : null }}">
                <a href="{{ route('peserta.index') }}">Peserta </a>
            </li>
            <li class="{{ Request::segment(2) === 'data-ujian' ? 'active' : null }}">
                <a href="{{ route('data-ujian.index') }}"> Data Ujian </a>
            </li>
            <li class="{{ Request::segment(2) === 'jadwal' ? 'active' : null }}">
                <a href="{{ route('jadwal.index') }}"> Jadwal </a>
            </li>
            <li class="{{ Request::segment(2) === 'soal' ? 'active' : null }}">
                <a href="{{ route('soal.index') }}"> Soal </a>
            </li>
            <li class="{{ Request::segment(2) === 'jawaban-mhs' ? 'active' : null }}">
                <a href="{{ route('jawaban.mhs') }}"> Data Jawaban Mhs </a>
            </li>

        </ul>
    </li>
    @endif
    <li class="menu">
        <a href="#ui-krs" data-toggle="collapse"
            aria-expanded="{{ Request::segment(1) === 'krs' ? 'true' : 'false' }}" class="dropdown-toggle">
            <div class="">
                <i class="flaticon-crm-screen"></i>
                <span>KRS</span>
            </div>
            <div>
                <i class="flaticon-right-arrow"></i>
            </div>
        </a>
        <ul class="collapse submenu list-unstyled {{ Request::segment(1) === 'krs' ? 'show' : '' }}" id="ui-krs"
            data-parent="#accordionExample">
            
            <li class="{{ Request::segment(2) === 'jadwal' ? 'active' : null }}">
                <a href="{{ route("jadwal.index") }}">Jadwal Kuliah </a>
            </li>
            <li class="{{ Request::segment(2) === 'index' ? 'active' : null }}">
                <a href="{{ route('krs.index') }}"> KRS WAR </a>
            </li>
            
            
           

        </ul>
    </li>

    <li class="menu">
        <a href="#ui-features" data-toggle="collapse"
            aria-expanded="{{ Request::segment(1) === 'layanan' ? 'true' : 'false' }}" class="dropdown-toggle">
            <div class="">
                <i class="flaticon-package"></i>
                <span>{{ trans('baku.Layanan') }}</span>
            </div>
            <div>
                <i class="flaticon-right-arrow"></i>
            </div>
        </a>
        <ul class="collapse submenu list-unstyled {{ Request::segment(1) === 'layanan' ? 'show' : '' }}"
            id="ui-features" data-parent="#accordionExample">
            <li class="{{ Request::segment(2) === 'pembayaran' ? 'active' : null }}">
                <a href="{{ route('pembayaran.index') }}"> {{ trans('baku.Pembayaran') }} </a>
            </li>
            <li class="{{ Request::segment(2) === 'histori' ? 'active' : null }}">
                <a href="{{ route('histori.pembayaran') }}"> {{ trans('baku.riwayat_pembayaran') }}</a>
            </li>
            <li class="{{ Request::segment(2) === 'sgs' ? 'active' : null }}">
                <a href="{{ route('sgs') }}"> SGS</a>
            </li>
            @if (in_array('9', role()) || in_array('2', role()) || in_array('4', role()) || in_array('3', role()))

            @else
            <li class="{{ Request::segment(2) === 'tagihan' ? 'active' : null }}">
                <a href="{{ route('tagihan.tagihanKu') }}"> Tagihan</a>
            </li>
            <li class="{{ Request::segment(2) === 'teman' ? 'active' : null }}">
                <a href="{{ route('teman') }}"> Teman</a>
            </li>

            @endif
            


</ul>
</li>
<li class="menu">
    <a href="#ui-features3" data-toggle="collapse"
        aria-expanded="{{ Request::segment(1) === 'e-class' ? 'true' : 'false' }}" class="dropdown-toggle">
        <div class="">
            <i class="flaticon-notes"></i>
            <span>E-Class</span>
        </div>
        <div>
            <i class="flaticon-right-arrow"></i>
        </div>
    </a>
    <ul class="collapse submenu list-unstyled {{ Request::segment(1) === 'e-class' ? 'show' : '' }}" id="ui-features3"
        data-parent="#accordionExample">
        <li class="{{ Request::segment(2) === 'e-class' ? 'active' : null }}">
            <a href="{{ route('e-class.dashboard') }}">Dashboard </a>
        </li>
        {{-- <li class="{{ Request::segment(2) === 'data-ujian' ? 'active' : null }}">
            <a href="{{ route('data-ujian.index') }}"> Data Ujian </a>
        </li>
        <li class="{{ Request::segment(2) === 'jadwal' ? 'active' : null }}">
            <a href="{{ route('jadwal.index') }}"> Jadwal </a>
        </li>
        <li class="{{ Request::segment(2) === 'soal' ? 'active' : null }}">
            <a href="{{ route('soal.index') }}"> Soal </a>
        </li>
        <li class="{{ Request::segment(2) === 'jawaban-mhs' ? 'active' : null }}">
            <a href="{{ route('jawaban.mhs') }}"> Data Jawaban Mhs </a>
        </li> --}}

    </ul>
</li>

<li class="menu">
    <a href="#ui-features4" data-toggle="collapse"
        aria-expanded="{{ Request::segment(1) === 'developer' ? 'true' : 'false' }}" class="dropdown-toggle">
        <div class="">
            <i class="flaticon-building-1"></i>
            <span>Developer</span>
        </div>
        <div>
            <i class="flaticon-right-arrow"></i>
        </div>
    </a>
    <ul class="collapse submenu list-unstyled {{ Request::segment(1) === 'developer' ? 'show' : '' }}" id="ui-features4"
        data-parent="#accordionExample">
        <li class="{{ Request::segment(2) === 'app' ? 'active' : null }}">
            <a href="{{ route('tentang') }}">Tentang Aplikasi </a>
        </li>
        <li class="{{ Request::segment(2) === 'saran' ? 'active' : null }}">
            <a href="{{ route('saran.index') }}"> Kritik Dan Saran </a>
        </li>
         {{--<li class="{{ Request::segment(2) === 'jadwal' ? 'active' : null }}">
            <a href="{{ route('jadwal.index') }}"> Jadwal </a>
        </li>
        <li class="{{ Request::segment(2) === 'soal' ? 'active' : null }}">
            <a href="{{ route('soal.index') }}"> Soal </a>
        </li>
        <li class="{{ Request::segment(2) === 'jawaban-mhs' ? 'active' : null }}">
            <a href="{{ route('jawaban.mhs') }}"> Data Jawaban Mhs </a>
        </li> --}}

    </ul>
</li>

{{-- <li class="menu {{ Request::segment(1) === 'dashboard' ? 'active' : '' }} ">
    <a href="{{route('soal.mhs')}}" aria-expanded="false" class="dropdown-toggle">
        <div class="{{ Request::segment(1) === 'dashboard' ? 'active' : '' }}">
            <i class="flaticon-computer-6  ml-3"></i>
            <span>Dashboard Ujian</span>
        </div>


    </a>

</li> --}}



</ul>